<?php
// HomeAutomation
// Copyright (C) 2021 Tom Rosenback (tom.rosenback@gmail.com), Daniel Malmgren (daniel.malmgren@kolefors.se)
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

// error_reporting(E_ALL);
error_reporting(E_ALL ^ E_NOTICE);

// ini_set("default_charset", "UTF-8");
// header("Content-Type: text/html; charset=UTF-8");
// mb_internal_encoding("UTF-8");

define("HA_ROOT_PATH", dirname(__FILE__));

include(HA_ROOT_PATH."/system/classes/load.php");
include(HA_ROOT_PATH."/system/defaults.php");

if(file_exists(HA_ROOT_PATH."/config.php")) {
	include(HA_ROOT_PATH."/config.php");
}

include(HA_ROOT_PATH."/functions.php");

defineConstants($config, "", "CFG_");
include(HA_ROOT_PATH."/system/functions.php");
//include(HA_ROOT_PATH."/dbfunctions.php");
include(HA_ROOT_PATH."/infobox.php");
include(HA_ROOT_PATH."/sensors/functions.php");

$db = new db();

if(file_exists(HA_ROOT_PATH."/system/mysql_settings.php")) {
	require_once(HA_ROOT_PATH."/system/mysql_settings.php");
	$db->setCredentials($mysqlHost, $mysqlUsername, $mysqlPassword, $mysqlDatabase);
}

// if(isset($_GET["lang"]) || $_SESSION[CFG_SESSION_KEY]["language"] == "" || !isset($_SESSION[CFG_SESSION_KEY]["language"]))
{
	$_SESSION[CFG_SESSION_KEY]["language"] = getClientLanguage();
}

// always include english first for fallback, after that include client language.
include(HA_ROOT_PATH."/resources/languages/en.php");
// missing elements in client language will be in english now.
if($_SESSION[CFG_SESSION_KEY]["language"] != "en") {
	include(HA_ROOT_PATH."/resources/languages/".$_SESSION[CFG_SESSION_KEY]["language"].".php");
}

defineConstants($translation);

include(HA_ROOT_PATH."/system/syssettings.php");

define("THISPAGE", getRequestURIPageName());

if(empty($_SESSION[CFG_SESSION_KEY]["settings"])) {
	$_SESSION[CFG_SESSION_KEY]["settings"] = Settings::get("", true);
}

if($_SESSION[CFG_SESSION_KEY]["settings"]["debug"]) {
	if(!is_dir(HA_ROOT_PATH."/logs")) {
		mkdir(HA_ROOT_PATH."/logs");
	}
}

// load the available systemplugins
//$systemPlugins = new SystemPlugins();

?>
