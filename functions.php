<?php
// HomeAutomation
// Copyright (C) 2021 Tom Rosenback (tom.rosenback@gmail.com), Daniel Malmgren (daniel.malmgren@kolefors.se)
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

function getStatusImage($type, $status, $id = -1, $prefix = "/") {
	$image = "";

	switch($status) {
		case 0: {
			$status = "_off";
			break;
		}
		case 1: {
			$status = "_on";
			break;
		}
		default: {
			$image = "questionmark.png";
			break;
		}
	}

	if($image == "") {
		if($id != -1 && file_exists(HA_ROOT_PATH."/resources/icons/".$prefix."/".$id.$status.".png")) {
			$image = "icons/".$prefix."/".$id.$status.".png";
		} else {
			if(file_exists(HA_ROOT_PATH."/resources/icons/".$type.$status.".png")) {
				$image = $type.$status.".png";
			} else {
				$image = "light".$status.".png";
			}
		}
	}

	return "./resources/".$image;
}

// $status - value used in HA in percent
// $maxSystemValue - the maximum dimlevel the system is using, eg 255 or 65535
// return - a dimlevel scaled to fit system needs
// example, status: 10, max 255 => 25
// 			status: 100, max 255 => 255
function scaleStatusToDimlevel($status, $maxSystemValue = 255) {
	return round($status * ($maxSystemValue / 100));
}


// $dimlevel - value used by system, eg 255 or 65535
// $maxSystemValue - the maximum dimlevel the system is using, eg 255 or 65535
// return - status in percent for HA
// example, dimlevel: 25, max 255 => 9.8 => rounded to nearest dimlevelstep => 50
// 			status: 255, max 255 => 255 => rounded to nearest dimlevelstep => 100
function scaleDimlevelToStatus($dimlevel, $maxSystemValue = 255) {
	$dimlevelstep = Settings::get("dimlevelstep");

	if($dimlevelstep === false && !is_numeric($dimlevelstep)) {
		$dimlevelstep = 5;
	}

	return round(($dimlevel / $maxSystemValue * 100) / $dimlevelstep) * $dimlevelstep;
}

function getStatusText($status) {
	$text = "";

	if($status == true || $status == 1) {
		$text = mb_convert_case(LBL_ON, MB_CASE_LOWER, "UTF-8");
	} else {
		$text = mb_convert_case(LBL_OFF, MB_CASE_LOWER, "UTF-8");
	}

	return $text;
}

function convertToBoolean($value) {
	$bool = false;

	if ($value === true || $value === "true" || $value === "True" || $value === "TRUE" || $value === "ON" || $value === "On" || $value === "on" || $value === "1" || $value >= 1) {
		$bool = true;
	}

	return $bool;
}

function convertToNumeric($value) {
	$numeric = 0;

	if ($value === true || $value === "true" || $value === "True" || $value === "TRUE" || $value === "ON" || $value === "On" || $value === "on" || $value === "1" || ($value * 1) >= 1) {
		$numeric = 1;
	}

	return $numeric;
}

function registerSession($username, $rememberMe = false) {
	$registered = false;

	$userInfo = Users::getDetails($username);

	if(is_array($userInfo)) {
		$_SESSION[CFG_SESSION_KEY]["fingerprint"] = md5(CFG_SESSION_KEY.$_SERVER["HTTP_USER_AGENT"].THISVERSION);
		$_SESSION[CFG_SESSION_KEY]["user"] = $username;
		$_SESSION[CFG_SESSION_KEY]["userid"] = $userInfo["id"];
		$_SESSION[CFG_SESSION_KEY]["userlevel"] = $userInfo["userlevel"];

		if($userInfo["firstname"] != "") {
			$_SESSION[CFG_SESSION_KEY]["firstname"] = $userInfo["firstname"];
		}

		if($userInfo["lastname"] != "") {
			$_SESSION[CFG_SESSION_KEY]["lastname"] = $userInfo["lastname"];
		}

		if($rememberMe) {
			setCookie(CFG_SESSION_KEY."_user", $userInfo["username"], time() + CFG_COOKIE_TIMEOUT);
			setCookie(CFG_SESSION_KEY."_hash", md5(md5($userInfo["salt"]).$userInfo["password"].md5($userInfo["salt"])), time() + CFG_COOKIE_TIMEOUT);
		}

		$registered = true;
	}

	return $registered;
}

function resetSession() {
	$currentLang = $_SESSION[CFG_SESSION_KEY]["language"];

	$_SESSION[CFG_SESSION_KEY] = array();

	$_SESSION[CFG_SESSION_KEY]["lastversionscheck"] = 0;
	$_SESSION[CFG_SESSION_KEY]["language"] = $currentLang;
	$_SESSION[CFG_SESSION_KEY]["settings"] = Settings::get("", true);

	// deleting cookies
	setcookie(CFG_SESSION_KEY."_user", "", time() - 3600);
	setcookie(CFG_SESSION_KEY."_hash", "", time() - 3600);
	// session_destroy();
}

function validateSession() {
	if(isset($_SESSION[CFG_SESSION_KEY]["fingerprint"])) {
		if($_SESSION[CFG_SESSION_KEY]["fingerprint"] != md5(CFG_SESSION_KEY.$_SERVER["HTTP_USER_AGENT"].THISVERSION)) {
			resetSession();
		}
	}
	else if(!is_array($_SESSION[CFG_SESSION_KEY]["settings"])) {
		resetSession();
	}

	if($_SESSION[CFG_SESSION_KEY]["userlevel"] == "") {
		$_SESSION[CFG_SESSION_KEY]["lastversionscheck"] = 0;
		checkAutoLogin();
	}
}

function getDimlevels($step = 0, $min = 0, $max = 100) {
	$dimlevels = array();

	$i = $min;

	if($step == 0) {
		$step = $_SESSION[CFG_SESSION_KEY]["settings"]["dimlevelstep"];
	}

	if($step == 0 || $step == "") {
		$step = 5;
	}

	if($min == 0) {
		$dimlevels[] = array(0, LBL_OFF);
		$i = $step;
	}

	for($i; $i <= $max; $i += $step) {
		$dimlevels[] = array($i, $i." %");
	}

	if($i > $max) {
		$i -= $step;
	}

	if($i < $max) {
		$dimlevels[] = array($max, $max." %");
	}

	return $dimlevels;
}

function generateSelector($selectedItem, $selectorName, $class = "text", $valueColOrMethod = 0, $nameColOrMethod = 1, $data, $defaultValue = -1, $defaultText = "Please select", $disabled = false, $script = "", $selectorId = "", $extraAttributes = "") {
	$disabledParameter = "";

	if($disabled) {
		$disabledParameter = " disabled=\"disabled\"";
	}

	if($selectorId == "") {
		$selectorId = $selectorName;
	}

	if($class != "") {
		$class = " class=\"".$class."\"";
	}

	$selector = "<select name=\"".$selectorName."\" id=\"".$selectorId."\"".$class.$disabledParameter.$script.$extraAttributes.">";

	if($defaultText != "") {
		$selector .= "<option value=\"".$defaultValue."\">".$defaultText."</option>";
	}

	if($data !== false) {
		foreach($data as $item) {
			$value = "";
			$name = "";

			if(is_object($item)) {
				// if item is an object name and value must have defined methods for get
				if(method_exists($item, $valueColOrMethod)) {
					$value = $item->$valueColOrMethod();
				}

				if(method_exists($item, $nameColOrMethod)) {
					$name = $item->$nameColOrMethod();
				}
			} else {
				$value = $item[$valueColOrMethod];
				$name = $item[$nameColOrMethod];
			}

			if(trim($value) == "optgroup_start") {
				$selector .= "<optgroup label=\"".$name."\">";
			} else if(trim($value) == "optgroup_end") {
				$selector .= "</optgroup>";
			} else {
				$selected = "";

				if($selectedItem == $value) {
					$selected = " selected=\"selected\"";
				}

				$selector .= "<option value=\"".$value."\"".$selected.">".$name."</option>";
			}
		}
	}

	$selector .= "</select>";

	return $selector;
}

function getTimezones() {
	$timezones = array();

	for($i = -10; $i <= 10; $i++) {
		$sign = "";

		if($i > 0) {
			$sign = "+";
		}

		$timezones[] = array(trim($i), "GMT ".$sign.$i);
	}

	return $timezones;
}

function checkRAWcommands($deviceId, $rawDevice = 0, $rawLearnCmd = "", $rawOffCmd = "", $rawOnCmd = "") {
	$msg = "";

	if($rawDevice == 1) {
		$err = 0;

		if($rawLearnCmd == "") {
			$msg .= mb_convert_case(LBL_TEACH, MB_CASE_LOWER, "UTF-8");
			$sep = ", ";
			$err++;
		}

		if($rawOffCmd == "") {
			$msg .= $sep.mb_convert_case(LBL_OFF, MB_CASE_LOWER, "UTF-8");
			$sep = ", ";
			$err++;
		}

		if($rawOnCmd == "") {
			$msg .= $sep.mb_convert_case(LBL_ON, MB_CASE_LOWER, "UTF-8");
			$sep = ", ";
			$err++;
		}

		if($err > 0) {
			if($err >= 3) {
				$msg = LBL_ALLMISSING;
			} else {
				$msg = ucfirst($msg)." ".mb_convert_case(LBL_MISSING, MB_CASE_LOWER, "UTF-8");
			}
		} else {
			$msg = LBL_OK;
		}
	}

	return $msg;
}

function getDayName($day) {
	$name = "";

	switch($day) {
		case -1:
			$name = LBL_ALL;
			break;
		case 1:
			$name = LBL_MONDAY;
			break;
		case 2:
			$name = LBL_TUESDAY;
			break;
		case 3:
			$name = LBL_WEDNESDAY;
			break;
		case 4:
			$name = LBL_THURSDAY;
			break;
		case 5:
			$name = LBL_FRIDAY;
			break;
		case 6:
			$name = LBL_SATURDAY;
			break;
		case 7:
			$name = LBL_SUNDAY;
			break;
		default:
			$name = "-";
			break;
	}

	return $name;
}

function getDayNames($dayArray) {
	$dayNames = "";
	$separator = "";

	if($dayArray[0] == -1) {
		$dayNames .= mb_convert_case(LBL_DAYS, MB_CASE_LOWER, "UTF-8");
	} else {
		for($i = 0; $i < count($dayArray); $i++) {
			$dayNames .= $separator.getDayName($dayArray[$i]);
			$separator = ", ";
		}

		$dayNames = replaceLastOccurance(",", " ".LBL_AND, $dayNames);
	}

	return $dayNames;
}



function replaceLastOccurance($search, $replace, $string) {
	return preg_replace("~(.*)".preg_quote($search, "~")."~", "$1".$replace, $string, 1);
}

function getMonthName($day) {
	$name = "";

	switch($day) {
		case -1:
			$name = LBL_ALL;
			break;
		case 1:
			$name = LBL_JANUARY;
			break;
		case 2:
			$name = LBL_FEBRUARY;
			break;
		case 3:
			$name = LBL_MARCH;
			break;
		case 4:
			$name = LBL_APRIL;
			break;
		case 5:
			$name = LBL_MAY;
			break;
		case 6:
			$name = LBL_JUNE;
			break;
		case 7:
			$name = LBL_JULY;
			break;
		case 8:
			$name = LBL_AUGUST;
			break;
		case 9:
			$name = LBL_SEPTEMBER;
			break;
		case 10:
			$name = LBL_OCTOBER;
			break;
		case 11:
			$name = LBL_NOVEMBER;
			break;
		case 12:
			$name = LBL_DECEMBER;
			break;
		default:
			$name = "-";
			break;
	}

	return $name;
}

function getMonthNames($monthArray) {
	$monthNames = "";
	$separator = "";

	if($monthArray[0] == -1) {
		$monthNames .= mb_convert_case(LBL_ALLMONTHS, MB_CASE_LOWER, "UTF-8");
	} else {
		for($i = 0; $i < count($monthArray); $i++) {
			$monthNames .= $separator.getMonthName($monthArray[$i]);
			$separator = ", ";
		}

		$monthNames = replaceLastOccurance(",", " ".LBL_AND, $monthNames);
	}

	return $monthNames;
}

function getBooleanText($bool) {
	$text = "";

	switch($bool) {
		case 0:
			$text = LBL_NO;
			break;
		case 1:
			$text = LBL_YES;
			break;
		default:
			$text = "-";
			break;
	}

	return $text;
}

function getActivationTypes() {
	$types = array(
					array("id" => "static", 		"text" => LBL_STATIC),
					array("id" => "suncontrolled",	"text" => LBL_SUNCONTROLLED),
					array("id" => "engineheater",	"text" => LBL_ENGINEHEATER),
					array("id" => "random",			"text" => LBL_RANDOM),
					array("id" => "dynamic",		"text" => LBL_DYNAMIC)
				);

	if(count(getPlugins("", "gcal")) > 0) {
		$types[] = array("id" => "gcal", "text" => LBL_GCAL);
	}

	return $types;
}

function getArrayAsString($array, $numMaxElements, $delimiter = ";", $index = "") {
	$tmpDelimiter = "";
	$string = "";

	if(is_array($array)) {
		if(count($array) == $numMaxElements) {
			$string = "-1";
		} else {
			foreach($array as $value) {
				if($index != "") {
					$value = $value[$index];
				}

				$string .= $tmpDelimiter.$value;
				$tmpDelimiter = $delimiter;
			}
		}
	}

	return $string;
}

function msort($array, $id = "timestamp", $sortLowToHigh = true) {
	$temp_array = array();

	while(count($array) > 0) {
		$lowest_id = 0;
		$index = 0;

		foreach($array as $item) {
			if ($item[$id] > $array[$lowest_id][$id]) {
				$lowest_id = $index;
			}

			$index++;
		}

		$temp_array[] = $array[$lowest_id];
		$array = array_merge(array_slice($array, 0, $lowest_id), array_slice($array, $lowest_id + 1));
	}

	if($sortLowToHigh) {
		$temp_array = array_reverse($temp_array);
	}

	return $temp_array;
}

function msortWithKey($array, $id = "timestamp", $sortLowToHigh = true) {
	$temp_array = array();

	while(count($array) > 0) {
		$lowest_id = "";

		foreach($array as $key => $value) {
			if ($value[$id] > $array[$lowest_id][$id]) {
				$lowest_id = $key;
			}
		}

		$temp_array[] = $array[$lowest_id];
		unset($array[$lowest_id]);
	}

	if($sortLowToHigh) {
		$temp_array = array_reverse($temp_array);
	}

	return $temp_array;
}

function replaceUnwantedCharacters($str) {
	$tmp = str_replace(" ", "_", $str);
	$tmp = str_replace("\\", "_", $tmp);
	$tmp = str_replace("/", "_", $tmp);
	$tmp = str_replace("*", "", $tmp);
	$tmp = str_replace(":", "", $tmp);
	$tmp = str_replace("?", "", $tmp);
	$tmp = str_replace("\"", "_", $tmp);
	$tmp = str_replace("<", "", $tmp);
	$tmp = str_replace(">", "", $tmp);
	$tmp = str_replace("|", "", $tmp);

	return $tmp;
}

function getSchedulerDayOfWeek($day) {
	switch($day) {
		case "1":
			$day = "MON";
			break;
		case "2":
			$day = "TUE";
			break;
		case "3":
			$day = "WED";
			break;
		case "4":
			$day = "THU";
			break;
		case "5":
			$day = "FRI";
			break;
		case "6":
			$day = "SAT";
			break;
		case "7":
			$day = "SUN";
			break;
		default:
			$day = "";	// something wrong if we get here
			break;
	}

	return $day;
}

function getSchedulerDayOfWeekString($dayArray) {
	$dayString = "";

	if($dayArray[0] == -1) {
		$dayString = "*";
	} else {
		$separator = "";

		foreach($dayArray as $day) {
			$dayString .= $separator.getSchedulerDayOfWeek($day);
			$separator = ",";
		}
	}

	return $dayString;
}

function getCrontabDayOfWeekString($dayArray) {
	$dayString = "";

	if($dayArray[0] == -1) {
		$dayString = "*";
	} else {
		$separator = "";

		foreach($dayArray as $day) {
			if($day == 7) {
				$day = 0;
			}

			$dayString .= $separator.$day;
			$separator = ",";
		}
	}

	return $dayString;
}

function getSchedulerMonthName($month) {
	switch($month) {
		case "1":
			$month = "JAN";
			break;
		case "2":
			$month = "FEB";
			break;
		case "3":
			$month = "MAR";
			break;
		case "4":
			$month = "APR";
			break;
		case "5":
			$month = "MAY";
			break;
		case "6":
			$month = "JUN";
			break;
		case "7":
			$month = "JUL";
			break;
		case "8":
			$month = "AUG";
			break;
		case "9":
			$month = "SEP";
			break;
		case "10":
			$month = "OCT";
			break;
		case "11":
			$month = "NOV";
			break;
		case "12":
			$month = "DEC";
			break;
		default:
			$month = "";	// something wrong if we get here
			break;
	}

	return $month;
}

function getSchedulerMonthNameString($monthArray) {
	$monthString = "";

	if($monthArray[0] == -1) {
		$monthString = "*";
	} else {
		$separator = "";

		foreach($monthArray as $month) {
			$monthString .= $separator.getSchedulerMonthName($month);
			$separator = ",";
		}
	}

	return $monthString;
}

function removeDevicesOfType($devices = array(), $type = "") {
	$newDevicesArray = array();

	foreach($devices as $device) {
		if($device["type"] != $type) {
			$newDevicesArray[] = $device;
		}
	}

	return $newDevicesArray;
}

function convertDateTimeToLocal($datetime, $returnOnlyTime = false, $returnOnlyTimeIfToday = false, $returnOnlyDate = false) {
	$tmp = explode(" ", $datetime);
	$date = $tmp[0];
	$time = substr($tmp[1], 0, -3);
	$date = explode("-", $date);
	$date = date($_SESSION[CFG_SESSION_KEY]["settings"]["dateformat"], mktime(0, 0, 0, ((int) $date[1]), ((int) $date[2]), ((int) $date[0])));

	if($returnOnlyTime) {
		return $time;
	}
	else if($returnOnlyTimeIfToday) {
		if(date($_SESSION[CFG_SESSION_KEY]["settings"]["dateformat"]) == $date) {
			return "Idag @ ".$time;
		} else {
			return $date." ".$time;
		}
	} else {
		if(!$returnOnlyDate) {
			return $date." ".$time;
		} else {
			return $date;
		}
	}
}

function isIpLocal() {
	$clientIp = getIpAddress();
	if(substr($clientIp, 0, 4) == "127." || $clientIp == "::1") {
		return true;
	}

	$isIpLocal = false;

	$localip = $_SESSION[CFG_SESSION_KEY]["settings"]["localip"];

	$localnets = explode(";", $localip);
	foreach($localnets as $localnet) {
		list($localnet, $localmask) = explode("/", $localnet);
		if($localmask == "") {
			$localmask = 32;
		}
		if($localmask == "" || $localmask > 32 || $localmask < 0) {
			$localmask = 32;
		}

		// $mask = $localmask;

		$localnet = ip2long($localnet);
		$localmask = ~((1 << (32-$localmask)) - 1);
		$remoteip = ip2long($clientIp);
		$maskedip = $remoteip & $localmask;
		$maskednet = $localnet & $localmask;

		// echo "<br />localnet:";
		// printf('%1$32b', $localnet);

		// echo "<br />localmask: (dec: ".$mask.")";
		// printf('%1$32b', $localmask);

		// echo "<br />remoteip:";
		// printf('%1$32b', $remoteip);

		// echo "<br />maskedip:";
		// printf('%1$32b', $maskedip);

		// echo "<br />maskednet:";
		// printf('%1$32b', $maskednet);

		if($maskedip == $maskednet) {
			// echo "<br />maskedip == maskednet";
			$isIpLocal = true;
			break;
		}
	}
	//$isIpLocal = false;
	return $isIpLocal;
}

function getIpAddress() {
  return $_SERVER["REMOTE_ADDR"];
  //return isset($_SERVER["HTTP_X_FORWARDED_FOR"]) ? $_SERVER["HTTP_X_FORWARDED_FOR"] : $_SERVER["REMOTE_ADDR"];
}

function getFormVariable($variable, $default = "") {
	$result = $default;

	if(isset($_POST[$variable])) {
		$result = $_POST[$variable];
	}
	else if(isset($_GET[$variable])) {
		$result = $_GET[$variable];
	}

	return $result;
}
// $allowedExtensions = "jpg|png|gif";
function saveUploadedFile($uploadedFile = -1, $relativePathWithFilename, $allowedExtensions = "") {
	$msg = false;

	// echo "<pre>";
	// print_r($uploadedFile);
	// echo $relativePathWithFilename;
	// echo "</pre>";

	if($uploadedFile != -1) {
		$userfile = $uploadedFile["tmp_name"];
		$userfile_name = $uploadedFile["name"];
		$userfile_size = $uploadedFile["size"];
		$userfile_type = $uploadedFile["type"];
		$userfile_error = $uploadedFile["error"];

		if($userfile_error == 0 && ($allowedExtensions == "" || preg_match("/\.(".$allowedExtensions.")/i", $userfile_name))) {
			if(is_uploaded_file($userfile)) {
				if (!move_uploaded_file($userfile, $relativePathWithFilename)) {
					// $msg = "Could not save file";
				} else {
					// $msg = true;
				}
			} else {
				// $msg = "Corrupted file";
			}
		}
	} else {
		// $msg = "No file uploaded";
	}

	return $msg;
}

function getExtension($file) {
	$ext = "";

	$i = strrpos($file, ".");

	if($i !== false) {
		$ext = strtolower(substr($file, $i + 1, strlen($file) - $i));
	}

	return $ext;
}

function getTemperatureFromExternalUrl() {
	$temp = "";

	if($_SESSION[CFG_SESSION_KEY]["settings"]["externaltempurl"] != "") {
		// valid settings are those who begins with http:// or is a local file
		if(stripos($_SESSION[CFG_SESSION_KEY]["settings"]["externaltempurl"], "http") === 0 || file_exists($_SESSION[CFG_SESSION_KEY]["settings"]["externaltempurl"])) {
			$url = $_SESSION[CFG_SESSION_KEY]["settings"]["externaltempurl"];
			$temp = readFileOrUrl($url);
		}

		$temp = trim($temp) * 1;
	}

	return $temp;
}

function readFileOrUrl($path) {
	$data = "";
	$timeout = 10; // 10sec timeout

	if(stripos($path, "http") === 0) {
		$ctx = stream_context_create(array('http' =>
			array(
				'timeout' => $timeout
			)
		));

		$start = microtime(true);

		$data = @file_get_contents($path, false, $ctx);

		if(microtime(true) - $start > $timeout) {
			SaveLog("readFileOrUrl timeout, ".$path);
		}
	} else {
		// allow_url_fopen needs to be enabled
		$handle = @fopen($path, "r");

		if($handle !== false) {
			while(!feof($handle)) {
				$data .= fread($handle, 8192);
			}

			fclose($handle);
		}
	}

	return $data;
}

// returns offset in minutes
function calculateEngineHeaterTimeOffset($temp) {
	$offset = 0;

	$offset = (-$temp / 10 * 60) + 60;

	if($offset > 120) {
		$offset = 120;
	}
	else if($offset < 0) {
		$offset = 0;
	}

	return $offset;
}

function generateSettingsList() {
	$list = "";

	$currentSettings = Settings::get();
	$grouping = "";

	$constArray = get_defined_constants(true);
	$constArray = $constArray["user"];

	foreach($currentSettings as $setting) {
		if($setting["grouping"] != $grouping) {
			$list .= "<tr>
						<td class=\"mediumBold\" colspan=\"2\">".$constArray[$setting["grouping"]]."&nbsp;</td>
					</tr>";
		}

		$list .= "<tr>
					<td class=\"bold\"><label for=\"set_".$setting["name"]."\">".$constArray[$setting["label"]].":</label></td>
					<td>";

		switch($setting["type"]) {
			case "radio":
			{
				$radioChecked = array();
				$radioChecked[0] = "";
				$radioChecked[1] = "";

				if($setting["value"] == 0) {
					$radioChecked[0] = " checked=\"checked\"";
				} else {
					$radioChecked[1] = " checked=\"checked\"";
				}

				$list .= "<label><input type=\"radio\" name=\"set_".$setting["name"]."\" value=\"1\"".$radioChecked[1].">".getBooleanText(1)."</label>&nbsp;<label><input type=\"radio\" name=\"set_".$setting["name"]."\" value=\"0\"".$radioChecked[0].">".getBooleanText(0)."</label></td>";

				break;
			}
			case "dropdown":
			{
				if($setting["name"] == "timezone") {
					$list .= generateSelector($setting["value"], "set_".$setting["name"], "text", 0, 1, getTimezones(), "", "");
				}
				else if($setting["name"] == "dimlevelstep") {
					$list .= generateSelector($setting["value"], "set_".$setting["name"], "text", 0, 1, getDimlevels(1, 5, 10), "", "");
				}
				else if($setting["name"] == "defaultdimlevel") {
					$list .= generateSelector($setting["value"], "set_".$setting["name"], "text", 0, 1, getDimlevels($_SESSION[CFG_SESSION_KEY]["settings"]["dimlevelstep"], 5), "", "");
				}
				else if($setting["name"] == "defaultpage") {
					// $pages = removeKeyFromArray(Pages::getForSettings(), array("logout", "about", "help"));
					$pages = Pages::getForSettings();
					// debugVariable($pages);
					$list .= generateSelector($setting["value"], "set_".$setting["name"], "text3", "name", "translation", $pages, "", "");
				}
				else if($setting["name"] == "theme") {
					$pages = array();

					foreach(glob(HA_ROOT_PATH."/resources/themes/*.css") as $filename) {
						$themename = basename($filename, ".css");
						$pages[] = array("page" => $themename, "translation" => $themename);
					}

					$list .= generateSelector($setting["value"], "set_".$setting["name"], "text3", "page", "translation", $pages, "", "");
				}
				else if($setting["name"] == "iblogevents" || $setting["name"] == "ibupcomingevents") {
					$list .= generateSelector($setting["value"], "set_".$setting["name"], "text3", 0, 1, getNumberArray(1, 10), "", "");
				}
				else if($setting["name"] == "hoursstatusactive") {
					$list .= generateSelector($setting["value"], "set_".$setting["name"], "text3", 0, 1, getNumberArray(1, 10, 1, " ".LBL_HOURS), "", "");
				}
				else if($setting["name"] == "houseplanwidth") {
					$list .= generateSelector($setting["value"], "set_".$setting["name"], "text3", 0, 1, getNumberArray(100, 2000, 5, " px"), "", "");
				}
				else if($setting["name"] == "houseplaniconheight") {
					$list .= generateSelector($setting["value"], "set_".$setting["name"], "text3", 0, 1, getNumberArray(10, 50, 1, " px"), "", "");
				}
				else if($setting["name"] == "iconortext") {
					$choices = array();
					$choices[] = array("page" => "icon", "translation" => LBL_ICON);
					$choices[] = array("page" => "both", "translation" => LBL_ICONANDTEXT);
					$choices[] = array("page" => "text", "translation" => LBL_TEXT);
					$list .= generateSelector($setting["value"], "set_".$setting["name"], "text3", "page", "translation", $choices, "", "");
				}
				else if($setting["name"] == "ibsensor") {
					$sensors = Sensors::get();
					array_unshift($sensors, array("sensor_serial" => "nosensor", "description" => LBL_DONOTSHOW));

					$sensors[] = array("id" => "external", "sensor_serial" => "external", "description" => "Ext. temp: ".$_SESSION[CFG_SESSION_KEY]["settings"]["externaltemplocation"]);
					$list .= generateSelector($setting["value"], "set_".$setting["name"], "text3", "sensor_serial", "description", $sensors, "", "");
				} else {
					$list .= "<input type=\"text\" name=\"set_".$setting["name"]."\" id=\"".$setting["name"]."\" class=\"text3\" value=\"".$setting["value"]."\">";
				}

				break;
			}
			case "password":
			{
				$list .= "<input type=\"password\" name=\"set_".$setting["name"]."\" id=\"".$setting["name"]."\" class=\"text3\" value=\"".$setting["value"]."\" autocomplete=\"off\">";
				break;
			}
			case "text":
			default:
			{
				$list .= "<input type=\"text\" name=\"set_".$setting["name"]."\" id=\"".$setting["name"]."\" class=\"text3\" value=\"".$setting["value"]."\">";
				break;
			}
		}

		$list .= "</td>
				</tr>";

		$grouping = $setting["grouping"];
	}

	return $list;
}

function saveSettings($data = array()) {
	foreach($data as $key => $value) {
		$keyPrefix = substr($key, 0, 4);
		$name = substr($key, 4);

		if($keyPrefix == "set_") {
			Settings::update($name, $value);
		}
	}
}

function saveSystemPluginSettings($data = array(), $systemPluginName = "") {
	if($systemPluginName != "")	{
		foreach($data as $key => $value) {
			$keyPrefix = substr($key, 0, 4);
			$name = substr($key, 4);

			if($keyPrefix == "set_") {
				SystemPlugins::saveSetting($systemPluginName, $name, $value);
			}
		}
		$plugin = SystemPlugins::load($systemPluginName);
		$plugin->saveSettings($data);
	}
}

// generates an array containing number from min to max
function getNumberArray($min = 1, $max = 10, $step = 1, $text = "") {
	$arr = array();

	for($i = $min; $i <= $max; $i += $step) {
		$arr[] = array($i, $i.$text);
	}

	return $arr;
}

// getting browser language
function getClientLanguage() {
	$languages = explode(",", $_SERVER["HTTP_ACCEPT_LANGUAGE"]);
	$clientLanguage = explode("-", trim($languages[0]));
	$clientLanguage = trim($clientLanguage[0]);

	if(!file_exists(HA_ROOT_PATH."/resources/languages/".$clientLanguage.".php")) {
		$clientLanguage = "en";
	}

	return $clientLanguage;
}

function defineConstants($constantsArray = array(), $parent = "", $prefix = "LBL_") {
	if($parent != "") {
		$parent = $parent."_";
	}

	foreach($constantsArray as $key => $constant) {
		if(is_array($constant)) {
			defineConstants($constant, $parent.$key, $prefix);
		} else {
			define($prefix.strtoupper($parent.$key), $constant);
		}
	}
}

function toggleSortDirection($sortDirection = "asc") {
	if($sortDirection == "asc") {
		$sortDirection = "desc";
	} else {
		$sortDirection = "asc";
	}

	return $sortDirection;
}

function getRequestURIPageName() {
	$pageName = basename($_SERVER["REQUEST_URI"]);

	if (strpos($pageName, ".php") !== false) {
		$tmp = explode("?", $pageName);
		$pageName = reset($tmp);
	} else {
		$pageName = "";
	}

	return "./".$pageName;
}

function getFullRequestURI() {
    $s = empty($_SERVER["HTTPS"]) ? '' : ($_SERVER["HTTPS"] == "on") ? "s" : "";
    $protocol = substr(strtolower($_SERVER["SERVER_PROTOCOL"]), 0, stripos($_SERVER["SERVER_PROTOCOL"], "/")).$s;
    $port = ($_SERVER["SERVER_PORT"] == "80") ? "" : (":".$_SERVER["SERVER_PORT"]);
    return $protocol."://".$_SERVER['SERVER_NAME'].$port.$_SERVER['REQUEST_URI'];
}

function removeDeviceType($deviceTypes = array(), $deviceTypesToRemove = array()) {
	$tmp = array();

	foreach($deviceTypes as $deviceType) {
		if(!in_array($deviceType["type"], $deviceTypesToRemove)) {
			$tmp[] = $deviceType;
		}
	}

	return $tmp;

}

function generateMenu($parentId = -1, $generateCustomPages = false) {
	global $ha;
	$menu = "";

	if($ha->userAgent != "") {
		// generate mobile menu
		$menu = generateMobileMenu($parentId);
	} else {
		// Generate desktop menu
		if(!$generateCustomPages) {
			$pages = Pages::get($parentId, -1, true, false);
		} else {
			$pages = $ha->getCustomPages();
		}

		$separator = "";

		$iconortext = $_SESSION[CFG_SESSION_KEY]["settings"]["iconortext"];

		if($parentId == -1 && $iconortext != "icon") {
			$separator = "| ";
		}

		if(is_array($pages) && count($pages) > 0) {
			$menuClass = "";
			$transition = "";
			$linkRel = "";

			if($ha->userAgent == "") {
				$menuClass = " class=\"menu\"";
			} else {
				// $linkRel = " rel=\"external\"";
				$menuClass = " data-role=\"listview\"";
				$transition = " data-transition=\"slide reverse\"";

				if($parentId == -1) {
				}
			}

			if($page["module"] !== "custompages" || ($page["module"] === "custompages" && count($ha->getCustomPages()) > 0)) {
				$menu = "<ul".$menuClass.">";

				foreach($pages as $page) {
					$translation = $page["translation"];

					if(array_key_exists("module", $page) && $page["module"] == "custompages" && $ha->getCustomPagesTitle() != "") {
						$translation = $ha->getCustomPagesTitle();
					}

					if(array_key_exists("module", $page)) {
						$subMenu = generateMenu($page["id"], $page["module"] == "custompages");
					}

					if($subMenu == "" && $page["module"] == "custompages") {
						continue;
					}

					$class = "";
					$onClick = "";

					if($page["id"] == $ha->currentPage["id"] || in_array($page["id"], $ha->currentPage["parentids"])) {
						if($ha->userAgent == "") {
							$class = " class=\"selected\"";
						} else {
							// $class = " class=\"ui-btn-active\"";
						}
					}

					$action = "";

					if($page["menuaction"] != "") {
						$action = "&action=".$page["menuaction"];
					}

					$menu .= "<li>".$separator."<a href=\"".THISPAGE."?page=".$page["name"].$action."\"".$onClick.$class.$transition.$linkRel.">";

					if($iconortext != "text" && $parentId == -1) {
						if(file_exists(HA_ROOT_PATH."/resources/icons/menu/".$page["name"].".png")) {
							$menu .= "<img border=\"0\" src=\"resources/icons/menu/".$page["name"].".png\" title=\"".$page["translation"]."\"/>";
						} else {
							$menu .= $translation;
						}
					}

					if($iconortext != "icon" || $parentId > 0) {
						$menu .= $translation;
					}

					$menu .= "</a>
							".$subMenu."
							</li>";
				}

				$menu .= "</ul>";
			}
		}
	}

	return $menu;
}

function generateMobileMenu($parentId = -1, $generateCustomPages = false) {
	global $ha;

	if(!$generateCustomPages) {
		$pages = Pages::get($parentId, -1, true, true);
	} else {
		$pages = $ha->getCustomPages();
	}

	$menu = "";

	if(is_array($pages) && count($pages) > 0) {
		$menu = "<ul data-role=\"none\">";

		foreach($pages as $page) {
			$action = "";
			$translation = $page["translation"];
			$linkExtras = " data-transition=\"none\" data-role=\"none\"";

			if($page["module"] == "login") {
				 $linkExtras .= " rel=\"external\"";
			}

			if($page["menuaction"] != "") {
				$action = "&action=".$page["menuaction"];
			}

			if(array_key_exists("module", $page) && $page["module"] == "custompages" && $ha->getCustomPagesTitle() != "") {
				$translation = $ha->getCustomPagesTitle();
			}

			if(array_key_exists("module", $page)) {
				$subMenu = generateMobileMenu($page["id"], $page["module"] == "custompages");
			}

			if($subMenu == "") {
				if($page["module"] != "custompages") {
					$menu .= "<li>
								<h3>
									<a href=\"".THISPAGE."?page=".$page["name"].$action."\"".$linkExtras.">".$translation."<span class=\"closed\">&nbsp;</span></a>
								</h3>
							</li>";
				}
			} else {
				$menu .= "<li>
							<h3>
								<a href=\"".THISPAGE."?page=".$page["name"].$action."\"".$linkExtras.">".$translation."<span class=\"closed\">&nbsp;</span></a>
								".$subMenu."
							</h3>
						</li>";
			}
		}

		$menu .= "</ul>";
	}

	return $menu;
}

function removeKeyFromArray($array = array(), $keysToRemove = array()) {
	$tmp = array();

	if(is_array($array)) {
		foreach($array as $key => $value) {
			if(!in_array($key, $keysToRemove)) {
				$tmp[$key] = $value;
			}
		}
	}

	return $tmp;
}

/*function getPages($isMobile = false) {
	include(HA_ROOT_PATH."/system/syspages.php");

	if($isMobile) {
		$pages = removeKeyFromArray($pages, array("scheduler", "conf", "about", "help", "logs", "houseplan"));
	}

	$pages = removeKeyFromArray($pages, array("about"));

	$pages["logs"]["subpages"] = removeKeyFromArray($pages["logs"]["subpages"], array("measurements"));
	$pages["configurator"]["subpages"] = removeKeyFromArray($pages["conf"]["subpages"], array("sensors"));

	if(count(getPlugins("", "gcal")) == 0) {
		$pages["configurator"]["subpages"] = removeKeyFromArray($pages["conf"]["subpages"], array("gcal"));
	}

	return $pages;
}*/



function getDevicesByString($deviceString) {
	$devices = array();

	$deviceIds = explode(";", $deviceString);

	if($deviceIds[0] == "") {
		$deviceIds[0] = "-1";
	}

	if($deviceIds[0] != "-1") {
		if(count($deviceIds) > 0) {
			foreach($deviceIds as $id) {
				$device = Devices::get($id);

				if($device[0]["id"] != "") {
					$devices[] = $device[0];
				}
			}
		}
	} else {
		$devices = Devices::get();
	}

	return $devices;
}

function getTimeOfSunAtPosition($sunPosition, $time, $latitude, $longitude, $timezone = 0, $offset = "0:00") {
	$dst = "0".date("I").":00";
	$sunTime = "0:00";

	$zenith = (90 + (50 / 60));
	$twilightZenith = 96;

	switch($sunPosition) {
		case "dawn":
			$sunTime = addTimes(addTimes(date_sunrise($time, SUNFUNCS_RET_STRING, $latitude, $longitude, $twilightZenith, $timezone), $offset, "+"), $dst, "+");
			break;
		case "sunrise":
			$sunTime = addTimes(addTimes(date_sunrise($time, SUNFUNCS_RET_STRING, $latitude, $longitude, $zenith, $timezone), $offset, "+"), $dst, "+");
			break;
		case "dusk":
			$sunTime = addTimes(addTimes(date_sunset($time, SUNFUNCS_RET_STRING, $latitude, $longitude, $twilightZenith, $timezone), $offset, "+"), $dst, "+");
			break;
		case "sunset":
			$sunTime = addTimes(addTimes(date_sunset($time, SUNFUNCS_RET_STRING, $latitude, $longitude, $zenith, $timezone), $offset, "+"), $dst, "+");
			break;
		default:
			break;
	}

	return $sunTime;
}

function getPlugins($type = "", $name = "") {
	$plugins = array();

	foreach(glob(HA_ROOT_PATH."/plugins/".$type."/".$name."*") as $plugin) {
		$plugins[] = basename($plugin);
	}

	return $plugins;
}

function getAvailableSystemPlugins() {
	$plugins = array();
	$pluginsDir = HA_ROOT_PATH."/system/systemplugins/";

	if ($handle = opendir($pluginsDir)) {
		while ($entry = readdir($handle)) {
			if ($entry != "." && $entry != ".." && is_dir(HA_ROOT_PATH."/system/systemplugins/".$entry) && file_exists($pluginsDir.$entry."/".$entry.".plugin.php")) {
				$plugins[$entry] = $entry;
			}
		}

		closedir($handle);
	}

	return $plugins;
}

function generateActivationParamArray($type = 1, $params = "") {
	$parameters = array();

	$params = explode(";", $params);

	// activation parameters are stored as a text string,
	// parameters are separated by a ';',
	// if one parameter has subparameters it is separated by a '|',
	// the following level is separated by a '#'

	switch($type) {
		case "static": {
			// status;time;deviceid#dimlevel|deviceid#dimlevel
			$parameters["status"] 		= $params[0];
			$parameters["time"] 		= $params[1];
			$parameters["dimlevel"]		= generateDimLevelParamArray($params[2]);
			break;
		}
		case "suncontrolled": {
			// status;sunactivity;offset;deviceid#dimlevel|deviceid#dimlevel
			$parameters["status"]		= $params[0];
			$parameters["sunactivity"] 	= $params[1];
			$parameters["offset"] 		= $params[2];
			$parameters["dimlevel"] 	= generateDimLevelParamArray($params[3]);
			break;
		}
		case "engineheater": {
			// status;departure;sensor
			$parameters["status"]		= $params[0];
			$parameters["departure"] 	= $params[1];
			$parameters["tempsensor"]	= $params[2];
			break;
		}
		case "random": {
			// status;earliest;latest;deviceid#dimlevel|deviceid#dimlevel
			$parameters["status"]		= $params[0];
			$parameters["earliest"] 	= $params[1];
			$parameters["latest"] 		= $params[2];
			$parameters["dimlevel"] 	= generateDimLevelParamArray($params[3]);
			break;
		}
		case "dynamic": {
			// status;device;operator;value;deviceid#dimlevel|deviceid#dimlevel
			$parameters["status"]		= $params[0];
			$parameters["dyndevice"] 	= $params[1];
			$parameters["dynoperator"] 	= $params[2];
			$parameters["dynvalue"] 	= $params[3];
			$parameters["dimlevel"] 	= generateDimLevelParamArray($params[4]);
			break;
		}
		case "gcal":
			break;
		default:
			break;
	}

	return $parameters;
}

function generateDimLevelParamArray($param) {
	$paramArray = array();

	$dimlevels = explode("|", $param);

	foreach($dimlevels as $deviceDim) {
		$tmp = explode("#", $deviceDim);

	/*	$paramArray[deviceid] = dimlevel;	*/
		$paramArray[$tmp[0]] = $tmp[1];
	}

	return $paramArray;
}

function calculateNextRunTime($days, $type, $params = array(), $includetoday) {
	$date = date("y-m-d");
	if(!$includetoday) {
		$date = date("y-m-d", strtotime($date . " +1 day"));
	}
	for($i = 0; $i <= 8; $i += 1) {
		$weekday = date("w", strtotime($date));
		if($weekday == 0) {
			$weekday = "7";
		}
		if(strpos($days, $weekday) !== false || $days == "-1") {
			break;
		}
		$date = date("y-m-d", strtotime($date . " +1 day"));
	}

	$nextRunTime = "";

	//echo("Params:");
	//print_r($params);
	//echo("Type: ".$type."\n");

	switch($type) {
		case "static": {
			$nextRunTime = $params["time"];
			break;
		}
		case "suncontrolled": {
			$latitude = $_SESSION[CFG_SESSION_KEY]["settings"]["latitude"];
			$longitude = $_SESSION[CFG_SESSION_KEY]["settings"]["longitude"];
			$timezone = $_SESSION[CFG_SESSION_KEY]["settings"]["timezone"];
			$sunevent = getTimeOfSunAtPosition($params["sunactivity"], strtotime($date), $latitude, $longitude, $timezone);
			$nextRunTime = addTimes($sunevent, $params["offset"], "+");
			break;
		}
		case "engineheater": {
			$nextRunTime = addTimes($params["departure"], "02:00", "-");
			break;
		}
		case "random": {
			$nextRunTime = date("H:i", rand(strtotime($params["earliest"]), strtotime($params["latest"])));
			break;
		}
		case "dynamic": {
			break;
		}
		case "gcal": {
			break;
		}
		default:
			break;
	}

	return $date." ".$nextRunTime.":00";
}

function getDefaultActivationParams() {
	$parameters = array();

	$parameters["time"]			= "12:00";
	$parameters["sunactivity"]	= "dawn";
	$parameters["offset"] 		= "00:00";
	$parameters["departure"] 	= "08:00";
	$parameters["earliest"] 	= "12:00";
	$parameters["latest"] 		= "13:00";

	return $parameters;
}

function getCommandLineArguments($args) {
	$arguments = array();
	$arguments["command"] = NULL;
	$arguments["id"] = NULL;
	$arguments["status"] = NULL;
	$arguments["user"] = NULL;
	$failure = false;

	// getting all arguments
	for($i = 1; $i < count($args); $i++) {
		$tmp = explode("=", $args[$i]);

		// if argument is appearing several times this is considered as a failure
		if(array_key_exists($tmp[0], $arguments) && $arguments[$tmp[0]] != null) {
			// echo "fail: ".$tmp[0];
			$failure = true;
			break;
		}

		switch($tmp[0]) {
			case "command":
			{
				if($tmp[1] == "activation" || $tmp[1] == "group" || $tmp[1] == "device" || $tmp[1] == "macro" || $tmp[1] == "updateschedules") {
					$arguments["command"] = $tmp[1];
				} else {
					$failure = true;
				}
				break;
			}
			case "id":
			case "status":
			{
				$arguments[$tmp[0]] = explode(";", $tmp[1]);
				break;
			}
			case "user":
			{
				$arguments["user"] = $tmp[1];
				break;
			}
			default:
			{
				$failure = true;
				break;
			}
		}
	}

	if(is_null($arguments["user"])) {
		$arguments["user"] = "system";
	}

	// removing NULL initialized array objects
	foreach($arguments as $key => $value) {
		if(is_null($value)) {
			unset($arguments[$key]);
		}
	}

	// checking validity of arguments
	if(!array_key_exists("command", $arguments)) {
		// no command given => failure
		$failure = true;
	} else {
		switch($arguments["command"]) {
			case "activation":
			{
				// only accepting one id for activations
				if(!array_key_exists("id", $arguments) || count($arguments["id"]) > 1) {
					$failure = true;
				}

				break;
			}
			case "device":
			case "group":
			case "macro":
			{
				// device, group and macro must have at least one id
				if(!array_key_exists("id", $arguments)) {
					$failure = true;
				} else {
					if($arguments["command"] == "device" || $arguments["command"] == "group") {
						// device and group must have at least one status
						if(!array_key_exists("status", $arguments)) {
							$failure = true;
						} else {
							// either one status for all id´s or the amount of id´s and statuses must match
							if(count($arguments["status"]) > 1 && count($arguments["status"]) != count($arguments["id"]))
							{
								$failure = true;
							}
						}
					}
				}

				break;
			}
			case "updateschedules":
			{
				break;
			}
			default:
			{
				$failure = true;
				break;
			}
		}
	}

	$arguments["failure"] = $failure;

	if(is_array($arguments["status"]) && count($arguments["status"]) == 1) {
		$arguments["status"] = $arguments["status"][0];
	}

	// echo "<pre>";
	// print_r($args);
	// print_r($arguments);
	// echo "</pre>";

	return $arguments;
}

function generateScheduleStatusArray($status, $dimlevels = array(), $deviceIds = array()) {
	$statuses = array();

	foreach($deviceIds as $id) {
		if(is_array($dimlevels) && isset($dimlevels[$id]) && $dimlevels[$id] != "") {
			$statuses[$id] = $dimlevels[$id];
		} else {
			$statuses[$id] = $status;
		}
	}

	return $statuses;
}

// Function helper to tell if there exists of given type
// type = "" gives all types
// returns true / false
function existsActivations($type = "") {
	$exists = false;

	if(count(Schedules::getActivations(-1, -1, $type)) > 0) {
		$exists = true;
	}

	return $exists;
}

function getOperators() {
	$operators = array(
						"S"  =>	array("S", LBL_SMALLERTHAN),
						"SM" =>	array("SM", LBL_SMALLERTHANOREQUAL),
						"E" =>	array("E", LBL_EQUAL),
						"LE" =>	array("LE", LBL_LARGERTHANOREQUAL),
						"L"	 =>	array("L", LBL_LARGERTHAN)
					);

	return $operators;
}

function validateActivationForExecution($activation) {
	$result["execute"] = false;
	$result["msg"] = "Executing ".$activation["type"]." activation\n";;

	switch($activation["type"]) {
		case "dynamic": {
			// checking if the previous activation (non dynamic) has run
			// and that next activation has not run => dynamic activation is active
			if(convertToBoolean(Schedules::hasPreviousActivationRun($activation["id"])) && !convertToBoolean(Schedules::hasNextActivationRun($activation["id"]))) {
				$currValue = 0;

				$dynDevice = $activation["params"]["dyndevice"];
				$dynOperator = $activation["params"]["dynoperator"];
				$dynValue = $activation["params"]["dynvalue"];

				$deviceName = "";

				// get current value
				if(substr($dynDevice, 0, 3) == "t@@") {
					$sensorSerial = substr($dynDevice, 3);

					$reading = Sensors::getCurrentReadings($sensorSerial);
					$reading = $reading[0];
					$currValue = $reading["last_reading"];

					$deviceName = $temp["name"]." (temperature)";
				} else if($dynDevice == "external") {
					$deviceName = "external tempsensor (".$_SESSION[CFG_SESSION_KEY]["settings"]["externaltemplocation"].")";
					$currValue = getTemperatureFromExternalUrl();
				} else {
					$device = Devices::get($dynDevice);
					$deviceName = $device[0]["description"];
					$lastEvent = $device[0]["status"];
					$currValue = $lastEvent->state ? 1 : 0;

					if($dynValue > 1) {
						$currValue = $lastEvent->dimlevel;
					}

					// echo "<pre>";
					// debugVariable($device);
					// echo "</pre>";
				}

				$operator = getOperators();
				$operator = $operator[$dynOperator][1];

				$result["msg"] .= "Device: ".$deviceName."\n";
				$result["msg"] .= "Current value: ".$currValue."\n";
				$result["msg"] .= "Comparison: ".$operator."\n";
				$result["msg"] .= "Criteria: ".$dynValue."\n";

				// compare current value against dynamic value
				switch($dynOperator) {
					case "S": // smaller than
					{
						if($currValue < $dynValue) {
							$result["execute"] = true;
						}
						break;
					}
					case "SM": // smaller than or equal
					{
						if($currValue <= $dynValue) {
							$result["execute"] = true;
						}
						break;
					}
					case "E": // equal
					{
						if($currValue == $dynValue) {
							$result["execute"] = true;
						}
						break;
					}
					case "LE": // larger than or equal
					{
						if($currValue >= $dynValue) {
							$result["execute"] = true;
						}
						break;
					}
					case "L": // larger than
					{
						if($currValue > $dynValue) {
							$result["execute"] = true;
							$result["msg"] = "";
						}
						break;
					}
					default: {
						// not possible to get here
						break;
					}
				}

				if($result["execute"]) {
					$result["msg"] .= "Criteria met, continuing execution\n";
				} else {
					$result["msg"] .= "Criteria not met, skipping execution\n";
				}
			} else {
				$result["execute"] = false;
				$result["msg"] = "Dynamic activation is not active due to previous non dynamic has not run or/and the next has run\n";
			}

			break;
		}
		case "engineheater": {
			$currTemp = "";
			$sensorName = $_SESSION[CFG_SESSION_KEY]["settings"]["externaltemplocation"];

			//get temperature
			if($activation["params"]["tempsensor"] == "external") {
				$currTemp = getTemperatureFromExternalUrl();
			} else {
				if(substr($activation["params"]["tempsensor"], 0, 3) == "t@@") {
					$activation["params"]["tempsensor"] = substr($activation["params"]["tempsensor"], 3);
				}

				$temp = Sensors::getCurrentReadings($activation["params"]["tempsensor"]);
				$currTemp = $temp[0]["last_reading"];
				$sensorName = $temp[0]["name"];
			}

			$result["msg"] .= "Using tempsensor: ".$sensorName."\n";
			$result["msg"] .= "Current temp: ".$currTemp."\n";

			// if no temperature is aquired we let the task be excuted
			if($currTemp != "") {
				//calculating offset (minutes to be subtracted from planned departure time)
				$timeOffset = calculateEngineHeaterTimeOffset($currTemp) * 1;

				//calculate new time on
				$recalculatedStartTime = addTimes($activation["params"]["departure"], date("H:i", mktime(0, $timeOffset)), "-");
				$tmpTime = explode(":", $recalculatedStartTime);

				$tmpTime[0] = $tmpTime[0] * 1;
				$tmpTime[1] = $tmpTime[1] * 1;
				$tmpTime = mktime($tmpTime[0], $tmpTime[1]);

				$timeUntilActivation = -(date("U") - date("U", $tmpTime)) / 60;

				$result["msg"] .= "Time now: ".date("H:i")."\n";
				$result["msg"] .= "Departure time: ".$activation["params"]["departure"]."\n";
				$result["msg"] .= "Activation time: ".$recalculatedStartTime."\n";
				$result["msg"] .= "Time until activation: ".$timeUntilActivation." minutes\n";
				$result["msg"] .= "Engineheater will be active for: ".round($timeOffset, 0)." minutes before departure\n";

				if($timeUntilActivation > -105) {
					//hysteresis of 15 minutes to avoid putting on at all
					if($timeOffset > 15) {
						// no need to wait 15 minutes before putting on, putting on directly
						if($timeOffset < 105 && $timeUntilActivation > 1) {
							$snoozeActivation = date("Y-m-d H:i:s", time() + ($timeUntilActivation * 60));

							// update upcomingruntime and add snooze task
							$params = array(
								new dbParam(dbParamType::$STRING, "upcomingruntime", $snoozeActivation)
							);

							Schedules::saveActivation($params, $activation["id"]);
							updateAtForActivation($activation["id"]);

							$result["msg"] .= "Adding new task for snoozing, new on time: ".$recalculatedStartTime."\n";
						} else {
							$result["msg"] .= "Engine heater on\n";
							$result["execute"] = true;
						}
					} else {
						// no engine heating needed today => snooze
						$result["msg"] .= "Warm enough to not need any engine heating today (less than 15 minutes to departure)\n";
					}
				} else {
					// for some reason we are late (server shutdown eg.)
					// no need to put on for 15 minutes before departure
					$result["msg"] .= "We are late due to eg. server shutdown/restart. No activation due to less than 15 minutes to departure\n";
				}
			}

			break;
		}
		default: {
			$result["msg"] .= "Activation has no criterias, execute.\n";
			$result["execute"] = true;
			break;
		}
	}

	return $result;
}

function getActivationDevicesAndStatuses($schedule, $activation) {
	$devicesAndStatuses = array();

	$status = $activation["params"]["status"];
	$dimlevel = $activation["params"]["dimlevel"];

	if($schedule["devices"] != "") {
		$deviceIds[] = $schedule["devices"];
	}

	if($schedule["devices"] != "-1" && $schedule["devicegroups"] != "") {
		$groupIds = explode(";", $schedule["devicegroups"]);

		foreach($groupIds as $group) {
			$groupRelations = DeviceGroups::getRelations($group);
			$deviceIds[] = getArrayAsString($groupRelations, (count($groupRelations) + 1), ";", "deviceid");
		}
	}

	$deviceIds = array_unique(explode(";", getArrayAsString($deviceIds, (count($deviceIds) + 1), ";")));
	$status = generateScheduleStatusArray($status, $dimlevel, $deviceIds);

	$devicesAndStatuses["deviceids"] = $deviceIds;
	$devicesAndStatuses["status"] = $status;

	if($devicesAndStatuses["deviceids"][0] == "-1") {
		$devicesAndStatuses["status"] = $devicesAndStatuses["status"]["-1"];
	}

	return $devicesAndStatuses;
}

function checkAutoLogin() {
	if(isset($_COOKIE[CFG_SESSION_KEY."_user"]) && isset($_COOKIE[CFG_SESSION_KEY."_hash"])) {
		if(Users::verifyCookie($_COOKIE[CFG_SESSION_KEY."_user"], $_COOKIE[CFG_SESSION_KEY."_hash"])) {
			// correct credentials, register session and update cookie
			registerSession($_COOKIE[CFG_SESSION_KEY."_user"], true);
		}
	}
}

function debugVariable($var) {
	echo "<pre>";
	print_r($var);
	echo "</pre>";
}

function getRecursiveFileList($root, $dirToScan = "") {
	$output = array();

	if ($handle = opendir($root.$dirToScan)) {
		while (false !== ($entry = readdir($handle))) {
			if ($entry != "." && $entry != ".." && strpos($entry, ".svn") === false) {
				$isDir = convertToNumeric(is_dir($root.$dirToScan."/".$entry));

				$output[] = array(
								"filename" => $dirToScan."/".$entry,
								"isdir"		=> $isDir
							);

				if($isDir) {
					$output = array_merge($output, getRecursiveFileList($root, $dirToScan."/".$entry));
				}
			}
		}

		closedir($handle);
	}

	return $output;
}

function deleteRecursive($directory) {
	$files = glob($directory."/*", GLOB_MARK);

	foreach($files as $file) {
		if(is_dir($file)) {
			deleteRecursive($file);
		} else {
			@unlink($file);
		}
	}

	if(is_dir($directory)) {
		@rmdir($directory);
	}
}

function installSystemPlugin($pluginName, $freshInstall = false) {
	$plugin = SystemPlugins::load($pluginName);
	$installedVersion = false;
	$result = (object) [];

	if(is_object($plugin) && ($freshInstall || version_compare($plugin->getVersion(), $plugin->installedVersion, "<"))) {
		// if we are doing a fresh install or if the installed version is newer than the one being installed we need to uninstall plugin from database first
		uninstallSystemPlugin($plugin);
		$plugin = null;
	}

	if(!is_object($plugin))	{
		$pluginToInstall = HA_ROOT_PATH."/system/systemplugins/".$pluginName."/".$pluginName.".plugin.php";

		if(file_exists($pluginToInstall)) {
			include_once(HA_ROOT_PATH."/system/classes/BaseSystemPlugin.php");
			include_once($pluginToInstall);

			if (class_exists($pluginName)) {
				$plugin = new $pluginName();
				$result = $plugin->install();

				if(!$result->error) {
					SystemPlugins::saveVersion($pluginName, $plugin->getVersion());
					$installedVersion = $plugin->getVersion();

					$result->msg = new SystemMessage();
					$result->msg->setText(LBL_PLUGIN_SUCCESSFULLY_INSTALLED);
					$result->msg->setType(SystemMessageType::$SUCCESS);
				}
			}
		}
	} else {
		// otherwise we just upgrade it
		$result = $plugin->upgrade();

		if(!$result->error) {
			$result->msg = new SystemMessage();
			$result->msg->setText(LBL_PLUGIN_SUCCESSFULLY_UPDATED);
			$result->msg->setType(SystemMessageType::$SUCCESS);
		}

		$installedVersion = $plugin->getVersion();
	}

	if($installedVersion !== false) {
		Versions::updateChecks($pluginName, $installedVersion);
		Versions::dismiss($pluginName);
	}

	return $result;
}

function uninstallSystemPlugin($plugin) {
	SystemPlugins::deleteSettings($plugin->getPluginName());
	SystemPlugins::delete($plugin->getPluginName());
	$plugin->uninstall();
}

function str2ascii($string) {
	$ret = "";

	$chars = preg_split("//", $string, -1, PREG_SPLIT_NO_EMPTY);

	foreach($chars as $char) {
		$ret .= "&#".ord($char).";";
	}

	return $ret;
}

function generatePageList($userlevels = array(), $pages = array(), $isRoot = true) {
	$list = "";
	$style = "";

	if(!$isRoot) {
		$style = " style=\"display: none;\"";
	}

	foreach($pages as $page) {
		$subList = "";
		$imgExpand = "";
		$class = "";
		$subPages = Pages::get($page["id"]);

		if(count($subPages) > 0) {
			$subList = generatePageList($userlevels, $subPages, false);
			$imgExpand = "<img src=\"resources/expand.png\" class=\"expandCollapseChildren\" id=\"expander-".$page["id"]."\" />";
		}

		if($page["parentid"] != -1) {
			$class .= " class=\"parentid-".$page["parentid"]."\"";
		}

		//$userPermissions = Users::getUserLevelPermissions($page["id"]);

		$list .= "<tr id=\"pages_".$page["id"]."\"".$class.$style.">
					<td>".$imgExpand."</td>
					<td>".$page["translation"]."</td>
					<td>".$page["name"]."</td>

				</tr>";

		$list .= $subList;

	}

	return $list;
}

function getGaugeTypes() {
	include(HA_ROOT_PATH."/resources/gaugeTypes.php");
	return $gaugeTypes;
}

function getGraphTypes() {
	include(HA_ROOT_PATH."/resources/graphTypes.php");
	return $graphTypes;
}

function getSensorImage($sensorId, $value, $sensorTypeId = "") {
	$image = "";

	if($sensorTypeId == "") {
		$sensor = Sensors::get("", $sensorId);
		$sensorTypeId = $sensor[0]["sensortype"];
	}

	if($value >= 0) {
		$image = "resources/icons/sensortypes/".$sensorTypeId."_high.png";
	} else {
		$image = "resources/icons/sensortypes/".$sensorTypeId."_low.png";
	}

	if(!file_exists(HA_ROOT_PATH."/".$image)) {
		if($value >= 0) {
			$image = "resources/temp_high.png";
		} else {
			$image = "resources/temp_low.png";
		}
	}

	return $image;
}

function generateSystemMessages() {
	global $ha;
	$messages = "";

	foreach($ha->getMessages() as $message) {
		$messages .= $message->generate();
	}

	return $messages;
}

function executeDynamicActivations($device = -1, $sensor = "") {
	$selectedScenario = Settings::get("selectedscenario");

	if($device != -1) {
		$activations = Schedules::getActivations(-1, -1, "dynamic", $device);
	} else if($sensor != "") {
		$activations = Schedules::getActivations(-1, -1, "dynamic", "t@@".$sensor);
	} else {
		$activations = Schedules::getActivations(-1, -1, "dynamic");
	}

	foreach($activations as $activation) {
		$schedule = Schedules::get($activation["scheduleid"]);
		$schedule = $schedule[0];

		// schedule is enabled and scenario ALL or selected scenario is matching configured scenario in schedule
		if(convertToBoolean($schedule["enabled"]) && ($schedule["scenario"] == -100 || ($selectedScenario !== false && $schedule["scenario"] == $selectedScenario))) {
			// checking activation (if dynamic should run, if enginheater should be on...)
			$result = validateActivationForExecution($activation);

			SaveLog("Running schedule: ".$schedule["name"]."\n");

			if($result["msg"] != "") {
				//echo $result["msg"]."<br />";
				SaveLog($result["msg"]);
			}
			// validation result
			if($result["execute"]) {
				//Re-read triggered info from database in case it has changed (try to avoid race condition)
				if(!Schedules::isActivationTriggered($activation["id"])) {
					$params = array(
						new dbParam(dbParamType::$INTEGER, "triggered", 1),
						new dbParam(dbParamType::$STRING, "upcomingruntime", dbParam::now())
					);

					Schedules::saveActivation($params, $activation["id"]);

					$devicesAndStatuses = getActivationDevicesAndStatuses($schedule, $activation);
					executeToggleAction($devicesAndStatuses["status"], $devicesAndStatuses["deviceids"], $activation["type"]);
					runScheduleMacros($schedule["macros"]);

					Schedules::updateActivationsUpcomingRuntime($activation["id"]);
				}
			} else {
				$params = array(
					new dbParam(dbParamType::$INTEGER, "triggered", 0)
				);

				Schedules::saveActivation($params, $activation["id"]);
			}
		}
	}
}

function installDB($installedVersion) {
	global $db;

	$result = "";
	$breakLine = "";

	if($installedVersion == "" || version_compare($installedVersion, "2.0", "<")) {
		// fresh install
		$db->importDumpFile(HA_ROOT_PATH."/install/database/clean-structure.sql");
		$result .= LBL_IMPORTED." install/database/clean-structure.sql";

		$db->importDumpFile(HA_ROOT_PATH."/install/database/clean-defaults.sql");
		$result .= LBL_IMPORTED." install/database/clean-defaults.sql";

		$breakLine = "\n";

		if(strtoupper(substr(PHP_OS, 0, 3)) === "WIN") {
			$db->importDumpFile(HA_ROOT_PATH."/install/database/clean-defaults-win.sql");
			$result .= $breakLine.LBL_IMPORTED." install/database/clean-defaults-win.sql";
		} else {
			$db->importDumpFile(HA_ROOT_PATH."/install/database/clean-defaults-linux.sql");
			$result .= $breakLine.LBL_IMPORTED." install/database/clean-defaults-linux.sql";
		}
	} else {
		// updating to v3.0
		if(version_compare($installedVersion, "3.0", "<")) {
			$db->importDumpFile(HA_ROOT_PATH."/install/database/update_2_0_2_to_3_0.sql");
			$result .= $breakLine.LBL_IMPORTED." install/database/update_2_0_2_to_3_0.sql";
			$breakLine = "\n";
		}

		// updating to v3.0.1
		if(version_compare($installedVersion, "3.0.1", "<")) {
			$db->importDumpFile(HA_ROOT_PATH."/install/database/update_3_0_to_3_0_1.sql");
			$result .= $breakLine.LBL_IMPORTED." install/database/update_3_0_to_3_0_1.sql";
			$breakLine = "\n";
		}

		// updating to v3.0.2
		if(version_compare($installedVersion, "3.0.2", "<")) {
			$db->importDumpFile(HA_ROOT_PATH."/install/database/update_3_0_1_to_3_0_2.sql");
			$result .= $breakLine.LBL_IMPORTED." install/database/update_3_0_1_to_3_0_2.sql";
			$breakLine = "\n";
		}

		// updating to v3.2
		if(version_compare($installedVersion, "3.2", "<")) {
			$db->importDumpFile(HA_ROOT_PATH."/install/database/update_3_1_to_3_2.sql");
			$result .= $breakLine.LBL_IMPORTED." install/database/update_3_1_to_3_2.sql";
			$breakLine = "\n";
		}

		// updating to v3.3
		if(version_compare($installedVersion, "3.3", "<")) {
			$db->importDumpFile(HA_ROOT_PATH."/install/database/update_3_2_to_3_3.sql");
			$result .= $breakLine.LBL_IMPORTED." install/database/update_3_2_to_3_3.sql";
			$breakLine = "\n";
		}

		// updating to v3.4
		if(version_compare($installedVersion, "3.4", "<")) {
			$db->importDumpFile(HA_ROOT_PATH."/install/database/update_3_3_2_to_3_4.sql");
			$result .= $breakLine.LBL_IMPORTED." install/database/update_3_3_2_to_3_4.sql";
			$breakLine = "\n";
		}
	}

	if($result == "") {
		$result = LBL_DATABASEUPTODATE;
	}

	SaveLog($result."\n", true);

	return $result;
}

function redirectTo($url, $statusCode = 303) {
	if($url !== "") {
		SaveLog("Redirecting to ".$url, false, "redirects");
		header('Location: ' . $url, true, $statusCode);

   	die();
	}
}

function replaceCustomStrings($text, $device, $status) {
	$text = str_replace("%name%", $device["description"], $text);
	$text = str_replace("%type%", $device["type"], $text);
	$text = str_replace("%status%", $status, $text);
	$text = str_replace("%statustext%", getStatusText($status), $text);
	$text = str_replace("%deviceid%", $device["id"], $text);
	$text = str_replace("%systemdeviceid%", $device["systemdeviceid"], $text);
	return $text;
}
