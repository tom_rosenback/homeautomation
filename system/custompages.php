<?php
// HomeAutomation
// Copyright (C) 2021 Tom Rosenback (tom.rosenback@gmail.com), Daniel Malmgren (daniel.malmgren@kolefors.se)
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

// This is the text string for parent menu level of the custom pages.
// Translations from language files can be used aswell.
$customMenuText = LBL_CUSTOMPAGES; // LBL_DEVICES

/* If you want to have custom pages showing copy the section below and paste it as a new section,
then edit the data to match your custom page.

The custom pages can either be PHP pages or pure HTML.
No <html>, <body> or <head> tags should be in the page, just the content.

// COPY THE SECTION STARTING FROM THE NEXT ROW
$customPages["mypage"] = array(	"page"			=> "mypage",			// A string that separates the custompages, same string to be entered twice on this row
								"translation" 	=> LBL_CUSTOMPAGE, 		// Menu text for the custom page, can be translation variable like LBL_NETCAM
								"userlevel"		=> 1, 					// Define who can see this page, 1 = demo, 2 = user, 3 = admin
								"path"			=> "/wwwroot/test.php"	// Absolute path to the custom page, can be a URL
							);
// SECTION TO COPY ENDS ON PREVIOUS ROW */

// Paste it here below and edit.
// Custom page configuration comes here
/*$customPages["cons"] = array(	"page"			=> "cons",			// A string that separates the custompages, same string to be entered twice on this row
								"translation" 	=> "Strömförbrukning", 		// Menu text for the custom page, can be translation variable like LBL_NETCAM
								"userlevel"		=> 1, 					// Define who can see this page, 1 = demo, 2 = user, 3 = admin
								"path"			=> "D:/www/homeautomation_dev/system/consumption.php"	// Absolute path to the custom page, can be a URL
							);*/
?>
