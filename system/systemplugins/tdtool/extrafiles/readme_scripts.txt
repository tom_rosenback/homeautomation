Howto get sensor readings from Tellstick Duo directly to HomeAutomation

Copy (or make a link) the file sensorevent.sh to /usr/local/share/telldus/scripts/sensorevent/. You might need to create the directory.
Edit the file to put in the sensors you want to whitelist.
Restart the telldus service.


Howto get device status changes from Tellstick Duo directly to HomeAutomation

Copy (or make a link) the file deviceevent.sh to /usr/local/share/telldus/scripts/deviceevent/. You might need to create the directory.
Restart the telldus service.

