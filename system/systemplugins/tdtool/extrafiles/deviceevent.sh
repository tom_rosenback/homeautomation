#!/bin/bash

timestamp=$(date +"%d %b %T")

case ${METHOD} in
        1)
                status=1
                ;;
        2)
                status=0
                ;;
esac

echo "${timestamp} - Device ${DEVICEID} -> ${status} " >> /tmp/tddevice.log

url="http://localhost/homeautomation/api.php?do=devices/updateStatus&status=${status}&systempluginname=tdtool&systemdeviceid=${DEVICEID}"

wget --header='Accept-Language: sv' -qO- "${url}" &> /dev/null
