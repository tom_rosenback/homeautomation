#!/bin/bash

declare -A whitelist=([oregon-2914-11-rainrate]=1
					[oregon-2914-11-raintotal]=1
					[fineoffset-temperature-1]=1
					[fineoffset-temperature-167]=1
					[fineoffset-temperature-248]=1)

timestamp=$(date +"%d %b %T")

extraarguments=""

case ${DATATYPE} in
	1)
		haid="${PROTOCOL}-${MODEL}-${SENSORID}"
		;;
	2)
		haid="${PROTOCOL}-${MODEL}-${SENSORID}-humidity"
		;;
	4)
		haid="${PROTOCOL}-${MODEL}-${SENSORID}-rainrate"
		;;
	8)
		haid="${PROTOCOL}-${MODEL}-${SENSORID}-raintotal"
		extraarguments="&calculatevalues=true"
		;;
	16)
		haid="${PROTOCOL}-${MODEL}-${SENSORID}-winddirection"
		;;
	32)
		haid="${PROTOCOL}-${MODEL}-${SENSORID}-windaverage"
		;;
	64)
		haid="${PROTOCOL}-${MODEL}-${SENSORID}-windgust"
		;;
esac


if [[ ${whitelist["$haid"]} ]] ; then

	echo "${timestamp} - ${haid} -> ${VALUE} " >> /tmp/tdsensor.log

	url="http://localhost/homeautomation/api.php?do=sensors/setValue&serial=${haid}&name=${haid}&value=${VALUE}${extraarguments}"

	wget --header='Accept-Language: sv' -qO- "${url}" &> /dev/null

fi
