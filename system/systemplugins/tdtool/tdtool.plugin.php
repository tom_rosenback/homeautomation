<?php
// HomeAutomation
// Copyright (C) 2021 Tom Rosenback (tom.rosenback@gmail.com), Daniel Malmgren (daniel.malmgren@kolefors.se)
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

class tdtool extends BaseSystemPlugin
{
	private $dongleLockEnabled = false;
	private $numSignalRepetitions = 1;
	private $telldusDevices;
	private $telldusDevicesDirty;

	function getPluginName()
	{
		return "tdtool";
	}

	function getDisplayName()
	{
		return "Telldus tdtool";
	}

	function getAuthor()
	{
		return "Tom Rosenback";
	}

	function getAuthorEmail()
	{
		return "tom.rosenback@gmail.com";
	}

	function getVersion()
	{
		return "1.4.2";
	}

	function __construct()
	{
		parent::__construct();
		$this->dongleLockEnabled = $this->getSettings("donglelockenabled");
		$this->numSignalRepetitions = $this->getSettings("numsignalrepetitions");
		$this->telldusDevices = array();
		$this->telldusDevicesDirty = true;
	}

	function install()
	{
		$this->getSettings(); // ensuring that there are no existing settings, if so, delete them

		if(count($this->settings) > 0) {
			SystemPlugins::deleteSettings($this->getPluginName());
			SaveLog($this->getDisplayName().": existing settings deleted during install\n".print_r($this->settings, true), true);
		}

		SystemPlugins::saveSetting($this->getPluginName(), "tellduspath", "", "text", 0, true);
		SystemPlugins::saveSetting($this->getPluginName(), "donglelockenabled", "0", "checkbox", 1, true);
		SystemPlugins::saveSetting($this->getPluginName(), "numsignalrepetitions", "0", "dropdown", 2, true);
	}

	function uninstall()
	{
		// nothing to do
	}

	function upgrade()
	{
		if(version_compare($this->getVersion(), $this->installedVersion, ">")) {
			switch($this->installedVersion) {
				case "1.0": {
					SystemPlugins::saveSetting($this->getPluginName(), "donglelockenabled", "0", "checkbox", 1, true);
					SystemPlugins::saveSetting($this->getPluginName(), "numsignalrepetitions", "0", "dropdown", 2, true);
					break;
				}
				case "1.1":
				case "1.2": {
					SystemPlugins::saveSetting($this->getPluginName(), "numsignalrepetitions", "0", "dropdown", 2, true);
					break;
				}
				default:
					break;
			}

			$this->updateVersion();
		}
	}

	function toggleDevices($devicesToToggle, $statuses) {
		$this->telldusDevicesDirty = true;

		$output = "";
		$command = "";

		foreach($devicesToToggle as $device)
		{
			$status = $statuses[$device["id"]];

			if(convertToBoolean($device["rawdevice"]) && $device["type"] != "absdimmer")
			{
				$command .= " --raw ".HA_ROOT_PATH."/rawcommands/".$device["id"]."_".$this->getTelldusStatus(convertToBoolean($status)).".tdtool";
			}
			else
			{
				if($device["type"] == "bell")
				{
					$command .= $this->getTdtoolParameters($device, 1);
				}
				else
				{
					$command .= $this->getTdtoolParameters($device, $status);
				}
			}
		}

		if($command != "")
		{
			$sudoCmd = "";

			if(defined("CFG_SUDO_COMMAND") && CFG_SUDO_COMMAND != "")
			{
				$sudoCmd = trim(CFG_SUDO_COMMAND)." ";
			}

			$output = $this->doActionOnTellStick($sudoCmd.$this->getTelldusPath()."tdtool".SYS_FILE_EXT." ".trim($command));
		}

		return $output;
	}

	function readStatuses($devicesToRead = array()) {
		$this->telldusDevices = ($this->telldusDevicesDirty ? $this->getTelldusDevices() : $this->telldusDevices);
		$readDevices = array();

		foreach($devicesToRead as $device) {
			//debugVariable($this->telldusDevices, true);
			foreach ($this->telldusDevices as $telldusDevice) {
				if ($device["systemdeviceid"] == $telldusDevice["id"]) {
					$device["status"]->state = $telldusDevice["status"]->state;
					$device["status"]->dimlevel = $telldusDevice["status"]->dimlevel;
					$readDevices[] = $device;
				}
			}
		}

		$this->telldusDevicesDirty = false;

		return $readDevices;
	}

	function importDevices() {
		$this->telldusDevices = ($this->telldusDevicesDirty ? $this->getTelldusDevices() : $this->telldusDevices);
		$this->telldusDevicesDirty = false;

		foreach($this->telldusDevices as $device)
		{
			$systemDeviceId = $device["id"];
			$desc = $device["description"];
			$active = $device["active"];

			SystemPlugins::saveDevice($this->getPluginName(), $systemDeviceId, $desc);
		}
	}

	/* SystemPlugin specific methods */

	function getTdtoolParameters($device, $status)
	{
		$params = "";

		if($device["type"] == "dimmer" && ($status == "true" || $status == "1"))
		{
			$params .= " --".$this->getTelldusStatus(false)." ".$device["systemdeviceid"]." --".$this->getTelldusStatus(true)." ".$device["systemdeviceid"];
		}
		else if ($device["type"] == "absdimmer" && $status >= 1)
		{
			$params .= " --dimlevel ".scaleStatusToDimlevel($status)." --dim ".$device["systemdeviceid"];
		}
		else
		{
			$params .= " --".$this->getTelldusStatus(convertToBoolean($status))." ".$device["systemdeviceid"];
		}

		return $params;
	}

	function doActionOnTellStick($command)
	{
		$output = array();

		// checking if dongle is locked by another process, if so wait for release (max 10 seconds)
		$this->checkDongleLock();

		// now it is our turn, lock dongle from others
		$this->lockDongle();

		for($i = 0; $i <= $this->numSignalRepetitions; $i++)
		{
			// echo "\n".$command."\n";
			// Comment out while being in instable development phase.
			// _exec($command, $output); // this makes read of status show incorrect status
			exec($command, $output);

			SaveLog("Command: ".$command."\nOutput:\n".parseExecOutputToString($output));
		}

		// done, unlock dongle
		$this->unlockDongle();

		return $output;
	}

	function getTelldusStatus($status)
	{
		$text = "";

		if($status == true || $status == 1)
		{
			$text = "on";
		}
		else
		{
			$text = "off";
		}

		return $text;
	}

	function checkDongleLock()
	{
		if(!$this->dongleLockEnabled) {
			return;
		}

		$i = 1;

		while(file_exists(HA_ROOT_PATH."/logs/dongle.lock"))
		{
			if($_SESSION[CFG_SESSION_KEY]["settings"]["debug"] && $i == 1)
			{
				SaveLog("Dongle locked, waiting for release\n");
				echo LBL_WAITINGFORDONGLETOBEFREE."<br>";
			}

			sleep(1);

			// waiting for max 10 seconds
			if($i >= 10)
			{
				SaveLog("Dongle lock bruteforced");

				break;
			}

			$i++;
		}
	}

	function lockDongle()
	{
		if(!$this->dongleLockEnabled) {
			return;
		}

		$absPathLogs = HA_ROOT_PATH."/logs";

		if(is_dir(HA_ROOT_PATH."/logs") != "1")
		{
			mkdir(HA_ROOT_PATH."/logs", intval(CFG_PERMISSION_MASK, 8));
		}
		else
		{
			chmod(HA_ROOT_PATH."/logs", intval(CFG_PERMISSION_MASK, 8));
		}

		SaveLog("Locking dongle");

		// creating lock file for dongle
		$lockFile = HA_ROOT_PATH."/logs/dongle.lock";
		$lockFileHandle = fopen($lockFile, 'w'); // or die("can't create dongle lock file")
		fclose($lockFileHandle);
	}

	function unlockDongle()
	{
		if(!$this->dongleLockEnabled) {
			return;
		}

		$absPathFilename = HA_ROOT_PATH."/logs/dongle.lock";

		SaveLog("Unlocking dongle");

		// unlocking dongle
		deleteFile($absPathFilename);
	}

	function getTelldusDevices($telldusId = -1) {
		$output = array();
		$command = "";
		$sudoCmd = "";

		if(defined("CFG_SUDO_COMMAND") && CFG_SUDO_COMMAND != "")
		{
			$sudoCmd = trim(CFG_SUDO_COMMAND)." ";
		}

		$command .= $sudoCmd.$this->getTelldusPath()."tdtool".SYS_FILE_EXT." --list";

		// $pageGenerationStart = microtime(true);
		// here we need to use PHP exec function to be able to retrieve the output
		exec($command, $output, $return);
		// echo number_format(microtime(true) - $pageGenerationStart, 4)."<br/>";

		SaveLog("Command: ".$command."\nOutput:\n".parseExecOutputToString($output));

		array_shift($output);

		$devices = array();

		foreach($output as $row)
		{
			$row = trim($row);
			//debugVariable($row, true);
			if($row != "")
			{
				$device = array();
				$device = explode("\t", $row);

				$id = trim($device[0]);

				if(is_numeric($id))
				{
					$description = trim($device[1]);
					$status = new Status();

					$statusStr = trim(strtolower($device[2]));

					if(strpos($statusStr, "dimmed:") !== false) {
						$statusStr = explode(":", $statusStr);
						$status->dimlevel = scaleDimlevelToStatus($statusStr[1]);
						$status->state = true;
					} else {
						$status->state = convertToBoolean($statusStr);
					}

					if($telldusId == -1 || $id == $telldusId) {
						//debugVariable($status);
						$devices[] = array("id" => $id, "description" => $description, "status" => $status);
					}
				}
			}
		}

		return $devices;
	}

	function getTelldusPath() {
		$telldusPath = trim($this->getSettings("tellduspath"));

		if($telldusPath != "") {
			$telldusPath .= "/";
		}

		return $telldusPath;
	}

}


?>
