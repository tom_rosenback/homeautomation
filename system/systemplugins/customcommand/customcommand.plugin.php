<?php
// HomeAutomation
// Copyright (C) 2021 Tom Rosenback (tom.rosenback@gmail.com), Daniel Malmgren (daniel.malmgren@kolefors.se)
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

class customcommand extends BaseSystemPlugin
{
	function getPluginName()
	{
		return "customcommand";
	}

	function getDisplayName()
	{
		return "custom command";
	}

	function getAuthor()
	{
		return "Daniel Malmgren";
	}

	function getAuthorEmail()
	{
		return "daniel@kolefors.se";
	}

	function getVersion()
	{
		return "0.1";
	}

	function __construct()
	{
		parent::__construct();
	}

	function install()
	{
		$this->getSettings(); // ensuring that there are no existing settings, if so, delete them

		if(count($this->settings) > 0) {
			SystemPlugins::deleteSettings($this->getPluginName());
			SaveLog($this->getDisplayName().": existing settings deleted during install\n".print_r($this->settings, true), true);
		}

		SystemPlugins::saveSetting($this->getPluginName(), "command_on", "", "text", 0, true);
		SystemPlugins::saveSetting($this->getPluginName(), "command_off", "", "text", 1, true);
	}

	function uninstall()
	{
		// nothing to do
	}

	function upgrade()
	{
		if(version_compare($this->getVersion(), $this->installedVersion, ">")) {
			$this->updateVersion();
		}
	}

	function toggleDevices($devicesToToggle, $statuses) {
		$output = array();
		$command = "";

		foreach($devicesToToggle as $device)
		{
			$status = $statuses[$device["id"]];
			if($status == 0) {
				$command = $this->getSettings("command_off");
			} else {
				$command = $this->getSettings("command_on");
			}

			if(!empty($command)) {
				$command = replaceCustomStrings($command, $device, $statuses[$device["id"]]);

				exec($command, $output);

				SaveLog("Command: ".$command."\nOutput:\n".parseExecOutputToString($output));
			}
		}

		return "";
	}

	function readStatuses($devicesToRead = array()) {
		return $devicesToRead;
	}

	function importDevices() {
		//Nothing to do
	}

}

?>
