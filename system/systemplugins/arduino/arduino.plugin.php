<?php
// HomeAutomation
// Copyright (C) 2021 Tom Rosenback (tom.rosenback@gmail.com), Daniel Malmgren (daniel.malmgren@kolefors.se)
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

class arduino extends BaseSystemPlugin {
	private $statuses;
	private $statusesDirty;
	private $timeout;

	function getPluginName() {
		return 'arduino';
	}

	function getDisplayName() {
		return 'Arduino HomeAutomation';
	}

	function getAuthor() {
		return 'Tom Rosenback';
	}

	function getAuthorEmail() {
		return 'tom.rosenback@gmail.com';
	}

	function getVersion() {
		return '1.0';
	}

	function __construct() {
		parent::__construct();
		$this->statuses = array();
		$this->statusesDirty = true;
		$this->timeout = 5;
	}

	function toggleDevices($devicesToToggle, $newStatuses) {
		$output = array();

		foreach ($devicesToToggle as $device) {
			$output[] = json_decode($this->request('http://'.$device['systemdeviceid'].'/'.$newStatuses[$device['id']]));
		}

		$this->statusesDirty = true;

		return $output;
	}

	function postToggleDevices($devicesToggled, $newStatuses) {
		$output = array();

		$statusChanged = false;

		foreach ($devicesToggled as $device) {
			if($newStatuses[$device['id']] == 2) {
				$events = Events::get($device['id'], 1, 'st', 'desc', 'timestamp DESC, ', 'dimlevel < 2');
				$oldStatus = 0;

				if(count($events) == 1) {
					$event = $events[0];
					$oldStatus = $event['status'] ? 1 : 0;
					//Events::insert(convertToBoolean($oldStatus), $device['id'], $event['ipaddress'], $event['userid'], $oldStatus);
					Devices::updateStatus($device['id'], $oldStatus, $oldStatus);
					$statusChanged = true;
				}
			}
		}

		// if($statusChanged) {
		// 	executeDynamicActivations();
		// }

		$this->statusesDirty = true;

		return $output;
	}

	function readStatuses($devicesToRead = array()) {
		$this->statusesDirty = false;

		$arduinos = $this->getArduinos();
		$readDevices = array();

		//debugVariable($arduinos);

		foreach($arduinos as $ip) {
			$this->statuses[$ip] = (($this->statusesDirty || !array_key_exists($ip, $this->statuses) || $this->statuses[$ip] == '') ? $this->request('http://'.$ip.'/status') : $this->statuses[$ip]);

			if($this->statuses[$ip] != '') {
				$devices = json_decode($this->statuses[$ip]);
				$mcpDevices = $devices->mcps;

				foreach ($devicesToRead as $device) {
					foreach($mcpDevices as $key => $mcpPin) {
						$systemDeviceId = $ip.'/'.$mcpPin->id;
						//debugVariable($systemDeviceId);
						if ($device['systemdeviceid'] == $systemDeviceId) {
							$device['status']->state = $mcpPin->status;
							$readDevices[] = $device;
							unset($mcpDevices[$key]);
						}
					}
				}
			}
		}

		return $readDevices;
	}

	function importDevices() {
		$this->statusesDirty = false;

		$arduinos = $this->getArduinos();

		foreach($arduinos as $ip) {
			$this->statuses[$ip] = $this->request('http://'.$ip.'/status');

			if($this->statuses[$ip] != '') {
				$devices = json_decode($this->statuses[$ip]);
				$mcpDevices = $devices->mcps;

				foreach($mcpDevices as $device) {
					$systemDeviceId = $ip.'/'.$device->id;
					SystemPlugins::saveDevice($this->getPluginName(), $systemDeviceId, $systemDeviceId, 'light', false);
				}
			}
		}
	}

	function install() {
		$this->getSettings(); // ensuring that there are no existing settings, if so, delete them

		if(count($this->settings) > 0) {
			SystemPlugins::deleteSettings($this->getPluginName());
			SaveLog($this->getDisplayName().': existing settings deleted during install\n'.var_dump($this->settings), true);
		}

		SystemPlugins::saveSetting($this->getPluginName(), 'arduinos', '', '', 0, true);
	}

	function uninstall() {
		// do nothing in arduino
	}

	function upgrade() {
		// do nothing in arduino
	}

	function forceEventLogging($systemDeviceId = -1, $newStatus = -2) {
		return false;
	}

	function holdClick($systemDeviceId) {
	}

	function getArduinos() {
		$str = preg_replace('/;/', ',', $this->getSettings('arduinos'));
		return explode(',', $str);
	}

	function getArduinoDevices() {
		$devices = array();

		foreach($this->getArduinos() as $ip) {
			$devices[] = $this->request('http://'.$ip.'/status');
		}

		return $devices;
	}

	function request($url) {
		$ctx = stream_context_create(array('http' =>
			array(
				'timeout' => $this->timeout
			)
		));

		$start = microtime(true);

		$content = @file_get_contents($url, false, $ctx);

		if(microtime(true) - $start > $this->timeout) {
			mail('tom.rosenback@gmail.com',
				'ALERT - ARDUINO NETWORK',
				'There seems to be a problem with the Arduino at "'.$url.'"',
				'From: arduino.no.reply@homeautomation <arduino.no.reply@homeautomation>\r\nMIME-Version: 1.0\r\nContent-type: text/plain; charset=UTF-8\r\n');
		}

		return $content;
	}

	function getStatusText($status, $dimlevel) {
		if($dimlevel == 2) {
			return mb_convert_case($this->getTranslation('longclick'), MB_CASE_LOWER, 'UTF-8');
		} else {
			return getStatusText($status);
		}
	}

	function isAlive() {
		$ip = $_SERVER['REMOTE_ADDR'];
		$devices = Devices::get(-1, 1, false, $this->getPluginName());
		$arduinos = $this->getArduinos();

		$maxAge = 6 * 60 * 60;
		$currentHour = date('G');

		foreach($devices as $device) {
			$diff = date('U') - $device['status']->last_updated;

			if($diff < $maxAge && $currentHour >= 7 && $currentHour <= 23) {
				$this->request('http://'.$device['systemdeviceid'].'/'.($device['status']->state ? '1' : '0'));
			}
		}
	}
}
