<?php
// HomeAutomation
// Copyright (C) 2021 Tom Rosenback (tom.rosenback@gmail.com), Daniel Malmgren (daniel.malmgren@kolefors.se)
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

class dummy extends BaseSystemPlugin
{
	function getPluginName()
	{
		return "dummy";
	}

	function getDisplayName()
	{
		return "dummy";
	}

	function getAuthor()
	{
		return "Tom Rosenback";
	}

	function getAuthorEmail()
	{
		return "tom.rosenback@gmail.com";
	}

	function getVersion()
	{
		return "0.1.1";
	}

	function __construct()
	{
		parent::__construct();
	}

	function toggleDevices($devicesToToggle, $statuses)
	{
		// do nothing in dummy
		return "";
	}

	// function readStatuses($systemDeviceId = -1)
	// {
		// do nothing in dummy
		// return array();
	// }

	function readStatuses($devicesToRead = array())
	{
		$readDevices = array();

		foreach($devicesToRead as $device) {
			$event = Events::get($device["id"], 1);
			$event = $event[0];
			$device["status"]->state = convertToNumeric($event["status"]);
			$readDevices[] = $device;
		}

		return $readDevices;
	}

	function importDevices()
	{
		// do nothing in dummy
	}

	function install()
	{
		// do nothing in dummy
	}

	function uninstall()
	{
		// do nothing in dummy
	}

	function upgrade()
	{
		$this->updateVersion();
	}
}


?>
