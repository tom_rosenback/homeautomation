<?php

$translation["server"] = "Server hostname or IP";
$translation["port"] = "Server TCP port";
$translation["terminator"] = "Terminator hex code";
$translation["password"] = "Password";
$translation["showErrors"] = "Show errors";
$translation["debug"] = "Debug mode";

?>