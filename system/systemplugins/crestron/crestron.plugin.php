<?php
// HomeAutomation
// Copyright (C) 2021 Tom Rosenback (tom.rosenback@gmail.com), Daniel Malmgren (daniel.malmgren@kolefors.se)
//
// PHPTelnet 1.1.1
// by Antone Roundy
// adapted from code found on the PHP website
// public domain
//
// Crestron systemplugin for HomeAutomation by Patrik Pal Bodin the@pal.pp.se © 2010-2012
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

class crestron extends BaseSystemPlugin {

	var $server = "localhost";
	var $ip = "127.0.0.1";
	var $port = 23;
	var $terminator = "03";
	var $hexTerminator = NULL;
    var $password = "na";
	var $showErrors = true;
	var $debug = false;
	var $socket = NULL;
	var $output = array();

	function getPluginName()
	{
		return "crestron";
	}

	function getDisplayName()
	{
		return "Crestron";
	}

	function getAuthor()
	{
		return "Patrik Bodin";
	}

	function getAuthorEmail()
	{
		return "the@pal.pp.se";
	}

	function getVersion()
	{
		return "1.0.1";
	}

	function __construct()
	{
		parent::__construct();
	}

	function install()
	{
		$this->getSettings(); // ensuring that there are no existing settings, if so, delete them

		if(count($this->settings) > 0) {
			SystemPlugins::deleteSettings($this->getPluginName());
			SaveLog($this->getDisplayName().": existing settings deleted during install\n".var_dump($this->settings), true);
		}

		SystemPlugins::saveSetting($this->getPluginName(), "server", "", "text", 0, true);
		SystemPlugins::saveSetting($this->getPluginName(), "port", 23, "text", 1, true);
		SystemPlugins::saveSetting($this->getPluginName(), "password", "na", "text", 2, true);
		SystemPlugins::saveSetting($this->getPluginName(), "terminator", "03", "text", 3, true);
		SystemPlugins::saveSetting($this->getPluginName(), "showErrors", "true", "checkbox", 4, true);
		SystemPlugins::saveSetting($this->getPluginName(), "debug", "false", "checkbox", 5, true);
	}

	function uninstall()
	{
		// nothing to do
	}

	function upgrade()
	{
		if(version_compare($this->getVersion(), $this->installedVersion, ">"))
		{
			switch($this->installedVersion)
			{
				case "1.0":
				{
					// executeCustomNonQuery("UPDATE systempluginsettings SET value=\"text\" WHERE name=\"server\" AND pluginname=\"".$this->getPluginName()."\";");
					// executeCustomNonQuery("UPDATE systempluginsettings SET value=\"text\" WHERE name=\"port\" AND pluginname=\"".$this->getPluginName()."\";");
					// executeCustomNonQuery("UPDATE systempluginsettings SET value=\"text\" WHERE name=\"password\" AND pluginname=\"".$this->getPluginName()."\";");
					// executeCustomNonQuery("UPDATE systempluginsettings SET value=\"text\" WHERE name=\"terminator\" AND pluginname=\"".$this->getPluginName()."\";");
					// executeCustomNonQuery("UPDATE systempluginsettings SET value=\"text\" WHERE name=\"showErrors\" AND pluginname=\"".$this->getPluginName()."\";");
					// executeCustomNonQuery("UPDATE systempluginsettings SET value=\"text\" WHERE name=\"debug\" AND pluginname=\"".$this->getPluginName()."\";");
				}
			}

			$this->updateVersion();
		}
	}

	function toggleDevices($devicesToToggle, $statuses)
	{
		$commands = array();
		foreach($devicesToToggle as $device)
		{
			$status = $statuses[$device["id"]];
			array_push($commands, $this->getParameters($device, $status));
		}
		$receipt = $this->doAction($commands);
		return $receipt;
	}

	function getDevices($systemDeviceId = -1) {
		$commands = array("i=1");
		$receipt = $this->doAction($commands);
		$devices = array();

		if (!is_null($receipt[0])) {
			foreach ($receipt[0] as $key => $value) {
				if (substr($key,0,1) == "a" || substr($key,0,1) == "d") {
					$device = array();
					$device["systemdeviceid"] = substr($key,1);
					$device["type"] = (substr($key,0,1) == "a") ? "absdimmer" : "light";
					$device["description"] = (substr($key,0,1) == "a") ? "Unknown Crestron analogue device" : "Unknown Crestron digital device";
					$device["status"] = ($value < 1) ? 0 : 1;
					$device["dimlevel"] = ((+$value/655.35) > 100) ? 100 : (+$value/655.35) ;
					$device["dimlevel"] = ($device["dimlevel"] < 0) ? 0 : round($device["dimlevel"]);
					$device["dimlevel"] = (substr($key,0,1) == "d") ? $value : $device["dimlevel"];
					if ($systemDeviceId == -1 || $device["systemdeviceid"] == $systemDeviceId) {
						$devices[] = $device;
					}
				}
			}
		} else {
			$this->error('Error! Could not get devices from '.$this->server.' ('.$this->ip.') port '.$this->port.'!');
		}
		return $devices;
	}

	function readStatuses($devicesToRead = -1) {
		if($devicesToRead == -1)
		{
			$devicesToRead = array();
		}

		$commands = array("i=1");
		$receipt = $this->doAction($commands);
		$readDevices = array();
		if (!is_null($receipt[0])) {
			foreach ($devicesToRead as $device) {
				$device["oldStatus"] = $device["status"];
				$device["oldDimlevel"] = $device["dimlevel"];
				$prefix = ($device["type"] == "absdimmer") ? "a" : "d";
				if ($receipt[0][$prefix.$device["systemdeviceid"]]) {
					$value = $receipt[0][$prefix.$device["systemdeviceid"]];
					$device["status"] = (prefix == "a") ? (($value < 1) ? 0 : 1) : $value;
					$device["dimlevel"] = (scaleDimlevelToStatus($value,65535) > 100) ? 100 : scaleDimlevelToStatus($value,65535);
					$device["dimlevel"] = ($device["dimlevel"] < 0) ? 0 : round($device["dimlevel"]);
				} else {
					$device["status"] = 0;
					$device["dimlevel"] = 0;
				}
				$logstring = "";
				foreach ($device as $key => $value) {
					$logstring .= $key."=".$value." ";
				}
				SaveLog("Device: ".trim($logstring));
				$readDevices[] = $device;
			}
		} else {
			$this->error('Error! Could not get statuses from '.$this->server.' ('.$this->ip.') port '.$this->port.'!');
		}
		return $readDevices;
	}

	function importDevices()
	{
		$devices = $this->getDevices();
		foreach($devices as $device)
		{
//			$device["id"] = SystemPlugins::saveDevice($this->getPluginName(), $device["systemdeviceid"], $device["description"], $device["type"]);
//			Events::insert($device["status"], $device["id"], $ipaddress, $userid, $device["dimlevel"]);
		}
	}

	/* SystemPlugin specific methods */

	function getParameters($device, $status)
	{
		$params = "";

		if ($device["type"] == "absdimmer")
		{
			$params .= "a".$device["systemdeviceid"]."=".scaleStatusToDimlevel($status, 65535);
		}
		else
		{
			$params .= "d".$device["systemdeviceid"]."=1";
		}

		return $params;
	}

	function doAction($commands)
	{
		$this->connect();
		foreach($commands as $command)
		{
			$outputs = array();
			$this->send($command);
			$outputs[] = $this->output;
			foreach ($this->output as $key => $value) {
				$logstring .= $key."=".$value." ";
			}
			SaveLog("Command: ".$command." Output:".trim($logstring));
		}
		$this->disconnect();
		return $outputs;
	}

	function getStatus($status)
	{
		$text = "";

		if($status == true || $status == 1)
		{
			$text = "on";
		}
		else
		{
			$text = "off";
		}

		return $text;
	}

	function connect() {
		$server = $this->getSettings("server");
		if (strlen($server)) {
			if (preg_match('/[^0-9.]/',$server)) {
				$ip=gethostbyname($server);
				if ($ip==$server) {
					$this->error('Error! Could not get the IP address of '.$server.'!');
				}
			} else {
				$ip=$server;
			}
		} else {
			$ip='127.0.0.1';
		}

		$this->server = $server;
		$this->ip = $ip;
		$this->port = $this->getSettings("port");
		$this->terminator = $this->getSettings("terminator");
		$this->hexTerminator = ($terminator) ? chr(pack("H", $terminator)) : chr(0x03);
		$this->password = $this->getSettings("password");
		$this->showErrors = (bool) $this->getSettings("showErrors");
		$this->debug = (bool) $this->getSettings("debug");

		$this->debug('server');
		$this->debug('ip');
		$this->debug('port');
		$this->debug('password');
		$this->debug('terminator');
		$this->debug('hexTerminator');
		$this->debug('showErrors');

		if ($socket = fsockopen($this->ip, $this->port)) {
			$this->socket = $socket;
		} else {
			$this->error('Error! Could not connect to '.$this->server.' ('.$this->ip.') port '.$this->port.'!');
			return false;
		}
		$this->login();
		return true;
	}

	function login() {
		if ($this->socket) {
			$this->send('p='.$this->password);
			return true;
		} else {
			$this->error('Error! Could not login. Connection to '.$this->server.' ('.$this->ip.') port '.$this->port.' lost!');
			return false;
		}
	}

	function send($command) {
		if ($this->socket) {
			fputs($this->socket,$command.$this->hexTerminator);
			$result = $this->receive();
		} else {
			$this->error('Error! Could not send command '.$command.': Connection to '.$this->server.' ('.$this->ip.') port '.$this->port.' lost!');
			return false;
		}
		return ($this->socket && $result) ? true : false;
	}

	function receive() {
		unset($receipt);
		unset($this->output);
		if ($this->socket) {
			do {
				$receipt .= fread($this->socket,1000);
				$socketStatus = socket_get_status($this->socket);
			} while ($socketStatus['unread_bytes']);
			$receipt = explode($this->hexTerminator,$receipt);
			foreach ($receipt as $var) {
				list($key,$value) = explode('=',$var);
				$this->output[$key] = $value;
			}
			array_pop($this->output);
			if ($receipt) {
				return true;
			} else {
				$this->error('Error! Could not read output: Connection to '.$this->server.' ('.$this->ip.') port '.$this->port.' lost!');
				return false;
			}
		} else {
			$this->error('Error! Could not read output: Connection to '.$this->server.' ('.$this->ip.') port '.$this->port.' lost!');
			return false;
		}
		return ($this->socket && $receipt) ? true : false;
	}

	function disconnect() {
		if ($this->socket) {
			fclose($this->socket);
			$this->socket=NULL;
		}
		return true;
	}

	function error($error = 'Error! Unspecified error!') {
		SaveLog('Error: '.$error);
		if ($this->showErrors) {
			die($error);
		}
		return false;
	}

	function debug($var) {
		if ($this->debug) {
			SaveLog('Debug: '.$var.' = '.(($this->$var) ? $this->$var : 'empty!'));
//			echo('Debug: '.$var.' = '.(($this->$var)? $this->$var : 'empty!')."<br />\n");
//			flush();
		}
	}
}
