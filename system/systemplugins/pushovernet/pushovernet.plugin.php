<?php
// HomeAutomation
// Copyright (C) 2021 Tom Rosenback (tom.rosenback@gmail.com), Daniel Malmgren (daniel.malmgren@kolefors.se)
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

class pushovernet extends BaseSystemPlugin
{
	private $token = "aii36ivwkc3epyj8t9pzxsem7n2ift";

	function getPluginName()
	{
		return "pushovernet";
	}

	function getDisplayName()
	{
		return "pushover.net";
	}

	function getAuthor()
	{
		return "Daniel Malmgren";
	}

	function getAuthorEmail()
	{
		return "daniel@kolefors.se";
	}

	function getVersion()
	{
		return "0.1";
	}

	function __construct()
	{
		parent::__construct();
		$this->hasPluginPage = true;
	}

	function install()
	{
		$this->getSettings(); // ensuring that there are no existing settings, if so, delete them

		if(count($this->settings) > 0) {
			SystemPlugins::deleteSettings($this->getPluginName());
			SaveLog($this->getDisplayName().": existing settings deleted during install\n".print_r($this->settings, true), true);
		}

		SystemPlugins::saveSetting($this->getPluginName(), "text", "Device %name% has been turned %statustext%", "text", 0, true);
		SystemPlugins::saveSetting($this->getPluginName(), "userkey", "", "text", 1, true);
	}

	function uninstall()
	{
		// nothing to do
	}

	function upgrade()
	{
		if(version_compare($this->getVersion(), $this->installedVersion, ">")) {
			$this->updateVersion();
		}
	}

	function toggleDevices($devicesToToggle, $statuses) {
		$text = "";

		foreach($devicesToToggle as $device)
		{
			$text = replaceCustomStrings($this->getSettings("text"), $device, $statuses[$device["id"]]);

			curl_setopt_array($ch = curl_init(), array(
				CURLOPT_URL => "https://api.pushover.net/1/messages.json",
				CURLOPT_POSTFIELDS => array(
					"token" => $this->token,
					"user" => $this->getSettings("userkey"),
					"message" => $text,
				),
				CURLOPT_SAFE_UPLOAD => true,
			));
			curl_exec($ch);
			curl_close($ch);

			SaveLog("Sent ".$text." to pushover.net.\n");
		}

		return "";
	}

	function readStatuses($devicesToRead = array()) {
		return $devicesToRead;
	}

	function importDevices() {
		//Nothing to do
	}

	function pluginPage()
	{
		$content = "<tr><td>
					<a target=\"_blank\" href=\"/api.php?do=systemplugins/executeCustomFunction&pluginname=pushovernet&methodname=testNotification\">
					".$this->getTranslation("testnotification")."</a></td></tr>";
		return $content;
	}

	function testNotification()
	{
		$result = "";
		if($this->getSettings("userkey") == "") {
			$result = $this->getTranslation("testwarning");
		} else {
			curl_setopt_array($ch = curl_init(), array(
				CURLOPT_URL => "https://api.pushover.net/1/messages.json",
				CURLOPT_POSTFIELDS => array(
					"token" => $this->token,
					"user" => $this->getSettings("userkey"),
					"message" => $this->getTranslation("testtext"),
				),
				CURLOPT_SAFE_UPLOAD => true,
			));
			curl_exec($ch);
			curl_close($ch);

			SaveLog("Sent test notification to pushover.net.\n");
			$result = $this->getTranslation("testsuccess");
		}

		return $result;
	}

}

?>
