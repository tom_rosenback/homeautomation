<?php

$translation["text"] = "Text att skicka i notifiering";
$translation["userkey"] = "Användarnyckel (30 tecken)";
$translation["testnotification"] = "Testa notifiering";
$translation["testwarning"] = "Du måste ange en användarnyckel och spara först!";
$translation["testtext"] = "Testnotifiering från HomeAutomation";
$translation["testsuccess"] = "Skickade testnotifiering till pushover.net.";

?>
