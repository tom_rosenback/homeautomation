<?php

$translation["text"] = "Text to send in notification";
$translation["userkey"] = "User key (30 characters)";
$translation["testnotification"] = "Test notification";
$translation["testwarning"] = "You must set a user key and save first!";
$translation["testtext"] = "Test notification from HomeAutomation";
$translation["testsuccess"] = "Sent test notification to pushover.net.";

?>
