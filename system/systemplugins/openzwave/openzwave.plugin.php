<?php
// HomeAutomation
// Copyright (C) 2021 Tom Rosenback (tom.rosenback@gmail.com), Daniel Malmgren (daniel.malmgren@kolefors.se)
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

require_once("ZwaveServer.php");

class openzwave extends BaseSystemPlugin
{
	private $openzwaveDevices;

	function getPluginName()
	{
		return "openzwave";
	}

	function getDisplayName()
	{
		return "OpenZWave";
	}

	function getAuthor()
	{
		return "Daniel Malmgren";
	}

	function getAuthorEmail()
	{
		return "daniel@kolefors.se";
	}

	function getVersion()
	{
		return "0.1";
	}

	function __construct()
	{
		parent::__construct();
		$this->hasPluginPage    = true;
		$this->openzwaveDevices = array();
	}

	function install()
	{
		$this->getSettings(); // ensuring that there are no existing settings, if so, delete them

		if(count($this->settings) > 0) {
			SystemPlugins::deleteSettings($this->getPluginName());
			SaveLog($this->getDisplayName().": existing settings deleted during install\n".var_dump($this->settings), true);
		}

		SystemPlugins::saveSetting($this->getPluginName(), "server", "localhost", "text", 0, true);
		SystemPlugins::saveSetting($this->getPluginName(), "port", 6004, "text", 1, true);
	}

	function uninstall()
	{
		// nothing to do
	}

	function upgrade()
	{
		// nothing to do
	}

	function toggleDevices($devicesToToggle, $statuses)
	{
		SaveLog("Openzwave toggleDevices(). Now: ".date(DATE_RFC2822).", last updated: ".date(DATE_RFC2822, SystemPlugins::getLastUpdated($this->getPluginName()))."\n", false);
		SystemPlugins::setLastUpdated($this->getPluginName(), strtotime('2000-01-01 00:00:00')); //A long time ago, to be considered dirty
		$zwaveServer = new ZwaveServer($this->getSettings("server"), $this->getSettings("port"));
		$output = "";

		foreach($devicesToToggle as $device) {
			$status = $statuses[$device["id"]];
			$zwaveServer->send("SETVALUE~".$device["systemdeviceid"]."~".$status);
		}

		return $output;
	}

	function readStatuses($devicesToRead = array())
	{
		SaveLog("Openzwave readStatuses(). Now: ".date(DATE_RFC2822).", last updated: ".date(DATE_RFC2822, SystemPlugins::getLastUpdated($this->getPluginName()))."\n", false);
		//Let's assume devices are always reported through api. Then we'll never need to do the following stuff :-)
		/*$this->openzwaveDevices = (((time() - SystemPlugins::getLastUpdated($this->getPluginName()) > 600) || empty($this->openzwaveDevices)) ? $this->getOpenzwaveDevices() : $this->openzwaveDevices);
		$readDevices = array();
		foreach ($this->openzwaveDevices as $device) {
			$device = explode("~", $device);
			foreach ($devicesToRead as $deviceToRead)
			{
				if ($device["1"] == $deviceToRead["systemdeviceid"])
				{
					$deviceToRead["status"]->state = $device["2"];
					$readDevices[] = $deviceToRead;
				}
			}
		}

		return $readDevices;*/

		return $devicesToRead;
	}

	function importDevices()
	{
		SaveLog("Openzwave importDevices(). Now: ".date(DATE_RFC2822).", last updated: ".date(DATE_RFC2822, SystemPlugins::getLastUpdated($this->getPluginName()))."\n", false);
		$devicesList = $this->getOpenzwaveDevices();
		foreach($devicesList as $device) {
			$device = explode("~", $device);
			SystemPlugins::saveDevice($this->getPluginName(), $device[1], $device[0]);
		}
	}

	function changeDeviceName($deviceID, $newName)
	{
		$zwaveServer = new ZwaveServer($this->getSettings("server"), $this->getSettings("port"));
		$zwaveServer->send("SETNAME~".$deviceID."~".$newName);
		$zwaveServer->close();
	}

	function pluginPage()
	{
		$content = "";
		$settingsList = $this->getOpenzwaveSettings();
		if(count($settingsList) > 1 || $settingsList[0] != "")
		{
			$content .= "<tr><td class=\"bold\" valign=\"top\">Device specific settings</td></tr>";
			foreach($settingsList as $setting) {
				$setting = explode("~", $setting);
				$content .= "<tr>
								<td class=\"bold\" valign=\"top\">
									<label for=\"ozw_".$setting[1]."\">".$setting[0]." (".$setting[1].")</label>
								</td>
								<td>";
				if(strlen($setting[5]) > 7 && substr_compare($setting[5], "list", 0, 4) == 0)
				{
					$arrayValues = explode("^", substr($setting[5], 5));
					$availableValues = array();
					foreach($arrayValues as $value)
					{
						array_push($availableValues, array($value));
					}
					$content .= generateSelector($setting[2], "ozw_".$setting[1], "text3", 0, 0, $availableValues, -1, "", $setting[4]);
				}
				else
				{
					$readonly = $setting[4]?"disabled":"";
					$content .= "<input id=\"ozw_".$setting[1]."\" class=\"text3\" type=\"text\" value=\"".$setting[2]."\" name=\"ozw_".$setting[1]."\" ".$readonly.">".$setting[3];
				}
				$content .= "</td>
							</tr>";
			}
		}
		/*$associationsList = $this->getOpenzwaveAssociations();
		if(count($associationsList) > 1 || $associationsList[0] != "")
		{
			$content .= "<td class=\"bold\" valign=\"top\">Device associations</td>";
			foreach($associationsList as $association) {
				$content .= "<tr>";
				$association = explode("~", $association);
				foreach($association as $sAssociation) {
				$content .= "<td class=\"bold\" valign=\"top\">
								".$sAssociation."
							</td>";
				}
				$content .= "</tr>";
			}
		}*/
		$content .= "<tr><td>
					<a target=\"_blank\" href=\"/api.php?do=systemplugins/executeCustomFunction&pluginname=openzwave&methodname=enterInclusionMode\">
					Enter inclusion mode</a></td></tr>";
		$content .= "<tr><td>
					<a target=\"_blank\" href=\"/api.php?do=systemplugins/executeCustomFunction&pluginname=openzwave&methodname=enterExclusionMode\">
					Enter exclusion mode</a></td></tr>";
		$content .= "<tr><td>
					<a target=\"_blank\" href=\"/api.php?do=systemplugins/executeCustomFunction&pluginname=openzwave&methodname=exitMode\">
					Exit inclusion/exclusion mode</a></td></tr>";
		return $content;
	}

	/* SystemPlugin specific methods */

	function getOpenzwaveAssociations()
	{
		SaveLog("Openzwave getOpenzwaveAssociations().\n", false);
		$zwaveServer = new ZwaveServer($this->getSettings("server"), $this->getSettings("port"));
		$zwaveServer->send("GETASSOCIATIONS");
		$list = $zwaveServer->read();
		$list = substr($list, 0, strlen($list) - 1);
		$zwaveServer->close();
		//SystemPlugins::setLastUpdated($this->getPluginName());
		return explode("#", $list);
	}

	function getOpenzwaveSettings()
	{
		SaveLog("Openzwave getOpenzwaveSettings().\n", false);
		$zwaveServer = new ZwaveServer($this->getSettings("server"), $this->getSettings("port"));
		$zwaveServer->send("SETTINGLIST");
		$list = $zwaveServer->read();
		$list = substr($list, 0, strlen($list) - 1);
		$zwaveServer->close();
		//SystemPlugins::setLastUpdated($this->getPluginName());
		return explode("#", $list);
	}

	function getOpenzwaveDevices()
	{
		SaveLog("Openzwave getOpenzwaveDevices().\n", false);
		$zwaveServer = new ZwaveServer($this->getSettings("server"), $this->getSettings("port"));
		$zwaveServer->send("SWITCHLIST");
		$list = $zwaveServer->read();
		$list = substr($list, 0, strlen($list) - 1);
		$zwaveServer->close();
		//SystemPlugins::setLastUpdated($this->getPluginName());
		return explode("#", $list);
	}

	function saveSettings($data)
	{
		SaveLog("Openzwave saveSettings().\n", false);
		$settingsList = $this->getOpenzwaveSettings();
		foreach($settingsList as $setting) {
			$setting = explode("~", $setting);
			if(!$setting[4] && isset($data["ozw_".$setting[1]]) && $data["ozw_".$setting[1]] != $setting[2]) {
				SaveLog("Openzwave saveSettings(). Changing setting ".$setting[1]." to ".$data["ozw_".$setting[1]]."\n", false);
				$zwaveServer = new ZwaveServer($this->getSettings("server"), $this->getSettings("port"));
				$zwaveServer->send("SETVALUE~".$setting[1]."~".$data["ozw_".$setting[1]]);
			}
		}
	}

	/* Custom functions to be called from API */
	function enterInclusionMode()
	{
		SaveLog("Openzwave enterInclusionMode().\n", true);
		$zwaveServer = new ZwaveServer($this->getSettings("server"), $this->getSettings("port"));
		$zwaveServer->send("ENTERINCLUSIONMODE");
		$zwaveServer->close();
		return "Entering inclusion mode";
	}

	function enterExclusionMode()
	{
		SaveLog("Openzwave enterExclusionMode().\n", true);
		$zwaveServer = new ZwaveServer($this->getSettings("server"), $this->getSettings("port"));
		$zwaveServer->send("ENTEREXCLUSIONMODE");
		$zwaveServer->close();
		return "Entering exclusion mode";
	}

	function exitMode()
	{
		SaveLog("Openzwave exitMode().\n", true);
		$zwaveServer = new ZwaveServer($this->getSettings("server"), $this->getSettings("port"));
		$zwaveServer->send("EXITMODE");
		$zwaveServer->close();
		return "Exiting inclusion/exclusion mode";
	}

}

?>
