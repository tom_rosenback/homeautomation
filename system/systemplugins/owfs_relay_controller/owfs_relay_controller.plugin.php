<?php
// HomeAutomation
// Copyright (C) 2021 Tom Rosenback (tom.rosenback@gmail.com), Daniel Malmgren (daniel.malmgren@kolefors.se)
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

class owfs_relay_controller extends BaseSystemPlugin {
	function getPluginName() {
		return "owfs_relay_controller";
	}

	function getDisplayName() {
		return "OWFS Relay Controller";
	}

	function getAuthor() {
		return "Tom Rosenback";
	}

	function getAuthorEmail() {
		return "tom.rosenback@gmail.com";
	}

	function getVersion() {
		return "1.1";
	}

	function __construct() {
		parent::__construct();
	}

	function toggleDevices($devicesToToggle, $statuses) {
		$result = "";

		foreach($devicesToToggle as $device) {
			$status = $statuses[$device["id"]];
			$command = "owwrite ".$device["systemdeviceid"]." ".convertToNumeric($status);
			exec($command, $output);
			$result .= $output;
			SaveLog("Command: ".$command."\nOutput:\n".parseExecOutputToString($output));
		}

		return $result;
	}

	function readStatuses($devicesToRead = array()) {
		$devices = $this->OWRead();
		$readDevices = array();

		foreach($devicesToRead as $device) {
			foreach ($devices as $owdevice) {
				if ($device["systemdeviceid"] == $owdevice["id"]) {
					$device["status"]->state = $owdevice["status"];
					$readDevices[] = $device;
				}
			}
		}

		return $readDevices;
	}

	function importDevices() {
		$devices = $this->OWRead();

		foreach($devices as $device) {
			SystemPlugins::saveDevice($this->getPluginName(), $device["id"], $device["description"]);
		}
	}

	function install() {
		$this->getSettings(); // ensuring that there are no existing settings, if so, delete them

		if(count($this->settings) > 0) {
			SystemPlugins::deleteSettings($this->getPluginName());
			SaveLog($this->getDisplayName().": existing settings deleted during install\n".var_dump($this->settings), true);
		}

		SystemPlugins::saveSetting($this->getPluginName(), "adresses", "", "text", 0, true);
	}

	function uninstall() {
		$this->getSettings(); // ensuring that there are no existing settings, if so, delete them

		if(count($this->settings) > 0) {
			SystemPlugins::deleteSettings($this->getPluginName());
			SaveLog($this->getDisplayName().": existing settings deleted during install\n".var_dump($this->settings), true);
		}
	}

	function upgrade() {
		if(version_compare($this->getVersion(), $this->installedVersion, ">")) {
			switch($this->installedVersion) {
				case "1.0": {
					// update db
				}
			}

			$this->updateVersion();
		}
	}

	function OWRead($deviceId = -1) {
		$devices = array();
		$adresses = trim($this->getSettings("adresses"));

		foreach(explode(",", $adresses) as $adress) {
			if(trim($adress) != "") {
				$result = "";
				$port = ($deviceId == -1 ? "ALL" : $deviceId);
				$result = exec("owread ".$adress."/PIO.".$port);
				$pios = explode(",", $result);

				for($i = 0; $i < count($pios); $i++) {
					$id = ($deviceId == -1 ? $adress."/PIO.".$i : $deviceId);
					$status = convertToBoolean($pios[$i]);

					if($deviceId == -1 || $id == $deviceId) {
						$devices[] = array("id" => $id, "description" => $id, "status" => $status);
					}
				}
			}
		}

		return $devices;
	}
}


?>
