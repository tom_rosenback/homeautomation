<?php
// HomeAutomation
// Copyright (C) 2021 Tom Rosenback (tom.rosenback@gmail.com), Daniel Malmgren (daniel.malmgren@kolefors.se)
//
// PHPTelnet 1.1.1
// by Antone Roundy
// adapted from code found on the PHP website
// public domain
//
// Crestron systemplugin for HomeAutomation by Patrik Bodin the@pal.pp.se © 2010-2012
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

class enigma1_PLi extends BaseSystemPlugin {

	var $server = "localhost";
	var $ip = "127.0.0.1";
	var $port = 23;
    var $username = "root";
    var $password = "dreambox";
	var $showErrors = true;
	var $debug = false;
	var $socket = NULL;
	var $output = array();

	function getPluginName()
	{
		return "enigma1_PLi";
	}

	function getDisplayName()
	{
		return "Dreambox Enigma PLi WebRemote";
	}

	function getAuthor()
	{
		return "Patrik Bodin";
	}

	function getAuthorEmail()
	{
		return "the@pal.pp.se";
	}

	function getVersion()
	{
		return "1.0.1";
	}

	function __construct()
	{
		parent::__construct();
	}

	function install()
	{
		$this->getSettings(); // ensuring that there are no existing settings, if so, delete them

		if(count($this->settings) > 0) {
			SystemPlugins::deleteSettings($this->getPluginName());
			SaveLog($this->getDisplayName().": existing settings deleted during install\n".var_dump($this->settings), true);
		}

		SystemPlugins::saveSetting($this->getPluginName(), "server", "", "text", 0, true);
		SystemPlugins::saveSetting($this->getPluginName(), "port", 23, "text", 1, true);
		SystemPlugins::saveSetting($this->getPluginName(), "username", "na", "text", 3, true);
		SystemPlugins::saveSetting($this->getPluginName(), "password", "na", "text", 2, true);
		SystemPlugins::saveSetting($this->getPluginName(), "showErrors", "true", "checkbox", 4, true);
		SystemPlugins::saveSetting($this->getPluginName(), "debug", "false", "checkbox", 5, true);
	}

	function uninstall()
	{
		// nothing to do
	}

	function upgrade()
	{
		if(version_compare($this->getVersion(), $this->installedVersion, ">"))
		{
			switch($this->installedVersion)
			{
				case "1.0":
				{
					// executeCustomNonQuery("UPDATE systempluginsettings SET value=\"text\" WHERE name=\"server\" AND pluginname=\"".$this->getPluginName()."\";");
					// executeCustomNonQuery("UPDATE systempluginsettings SET value=\"text\" WHERE name=\"port\" AND pluginname=\"".$this->getPluginName()."\";");
					// executeCustomNonQuery("UPDATE systempluginsettings SET value=\"text\" WHERE name=\"username\" AND pluginname=\"".$this->getPluginName()."\";");
					// executeCustomNonQuery("UPDATE systempluginsettings SET value=\"text\" WHERE name=\"password\" AND pluginname=\"".$this->getPluginName()."\";");
					// executeCustomNonQuery("UPDATE systempluginsettings SET value=\"text\" WHERE name=\"showErrors\" AND pluginname=\"".$this->getPluginName()."\";");
					// executeCustomNonQuery("UPDATE systempluginsettings SET value=\"text\" WHERE name=\"debug\" AND pluginname=\"".$this->getPluginName()."\";");
				}
			}

			$this->updateVersion();
		}
	}

	function toggleDevices($devicesToToggle, $statuses)
	{
	if ($this->debug) {
		$server = $this->getSettings("server");
			if (strlen($server)) {
				if (preg_match('/[^0-9.]/',$server)) {
					$ip=gethostbyname($server);
					SaveLog('Using '.$ip.' as IP address of hostname '.$server.'!');
					if ($ip==$server) {
						$this->error('Error! Could not get the IP address of '.$server.'!');
						SaveLog('Error! Could not get the IP address of '.$server.'!');
					}
				} else {
					SaveLog('Using '.$server.' as IP address!');
					$ip=$server;
				}
			} else {
				SaveLog('Error! No hostname or IP address set!');
				$ip='127.0.0.1';
			}
		}
		// echo "<script>alert(\"http://".$username.":".$password."@".$hostname.":".$port."/showRemoteControl!\");</script>";
		// TODO : Make the above URL open in a popup window.
	}

	function getDevices($systemDeviceId = -1) {
		$devices = array();
		$device["systemdeviceid"] = 1;
		$device["type"] = "tvbox";
		$device["description"] = "Dreambox Enigma PLi WebRemote";
		$device["status"] = 1;
		$device["dimlevel"] = 1;
		$devices[] = $device;
		return $devices;
	}

	function readStatuses($devicesToRead = -1) {
		$devices = array();
		$device["status"] = 1;
		$devices[] = $device;
		return $devices;
	}

	function importDevices()
	{
		$devices = $this->getDevices();

		foreach($devices as $device)
		{
			$device["id"] = SystemPlugins::saveDevice($this->getPluginName(), $device["systemdeviceid"], $device["description"], $device["type"]);
//			Events::insert($device["status"], $device["id"], $ipaddress, $userid, $device["dimlevel"]);
		}
	}

	function error($error = 'Error! Unspecified error!') {
		SaveLog('Error: '.$error);
		if ($this->showErrors) {
			die($error);
		}
		return false;
	}

	function debug($var) {
		if ($this->debug) {
			SaveLog('Debug: '.$var.' = '.(($this->$var) ? $this->$var : 'empty!'));
//			echo('Debug: '.$var.' = '.(($this->$var)? $this->$var : 'empty!')."<br />\n");
//			flush();
		}
	}
}
