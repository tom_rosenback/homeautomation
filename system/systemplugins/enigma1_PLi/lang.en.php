<?php

$translation["server"] = "Dreambox hostname or IP";
$translation["port"] = "Dreambox Webinterface TCP port";
$translation["username"] = "Username";
$translation["password"] = "Password";
$translation["showErrors"] = "Show errors";
$translation["debug"] = "Debug mode";

?>