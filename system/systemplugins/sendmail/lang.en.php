<?php

$translation["destinationaddress"] = "Destination address";
$translation["destinationname"] = "Destination name";
$translation["sourceaddress"] = "Source address";
$translation["sourcename"] = "Source name";
$translation["sendonmail"] = "Send mail when device is turned on";
$translation["sendoffmail"] = "Send mail when device is turned off";
$translation["subject"] = "Mail subject";
$translation["message"] = "Mail content";
$translation["sendtestmessage"] = "Send test message";
$translation["sendtestnodest"] = "You must set a destination address and save first!";
$translation["sendtestsuccess"] = "Sent test message";

?>
