<?php
// HomeAutomation
// Copyright (C) 2021 Tom Rosenback (tom.rosenback@gmail.com), Daniel Malmgren (daniel.malmgren@kolefors.se)
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

class sendmail extends BaseSystemPlugin
{
	function getPluginName()
	{
		return "sendmail";
	}

	function getDisplayName()
	{
		return "Send mail";
	}

	function getAuthor()
	{
		return "Daniel Malmgren";
	}

	function getAuthorEmail()
	{
		return "daniel@kolefors.se";
	}

	function getVersion()
	{
		return "0.4";
	}

	function __construct() {
		parent::__construct();
		$this->hasPluginPage = true;
	}

	function install() {
		$this->getSettings(); // ensuring that there are no existing settings, if so, delete them

		if(count($this->settings) > 0) {
			SystemPlugins::deleteSettings($this->getPluginName());
			SaveLog($this->getDisplayName().": existing settings deleted during install\n".var_dump($this->settings), true);
		}

		SystemPlugins::saveSetting($this->getPluginName(), "destinationaddress", "administrator@mydomain.net", "text", 0, true);
		SystemPlugins::saveSetting($this->getPluginName(), "destinationname", "Administrator", "text", 1, true);
		SystemPlugins::saveSetting($this->getPluginName(), "sourceaddress", "homeautomation@mydomain.net", "text", 2, true);
		SystemPlugins::saveSetting($this->getPluginName(), "sourcename", "HomeAutomation", "text", 3, true);
		SystemPlugins::saveSetting($this->getPluginName(), "sendonmail", "1", "checkbox", 4, true);
		SystemPlugins::saveSetting($this->getPluginName(), "sendoffmail", "1", "checkbox", 5, true);
		SystemPlugins::saveSetting($this->getPluginName(), "subject", "%name% %statustext%", "text", 6, true);
		SystemPlugins::saveSetting($this->getPluginName(), "message", "%name% %statustext%", "text", 7, true);
	}

	function uninstall()
	{
		// nothing to do
	}

	function upgrade()
	{
		if(version_compare($this->getVersion(), $this->installedVersion, ">")) {
			switch($this->installedVersion)
			{
				case "0.1":
                {
					SystemPlugins::saveSetting($this->getPluginName(), "destinationname", "Administrator", "", 1, true);
					SystemPlugins::saveSetting($this->getPluginName(), "sourceaddress", "homeautomation@mydomain.net", "", 2, true);
					SystemPlugins::saveSetting($this->getPluginName(), "sourcename", "HomeAutomation", "", 3, true);
					SystemPlugins::saveSetting($this->getPluginName(), "sendonmail", "1", "checkbox", 4, true);
					SystemPlugins::saveSetting($this->getPluginName(), "sendoffmail", "1", "checkbox", 5, true);
					SystemPlugins::saveSetting($this->getPluginName(), "subject", "%name% %statustext%", "text", 6, true);
					SystemPlugins::saveSetting($this->getPluginName(), "message", "%name% %statustext%", "text", 7, true);
				}
				case "0.2":
                {
					SystemPlugins::saveSetting($this->getPluginName(), "sendonmail", "1", "checkbox", 4, true);
					SystemPlugins::saveSetting($this->getPluginName(), "sendoffmail", "1", "checkbox", 5, true);
					SystemPlugins::saveSetting($this->getPluginName(), "subject", "%name% %statustext%", "text", 6, true);
					SystemPlugins::saveSetting($this->getPluginName(), "message", "%name% %statustext%", "text", 7, true);
				}
				case "0.3":
				{
					SystemPlugins::saveSetting($this->getPluginName(), "subject", "%name% %statustext%", "text", 6, true);
					SystemPlugins::saveSetting($this->getPluginName(), "message", "%name% %statustext%", "text", 7, true);
				}
			}

			$this->updateVersion();
		}
	}

	function toggleDevices($devicesToToggle, $statuses)
	{
		foreach($devicesToToggle as $device) {
			if(($this->getSettings("sendonmail") && $statuses[$device["id"]]) ||
				($this->getSettings("sendoffmail") && !$statuses[$device["id"]]))
			{
				mail($this->getSettings("destinationname")." <".$this->getSettings("destinationaddress").">",
					replaceCustomStrings($this->getSettings("subject"), $device, $statuses[$device["id"]]),
					replaceCustomStrings($this->getSettings("message"), $device, $statuses[$device["id"]]),
					"From: ".$this->getSettings("sourcename")." <".$this->getSettings("sourceaddress").
							">\r\nMIME-Version: 1.0\r\nContent-type: text/plain; charset=UTF-8\r\n");
			}
		}

		return "";
	}

	function readStatuses($devicesToRead = array())
	{
		return $devicesToRead;
	}

	function importDevices()
	{
		//Nothing to do
	}

	function pluginPage()
	{
		$content = "<tr><td>
					<a target=\"_blank\" href=\"/api.php?do=systemplugins/executeCustomFunction&pluginname=sendmail&methodname=testNotification\">
					".$this->getTranslation("sendtestmessage")."</a></td></tr>";
		return $content;
	}

	function testNotification()
	{
		$result = "";
		if($this->getSettings("destinationaddress") == "") {
			$result = $this->getTranslation("sendtestnodest");
		} else {
			mail($this->getSettings("destinationname")." <".$this->getSettings("destinationaddress").">",
				"Test notification from HomeAutomation",
				"Test notification from HomeAutomation",
				"From: ".$this->getSettings("sourcename")." <".$this->getSettings("sourceaddress").
						">\r\nMIME-Version: 1.0\r\nContent-type: text/plain; charset=UTF-8\r\n");

			SaveLog("Sent test e-mail message.\n");
			$result = $this->getTranslation("sendtestsuccess");
		}

		return $result;
	}

}

?>
