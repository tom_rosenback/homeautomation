<?php

$translation["destinationaddress"] = "Mottagaraddress";
$translation["destinationname"] = "Mottagarnamn";
$translation["sourceaddress"] = "Avsändaradress";
$translation["sourcename"] = "Avsändarnamn";
$translation["sendonmail"] = "Skicka mail när enhet slås på";
$translation["sendoffmail"] = "Skicka mail när enhet slås av";
$translation["subject"] = "Mailets ärende";
$translation["message"] = "Mailets innehåll";
$translation["sendtestmessage"] = "Skicka testmeddelande";
$translation["sendtestnodest"] = "Du måste ange och spara en mottagaradress först för att kunna testa!";
$translation["sendtestsuccess"] = "Skickade testmeddelande";

?>
