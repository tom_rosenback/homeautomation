<?php
// HomeAutomation
// Copyright (C) 2021 Tom Rosenback (tom.rosenback@gmail.com), Daniel Malmgren (daniel.malmgren@kolefors.se)
//
// Boxcar systemplugin for HomeAutomation
// Copyright (C) 2015 Jesper Lindberg (jl@bejk.se)
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.


class boxcar extends BaseSystemPlugin
{
	function getPluginName() {
		return "boxcar";
	}

	function getDisplayName() {
		return "Boxcar";
	}

	function getAuthor() {
		return "Jesper Lindberg";
	}

	function getAuthorEmail() {
		return "jl@bejk.se";
	}

	function getVersion() {
		return "0.1";
	}

	function __construct() {
		parent::__construct();
	}

	function install() {
		$this->getSettings(); // ensuring that there are no existing settings, if so, delete them

		if(count($this->settings) > 0) {
			SystemPlugins::deleteSettings($this->getPluginName());
			SaveLog($this->getDisplayName().": existing settings deleted during install\n".var_dump($this->settings), true);
		}

		SystemPlugins::saveSetting($this->getPluginName(), "boxcartoken", "", "", 0, true);
		SystemPlugins::saveSetting($this->getPluginName(), "soundid", "", "beep-crisp", 1, true);
		SystemPlugins::saveSetting($this->getPluginName(), "sensors", "", "", 2, true);
		SystemPlugins::saveSetting($this->getPluginName(), "haaddress", "", str_replace('index.php', '', 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['SCRIPT_NAME']), 3, true);
	}

	function uninstall() {
		// nothing to do
	}

	function upgrade() {
		if(version_compare($this->getVersion(), $this->installedVersion, ">")) {
			$this->updateVersion();
		}
	}

	function toggleDevices($devicesToToggle, $statuses) {
		$subject = "";

		foreach($devicesToToggle as $device) {
			$status = getStatusText($statuses[$device["id"]]);
			$subject .= $device["description"]." ".$status."\n";
		}

		if($subject != "") {
		//Check curl lib
		if  (in_array  ('curl', get_loaded_extensions())) {


		//GET SENSOR VALUES
		$haaddress = $this->getSettings("haaddress");
		$sensorname = $this->getSettings("sensors");


		$xml = simplexml_load_file($haaddress."api.php?do=sensors/get&output=xml");
		$json = json_encode($xml);
		$array = json_decode($json,TRUE);


		$sensorname = preg_replace('/\s/', '', $sensorname); //Remove spaces
		$namearray = explode(',', $sensorname); //split string into array seperated by ', '

		$message = "";
		foreach($array as $array2) {
		   foreach($array2 as $key => $item_array)
		   {
				foreach($item_array as $item => $value)
				if (in_array($value, $namearray)){
				$message .= $value.': '.$array['sensor'][$key]['value'].$array['sensor'][$key]['unit'].'</br>';
				}
		   }
		}
		// SENSOR VALUES END

			//Load curl
			$curl = curl_init();

			//Set options
			$boxcarsettings = array(
				user_credentials => $this->getSettings("boxcartoken"),
				'notification[title]' => $subject,
				'notification[long_message]' => $message,
				'notification[sound]' => $this->getSettings("soundid"),
				'notification[source_name]' => 'Home Automation',
				'notification[icon_url]' => $haaddress.'resources/absdimmer_on.png'

				);
			$curloptpostfields = http_build_query($boxcarsettings);

			curl_setopt_array($curl, array(
			CURLOPT_RETURNTRANSFER => 1,
			CURLOPT_URL => 'https://new.boxcar.io/api/notifications', //Boxcar API address
			CURLOPT_POST => 1,
			CURLOPT_POSTFIELDS => $curloptpostfields
			));

			//Send http
			$post = curl_exec($curl);

			curl_close($curl);
			//SaveLog($this->getDisplayName().":".$post. "\n".var_dump($this->settings), true);
		}


		}

		return "";
	}



	function readStatuses($devicesToRead = array()) {
		//Nothing to do
	}

	function importDevices() {
		//Nothing to do
	}

}

?>
