<?php
// HomeAutomation
// Copyright (C) 2021 Tom Rosenback (tom.rosenback@gmail.com), Daniel Malmgren (daniel.malmgren@kolefors.se)
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

class telldus_live extends BaseSystemPlugin {
	protected $telldusDevices;
	protected $telldusDevicesDirty;
	private static $api = false;

	function __construct() {
		parent::__construct();

		require_once("TelldusLiveApi.php");
		$this->hasPluginPage = true;
		$this->telldusDevices = array();
		$this->telldusDevicesDirty = true;

		$this->token = $this->getSettings("token");
		$this->token_secret = $this->getSettings("secret");
	}

	function getPluginName() {
		return "telldus_live";
	}

	function getDisplayName() {
		return "Telldus Live";
	}

	function getAuthor() {
		return "Tom Rosenback";
	}

	function getAuthorEmail() {
		return "tom.rosenback@gmail.com";
	}

	function getVersion() {
		return "1.2.1";
	}

	function install() {
		$result = (object) [];
		// $result->msg = new SystemMessage();
		// $result->msg->setText($this->getTranslation("Missign OpenSSL"));
		// $result->msg->setType(SystemMessageType::$ERROR);
		// $result->error = true;
		// return $result;

		$this->getSettings(); // ensuring that there are no existing settings, if so, delete them

		if(count($this->settings) > 0) {
			SystemPlugins::deleteSettings($this->getPluginName());
			SaveLog($this->getDisplayName().": existing settings deleted during install\n".print_r($this->settings, true), true);
		}

		SystemPlugins::saveSetting($this->getPluginName(), "token", "", "text", 0, true);
		SystemPlugins::saveSetting($this->getPluginName(), "secret", "", "text", 0, true);

		return $result;
	}

	function uninstall() {
		// nothing to do
	}

	function upgrade() {
		if(version_compare($this->getVersion(), $this->installedVersion, ">")) {
			switch($this->installedVersion)	{
				default:
					break;
			}

			$this->updateVersion();
		}
	}

	function toggleDevices($devicesToToggle, $statuses) {
		$success = false;

		$this->telldusDevicesDirty = true;

		foreach($devicesToToggle as $device) {
			$status = $statuses[$device["id"]];
			$response = false;

			if($device["type"] == "bell") {
				$response = $this->api()->bell($device["systemdeviceid"]);
			} else if($device["type"] == "dimmer" && ($status == "true" || $status == "1")) {
				$this->api()->turnOff($device["systemdeviceid"]);
				$response = $this->api()->turnOn($device["systemdeviceid"]);
			} else if ($device["type"] == "absdimmer" && $status >= 1) {
				$response = $this->api()->turnOn($device["systemdeviceid"], scaleStatusToDimlevel($status));
			} else {
				if($status == 1) {
					$response = $this->api()->turnOn($device["systemdeviceid"]);
				} else {
					$response = $this->api()->turnOff($device["systemdeviceid"]);
				}
			}
			
			$success &= $response !== false ? $response->status : false;
		}

		return $success;
	}

	function readStatuses($devicesToRead = array()) {
		$this->loadDevices();
		$readDevices = array();
		foreach($devicesToRead as $device) {
			foreach ($this->telldusDevices as $telldusDevice) {
				if ($device["systemdeviceid"] == $telldusDevice["id"]) {
					$device["status"]->state = convertToBoolean($telldusDevice["status"]->state);
					$device["status"]->dimlevel = convertToNumeric($telldusDevice["status"]->dimlevel);
					$readDevices[] = $device;
				}
			}
		}

		$this->telldusDevicesDirty = false;

		return $readDevices;
	}

	function importDevices() {
		$this->loadDevices();

		if($this->telldusDevices !== false) {
			foreach($this->telldusDevices as $device) {
				$systemDeviceId = $device["id"];
				$desc = $device["description"];
				$active = $device["active"];
				SystemPlugins::saveDevice($this->getPluginName(), $systemDeviceId, $desc);
			}
		}
	}

	function pluginPage() {
		$returnUrl = getFullRequestURI();

		if(getFormVariable("authorize") == "token") {
			$returnUrl = str_replace("&action=editsettings", "", str_replace("&authorize=token", "", $returnUrl));

			$result = $this->api()->getAccessToken();

			if($result["success"]) {
				SystemPlugins::saveSetting($this->getPluginName(), "token", $result["token"]);
				SystemPlugins::saveSetting($this->getPluginName(), "secret", $result["secret"]);

				$sysMsg = new SystemMessage();
				$sysMsg->setText($this->getTranslation("Authorized successfully"));
				$sysMsg->setType(SystemMessageType::$SUCCESS);
				redirectTo($returnUrl."&msg=".$sysMsg->encode());
			}
		} else {
			if(!$this->api()->authorize($returnUrl."&authorize=token")) {
				$returnUrl = str_replace("&action=editsettings", "", str_replace("&authorize=token", "", $returnUrl));

				$sysMsg = new SystemMessage();
				$sysMsg->setText($this->getTranslation("Unable to authorize, check logs for hints"));
				$sysMsg->setType(SystemMessageType::$ERROR);

				redirectTo($returnUrl."&msg=".$sysMsg->encode());
			}
		}
	}

	function getPluginPageLinkText() {
		return $this->token != "" && $this->secret != ""  ? $this->getTranslation("authorize") : $this->getTranslation("reauthorize");
	}

	/* SystemPlugin specific methods */
	private function loadDevices() {
		$this->telldusDevices = $this->api()->getDevices();
	}

	private function api() {
		if(self::$api == false) {
			self::$api = new TelldusLiveApi($this->token, $this->token_secret);
		}

		return self::$api;
	}
}


?>
