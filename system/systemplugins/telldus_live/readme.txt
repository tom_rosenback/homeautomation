HTTP_OAuth is required for TelldusLive to work, install it easily with the below command

pear install HTTP_OAuth-alpha

read more here http://pear.php.net/package/HTTP_OAuth/

in PHP > v7.x change 

$v_att_list = & func_get_args(); 
to
$v_att_list &= func_get_args();

in Pear\Archive\Tar.php

Uncomment following in \php\pear\HTTP\Request2\SocketWrapper.php on row 112 - 120

foreach ($contextOptions as $wrapper => $options) {
	foreach ($options as $name => $value) {
		if (!stream_context_set_option($context, $wrapper, $name, $value)) {
			throw new HTTP_Request2_LogicException(
				"Error setting '{$wrapper}' wrapper context option '{$name}'"
			);
		}
	}
}