<?php

class TelldusLiveApi {
	protected $baseUrl = "https://api.telldus.com";

	private $supportedMethods = array();

	protected $public_key = "WUREFRUSPAT2ECASWA2A5WEVEQUDECAZ";
	protected $private_key = "CA2EPUWEPR7XUTHAH529ECE2AMEHAMUS";
	protected $consumer;

	public function __construct($token = "", $secret = "") {
		require_once 'HTTP/OAuth/Consumer.php';
		$this->initConsumer($token, $secret);

		$this->supportedMethods = array(
			"on" 	=> 1,
			"off" 	=> 2,
			"bell" 	=> 4,
			"dim"	=> 16,
		);
	}

	public function initConsumer($token = "", $secret = "") {
		$this->consumer = new HTTP_OAuth_Consumer($this->public_key, $this->private_key, $token, $secret);
	}

	public function authorize($returnUrl) {
		$result = true;
		try {
			$this->consumer = new HTTP_OAuth_Consumer($this->public_key, $this->private_key);
			$this->consumer->getRequestToken($this->baseUrl."/oauth/requestToken", $returnUrl);
			$_SESSION['telldus_live_request_token'] = $this->consumer->getToken();
			$_SESSION['telldus_live_request_token_secret'] = $this->consumer->getTokenSecret();
			redirectTo($this->consumer->getAuthorizeUrl($this->baseUrl."/oauth/authorize"));
		}
		catch(Exception $e) {
			SaveLog("TelldusLiveApi failed on getRequestToken, exception:\n".print_r($e, true), true);
			$result = false;
		}

		return $result;
	}

	public function getAccessToken() {
		$result = array("token" => "", "secret" => "", "success" => true);
		try {
				$this->initConsumer($_SESSION["telldus_live_request_token"], $_SESSION["telldus_live_request_token_secret"]);
				unset($_SESSION["telldus_live_request_token"]);
				unset($_SESSION["telldus_live_request_token_secret"]);

	      $this->consumer->getAccessToken($this->baseUrl."/oauth/accessToken");
	      $result["token"] = $this->consumer->getToken();
	      $result["secret"] = $this->consumer->getTokenSecret();
		} catch (Exception $e) {
	  	$result["success"] = false;
			SaveLog("TelldusLiveApi failed on getAccessToken, exception:\n".print_r($e, true), true);
		}

		return $result;
	}

	protected function request($endpoint, $params = array(), $method = "GET") {
		$response = (object) ["error" => true];

		try {
			$response = $this->consumer->sendRequest($this->baseUrl."/json".$endpoint, $params, $method);
			$response = json_decode($response->getBody());
		}
		catch (Exception $e) {
			SaveLog("TelldusLiveApi failed on request, exception:\n".print_r($e, true), true);
		}

		return $response;

	}

	protected function methodBits($methods = array()) {
		$bits = 0;

		if(empty($methods)) {
			$methods = array_keys($this->supportedMethods);
		}

		foreach($methods as $m) {
			$bits |= $this->supportedMethods[$m];
		}

		return $bits;
	}

	public function getDevices($telldusId = -1) {
		$devices = false;

		$params = array(
			"supportedMethods" => $this->methodBits()
		);

		$response = $this->request("/devices/list", $params);

		if(!$response->error) {
			$devices = array();

			foreach($response->device as $d) {
				$description = $d->name;
				$status = new Status();

				if($d->state == $this->supportedMethods["off"]) {
					$status->state = false;
				} else {
					if($d->methods == $this->methodBits(array("on", "off", "dim"))) {
						$dimlevel = $d->statevalue * 1 == 0 ? 255 : $d->statevalue;
						$status->dimlevel = scaleDimlevelToStatus($dimlevel);
					}

					$status->state = true;
				}

				if($telldusId == -1 || $d->id == $telldusId) {
					$devices[] = array("id" => $d->id, "description" => $description, "status" => $status);
				}
			}
		}

		return $devices;
	}

	public function getSensors() {
		$sensors = false;
    $params = array(
      "includeValues" => 1
    );

		$response = $this->request("/sensors/list", $params);

		if(!$response->error) {
			$sensors = $response->sensor;
		}

		return $sensors;
	}

	public function turnOn($deviceId) {
		$params = array(
			"id" => $deviceId
		);

		return $this->request("/device/turnOn", $params);
	}

	public function turnOff($deviceId) {
		$params = array(
			"id" => $deviceId
		);

		return $this->request("/device/turnOff", $params);
	}

	public function dim($deviceId, $dimLevel = 0) {
		$params = array(
			"id" => $deviceId,
			"dimlevel" => $dimLevel
		);

		return $this->request("/device/dim", $params);
	}
}
?>
