<?php

class Scenarios {
	function __construct() {
	}

	public static function get($id = -1, $forScenarioActivator = false) {
		global $db;
		$params = array();

		$scenarios = array();

		$query = "SELECT id, name FROM scenarios";

		if($id != -1) {
			$query .= " WHERE id = ?";
			$params[] = new dbParam(dbParamType::$INTEGER, "id", $id);
		}

		$query .= " ORDER BY sort, name";

		$result = $db->fetch_many($query, $params);

		if($result !== false) {
			if($id == -1 && !$forScenarioActivator) {
				// forcing all scenarios to be first
				$scenarios[] = array("id" => -100, "name" => LBL_ALL);
			}

			foreach ($result as $s) {
				$scenarios[] = $s;
			}
		}

		return $scenarios;
	}
	
	public static function save($params, $id = -1) {
		global $db;
		return $db->save("scenarios", $params, $id);
	}
	
	public static function delete($id = "") {
		global $db;
		
		$params = array(
			new dbParam(dbParamType::$INTEGER, "id", $id)
		);
		
		return $db->delete("scenarios", $params);
	}
}

?>
