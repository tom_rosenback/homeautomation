<?php
// HomeAutomation
// Copyright (C) 2021 Tom Rosenback (tom.rosenback@gmail.com), Daniel Malmgren (daniel.malmgren@kolefors.se)
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

class HomeAutomation {
	var $currentPage;
	var $headers;
	var $body;
	var $messages;
	var $onLoadScript;
	var $showInfoBox;
	var $isMobile;
	var $systemPlugins;
	var $customPages;

	function __construct($userAgent = "") {
		$this->currentPage		= array();
		$this->headers 			= array();
		$this->body 			= "";
		$this->messages 		= array();
		$this->onLoadScript		= "";
		$this->showInfoBox		= false;
		$this->userAgent		= $userAgent;
		$this->systemPlugins	= NULL;
		$this->customPages		= NULL;

		include_once(HA_ROOT_PATH."/system/custompages.php");
		if(is_array($customPages) && count($customPages) > 0) {
			$this->setCustomPages($customMenuText, $customPages);
		}
	}

	function addBody($part)
	{
		$this->body .= $part;
	}

	function getBody()
	{
		return $this->body;
	}

	function addHeader($identifier, $header)
	{
		$this->headers[$identifier] = $header;
	}

	function removeHeader($identifier)
	{
		unset($this->headers[$identifier]);
	}

	function setCustomPages($title, $pages)
	{
		$tmpPages = array();

		foreach($pages as $key => $page)
		{
			$tmpPages["custompages-".$key] = $page;
			$tmpPages["custompages-".$key]["id"] = "custompages-".$pages[$key]["page"];
			$tmpPages["custompages-".$key]["name"] = "custompages-".$pages[$key]["page"];
		}

		$this->customPages = array("title" => $title, "pages" => $tmpPages);
	}

	function getCurrentPagePath()
	{
		return $this->currentPage["path"];
	}

	function getCustomPagesTitle()
	{
		return $this->customPages["title"];
	}

	function getCustomPages()
	{
		return $this->customPages["pages"];
	}

	function generateHeader()
	{
		$head = "";

		foreach($this->headers as $header)
		{
			if($header != "")
			{
				$head .= $header."\n";
			}
		}

		return $head;
	}

	function addMessage($message)
	{
		if(!$message->showOnlyInDebug || ($message->showOnlyInDebug && $_SESSION[CFG_SESSION_KEY]["settings"]["debug"]))
		{
			if($message->text != "")
			{
				if($message->id != "")
				{
					$this->messages[$message->id] = $message;
				}
				else
				{
					$this->messages[] = $message;
				}
			}
		}
	}

	function getMessages()
	{
		return $this->messages;
	}

	function setOnLoadScript($script = "")
	{
		$this->onLoadScript = $script;
	}

	function getOnLoadScript()
	{
		return $this->onLoadScript;
	}

	function setCurrentPage($pageName) {
		global $db;

		$this->currentPage = array();

		if(!isset($_SESSION[CFG_SESSION_KEY]["userlevel"]) || $_SESSION[CFG_SESSION_KEY]["userlevel"] < 1)
		{
			// user not logged in
			$this->currentPage["id"]			= -1;
			$this->currentPage["name"]			= $pageName;
			$this->currentPage["userlevel"]		= "";
			$this->currentPage["modulegroup"]	= "";
			$this->currentPage["module"] 		= "login";
			$this->currentPage["defaultaction"]	= "default";
			$this->currentPage["parentids"] 	= array();
			$this->currentPage["sessionexpired"]= true;
		}
		else
		{
			if(stripos($pageName, "custompages") === 0)
			{
				$this->currentPage = Pages::getByName("custompages");
			}
			else
			{
				$this->currentPage = Pages::getByName($pageName);
			}

			if($this->currentPage["module"] == "custompages" && $this->customPages != NULL)
			{
				if(array_key_exists($pageName, $this->customPages["pages"]))
				{
					$this->currentPage = $this->customPages["pages"][$pageName];
				}
				else
				{
					$this->currentPage = reset($this->customPages["pages"]);
				}

				$this->currentPage["parentid"] = 0;
				$this->currentPage["module"] = "custompages";
			}

			$this->currentPage["parentids"] = array();

			// debugVariable($this->currentPage);

			if($_SESSION[CFG_SESSION_KEY]["userlevel"] >= $this->currentPage["userlevel"])
			{
				if($this->currentPage["parentid"] > 0)
				{
					$this->currentPage["parentids"] = Pages::getParentIds($this->currentPage["id"]);
				}
			}
			else
			{
				$message = new SystemMessage();
				$message->setText(LBL_NOTSUFFICIENTPRIVILEGES);
				$message->setType(SystemMessageType::$ERROR);
				$this->addMessage($message);
			}
		}
	}

	function getCurrentPageModule() {
		return $this->currentPage["module"];
	}

	function loadModule($action = "") {
		global $db;

		if($_SESSION[CFG_SESSION_KEY]["userlevel"] >= $this->currentPage["userlevel"])
		{
			if($action == "" || (isset($this->currentPage["sessionexpired"]) && $action != "login" && $action != "logout"))
			{
				$action = "default";
			}

			$moduleLoadStart = microtime(true);

			$module = $this->currentPage["module"];
			$modulePath = $module;

			if($this->currentPage["modulegroup"] != "")
			{
				$modulePath = $this->currentPage["modulegroup"]."/".$module;
			}

			if(file_exists(HA_ROOT_PATH."/modules/".$modulePath."/".$module.".module.php"))
			{
				include(HA_ROOT_PATH."/modules/".$modulePath."/".$module.".module.php");

				if($action != "")
				{
					$loaded = false;

					if($this->userAgent != "")
					{
						// trying to load action page by useragent
						if(!$this->loadModuleAction($modulePath, $action, true))
						{
							// action page for useragent not found loading default page
							SaveLog("Couldn�t load action page for useragent ".$this->userAgent."\n");
							$this->loadModuleAction($modulePath, $action);
						}
					}
					else
					{
						// loading action page
						$this->loadModuleAction($modulePath, $action);
					}
				}
			}
			else
			{
				$file = "/modules/".$modulePath."/".$module.".module.php";

				$message = new SystemMessage();
				$message->setText(str_replace("{file}", $file, LBL_FILENOTFOUND));
				$message->setType(SystemMessageType::$ERROR);

				$this->addMessage($message);
			}

			if($_SESSION[CFG_SESSION_KEY]["userlevel"] > 2)
			{
				$time = number_format((microtime(true) - $moduleLoadStart), 4);

				$message = new SystemMessage();
				$message->setText(str_replace(array("{module}", "{time}"), array($modulePath, $time), LBL_MODULELOADEDIN));
				$message->setAutoClose(true);
				$message->setShowOnlyInDebug(true);

				$this->addMessage($message);
			}
		}
	}

	function loadModuleAction($modulePath, $action, $useUserAgent = false) {
		global $db;

		$loaded = false;

		$userAgent = "";

		if($useUserAgent)
		{
			$userAgent = $this->userAgent.".";
		}

		$userAgentFile = "/modules/".$modulePath."/action.".$userAgent.$action.".php";

		if(file_exists(HA_ROOT_PATH.$userAgentFile))
		{
			include(HA_ROOT_PATH.$userAgentFile);
			$actionResult = "";
			$loaded = true;
		}
		else
		{
			if(!$useUserAgent)
			{
				$message = new SystemMessage();
				$message->setText(str_replace("{file}", $userAgentFile, LBL_FILENOTFOUND));
				$message->setType(SystemMessageType::$ERROR);

				$this->addMessage($message);
			}
		}

		return $loaded;
	}

	function getSystemPlugins() {
		if($systemPlugins === NULL) {
		}
	}

	private function loadSystemPlugins() {
	}
}

?>
