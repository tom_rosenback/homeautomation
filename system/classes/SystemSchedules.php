<?php

class SystemSchedules {
	function __construct() {
	}
	
	public static function get($id = -1) {
		global $db;
		$schedules = array();

		$idClause = "";

		if($id != -1) {
			$idClause = "WHERE s.id = ?";
			$params[] = new dbParam(dbParamType::$INTEGER, "id", $id);
		}

		$query = "SELECT s.id, s.enabled, s.description, s.executable, s.arguments, s.settings 
					FROM systemschedules s 
					".$idClause." 
					ORDER BY s.sort, s.description";
		
		$result = $db->fetch_many($query, $params);
		
		if($result !== false) {
			foreach($result as $s) {
				$s["settings"] = explode(";", $s["settings"]);
				$schedules[] = $s;
			}
		}
		
		return $schedules;
	}
	
	public static function save($params, $id = -1) {
		global $db;
		return $db->save("systemschedules", $params, $id);
	}
	
	public static function delete($id = -1) {
		global $db;
		
		$params = array(
			new dbParam(dbParamType::$INTEGER, "id", $id)
		);
		
		return $db->delete("systemschedules", $params);
	}
}

?>
