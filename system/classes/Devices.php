<?php

class Devices {
	function __construct() {
	}

	public static function get($id = -1, $active = 1, $onlyDimmable = false, $systemPluginName = "") {
		global $db;
		$params = array();
		$devices = array();

		$andWord = "";
		$whereWord = "";
		$idClause = "";
		$activeClause = "";
		$onlyDimmableClause = "";
		$pluginClause = "";

		if($id != -1) {
			$idClause = " d.id = ?";
			$whereWord = " WHERE ";
			$andWord = " AND ";
			$params[] = new dbParam(dbParamType::$INTEGER, "id", $id);
		}

		if($active != -1) {
			$activeClause = $andWord." d.active = ?";
			$andWord = " AND ";
			$whereWord = " WHERE ";
			$params[] = new dbParam(dbParamType::$INTEGER, "active", $active);
		}

		if($onlyDimmable != false) {
			$onlyDimmableClause = $andWord." dt.id = 4"; // abs dimmer
			$andWord = " AND ";
			$whereWord = " WHERE ";
		}

		if($systemPluginName != "") {
			$pluginClause = $andWord." d.systempluginname = ?";
			$andWord = " AND ";
			$whereWord = " WHERE ";
			$params[] = new dbParam(dbParamType::$STRING, "systempluginname", $systemPluginName);
		}

		$query = "SELECT
					d.id, d.systemdeviceid, d.systempluginname, d.description, dt.id AS typeid, dt.type, d.date_added,
					d.active, d.rawdevice, d.rawlearncmd, d.rawoffcmd, d.rawoncmd, d.consumption,
					d.last_state, d.last_dimlevel, UNIX_TIMESTAMP(d.last_updated) AS last_updated
				FROM devices d
				INNER JOIN devicetypes dt ON d.type = dt.id
				".$whereWord.
				$idClause.
				$activeClause.
				$onlyDimmableClause.
				$pluginClause."
				ORDER BY d.sort, d.description";

		$constArray = get_defined_constants(true);
		$constArray = $constArray["user"];

		$result = $db->fetch_many($query, $params);

		if($result !== false) {
			foreach($result as $d) {
				$typeText = $d["type"];
				$translationKey = "LBL_".mb_convert_case($typeText, MB_CASE_UPPER, "UTF-8");

				if(array_key_exists($translationKey, $constArray)) {
					$typeText = $constArray[$translationKey];
				}

				$d["typetext"] = $typeText;

				$status 						= new Status();
				$status->state 					= convertToBoolean($d["last_state"]);
				$status->dimlevel				= $d["last_dimlevel"];
				$status->updated				= $d["last_updated"];

				unset($d["last_state"]);
				unset($d["last_dimlevel"]);
				unset($d["last_updated"]);

				$d["status"] = $status;

				$devices[] = $d;
			}
		}

		return $devices;
	}

	public static function save($params, $id = -1) {
		global $db;
		return $db->save("devices", $params, $id);
	}

	public static function delete($deviceId = -1) {
		global $db;

		$params = array(
			new dbParam(dbParamType::$INTEGER, "id", $deviceId)
		);

		return $db->delete("devices", $params);
	}

	public static function updateStatus($id, $state, $dimlevel) {
		global $db;

		// Update latest status for the device too
		$params = array(
			new dbParam(dbParamType::$INTEGER, "last_state", $state),
			new dbParam(dbParamType::$INTEGER, "last_dimlevel", $dimlevel),
			new dbParam(dbParamType::$STRING, "last_updated", dbParam::now())
		);

		$db->update("devices", $params, $id);

		return $true;
	}

	public static function getTypes($id = -1) {
		global $db;
		$params = array();

		$types = array();

		$idClause = "";

		if($id != -1) {
			$idClause = "WHERE id = ?";
			$params[] = new dbParam(dbParamType::$INTEGER, "id", $id);
		}

		$query = "SELECT id, type, dateadded FROM devicetypes ".$idClause." ORDER BY text";
		$result = $db->fetch_many($query, $params);

		if($result !== false) {
			$constArray = get_defined_constants(true);
			$constArray = $constArray["user"];

			foreach($result as $t) {
				$typeText = $t["type"];
				$translationKey = "LBL_".mb_convert_case($typeText, MB_CASE_UPPER, "UTF-8");

				if(array_key_exists($translationKey, $constArray)) {
					$typeText = $constArray[$translationKey];
				}

				$t["text"] = $typeText;

				$types[] = $t;
			}
		}

		return $types;
	}

	public static function getBySystemDeviceId($id = -1, $systemPluginName = "tdtool") {
		global $db;
		$params = array();

		$device = array();

		$query = "SELECT d.id, d.systemdeviceid, d.systempluginname, d.description, dt.id AS typeid, dt.type, d.date_added, d.active,
							d.rawdevice, d.rawlearncmd, d.rawoffcmd, d.rawoncmd,
							d.consumption,
							d.last_state, d.last_dimlevel, UNIX_TIMESTAMP(d.last_updated) AS last_updated
							FROM devices d
							INNER JOIN devicetypes dt ON d.type = dt.id
							WHERE d.systemdeviceid = ?
							AND d.systempluginname = ?";

		$params[] = new dbParam(dbParamType::$STRING, "systemdeviceid", $id);
		$params[] = new dbParam(dbParamType::$STRING, "systempluginname", $systemPluginName);

		if($result = $db->fetch($query, $params)) {
			$constArray = get_defined_constants(true);
			$constArray = $constArray["user"];

			$typeText = $result["type"];
			$translationKey = "LBL_".mb_convert_case($typeText, MB_CASE_UPPER, "UTF-8");

			if(array_key_exists($translationKey, $constArray)) {
				$typeText = $constArray[$translationKey];
			}

			$device = $result;
			$device["typetext"]	= $typeText;

			$status 								= new Status();
			$status->state 					= convertToBoolean($device["last_state"]);
			$status->dimlevel				= $device["last_dimlevel"];
			$status->updated				= $device["last_updated"];

			unset($device["last_state"]);
			unset($device["last_dimlevel"]);
			unset($device["last_updated"]);

			$device["status"] = $status;
		}

		return $device;
	}

	public static function getConsumption($deviceId = -1) {
		global $db;
		$params = array();

		$consumption = array();

		$query = "SELECT e1.device_id AS deviceid, e1.timestamp, # AS 'on',
					(SELECT e2.timestamp FROM events e2 WHERE e2.device_id = e1.device_id AND e2.timestamp > e1.timestamp AND e2.status = 0 ORDER BY e2.device_id, e2.timestamp LIMIT 1) AS 'off',
					ROUND((TIMESTAMPDIFF(MINUTE, e1.timestamp, (SELECT e2.timestamp FROM events e2 WHERE e2.device_id = e1.device_id AND e2.timestamp > e1.timestamp AND e2.status = 0 ORDER BY e2.device_id, e2.timestamp LIMIT 1)) / 60 * d.consumption / 1000), 2) AS 'ontime'
					FROM events e1
					INNER JOIN devices d ON e1.device_id = d.id
					WHERE e1.status = 1 AND d.consumption > 0";

		if($deviceId != -1) {
			$query .= " AND d.id = ?";
			$params[] = new dbParam(dbParamType::$INTEGER, "id", $deviceId);
		}

		$query .= " ORDER BY e1.device_id, e1.timestamp";

		$result = $db->fetch_many($query, $params);

		if($result !== false) {
			$consumption = $result;
		}

		return $consumption;
	}

	// calculates the current estimated consumption and returns it in Wh
	public static function getCurrentConsumption() {
		global $db;
		$value = "-";

		$query = "SELECT SUM(IFNULL((SELECT d2.consumption
					FROM events e
					INNER JOIN devices d2 ON e.device_id = d2.id
					WHERE e.device_id = d.id AND e.status = 1
					ORDER BY e.timestamp DESC LIMIT 1), 0)) AS current
					FROM devices d";

		$result = $db->fetch($query);

		if($result !== false) {
			$value = $result["current"];
		}

		return $value;
	}
}

?>
