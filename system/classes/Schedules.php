<?php

class Schedules {
	function __construct() {
	}

	public static function get($scheduleId = -1) {
		global $db;
		$params = array();

		$schedules = array();
		$whereClause = "";

		if($scheduleId != -1) {
			$whereClause = " WHERE s.id = ? ";
			$params[] = new dbParam(dbParamType::$INTEGER, "id", $scheduleId);
		}

		$query =	"SELECT s.id, s.name, s.comment, s.devices, s.devicegroups, s.macros, s.days, s.scenario, sc.name as scenarioname, s.enabled, s.dateadded,
							(SELECT COUNT(*) FROM ".$db->tablePrefix."scheduleactivations sa WHERE sa.scheduleid = s.id) AS numactivations, s.sort
							FROM ".$db->tablePrefix."schedules s
							LEFT OUTER JOIN ".$db->tablePrefix."scenarios sc ON s.scenario = sc.id
							".$whereClause."
							ORDER BY s.enabled DESC, s.sort, s.scenario, s.name, s.devices, s.days";

		$result = $db->fetch_many($query, $params);

		if($result !== false) {
			$schedules = $result;
		}

		return $schedules;
	}

	public static function save($params, $id = -1) {
		global $db;
		return $db->save("schedules", $params, $id);
	}


	public static function delete($id = -1) {
		global $db;

		$params = array(
			new dbParam(dbParamType::$INTEGER, "scheduleid", $id)
		);

		$result = $db->delete("scheduleactivations", $params);

		$params = array(
			new dbParam(dbParamType::$INTEGER, "id", $id)
		);

		return $result && $db->delete("schedules", $params);
	}

	public static function getUpcomingActivations($limit = -1, $timelimit = "23:00", $dst = "00:00") {
		global $db;

		$activations = array();

		$selectedScenario = Settings::get("selectedscenario");

		$query = "SELECT sa.id, name, params, upcomingruntime AS sorttime, DATE_FORMAT(upcomingruntime, '%H:%i') AS upcomingruntime, DATEDIFF(upcomingruntime, NOW()) AS another_day
					FROM ".$db->tablePrefix."scheduleactivations sa
					INNER JOIN ".$db->tablePrefix."schedules ON sa.scheduleid = schedules.id
					WHERE enabled = 1 AND upcomingruntime IS NOT NULL AND upcomingruntime > NOW() AND TIMESTAMPDIFF(HOUR, now(), upcomingruntime) < 24
					AND (scenario = -100 OR scenario = ?) AND TYPE <> 'dynamic'
					UNION
					SELECT sa.id, name, params, nextdaysruntime, DATE_FORMAT(nextdaysruntime, '%H:%i'), DATEDIFF(nextdaysruntime, now()) AS another_day
					FROM ".$db->tablePrefix."scheduleactivations sa
					INNER JOIN ".$db->tablePrefix."schedules ON sa.scheduleid = schedules.id
					WHERE enabled = 1 AND nextdaysruntime IS NOT NULL AND TIMESTAMPDIFF(HOUR, now(), nextdaysruntime) < 24
					AND (scenario = -100 OR scenario = ?) AND TYPE <> 'dynamic'
					ORDER BY sorttime LIMIT ?";

		$params = array(
			new dbParam(dbParamType::$INTEGER, "scenario", $selectedScenario),
			new dbParam(dbParamType::$INTEGER, "scenario", $selectedScenario),
			new dbParam(dbParamType::$INTEGER, "limit", $limit)
		);

		$result = $db->fetch_many($query, $params);

		if($result !== false) {
			foreach ($result as $a) {
				$a["action"] = substr($a["params"], 0, 1);
				$activations[] = $a;
			}
		}

		return $activations;
	}

	public static function getActivations($scheduleId = -1, $activationId = -1, $type = "", $inputDeviceOrSensor = "") {
		global $db;
		$params = array();

		$activations = array();

		$whereClause = "";
		$andWord = "";

        if($inputDeviceOrSensor != "") {
            $whereClause .= "params like ?";
						$params[] = new dbParam(dbParamType::$STRING, "params", "_;".$inputDeviceOrSensor.";%");
            $andWord = " AND ";
        }

		if($scheduleId > 0) {
			$whereClause .= $andWord."sa.scheduleid = ?";
			$andWord = " AND ";
			$params[] = new dbParam(dbParamType::$INTEGER, "scheduleid", $scheduleId);
		}

		if($activationId > 0) {
			$whereClause .= $andWord."sa.id = ?";
			$andWord = " AND ";
			$params[] = new dbParam(dbParamType::$INTEGER, "id", $activationId);
		}

		if($type != "") {
			$whereClause .= $andWord."sa.type = ?";
			$params[] = new dbParam(dbParamType::$STRING, "type", $type);
		}

		if(strlen($whereClause) > 0) {
			$whereClause = "WHERE ".$whereClause." ";
		}

		$query =	"SELECT sa.id, sa.type, sa.scheduleid, sa.params, sa.upcomingruntime, sa.timestamp AS dateadded, sa.sort, sa.linuxatid
						FROM ".$db->tablePrefix."scheduleactivations sa
						".$whereClause."
						ORDER BY sa.sort";

		$result = $db->fetch_many($query, $params);

		if($result !== false) {
			foreach ($result as $a) {
				$a["params"] = generateActivationParamArray($a["type"], $a["params"]);
				$activations[] = $a;
			}
		}

		return $activations;
	}

	public static function saveActivation($params = array(), $activationId = -1) {
		global $db;
		return $db->save("scheduleactivations", $params, $activationId);
	}

	public static function deleteActivation($activationId) {
		global $db;

		$params = array(
			new dbParam(dbParamType::$INTEGER, "id", $activationId)
		);

		$db->delete("scheduleactivations", $params);
		return true;
	}

	public static function copyActivations($srcScheduleId, $destScheduleId) {
		global $db;
		$query =	"SELECT sa.type, sa.params, sa.upcomingruntime, sa.sort ".
								"FROM ".$db->tablePrefix."scheduleactivations sa ".
								"WHERE sa.scheduleid = ? ".
								"ORDER BY sa.sort";

		$params = array(
			new dbParam(dbParamType::$INTEGER, "scheduleid", $srcScheduleId)
		);

		$result = $db->fetch_many($query, $params);

		if($result !== false) {
			foreach ($result as $srcAct) {
				$params = array(
					new dbParam(dbParamType::$STRING, "type", $srcAct["type"]),
					new dbParam(dbParamType::$INTEGER, "scheduleid", $destScheduleId),
					new dbParam(dbParamType::$STRING, "params", $srcAct["params"]),
					new dbParam(dbParamType::$STRING, "upcomingruntime", $srcAct["upcomingruntime"]),
					new dbParam(dbParamType::$STRING, "sort", $srcAct["sort"])
				);

				$db->insert("scheduleactivations", $params);
			}
		}

		return true;
	}

	public static function updateActivationsUpcomingRuntime($activationid) {
		global $db;
		$query = "UPDATE scheduleactivations
					SET lastruntime = upcomingruntime, upcomingruntime = nextdaysruntime
					WHERE id = ?";
		$params = array(
			new dbParam(dbParamType::$INTEGER, "id", $activationid)
		);

		$db->execute($query, $params);

		return $true;
	}

	public static function updateRealActivationTimes($scheduleId = -1) {
		global $db;
		$params = array();

		$query = "SELECT sa.id id, sa.type type, sa.params params, s.days days
		FROM scheduleactivations sa
		INNER JOIN schedules s on sa.scheduleid = s.id";

		if($scheduleId != -1) {
			$query .= " AND s.id = ?";
			$params[] = new dbParam(dbParamType::$INTEGER, "id", $scheduleId);
		}

		$result = $db->fetch_many($query, $params);

		if($result !== false) {
			foreach ($result as $a) {
				$activationParams = generateActivationParamArray($a["type"], $a["params"]);
				$nextDaysRunTime = calculateNextRunTime($a["days"], $a["type"], $activationParams, $nextRunDate, false);

				if($nextDaysRunTime != "") { // maybe could be left out
					$params = array(
						new dbParam(dbParamType::$STRING, "nextdaysruntime", $nextDaysRunTime)
					);

					$db->save("scheduleactivations", $params, $a["id"]);
				}
			}
		}

		$query = "SELECT sa.id AS said, sa.sort AS sort, sa.nextdaysruntime AS ndrt, sa.params AS params, s.id AS sid
							FROM scheduleactivations sa
							INNER JOIN schedules s on sa.scheduleid = s.id
							WHERE sa.type != 'dynamic'";

		$params = array();

		if($scheduleId != -1) {
			$query .= " AND s.id = ?";
			$params = array(
				new dbParam(dbParamType::$INTEGER, "id", $scheduleId)
			);
		}

		$result = $db->fetch_many($query, $params);

		if($result !== false) {
			$inactiveParams = array();

			foreach ($result as $a) {
				$type = substr($a["params"], 0, 1);

				if($type == 1) {
					$type = 0;
				} else {
					$type = 1;
				}

				$sort = $a["sort"];
				$time = $a["ndrt"];
				$query = "SELECT COUNT(*) AS overlaps
							FROM scheduleactivations
							WHERE scheduleid = ? AND type != 'dynamic' AND
							((sort > ? AND nextdaysruntime < ? AND substring(params, 1, 1) = ?) OR
							(sort < ? AND nextdaysruntime > ? AND substring(params, 1, 1) = ?));";

				$params = array(
					new dbParam(dbParamType::$INTEGER, "scheduleid", $a["sid"]),
					new dbParam(dbParamType::$INTEGER, "sort", $sort),
					new dbParam(dbParamType::$STRING, "nextdaysruntime", $time),
					new dbParam(dbParamType::$STRING, "type", $type),
					new dbParam(dbParamType::$INTEGER, "sort", $sort),
					new dbParam(dbParamType::$STRING, "nextdaysruntime", $time),
					new dbParam(dbParamType::$STRING, "type", $type)
				);

				$checkforoverlaps = $db->fetch($query, $params);

				if($checkforoverlaps !== false && $checkforoverlaps["overlaps"] > 0) {
					$inactiveParams[] = new dbParam(dbParamType::$INTEGER, "id", $a["said"]);
				}
			}

			if(sizeof($inactiveParams) > 0) {
				$paramPlaceholders = implode(',', array_fill(0, count($inactiveParams), '?'));
				$query = "UPDATE scheduleactivations SET nextdaysruntime = NULL WHERE ID IN (".$paramPlaceholders.");";
				$db->execute($query, $inactiveParams);
			}
		}

		$query = "UPDATE scheduleactivations SET upcomingruntime = nextdaysruntime
							WHERE upcomingruntime < now() OR upcomingruntime > nextdaysruntime OR upcomingruntime IS NULL OR YEAR(upcomingruntime) = '0000';";
		$db->execute($query);
	}

	//Returns 0 if previous non dynamic activation hasn't run yet, 1 if it has
	public static function hasPreviousActivationRun($activationId) {
		global $db;

		$hasRun = 0;
		$numActivations = 0;

		$query = "SELECT IF(DATE(lastruntime) = CURDATE(), 1, 0) AS hasrun, IF(DATE(upcomingruntime) = CURDATE(), 1, 0) AS runtoday
					FROM scheduleactivations
					WHERE sort < (SELECT sort FROM scheduleactivations WHERE id = ?)
					AND scheduleid = (SELECT scheduleid FROM scheduleactivations WHERE id = ?)
					AND type <> 'dynamic';";

		$params = array(
			new dbParam(dbParamType::$INTEGER, "id", $activationId),
			new dbParam(dbParamType::$INTEGER, "id", $activationId)
		);

		$result = $db->fetch_many($query, $params);

		if($result !== false) {
			foreach($result as $a) {
				if($a["hasrun"] == "1") {
					// the activation has already run today
					$hasRun++;
				}

				$numActivations++;
			}
		}

		if($hasRun == $numActivations) {
			$hasRun = "1";
		} else {
			$hasRun = "0";
		}

		return $hasRun;
	}

	//Returns 0 if next non dynamic activation hasn't run yet, 1 if it has
	public static function hasNextActivationRun($activationId) {
		global $db;

		$hasRun = 0;
		$runOtherToday = 0;
		$numActivations = 0;

		$query = "SELECT IF(DATE(lastruntime) = CURDATE(), 1, 0) AS hasrun, IF(DATE(upcomingruntime) = CURDATE(), 1, 0) AS runtoday
					FROM scheduleactivations
					WHERE sort > (SELECT sort FROM scheduleactivations WHERE id = ?)
					AND scheduleid = (SELECT scheduleid FROM scheduleactivations WHERE id = ?)
					AND type <> 'dynamic';";

		$params = array(
			new dbParam(dbParamType::$INTEGER, "id", $activationId),
			new dbParam(dbParamType::$INTEGER, "id", $activationId)
		);

		$result = $db->fetch_many($query, $params);

		if($result !== false) {
			foreach($result as $a) {
				if($a["hasrun"] == "1" || ($a["runtoday"] == "0" && $a["hasrun"] == "0")) {
					// the activation has already run today OR shouldn�t run at all today => simulate that the activation has run
					$hasRun++;
				}

				$numActivations++;
			}

			if($hasRun > 0) {
				$hasRun = "1";
			} else {
				$hasRun = "0";
			}
		}

		return $hasRun;
	}

	public static function isActivationTriggered($activationId) {
		global $db;
		$triggered = false;

		$query = "SELECT triggered FROM scheduleactivations WHERE id = ?";
		$params = array(
			new dbParam(dbParamType::$INTEGER, "id", $activationId)
		);

		$result = $db->fetch($query, $params);

		if($result !== false) {
			$triggered = convertToBoolean($result["triggered"]);
		}

		return $triggered;
	}

	// Update schedules which used a previously deleted scenario
	public static function resetScenarios($deletedScenarioId) {
		global $db;
		$query = "UPDATE schedules SET scenario = -100 WHERE scenario = ?";
		$params = array(new dbParam(dbParamType::$INTEGER, "scenario", $deletedScenarioId));
		$db->execute($query, $params);
		return true;
	}
}

?>
