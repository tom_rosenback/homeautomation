<?php

class Device {
	var $id;
	var $systemPluginName;
	var $systemDeviceId;
	var $description;
	var $typeId;
	var $type;
	var $typeText;
	var $dateAdded;
	var $active;
	var $rawDevice;
	var $rawLearnCmd;
	var $rawOffCmd;
	var $rawOnCmd;
	var $consumption;

	function __construct() {
		$this->id = -1;
		$this->systemPluginName = "";
		$this->systemDeviceId = -1;
	}
}

?>
