<?php

class Pages {
	function __construct() {
	}
	
	public static function get($parentId = -1, $id = -1, $forMenu = false, $isMobileDevice = false) {
		global $db;
		$params = array();
		
		$pages = array();
		$whereClause = "";
		$andWord = "";

		if($id != -1) {
			$whereClause .= $andWord." id = ?";
			$andWord = " AND ";
			$params[] = new dbParam(dbParamType::$INTEGER, "id", $id);
		}

		if($parentId != 0) {
			$whereClause .= $andWord." parentid = ?";
			$andWord = " AND ";
			$params[] = new dbParam(dbParamType::$INTEGER, "parentid", $parentId);
		}

		if($forMenu) {
			$whereClause .= $andWord." userlevel <= ?";
			$andWord = " AND ";
			$params[] = new dbParam(dbParamType::$INTEGER, "userlevel", $_SESSION[CFG_SESSION_KEY]["userlevel"]);

			if($isMobileDevice) {
				$whereClause .= $andWord." isavailableformobile = 1";
				$andWord = " AND ";
			} else {
				$whereClause .= $andWord." isavailablefordesktop = 1";
				$andWord = " AND ";
			}
		}

		$query = "SELECT id, name, translation, modulegroup, module, menuaction, userlevel, parentid, isavailablefordesktop, isavailableformobile 
					FROM pages WHERE 
					".$whereClause." 
					ORDER BY sort";
					
		$result = $db->fetch_many($query, $params);

		if($result !== false) {
			$constArray = get_defined_constants(true);
			$constArray = $constArray["user"];
			
			foreach($result as $p) {
				$p["translation"] = $constArray[$p["translation"]];
				$pages[] = $p;
			}
		}
		
		return $pages;
	}

	public static function getByName($pageName) {
		global $db;
		$params = array();
		$page = array();

		if($pageName != "") {
			$query = "SELECT id, name, translation, modulegroup, module, menuaction, userlevel, parentid, isavailablefordesktop, isavailableformobile 
					FROM pages
					WHERE name = ?  
					ORDER BY sort";
			
			$params[] = new dbParam(dbParamType::$STRING, "name", $pageName);
			$result = $db->fetch($query, $params);
			
			if($result !== false) {
				$constArray = get_defined_constants(true);
				$constArray = $constArray["user"];

				$result["translation"] = $constArray[$result["translation"]];
				$page = $result;
			}
		}

		return $page;
	}

	public static function getForSettings($parentId = -1, $prefix = " ", $pages = array(), $exludedModules = "('login', 'help')") {
		global $db;
		$params = array();
		
		$pages = array();

		$query = "SELECT id, name, translation 
					FROM pages
					WHERE parentid = ?
					AND module NOT IN ".$exludedModules." 
					ORDER BY sort";
					
		$params = array(
			new dbParam(dbParamType::$INTEGER, "parentid", $parentId)
		);
		
		$result = $db->fetch_many($query, $params);
		
		if($result !== false)  {		
			$constArray = get_defined_constants(true);
			$constArray = $constArray["user"];

			foreach($result as $p) {
				$p["translation"] = $prefix.$constArray[$p["translation"]];
				$pages[] = $p;
				$subPages = Pages::getForSettings($p["id"], ("-".$prefix), $pages);
				
				if(count($subPages) > 0) {
					$pages = array_merge($pages, $subPages);
				}
			}
		}

		return $pages;
	}

	public static function getParentIds($pageId, $parentIds = array()) {
		global $db;
		$params = array(
			new dbParam(dbParamType::$INTEGER, "id", $pageId)
		);
		
		$parentId = -1;
		
		$query = "SELECT parentid FROM pages WHERE id = ?";
		$result = $db->fetch($query, $params);
		
		if($result !== false) {
			$parentId = $result["parentid"];
	
			if($parentId > 0) {
				$parentIds[] = $parentId;
				$parentIds = Pages::getParentIds($parentId, $parentIds);
			}
		}

		return $parentIds;
	}
}

?>
