
<?php

class Versions {
	function __construct() {
	}

	public static function updateChecks($name, $version) {
		global $db;

		$query = "INSERT INTO versionchecks (name, version) VALUES (?, ?)
					ON DUPLICATE KEY UPDATE dismissed = IF(version < ?, 0, dismissed), version = ?";

		$params = array(
			new dbParam(dbParamType::$STRING, "name", $name),
			new dbParam(dbParamType::$STRING, "version", $version),
			new dbParam(dbParamType::$STRING, "version", $version),
			new dbParam(dbParamType::$STRING, "version", $version)
		);
		
		$db->execute($query, $params);

		return true;
	}

	public static function dismiss($name = "") {
			global $db;
			$query = "UPDATE versionchecks SET dismissed = 1";

			if($name != "")	{
				$query .= " WHERE name = ?";
				$params[] = new dbParam(dbParamType::$STRING, "name", $name);
			}

			$db->execute($query, $params);
			return true;
	}

	public static function getUndismissed() {
		global $db;
		$params = array();

		$versions = array();
		$query = "SELECT name, version FROM versionchecks WHERE dismissed=0";
		$result = $db->fetch_many($query);

		if($result !== false) {
			$versions = $result;
		}

		return $versions;
	}
}

?>
