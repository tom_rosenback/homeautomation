<?php

class DeviceGroups {
	function __construct() {
	}

	public static function get($id = -1) {
		global $db;
		$params = array();

		$groups = array();
		$idClause = "";

		if($id != -1) {
			$idClause = "WHERE id = ?";
			$params[] = new dbParam(dbParamType::$INTEGER, "id", $id);
		}

		$query = "SELECT id, name, dateadded FROM devicegroups ".$idClause." ORDER BY sort, name";
		$result = $db->fetch_many($query, $params);

		if($result !== false) {
			foreach ($result as $g) {
				$g["relations"] = DeviceGroups::getRelations($g["id"]);
				$groups[] = $g;
			}
		}

		return $groups;
	}
	
	public static function save($params, $id = -1) {
		global $db;
		return $db->save("devicegroups", $params, $id);
	}
	
	public static function delete($groupId = -1) {
		global $db;
		
		$params = array(
			new dbParam(dbParamType::$INTEGER, "id", $groupId)
		);
		
		return $db->delete("devicegroups", $params);
	}

	public static function getRelations($groupId) {
		global $db;
		$params = array();
		$relations = array();

		if($groupId != "") {
			$query = "SELECT dgr.id, dgr.groupid, dgr.deviceid, d.description AS devicedesc, dgr.dateadded ".
								"FROM devicegrouprelations dgr ".
								"LEFT OUTER JOIN devices d ON d.id = dgr.deviceid ".
								"WHERE dgr.groupid = ? AND d.active=1 ORDER BY dgr.deviceid";

			$params[] = new dbParam(dbParamType::$INTEGER, "groupid", $groupId);
			$result = $db->fetch_many($query, $params);

			if($result !== false) {
				foreach ($result as $r) {
					$relations[] = $r;
				}
			}

			return $relations;
		}
	}
	
	public static function saveRelation($params, $groupId = -1) {
		global $db;
		return $db->save("devicegrouprelations", $params, $groupId);
	}
		
	public static function deleteRelations($groupId = -1) {
		global $db;
		
		$params = array(
			new dbParam(dbParamType::$INTEGER, "groupid", $groupId)
		);
		
		return $db->delete("devicegrouprelations", $params);
	}
}

?>
