<?php

class Settings {
	function __construct() {
	}

	public static function get($name = "", $forSession = false) {
		global $db;
		$settings = false;

		$params = array();
		$query = "SELECT name, value, type, label, grouping FROM ".$db->tablePrefix."settings WHERE";

		if($name != "") {
			$query .= " name = ?";
			$params[] = new dbParam(dbParamType::$STRING, "name", $name);
		} else {
			$query .= " sort <> -1";
		}

		$query .= " ORDER BY sort, grouping";
		$result = $db->fetch_many($query, $params);

		if($result !== false) {
			$settings = array();

			foreach($result as $s) {
				if($forSession) {
					$settings[$s["name"]] = $s["value"];
				} else {
					$s["label"]						= "LBL_SETTINGS_LABELS_".mb_convert_case($s["label"], MB_CASE_UPPER, "UTF-8");
					$s["grouping"]				= "LBL_SETTINGS_GROUPS_".mb_convert_case($s["grouping"], MB_CASE_UPPER, "UTF-8");
					$settings[$s["name"]] = $s;
				}
			}
		}

		if(count($settings) >= 0 && $name != "") {
			$settings = $settings[$name]["value"];
		}

		return $settings;
	}

	public static function update($name, $value, $dbTable = "settings") {
		if($name != "" && (($dbTable == "settings" && Settings::get($name) != -1) || ($dbTable == "gcalsettings" && count(GoogleCalendar::getSettings($name)) > 0))) {
			global $db;
			$query = "UPDATE ".$dbTable." SET value = ? WHERE name = ?";
			$params = array(
				new dbParam(dbParamType::$STRING, "value", $value),
				new dbParam(dbParamType::$STRING, "name", $name)
			);

			$db->execute($query, $params);
		}

		return true;
	}
}

?>
