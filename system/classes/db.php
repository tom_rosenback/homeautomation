<?php
// HomeAutomation
// Copyright (C) 2021 Tom Rosenback (tom.rosenback@gmail.com), Daniel Malmgren (daniel.malmgren@kolefors.se)
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

class db {
	protected static $connection = false;

	private $host = "localhost";
	private $user = "";
	private $password = "";
	private $database = "";
	private $port = 3306;
	private $credentialsSet = false;

	public $tablePrefix = "";
	public $lastError = "";
	public $lastQuery = "";

	function __construct() {

	}

	function setCredentials($host, $user, $password, $database, $port = 3306, $tablePrefix = "") {
		$this->host = $host;
		$this->user = $user;
		$this->password = $password;
		$this->database = $database;
		$this->port = $port;
		$this->tablePrefix = $tablePrefix;
		$this->credentialsSet = true;
	}

	function __desctruct() {
		if(self::$connection !== false) {
			self::$connection->close();
			self::$connection = false;
		}
	}

	public function connect($test = false) {
		if($this->credentialsSet && (!isset(self::$connection) || self::$connection === false)) {
			self::$connection = @new mysqli($this->host, $this->user, $this->password, $this->database, $this->port);

			if(self::$connection->connect_errno) {
				self::$connection = false;
				if(!$test) {
					die ($this->error());
				} else {
					$this->error("");
					self::$connection = false;
				}
			} else {
				self::$connection->set_charset("utf8");
			}
		}

		return self::$connection;
	}

	public function disconnect() {
		self::__desctruct();
	}

	private function error($query = "", $params = array()) {
		$str = "Oops, think we broke something while communicating with the database.<br />";

		if(self::$connection->connect_errno) {
			$str .= "Please check your configuration.<br /><br />";
			$this->lastError = "#".self::$connection->connect_errno.": ".self::$connection->connect_error;
		} else {
			$this->lastError = self::$connection->error;
		}

		$str .= "ERROR: ".$this->lastError."<br/ ><br />";

		if($query != "") {
			$this->lastQuery = $query;
			$str .= "Query: ".$query."<br /><br/ >";
		}

		if(is_array($params)) {
			$str .= "Params: <br /><pre>";
			$str .= print_r($params, true);
			$str .= "</pre><br /><br/ >";
		}

		$str .= "Please correct the problem and try again. <br /><br />Hint: If you get an error stating that you are missing a table and you have verified that your mysql_settings.php file is correct, delete the mysql_settings.php and put the install folder back and do the installation again.";

		self::$connection = false;

		return $str;
	}

	private function prepare($query, $params = array()) {
		$statement = false;

		$statement = self::$connection->prepare($query);

		if($statement !== false) {
			$types = "";
			$values = array();

			if(is_array($params) && count($params) > 0) {
				foreach($params as $p) {
					$types .= $p->type;
					$values[] = &$p->value;
				}

				call_user_func_array(array($statement, 'bind_param'), array_merge(array($types), $values));
			}

			if(is_array($params) && count($params) != substr_count($query, "?")) {
				die($query);
			}
		}

		return $statement;
	}

	private function release($resource) {
		$resource->close();
	}

	public function executeFetchResult($query, $params = array()) {
		return $this->execute($query, $params, true);
	}

	public function execute($query, $params = array(), $getResult = false) {
		$result = false;

		$start = microtime(true);

		if($this->connect()) {
			$stmt = $this->prepare($query, $params);

			if($stmt !== false) {
				$stmt->execute();

				if($getResult) {
					$result = $stmt->get_result();
				} else {
					$result = $stmt->insert_id;
				}

				$this->release($stmt);
			} else {
				die($this->error($query, $params));
			}
		}

		$execTime = number_format(microtime(true) - $start, 4);
		if($execTime > SLOW_QUERY_THRESHOLD) { // log only if query takes longer than SLOW_QUERY_THRESHOLD sec
			SaveLog("Exec time: ".($execTime)." sec, query:\n".$query."\n", (defined("CFG_FORCE_DEBUG_SLOW_QUERIES") && CFG_FORCE_DEBUG_SLOW_QUERIES), "db-slowquery");
		}

		return $result;
	}

	public function insert($table, $params = array()) {
		$result = false;

		if($this->connect() && $table != "" && count($params) > 0) {
			$fields = array();
			$values = array();

			foreach($params as $param) {
				$fields[] = $param->field;
				$values[] = "?";
			}

			$query = "INSERT INTO ".$this->tablePrefix.$table." (".implode(",", $fields).") VALUES (".implode(",", $values).")";
			$result = $this->execute($query, $params);
		}

		return $result;
	}

	public function update($table, $params = array(), $id, $additionalWhere = "") {
		$result = false;

		if($this->connect() && $id > 0 && $table != "" && count($params) > 0) {
			$query = "UPDATE ".$this->tablePrefix.$table." SET ";
			$sep = "";

			foreach($params as $param) {
				$query .= $sep.$param->field." = ? ";
				$sep = ", ";
			}

			if($additionalWhere != "") {
				$query .= $additionalWhere;
			}

			$query .= " WHERE id = ?";
			$params[] = new dbParam(dbParamType::$INTEGER, "id", $id);

			$this->execute($query, $params);
			$result = $id;
		}

		return $result;
	}


	public function save($table = "", $params = array(), $id = -1, $additionalWhere = "") {
		$result = -1;

		if($this->connect() && $id >= -1 && $table != "" && count($params) > 0) {
			if($id > 0) {
				$result = $this->update($table, $params, $id, $additionalWhere);
			} else {
				$result = $this->insert($table, $params);
			}
		}

		return $result;
	}

	public function delete($table = "", $params = array()) {
		if($this->connect() && $table != "" && count($params) > 0) {
			$query = "DELETE FROM ".$this->tablePrefix.$table." WHERE ";
			$sep = "";

			foreach($params as $param) {
				$query .= $sep.$param->field." = ? ";
				$sep = " AND ";
			}

			$this->execute($query, $params);
		}

		return true;
	}

	/* Prepared query to fetch single objects */
	public function fetch($query, $params = array()) {
		$result = false;

		if($res = $this->executeFetchResult($query, $params, true)) {
			$result = $res->fetch_assoc();
			$this->release($res);
		}
		// else {
		// 	die($this->error($query, $params));
		// }

		return $result;
	}

	/* Prepared query to fetch many objects */
	public function fetch_many($query, $params = array()) {
		$result = false;

		if($res = $this->executeFetchResult($query, $params, true)) {
			$result = $res->fetch_all(MYSQLI_ASSOC);
			$this->release($res);
		}
		// else {
		// 	die($this->error($query, $params));
		// }

		return $result;
	}

	/* Custom NONprepared query, preferrably do NOT use this due to that it has no injection protection */
	public function query($query, $getResult = false, $many = true) {
		$start = microtime(true);
		$result = false;

		if($this->connect()) {
			$res = self::$connection->query($query) or die($this->error($query));

			if($getResult) {
				if($many) {
					$result = $res->fetch_all(MYSQLI_ASSOC);
				} else {
					$result = $res->fetch_assoc();
				}
			} else {
				$result = true;
			}

			$this->release($res);
		}

		return $result;
	}

	public function truncate($table = "") {
		if($table != "") {
			$query = "TRUNCATE ".$this->tablePrefix.$table;
			$this->execute($query);
		}
	}

	public static function testConnection($mysqlHost, $mysqlDatabase, $mysqlUsername = "", $mysqlPassword = "", $port = 3306) {
		$connection = @new mysqli($mysqlHost, $mysqlUsername, $mysqlPassword, $mysqlDatabase, $port);

		$success = false;

		if(!$connection->connect_errno) {
			$success = true;
		}

		return $success;
	}

	public function importDumpFile($fileWithRelativePath) {
		$dumpFile = file_get_contents($fileWithRelativePath);

		if($dumpFile !== false) {
			$find = array("/;\r/", "/;\n\r/");
			$replaceWith = ";\n";

			$dumpFile = preg_replace($find, $replaceWith, $dumpFile);
			$dumpQueries = explode(";\n", $dumpFile);

			// echo count($dumpQueries);

			foreach($dumpQueries as $query) {
				$query = trim($query);

				if($query != "") {
					$this->execute($query);
				}
			}
		}
	}
}

// $db = new db();
?>
