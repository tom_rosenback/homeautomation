<?php
// HomeAutomation
// Copyright (C) 2021 Tom Rosenback (tom.rosenback@gmail.com), Daniel Malmgren (daniel.malmgren@kolefors.se)
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.


class BaseSystemPlugin {
	public $installedVersion;
	public $dateInstalled;
	public $settings;
	public $hasPluginPage;
	public $translation;

	function __construct() {
		$this->installedVersion = -1;
		$this->dateInstalled	= "";
		$this->settings		 	= null;
		$this->hasSettings	 	= false;
		$this->hasPluginPage 	= false;
		$this->translation 		= null;
	}

	private function loadTranslations() {
		$translation = array();
		$pluginsDir = HA_ROOT_PATH."/system/systemplugins/";

		if(file_exists($pluginsDir."/".$this->getPluginName()."/lang.en.php")) {
			include($pluginsDir."/".$this->getPluginName()."/lang.en.php");
		}

		if(file_exists($pluginsDir."/".$this->getPluginName()."/lang.".$_SESSION[CFG_SESSION_KEY]["language"].".php")) {
			include($pluginsDir."/".$this->getPluginName()."/lang.".$_SESSION[CFG_SESSION_KEY]["language"].".php");
		}

		$this->translation = $translation;
	}

	function getTranslation($label) {
		if(!is_array($this->translation)) {
			$this->loadTranslations();
		}

		if($label != "" && array_key_exists($label, $this->translation)) {
			$label = $this->translation[$label];
		}

		return $label;
	}

	private function loadSettings() {
		$this->settings = SystemPlugins::getSettings($this->getPluginName());

		if(count($this->settings) > 0) {
			$this->hasSettings = true;
		} else {
			$this->hasSettings = false;
		}
	}

	// returns settings array if key is given, else returns value of key
	function getSettings($key = "") {
		if(!is_array($this->settings)) {
			$this->loadSettings();
		}

		if($key != "") {
			if(is_array($this->settings) && array_key_exists($key, $this->settings)) {
				return $this->settings[$key]["value"];
			} else {
				return "";
			}
		} else {
			return $this->settings;
		}
	}

	function updateVersion() {
		SystemPlugins::saveVersion($this->getPluginName(), $this->getVersion());
	}

	function export() {
		$this->exportPlugin();
	}

	/* methods above this should not be modified in system plugins and below ones should be overridden. */

	function getPluginName() {
		SaveLog($this->getDisplayName().": getPluginName method not implemented.", true);
		return "";
	}

	function getDisplayName() {
		SaveLog($this->getDisplayName().": getDisplayName method not implemented.", true);
		return "";
	}

	function getAuthor() {
		SaveLog($this->getDisplayName().": getAuthor method not implemented.", true);
		return "";
	}

	function getAuthorEmail() {
		SaveLog($this->getDisplayName().": getAuthorEmail method not implemented.", true);
		return "";
	}

	function getVersion() {
		SaveLog($this->getDisplayName().": getVersion method not implemented.", true);
		return "";
	}

	function install() {
		// plugin specific installation method
		SaveLog($this->getDisplayName().": install method not implemented.", true);
	}

	function uninstall() {
		// plugin specific uninstallation method
		SaveLog($this->getDisplayName().": uninstall method not implemented.", true);
	}

	function upgrade() {
		// plugin specific upgrade method
		SaveLog($this->getDisplayName().": upgrade method not implemented.", true);
	}

	function toggleDevices($devicesToToggle, $statuses) {
		SaveLog($this->getDisplayName().": toggleDevices method not implemented.", true);
		return array();
	}

	function postToggleDevices($devicesToggled, $statuses) {
		SaveLog($this->getDisplayName().": postToggleDevices method not implemented.", true);
		return array();
	}

	function importDevices() {
		SaveLog($this->getDisplayName().": importDevices method not implemented.", true);
	}

	function readStatuses($systemDeviceId = -1) {
		// This method is to be used when one wants to check the status reported by system against latest database value and update if necessary
		SaveLog($this->getDisplayName().": readStatuses method not implemented.", true);
		return array();
	}

	function forceEventLogging($systemDeviceId = -1, $newStatus = -2) {
		return false;
	}

	function changeDeviceName($deviceID, $newName) {
		SaveLog($this->getDisplayName().": changeDeviceName method not implemented.", true);
	}

	function pluginPage() {
		// remember to set hasPluginPage flag
		return "";
	}

	function getPluginPageLinkText() {
		return LBL_EDITSETTINGS;
	}

	function saveSettings($data) {
		//Not implemented here
	}

	function getStatusText($status, $dimlevel) {
		return getStatusText($status);
	}
}

?>
