<?php

class Events {
	function __construct() {
	}

	public static function get($id = -1, $limit = -1, $sort = "timestamp", $sortDirection = "desc", $additionalSort = "", $additionalWhere = "") {
		global $db;
		$params = array();
		$events = array();

		$query = "SELECT
					e.device_id,
					e.status,
					e.dimlevel,
					e.timestamp,
					d.type AS device_type,
					d.systempluginname,
					IF(TIMESTAMPDIFF(HOUR, e.timestamp, now()) >= ?, false, true) AS active,
					e.ipaddress,
					(SELECT u.username FROM users u WHERE u.id = e.userid) AS username,
					e.userid,
					d.description, UNIX_TIMESTAMP(e.timestamp) AS 'unixtimestamp'
					FROM events e
					INNER JOIN devices d ON d.id = e.device_id";

		if(!isset($_SESSION[CFG_SESSION_KEY])) {
			$hoursStatusActive = Settings::get("hoursstatusactive");
		} else {
			$hoursStatusActive = $_SESSION[CFG_SESSION_KEY]["settings"]["hoursstatusactive"] * 1;
		}

		$params[] = new dbParam(dbParamType::$INTEGER, "statusactive", $hoursStatusActive);

		if($id != -1 || $additionalWhere != "") {
			$wherePart = "";
			$andWord = "";

			if($id != -1) {
				$wherePart .= $andWord."e.device_id = ?";
				$andWord = " AND ";
				$params[] = new dbParam(dbParamType::$INTEGER, "device_id", $id);
			}

			if($additionalWhere != "") {
				$wherePart .= $andWord.$additionalWhere;
				$andWord = " AND ";
			}

			if($wherePart != "") {
				$wherePart = " WHERE ".$wherePart;
			}

			$query .= $wherePart;
		}

		if($sortDirection == "") {
			$sortDirection = "DESC";
		}

		switch($sort) {
			case "dev": {
				$order = "d.description";
				break;
			}
			case "st": {
				$order = "e.status ".$sortDirection.", e.dimlevel";
				break;
			}
			case "usr": {
				$order = "username";
				break;
			}
			case "ip": {
				$order = "e.ipaddress";
				break;
			}
			case "ts":
			case "timestamp":
			default: {
				$order = "e.id";
				break;
			}
		}

		$query .= " ORDER BY ".$additionalSort."".$order." ".mb_convert_case($sortDirection, MB_CASE_UPPER, "UTF-8");

		if($limit != -1) {
			$query .= " LIMIT ?";
			$params[] = new dbParam(dbParamType::$INTEGER, "limit", $limit);
		}

		$result = $db->fetch_many($query, $params);

		if($result !== false) {
			foreach ($result as $e) {
				$e["status"] = convertToBoolean($e["status"]);

				if($_SESSION[CFG_SESSION_KEY]["settings"]["alwaysuselastknownstatus"]) {
					$e["active"] = true;
				}

				$events[] = $e;
			}
		}

		return $events;
	}

	public static function getLastEventTimeStamp($lastUpdate = -1, $deviceId = -1) {
		global $db;
		$params = array();

		$timestamp = -1;

		$query = "SELECT UNIX_TIMESTAMP(e.timestamp) AS timestamp
							FROM events e
							WHERE UNIX_TIMESTAMP(e.timestamp) >= ?";

		$params[] = new dbParam(dbParamType::$INTEGER, "timestamp", $timestamp);

		if($deviceId != -1) {
			$query .= " AND e.device_id = ?";
			$params[] = new dbParam(dbParamType::$INTEGER, "$deviceId", $deviceId);
		}

		$query .= " ORDER BY e.timestamp DESC LIMIT 1";

		if($result = $db->fetch($query, $params)) {
			$timestamp = $result["timestamp"];
		}

		return $timestamp;
	}

	public static function insert($status, $id, $ipaddress, $userid, $dimlevel = 100) {
		global $db;
		// store event
		$params = array(
			new dbParam(dbParamType::$INTEGER, "device_id", $id),
			new dbParam(dbParamType::$INTEGER, "status", $status),
			new dbParam(dbParamType::$INTEGER, "dimlevel", $dimlevel),
			new dbParam(dbParamType::$STRING, "ipaddress", $ipaddress),
			new dbParam(dbParamType::$INTEGER, "userid", $userid)
		);

		$db->insert("events", $params);

		// Update latest status for the device too
		Devices::updateStatus($id, $status, $dimlevel);

		return true;
	}
}

?>
