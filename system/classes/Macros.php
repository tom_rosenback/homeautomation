<?php

class Macros {
	function __construct() {
	}

	public static function get($id = -1) {
		global $db;
		$params = array();

		$macros = array();

		$idClause = "";

		if($id != -1) {
			$idClause = "WHERE id = ?";
			$params[] = new dbParam(dbParamType::$INTEGER, "id", $id);
		}

		$query = "SELECT id, name, comment, scenario, dateadded FROM macros ".$idClause." ORDER BY sort, name";
		$result = $db->fetch_many($query, $params);

		if($result !== false) {
			foreach ($result as $m) {
				$m["relations"] = Macros::getRelations($m["id"]);
				$macros[] = $m;
			}
		}

		return $macros;
	}

	public static function save($params, $id = -1) {
		global $db;
		return $db->save("macros", $params, $id);
	}
	
	public static function delete($macroId = -1) {
		global $db;
		
		$params = array(
			new dbParam(dbParamType::$INTEGER, "id", $macroId)
		);
		
		return $db->delete("macros", $params);
	}

	public static function getRelations($macroId) {
		global $db;
		$relations = array();

		if($macroId != "") {
			$query = "SELECT mr.id, mr.macroid, mr.deviceid, d.description AS devicedesc, mr.status ".
								"FROM macrorelations mr ".
								"LEFT OUTER JOIN devices d ON d.id = mr.deviceid ".
								"WHERE macroid = ? AND d.active = 1 ORDER BY devicedesc";

			$params = array(
				new dbParam(dbParamType::$INTEGER, "macroid", $macroId)
			);

			$result = $db->fetch_many($query, $params);

			if($result !== false) {
				$relations = $result;
			}

			return $relations;
		}
	}

	public static function saveRelations($params, $id = -1) {
		global $db;
		return $db->save("macrorelations", $params, $id);
	}
	
	public static function deleteRelations($macroId = -1) {
		global $db;
		
		$params = array(
			new dbParam(dbParamType::$INTEGER, "macroid", $macroId)
		);
		
		return $db->delete("macrorelations", $params);
	}
}

?>
