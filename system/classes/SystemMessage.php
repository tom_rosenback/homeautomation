<?php
// HomeAutomation
// Copyright (C) 2021 Tom Rosenback (tom.rosenback@gmail.com), Daniel Malmgren (daniel.malmgren@kolefors.se)
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

class SystemMessage
{
	var $id;
	var $text;
	var $additionalText;
	var $type;
	var $autoClose;
	var $showOnlyInDebug;

	function __construct() {
		$this->id				= "";
		$this->text				= "";
		$this->additionalText	= "";
		$this->type				= SystemMessageType::$INFO;
		$this->autoClose		= false;
		$this->showOnlyInDebug 	= false;
	}

	function setId($_id)
	{
		$this->id = $_id;
	}

	function setText($_txt)
	{
		$this->text = $_txt;
	}

	function setAdditionalText($_txt)
	{
		$this->additionalText = $_txt;
	}

	function setType($_type)
	{
		$this->type = $_type;
	}

	function setAutoClose($_autoclose)
	{
		$this->autoClose = $_autoclose;
	}

	function setShowOnlyInDebug($_showOnlyInDebug)
	{
		$this->showOnlyInDebug = $_showOnlyInDebug;
	}

	function generate()
	{
		$ret = "";
		$divId = "";
		$class = " ";

		if($_SESSION[CFG_SESSION_KEY]["userlevel"] > 1 || $this->type == SystemMessageType::$ERROR) {
			if($this->id != "")
			{
				$divId = " id=\"systemmessage-".$this->id."\"";
			}

			$class .= $this->type;
			if($this->autoClose)
			{
				$class .= " autoclose";
			}

			$readMore = "";

			if($this->additionalText != "") {
				$readMore = '&nbsp;<!--<a href="#" class="readmore">'.LBL_READMORE.'</a>--><span class="additionaltext">'.$this->additionalText.'</span>';
			}

			$ret = '<div '.$divId.'class="systemmessage'.$class.'">'.$this->text.
						$readMore.'
						<a href="#" class="close">&#215;</a>
					</div>';
		}

		return $ret;
	}

	function encode() {
		return base64_encode(json_encode(get_object_vars($this)));
	}

	function decode($base64) {
		$jsonData = json_decode(base64_decode($base64));

		foreach($jsonData as $key => $val) {
            if(property_exists(__CLASS__, $key))
			{
                $this->$key = $val;
            }
        }
	}
}

?>
