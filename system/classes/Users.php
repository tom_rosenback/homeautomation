<?php

class Users {
	function __construct() {
	}

	public static function get($id = "", $userlevel = -1) {
		global $db;
		$users = array();
		$params = array();

		$query = "SELECT u.id AS id, u.username, u.password, u.firstname, u.lastname, u.email,
					u.userlevel, userlevels.description AS userleveltext, u.editable
					FROM ".$db->tablePrefix."users u
					INNER JOIN ".$db->tablePrefix."userlevels ON u.userlevel = userlevels.userlevel";

		if($id != "") {
			$query .= " WHERE u.id = ?";
			$params[] = new dbParam(dbParamType::$INTEGER, "id", $id);
		} else if($userlevel > -1) {
			$query .= " WHERE u.userlevel >= ? AND u.editable > 1";
			$params[] = new dbParam(dbParamType::$INTEGER, "userlevel", $userlevel);
		}

		$result = $db->fetch_many($query, $params);

		if($result !== false) {
			foreach($result as $user) {
				$users[$user["id"]] = $user;
			}
		}

		return $users;
	}
	
	public static function save($params, $id = -1) {
		global $db;
		return $db->save("users", $params, $id);
	}
	
	public static function delete($id = -1) {
		global $db;
		
		$params = array(
			new dbParam(dbParamType::$INTEGER, "id", $id)
		);
		
		return $db->delete("users", $params);
	}

	public static function getUserLevels() {
		global $db;
		$query = "SELECT id, userlevel, description FROM ".$db->tablePrefix."userlevels ORDER BY userlevel";
		return $db->fetch_many($query);
	}
	
	public static function deleteUserLevel($id = -1) {
		global $db;
		
		$params = array(
			new dbParam(dbParamType::$INTEGER, "id", $id)
		);
		
		return $db->delete("userlevels", $params);
	}

	public static function getUserLevelPermissions($pageId = -1) {
		global $db;
		$query = "SELECT userlevelid FROM ".$db->tablePrefix."userlevelpermissions WHERE pageid = ?";
		$params = array(
			new dbParam(dbParamType::$INTEGER, "pageid", $pageId)
		);

		return $db->fetch_many($query, $params);
	}

	public static function authenticate($username, $password) {
		global $db;
		$authenticated = false;

		$query = "SELECT id FROM users WHERE username=? AND password=? LIMIT 1;";
		$params = array(
			new dbParam(dbParamType::$STRING, "username", $username),
			new dbParam(dbParamType::$STRING, "password", md5($password))
		);

		if($result = $db->fetch($query, $params)) {
			$authenticated = true;
		}

		return $authenticated;
	}

	public static function verifyCookie($username, $hash) {
		global $db;
		$authenticated = false;

		$query = "SELECT id FROM users WHERE username=? AND MD5(CONCAT(MD5(salt), password, MD5(salt)))=? LIMIT 1;";
		$params = array(
			new dbParam(dbParamType::$STRING, "username", $username),
			new dbParam(dbParamType::$STRING, "hash", $hash)
		);

		if($result = $db->fetch($query, $params)) {
			$authenticated = true;
		}

		return $authenticated;
	}

	public static function getDetails($username) {
		global $db;
		$userinfo = array();

		$query =	"SELECT u.id, u.firstname, u.lastname, ul.userlevel, u.editable, u.username, u.password, u.salt ".
							"FROM users u INNER JOIN userlevels ul ".
							"ON u.userlevel = ul.userlevel ".
							"WHERE username = ? LIMIT 1;";

		$params = array(
			new dbParam(dbParamType::$STRING, "username", $username)
		);

		if($result = $db->fetch($query, $params)) {
			$userInfo = $result;
		}

		return $userInfo;
	}
}

?>
