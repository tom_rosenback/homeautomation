<?php

class Sensors {
	function __construct() {
	}

	public static function get($sensorSerial = "", $sensorId = "-1", $orderBy = "sort", $active = 1) {
		global $db;
		$params = array();
		$tempSensors = array();

		if($sensorSerial == "external") {
			$tempSensors[] = array("id" => "external", "sensor_serial" => "external", "description" => "External temperature");
			return $tempSensors;
		}

		$andWord = "";
		$whereWord = "";
		$idClause = "";
		$serialClause = "";
		$activeClause = "";

		if($sensorSerial != "") {
			$serialClause = " s.serial = ?";
			$andWord = " AND ";
			$whereWord = " WHERE ";
			$params[] = new dbParam(dbParamType::$STRING, "serial", $sensorSerial);
		} else if($sensorId != "-1") {
            $idClause = " s.id = ?";
			$andWord = " AND ";
			$whereWord = " WHERE ";
			$params[] = new dbParam(dbParamType::$INTEGER, "id", $sensorId);
		}

		if($active != -1) {
			$activeClause = $andWord." s.active = ?";
			$andWord = " AND ";
			$whereWord = " WHERE ";
			$params[] = new dbParam(dbParamType::$INTEGER, "active", $active);
		}

		$query = "SELECT s.id, s.serial AS sensor_serial, s.name, s.sort, s.sensortype, st.description AS sensortypedesc,
					TRIM(st.unit) AS unit, IFNULL(s.gaugetype, st.gaugetype) AS gaugetype,
					IFNULL(s.min, st.min) AS min, IFNULL(s.max, st.max) AS max, s.active,
					IFNULL(s.high, st.high) AS high, IFNULL(s.low, st.low) AS low
					FROM tempsensors s
					LEFT OUTER JOIN sensortypes st ON s.sensortype = st.id
					".$whereWord.
					$serialClause.
					$idClause.
					$activeClause."
					ORDER BY ".$orderBy.", name";

		$result = $db->fetch_many($query, $params);

		if($result !== false) {
			foreach($result as $t) {
				$t["realid"] = $t["id"];
				$t["id"] = "t@@".$t["sensor_serial"];
				$t["description"] = $t["name"];
				$tempSensors[] = $t;
			}
		}

		return $tempSensors;
	}

	public static function save($params, $id = -1) {
		global $db;
		return $db->save("tempsensors", $params, $id);
	}


	public static function delete($id = "") {
		global $db;

		$params = array(
			new dbParam(dbParamType::$INTEGER, "id", $id)
		);

		return $db->delete("tempsensors", $params);
	}

	public static function getAvailable() {
		global $db;
		$tempSensors = array();

		$query = "SELECT DISTINCT sensor_serial FROM temps";

		$result = $db->fetch_many($query);

		if($result !== false) {
			$tempSensors = $result;
		}

		return $tempSensors;
	}

	public static function update($serial = "", $name = "") {
		$configuredSensors = Sensors::get();

		if($serial == "") {
			$availableSensors = Sensors::getAvailable();

			$i = count($configuredSensors);

			foreach($availableSensors as $availableSensor) {
				$configured = false;

				foreach($configuredSensors as $configuredSensor) {
					if($availableSensor["sensor_serial"] == $configuredSensor["sensor_serial"]) {
						$configured = true;
						break;
					}
				}

				if($configured == false) {
					$i++;

					$params = array(
						new dbParam(dbParamType::$STRING, "name", $availableSensor["sensor_serial"]),
						new dbParam(dbParamType::$STRING, "serial", $availableSensor["sensor_serial"]),
						new dbParam(dbParamType::$INTEGER, "sort", $i)
					);

					Sensors::save($params);
				}
			}
		} else {
			$result = Sensors::get($serial);

			if(empty($result)) {
				if($name == "") {
					$name = $serial;
				}

				$count = count($configuredSensors);
				$params = array(
					new dbParam(dbParamType::$STRING, "name", $name),
					new dbParam(dbParamType::$STRING, "serial", $serial),
					new dbParam(dbParamType::$INTEGER, "sort", $count++)
				);
				Sensors::save($params);
			}
		}
	}

	public static function getMinMaxReadings($query) {
		global $db;

		$min_max = array();
		$result = $db->query($query, true);

		if($result !== false) {
			foreach($result as $v) {
				$min_max[$v["name"]] = $v;
			}
		}

		return $min_max;
	}

	public static function getReadings($query, $type) {
		global $db;

		$temps = array();

		$result = $db->query($query, true);

		if($result !== false) {
			foreach($result as $r) {
				$temps[$r["name"]][$type]["value"] = $r["actual"];
				$temps[$r["name"]][$type]["unit"] = $r["unit"];
			}
		}

		return $temps;
	}

	public static function getStatistics() {
			global $db;
			$result = $db->query("SELECT * FROM temps WHERE date > DATE_SUB(NOW(), INTERVAL 3 YEAR) ORDER BY id", true, true);
			return $result !== false ? $result : array();
	}

	public static function writeReading($sensorId, $serial, $newvalue, $name = "", $calculateValues = false, $timestamp = "now") {
		global $db;

		$sensorData = Sensors::getCurrentReadings($serial, $sensorId);
		$id = -1;

		if(is_null($timestamp) || $timestamp == "now" || strlen($timestamp) != 19) {
			$timestamp = dbParam::now();
		}

		if(($sensorData == false || count($sensorData) == 0) && $name != "") {
			Sensors::update($serial, $name);
			$sensorData = Sensors::getCurrentReadings($serial, $sensorId);
		}

		if($sensorData !== false && count($sensorData) > 0) {
			$actualValue = $sensorData[0]["last_reading"] * 1;
			$previousValue = $sensorData[0]["previous_reading"] * 1;
			$newvalue = $newvalue * 1;

			if($actualValue == $previousValue && $newvalue == $actualValue &&
				substr($timestamp, 0, 13) == substr($sensorData[0]["last_reading_date"], 0, 13)) {
				$id = $sensorData[0]["last_reading_id"] * 1;
			}

			if($serial == "") {
				$serial = $sensorData[0]["sensor_serial"];
			}
		}

		if($serial != "" && $sensorData[0]["active"] == 1 &&
			($newvalue >= $sensorData[0]["min"] || $sensorData[0]["min"] == "") && ($newvalue <= $sensorData[0]["max"] || $sensorData[0]["max"] == "")) {

			if($calculateValues) {
				if(substr($timestamp, 0, 13) != substr($sensorData[0]["last_reading_date"], 0, 13)) {
					//A new hour has begun, time to calculate last hour's total
					SaveLog("writeReading() calculating hour total for sensor ".$serial);
					$hourStart = substr(date("Y-m-d H:i:s", strtotime("-1 hour")), 0, 13).":00:00";
					$hourEnd = substr(date("Y-m-d H:i:s"), 0, 13).":00:00";
					SaveLog("writeReading() last hour was ".$hourStart." to ".$hourEnd);
					$valueLastHour = Sensors::getReadingAtTime($hourStart, $serial)[0]["last_reading"];
					SaveLog("writeReading() the value for last hour was ".$valueLastHour);
					$valueDifference = max($actualValue-$valueLastHour, 0);
					SaveLog("writeReading() the difference last hour was ".$valueDifference);
					Sensors::writeReading(-1, $serial."-hourly", $valueDifference, $serial."-hourly", false, $hourEnd);
				}

				if(substr($timestamp, 0, 10) != substr($sensorData[0]["last_reading_date"], 0, 10)) {
					//A new day has begun, time to calculate yesterdays total
					SaveLog("writeReading() calculating day total for sensor ".$serial);
					$dayStart = substr(date("Y-m-d H:i:s", strtotime("-1 day")), 0, 10)." 00:00:00";
					SaveLog("writeReading() last day was ".$dayStart);
					$valueLastDay = Sensors::getReadingAtTime($dayStart, $serial)[0]["last_reading"];
					SaveLog("writeReading() the value for last day was ".$valueLastDay);
					$valueDifference = max($actualValue-$valueLastDay, 0);
					SaveLog("writeReading() the difference last day was ".$valueDifference);
					Sensors::writeReading(-1, $serial."-daily", $valueDifference, $serial."-daily", false, $dayStart);
				}

				if(substr($timestamp, 0, 7) != substr($sensorData[0]["last_reading_date"], 0, 7)) {
					//A new month has begun, time to calculate last month's total
					SaveLog("writeReading() calculating month total for sensor ".$serial);
					$monthStart = substr(date("Y-m-d H:i:s", strtotime("-1 month")), 0, 7)."-01 00:00:00";
					SaveLog("writeReading() last month was ".$monthStart);
					$valueLastMonth = Sensors::getReadingAtTime($monthStart, $serial)[0]["last_reading"];
					SaveLog("writeReading() the value for last month was ".$valueLastMonth);
					$valueDifference = max($actualValue-$valueLastMonth, 0);
					SaveLog("writeReading() the difference last month was ".$valueDifference);
					Sensors::writeReading(-1, $serial."-monthly", $valueDifference, $serial."-monthly", false, $monthStart);
				}
			}

			$params = array(
				new dbParam(dbParamType::$STRING, "sensor_serial", $serial),
				new dbParam(dbParamType::$DOUBLE, "temp_c", $newvalue),
				new dbParam(dbParamType::$STRING, "date", $timestamp)
			);
			$id = $db->save("temps", $params, $id);

			$params = array(
				new dbParam(dbParamType::$DOUBLE, "last_reading", $newvalue),
				new dbParam(dbParamType::$INTEGER, "last_reading_id", $id),
				new dbParam(dbParamType::$STRING, "last_reading_date", $timestamp),
				new dbParam(dbParamType::$DOUBLE, "previous_reading", $sensorData[0]["last_reading"])
			);
			$db->save("tempsensors", $params, $sensorData[0]["id"]);

			executeDynamicActivations(-1, $serial);
		}

		return true;
	}

	public static function getCurrentReadings($sensorSerial = "", $sensorId = -1) {
		global $db;
		$params = array();
		$readings = array();
		$result = false;

		if($sensorSerial == "external") {
			$reading = getTemperatureFromExternalUrl();
			$readings[] = array("last_reading" => $reading, "id" => "external", "name" => "External temperature", "sensor_serial" => "external", "active" => 1, "unit" => "&deg;C");
		} else {
			$query = "SELECT s.id, s.name, s.serial AS sensor_serial, s.active, st.unit, s.sensortype, IFNULL(s.gaugetype, st.gaugetype) AS gaugetype,
						IFNULL(s.min, st.min) AS min, IFNULL(s.max, st.max) AS max, IFNULL(s.high, st.high) AS high, IFNULL(s.low, st.low) AS low,
						ROUND(s.last_reading, 1) AS last_reading, s.last_reading_id, ROUND(s.previous_reading, 1) AS previous_reading,
						s.last_reading_date, st.graphtype
						FROM tempsensors s
						LEFT OUTER JOIN sensortypes st ON s.sensortype = st.id";

			if($sensorId != "-1") {
				$query .= " WHERE s.id = ?";
				$params[] = new dbParam(dbParamType::$INTEGER, "id", $sensorId);
			} else if($sensorSerial != "") {
				$query .= " WHERE s.serial = ?";
				$params[] = new dbParam(dbParamType::$STRING, "serial", $sensorSerial);
			}
			
			$query .= " ORDER BY s.sort, s.name";

			$result = $db->fetch_many($query, $params);
		}

		if($result !== false) {
			foreach($result as $r) {
				/*$r["temp"] 		= $r["last_reading"];
				$r["value"] 	= $r["last_reading"];
				$r["value_id"] 	= $r["last_reading_id"];
				$r["actual"]	= $r["last_reading"];
				$r["actual_id"] = $r["last_reading_id"];
				$r["previous"] 	= $r["previous_reading"];*/
				$r["image"] 	= getSensorImage($r["id"], $r["last_reading"]);
				$readings[] 	= $r;
			}
		}

		return $readings;
	}

	public static function getReadingAtTime($timestamp, $sensorSerial) {
		global $db;
		$params = array();
		$readings = array();

		$params[] = new dbParam(dbParamType::$STRING, "sensor_serial", $sensorSerial);

		if(strlen($timestamp) == 8) { //Only time, format 07:58:29
			$dateClause = "date_format(date, '%T') < '".$timestamp."'";
		} else if(strlen($timestamp) == 19) { //Date and time, format 2016-09-09 07:58:29
			$dateClause = "date < '".$timestamp."'";
		} else {
			return "getReadingsAtTime() wrong date/time format!";
		}

		$query = "select sensor_serial, temp_c as last_reading, date from temps where sensor_serial = ? and ".$dateClause." order by date desc limit 1";

		$result = $db->fetch_many($query, $params);

		return $result;
	}

	public static function getStats() {
		global $db;
		$stats = array();

		$query = "SELECT MIN(date) AS first, MAX(date) AS last, COUNT(*) AS datapoints FROM temps LIMIT 1";
		$result = $db->fetch($query);

		if($result !== false) {
			$stats = $result;
		}

		return $stats;
	}

	public static function getTrends($query, $direction = "up") {
		global $db;
		$timestampStart = 0;
		$timestampStop = 0;

		$trends = array();

		$i = 0;

		$result = $db->query($query, true);

		if($result !== false) {
			foreach($result as $t) {
				$tempArr[$i] = $t;

				if($i + 1 >= 3 && $timestampStart == 0) {
					if(($tempArr[$i]["temp"] - $tempArr[$i - 1]["temp"]) > 1 && ($tempArr[$i - 1]["temp"] - $tempArr[$i - 2]["temp"]) > 2 && ($tempArr[$i - 2]["temp"] - $tempArr[$i - 3]["temp"] > 3)) {
						$timestampStart = $tempArr[$i - 3]["timestamp"];
					}
				}

				if($timestampStart > 0 && $timestampStop == 0) {
					if(($tempArr[$i]["temp"] - $tempArr[$i - 1]["temp"]) < -1 && ($tempArr[$i - 1]["temp"] - $tempArr[$i - 2]["temp"]) < -1 && ($tempArr[$i - 2]["temp"] - $tempArr[$i - 3]["temp"] < -1)) {
						$timestampStop = $tempArr[$i - 3]["timestamp"];
					}
				}

				if($timestampStart > 0 && $timestampStop > 0) {
					$trends[] = $timestampStop - $timestampStart;

					$timestampStart = 0;
					$timestampStop = 0;
				}

				$i++;
			}
		}

		return $trends;
	}

	public static function getGraphData($sensor, $limit, $type = "") {
		global $db;
		$params = array();

		$data = array(
			"title" => "",
			"unit" => "",
			"sensors" => array(),
		);
        
        $sensorData = Sensors::get($sensor);

		$limit = $limit * 24 * 60 * 60; // 30 days

		// Query from the database
		if(($sensor != "" || $type != "")) {
            $query = "SELECT
						UNIX_TIMESTAMP(t1.date) AS date,
						t1.date AS updated,
						TIMESTAMPDIFF(MINUTE, t1.date, now()) AS lastUpdated,
						t1.temp_c AS value,
						s.name AS name,
						s.serial,
						st.unit,
						st.description AS sensortype
						FROM temps t1
						INNER JOIN tempsensors s ON t1.sensor_serial = s.serial
						INNER JOIN sensortypes st ON s.sensortype = st.id
						WHERE s.active = 1 AND t1.date > DATE_SUB(NOW(), INTERVAL ? SECOND)";
                        
			$query = "SELECT
						UNIX_TIMESTAMP(t1.date) AS date,
						t1.date AS updated,
						TIMESTAMPDIFF(MINUTE, t1.date, now()) AS lastUpdated,
						t1.temp_c AS value
						FROM temps t1
						WHERE t1.date > DATE_SUB(NOW(), INTERVAL ? SECOND)";

			$params[] = new dbParam(dbParamType::$INTEGER, "limit", $limit);

			if($sensor != "") {
				$query .= " AND t1.sensor_serial = ?";
				$params[] = new dbParam(dbParamType::$STRING, "sensor_serial", $sensor);
			}

			/*if($type != "") {
				$query .= " AND st.id = ?";
				$params[] = new dbParam(dbParamType::$INTEGER, "id", $type);
			}*/

			$query .= " ORDER BY t1.sensor_serial, UNIX_TIMESTAMP(t1.date);";

			$result = $db->fetch_many($query, $params);
			$last = false;

			$name = "";
			$sensortype = "";

			$maxDeviation = 20;

			if($result !== false) {
				foreach ($result as $d) {
					$serial = $d["serial"];

					if(!isset($data["sensors"][$serial])) {
						$last = false;

						$data["sensors"][$serial] = array(
							"values" => array(),
							"min" => 1000,
							"max" => -1000
						);
					}

					$date = $d["date"] * 1;
					$value = $d["value"] * 1;
					$lastUpdated = $d["lastUpdated"] * 1;

					$data["sensors"][$serial]["name"] = $sensorData[0]["name"];

					$data["sensors"][$serial]["values"][] = array($date * 1000, $value);
					$last = $value;

					$name = $sensorData[0]["name"];
					$sensortype = $sensorData[0]["sensortype"];

					$data["unit"] = html_entity_decode($sensorData[0]["unit"]);
				}
			}

			if($type == "" && $last !== false) {
				if($lastUpdated > 2880) {
					$lastUpdatedString = LBL_LASTUPDATED_DAYS;
					$lastUpdated /= 1440;
				} else if($lastUpdated > 120) {
					$lastUpdatedString = LBL_LASTUPDATED_HOURS;
					$lastUpdated /= 60;
				} else if($lastUpdated > 1) {
					$lastUpdatedString = LBL_LASTUPDATED;
				} else {
					$lastUpdatedString = LBL_LASTUPDATED_SINGLE;
				}
				$data["title"] = $name.", ".strtolower(LBL_NOW)." ".round($last, 1).$data["unit"].", ".str_replace("{time}", "".floor($lastUpdated), $lastUpdatedString);
			} else if($type != "") {
				$data["title"] = $sensortype;
			}
		}

		return $data;
	}

	public static function removeDuplicateData() {
		global $db;

		$delete = array();

		$query = "SELECT t.id, t.sensor_serial, t.temp_c AS current FROM temps t ORDER BY sensor_serial, id";
		$last = false;
		$sensor = false;

		$result = $db->fetch_many($query);

		if($result !== false) {
			foreach($result as $data) {
				$current = $data["current"] * 1;

				if($sensor == $data["sensor_serial"] && $last !== false && $last == $current) {
					$delete[] = new dbParam(dbParamType::$INTEGER, "id", $data["id"]);
				}

				$last = $current;
				$sensor = $data["sensor_serial"];
			}

			if(count($delete) > 0) {
				$paramPlaceholders = implode(',', array_fill(0, count($delete), '?'));
				$query = "DELETE FROM temps WHERE id IN (".$paramPlaceholders.")";
				$db->execute($query, $params);
			}
		}
	}

	public static function getTypes($sensorTypeId = -1, $hasSensorsCheck = false) {
		global $db;
		$params = array();

		$sensorTypes = array();

		$query = "SELECT DISTINCT st.*,
							CONCAT(st.description, ' (', TRIM(st.unit), ')') AS descwithunit,
							st.gaugetype, st.graphtype,
							st.min, st.max, st.high, st.low
							FROM sensortypes st";

		if($hasSensorsCheck) {
			$query .= " INNER JOIN tempsensors s ON s.sensortype = st.id";
		}

		if($sensorTypeId != -1) {
			$query .= " WHERE st.id = ?";
			$params[] = new dbParam(dbParamType::$INTEGER, "id", $sensorTypeId);
		}

		$query .= " ORDER BY st.sort, st.description";

		$result = $db->fetch_many($query, $params);

		if($result !== false) {
			$sensorTypes = $result;
		}

		return $sensorTypes;
	}

	public static function deleteType($id = -1) {
		global $db;

		$params = array(
			new dbParam(dbParamType::$INTEGER, "id", $id)
		);

		return $db->delete("sensortypes", $params);
	}
/*
	public static function getData($sensor, $interval = 86400) {
		$data = array();

		$query = "SELECT
		UNIX_TIMESTAMP(t1.date) AS date,
		t1.temp_c AS temp,
		s.name AS sensor,
		st.description AS sensordescription,
		st.unit
		FROM temps t1
		INNER JOIN tempsensors s ON t1.sensor_serial = s.serial
		INNER JOIN sensortypes st ON s.sensortype = st.id
		WHERE t1.date > DATE_SUB(NOW(), INTERVAL ".$interval." SECOND)
		AND t1.sensor_serial = \"".$sensor."\" ORDER BY UNIX_TIMESTAMP(t1.date);";

		$result = mysql_query($query) or die ("Error: ".mysql_error()."<br>".$query);

		// while -loop that we pull data on the database.
		while($row = mysql_fetch_array($result)) {
			$data[] = array(
				"date" 				=> $row["date"],
				"temp" 				=> $row["temp"],
				"sensor" 			=> $row["sensor"],
				"sensordescription" => $row["sensordescription"],
				"unit" 				=> $row["unit"],
			);
		}

		return $data;
	}*/

	public static function saveType($params, $id = -1) {
		global $db;
		return $db->save("sensortypes", $params, $id);
	}
}

?>
