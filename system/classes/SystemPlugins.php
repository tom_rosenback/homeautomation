<?php

class SystemPlugins {
	public $plugins = false;

	function __construct() {
		$this->plugins = SystemPlugins::load();
	}

	public static function getInstalled() {
		global $db;

		$plugins = array();

		$query = "SELECT s.pluginname, s.version, s.dateinstalled,
							IF((SELECT COUNT(ss.id)
								FROM systempluginsettings ss
								WHERE ss.pluginname = s.pluginname) > 0, 1, 0
							) AS hassettings
							FROM systemplugins s
							ORDER BY s.pluginname";

		$result = $db->fetch_many($query);

		if($result !== false) {
			foreach ($result as $p) {
				$plugins[$p["pluginname"]] = $p;
			}
		}

		return $plugins;
	}

	public static function delete($pluginName = "") {
		global $db;

		$params = array(
			new dbParam(dbParamType::$STRING, "pluginname", $pluginName)
		);

		return $db->delete("systemplugins", $params);
	}

	public static function load($pluginToLoad = "", $includeAvailable = false, $type = "") {
		global $db;

		$pluginsDir = HA_ROOT_PATH."/system/systemplugins/";
		include_once(HA_ROOT_PATH."/system/classes/BaseSystemPlugin.php");

		$availablePlugins = getAvailableSystemPlugins();
		$plugins = array();

		$andWord = "";
		$whereWord = "";
		$nameClause = "";
		$typeClause = "";

		if($pluginToLoad != "") {
			$nameClause = " s.pluginname = ?";
			$andWord = " AND ";
			$whereWord = " WHERE ";
			$params[] = new dbParam(dbParamType::$STRING, "pluginname", $pluginToLoad);
		}

		if($type != "") {
			$typeClause = $andWord." s.plugintype = ?";
			$whereWord = " WHERE ";
			$params[] = new dbParam(dbParamType::$STRING, "plugintype", $type);
		}

		$query = "SELECT s.pluginname, s.version, s.dateinstalled,
							IF((SELECT COUNT(ss.id)
								FROM systempluginsettings ss
								WHERE ss.pluginname = s.pluginname) > 0, 1, 0
							) AS hassettings
							FROM systemplugins s ".
							$whereWord.
							$nameClause.
							$typeClause."
							ORDER BY s.pluginname";

		$result = $db->fetch_many($query, $params);

		if($result !== false) {
			foreach ($result as $p) {

				$pluginName = $p["pluginname"];

				if(in_array($pluginName, $availablePlugins)) {
					include_once($pluginsDir.$pluginName."/".$pluginName.".plugin.php");
					$plugin = new $pluginName();

					$plugin->installedVersion = $p["version"];
					$plugin->dateInstalled = $p["dateinstalled"];
					$plugin->hasSettings = ($p["hassettings"] == 1) ? true : false;

					$plugins[$pluginName] = $plugin;
					unset($availablePlugins[$pluginName]);
				}
			}
		}

		if($pluginToLoad == "") {
			if($includeAvailable) {
				$notInstalledPlugins = array();
				foreach($availablePlugins as $pluginName) {
					if(!in_array($pluginName, $plugins)) {
						include_once($pluginsDir.$pluginName."/".$pluginName.".plugin.php");
						$notInstalledPlugins[] = new $pluginName();
					}
				}
				return $notInstalledPlugins;
			}

			return $plugins;
		} else {
			return $plugins[$pluginToLoad];
		}
	}

	public static function loadNotificationPlugins($deviceid, $includeAll = false) {
		global $db;

		$pluginsDir = HA_ROOT_PATH."/system/systemplugins/";
		include_once(HA_ROOT_PATH."/system/classes/BaseSystemPlugin.php");

		$plugins = array();

		$leftWord = "";

		if($includeAll) {
			$leftWord = " LEFT ";
		}

		$query = "select sp.pluginname, IF(dnp.deviceid = ".$deviceid.", 1, 0) as isconnected
				from systemplugins sp ".$leftWord." join devicenotificationplugins dnp on sp.id = dnp.systempluginid
				where sp.plugintype = 'notification';";

		$result = $db->fetch_many($query, $params);

		if($result !== false) {
			foreach ($result as $p) {
				$pluginName = $p["pluginname"];
				include_once($pluginsDir.$pluginName."/".$pluginName.".plugin.php");
				$plugin = new $pluginName();
				$plugins[$pluginName]["plugin"] = $plugin;
				$plugins[$pluginName]["isconnected"] = $p["isconnected"];
			}
		}

		return $plugins;
	}

	public static function setLastUpdated($pluginName, $timestamp = "") {
		global $db;
		if($name != "") {
			if($timestamp == "") {
				$timestamp = time();
			}

			$query = "UPDATE systemplugins SET lastupdated = ? WHERE pluginname = ?";
			$params = array(
				new dbParam(dbParamType::$STRING, "lastupdated", date("Y-m-d H:i:s", $timestamp)),
				new dbParam(dbParamType::$STRING, "pluginname", $pluginName)
			);

			$db->execute($query, $params);
		}

		return true;
	}

	public static function getLastUpdated($pluginName) {
		global $db;

		$time = time();

		if($pluginName != "") {
			$query = "SELECT lastupdated FROM systemplugins WHERE pluginname = ?";
			$params = array(
				new dbParam(dbParamType::$STRING, "pluginname", $pluginName)
			);

			$result = $db->fetch($query, $params);

			if($result !== false) {
				$time = $result["lastupdated"];
			}
		}

		return strtotime($time);
	}

	public static function getSettings($pluginName) {
		global $db;

		$settings = array();

		if($pluginName != "") {
			$query = "SELECT s.name, s.value, s.type FROM systempluginsettings s WHERE s.pluginname = ?";
			$params = array(
				new dbParam(dbParamType::$STRING, "pluginname", $pluginName)
			);

			$result = $db->fetch_many($query, $params);

			if($result !== false) {
				foreach ($result as $p) {
					$settings[$p["name"]] = $p;
				}
			}
		}

		return $settings;
	}

	public static function saveSetting($pluginName, $name, $value = "", $type = "", $sort = -1, $insert = false) {
		global $db;

		$params = array();

		if($pluginName != "" && $name != "") {
			$params[] = new dbParam(dbParamType::$STRING, "value", $value);

			if($type != "") {
				$params[] = new dbParam(dbParamType::$STRING, "type", $type);
				$type = ", type = ? ";
			}

			if($sort > -1) {
				$params[] = new dbParam(dbParamType::$INTEGER, "sort", $sort);
				$sort = ", sort = ? ";
			} else {
				$sort = "";
			}


			$params[] = new dbParam(dbParamType::$STRING, "name", $name);
			$params[] = new dbParam(dbParamType::$STRING, "pluginname", $pluginName);

			if($insert) {
				$db->insert("systempluginsettings", $params);
			} else {
				$query = "UPDATE systempluginsettings SET value = ? ".$type.$sort." WHERE name = ? AND pluginname = ?";
				$db->execute($query, $params);
			}
		}

		return true;
	}

	public static function deleteSettings($pluginName = "") {
		global $db;

		$params = array(
			new dbParam(dbParamType::$STRING, "pluginname", $pluginName)
		);

		return $db->delete("systempluginsettings", $params);
	}

	// used from system plugins, ONLY
	public static function saveDevice($systemPluginName, $systemDeviceId, $description, $type = NULL) {
		global $db;

		$dbDevice = Devices::getBySystemDeviceId($systemDeviceId, $systemPluginName);
		$dbTypes = Devices::getTypes();

		if (is_null($type)) {
			$typeId = 1;
		} else {
			foreach ($dbTypes as $dbType) {
				if ($dbType["type"] == $type) {
					$typeId = $dbType["id"];
				}
			}
		}

		if($dbDevice["id"] == "") {
				$params = array(
					new dbParam(dbParamType::$STRING, "systempluginname", $systemPluginName),
					new dbParam(dbParamType::$STRING, "systemdeviceid", $systemDeviceId),
					new dbParam(dbParamType::$STRING, "description", $description),
					new dbParam(dbParamType::$INTEGER, "active", 1),
					new dbParam(dbParamType::$INTEGER, "type", $typeId)
				);

				$result = Devices::save($params);

				if($result !== false) {
					$dbDevice["id"] = $result;
				}
		} /* else { //Commenting out this. The system should never overrule what the user has set, should it? /Daniel
			if($description != $dbDevice["description"]) {
					$params = array(
						new dbParam(dbParamType::$STRING, "description", $description)
					);

					if(!is_null($type)) {
						$params[] = new dbParam(dbParamType::$INTEGER, "type", $typeId);
					}

					Devices::save($params, $dbDevice["id"]);
				}
		} */

		return $dbDevice["id"];
	}

	public static function saveVersion($pluginName, $newVersion) {
		global $db;

		$query = "INSERT INTO systemplugins (pluginname, version) VALUES (?, ?)
					ON DUPLICATE KEY UPDATE version = ?, lastupdated = now()";

		$params = array(
			new dbParam(dbParamType::$STRING, "pluginname", $pluginName),
			new dbParam(dbParamType::$STRING, "version", $newVersion),
			new dbParam(dbParamType::$STRING, "version", $newVersion)
		);

		$db->execute($query, $params);
		return true;
	}
}

?>
