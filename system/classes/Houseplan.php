<?php

class Houseplan {
	function __construct() {
	}

	public static function get($deviceId = -1, $tempsensor = -1) {
		global $db;
		$params = array();

		$houseplan = array();
		$idClause = "";

		if($deviceId != "-1") {
			$idClause = " AND hp.deviceid = ?";
			$params[] = new dbParam(dbParamType::$INTEGER, "deviceid", $deviceId);
		}

		$query = "SELECT hp.id, hp.xposition, hp.yposition, hp.deviceid, hp.tempsensor, hp.groupid, hp.macroid
							FROM houseplan hp
							WHERE (hp.deviceid = -1 OR hp.deviceid IN
								(SELECT id FROM devices WHERE active = 1))
							AND (hp.tempsensor = -1 OR hp.tempsensor = 'external' OR hp.tempsensor IN
								(SELECT serial from tempsensors WHERE active = 1))
							".$idClause;

		$result = $db->fetch_many($query, $params);

		if($result !== false) {
			$houseplan = $result;
		}

		return $houseplan;
	}

	public static function save($params, $id = -1) {
		global $db;
		return $db->save("houseplan", $params, $id);
	}

	public static function delete($id = "") {
		global $db;

		$params = array(
			new dbParam(dbParamType::$INTEGER, "id", $id)
		);

		return $db->delete("houseplan", $params);
	}

	public static function deleteDevice($deviceId = "") {
		global $db;

		$params = array(
			new dbParam(dbParamType::$INTEGER, "deviceid", $deviceId)
		);

		return $db->delete("houseplan", $params);
	}

	public static function deleteGroup($groupId = "") {
		global $db;

		$params = array(
			new dbParam(dbParamType::$INTEGER, "groupid", $groupId)
		);

		return $db->delete("houseplan", $params);
	}

	public static function deleteMacro($macroId = "") {
		global $db;

		$params = array(
			new dbParam(dbParamType::$INTEGER, "macroid", $macroId)
		);

		return $db->delete("houseplan", $params);
	}
}

?>
