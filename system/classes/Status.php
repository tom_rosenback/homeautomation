<?php

class Status {
	public $state;
	public $dimlevel;
	public $updated;

	function __construct() {
		$this->state = 0;
		$this->dimlevel = 0;
		$this->updated = 0;
	}
}
