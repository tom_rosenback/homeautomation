<?php

class General {
	function __construct() {
	}

	public static function saveSorting($dbTable = "", $sort = 0, $id = 0) {
		global $db;
		$params = array();
		
		if($dbTable != "") {
			$query = "UPDATE ".$dbTable." SET sort = ?";
			$params[] = new dbParam(dbParamType::$INTEGER, "sort", $sort);
			
			if($id > 0) {
				$params[] = new dbParam(dbParamType::$INTEGER, "id", $id);
				$query .= " WHERE id = ?";
			}
			
			$db->execute($query, $params);
		}
		return true;
	}

	//Function to cull old data.
	//Removes sensor readings older than $days, BUT saves values that matches the min or max for every day
	//Removes all event log entries older than $days
	function cullOldData($days) {
		$query = "CREATE TEMPORARY TABLE mins_and_maxes
							SELECT CONCAT_WS(';',sensor_serial,MAX(temp_c),DATE(date)) FROM temps GROUP BY DATE(date), sensor_serial
							UNION
							SELECT CONCAT_WS(';',sensor_serial,MIN(temp_c),DATE(date)) FROM temps GROUP BY DATE(date), sensor_serial";
		$db->execute($query);

		$query = "DELETE FROM temps
							WHERE DATEDIFF(NOW(), date) > ".$days."
							AND CONCAT_WS(';',sensor_serial,temp_c,DATE(date)) NOT IN (SELECT * FROM mins_and_maxes)";
		$db->execute($query);

		$query = "DELETE FROM events WHERE DATEDIFF(NOW(), timestamp) > ?";
		$params = array(
			new dbParam(dbParamType::$INTEGER, "days", $days)
		);
		$db->execute($query, $params);

		return true;
	}
}

?>
