<?php

class GoogleCalendar {
	function __construct() {
	}	
	
	public static function getSettings($name = "") {
		global $db;
		$params = array();
		
		$values = array();
		
		$query = "SELECT name, value FROM gcalsettings";

		if($name != "") {
			$query .= " WHERE name = ?";
			$params[] = new dbParam(dbParamType::$STRING, "name", $name);
		}

		$result = $db->fetch_many($query, $params);
		
		if($result !== false) {
			foreach($result as $s) {
				$values[$s["name"]] = $s["value"];
			}
		}

		return $values;
	}

	
}

?>
