<?php

/* CONFIG defaults */

	// session key - to separate installations of HomeAutomation, and also from other applications
	$config["session_key"] = "HomeAutomation";

	// cookie timeout - used with remember me option for autologin, entered in seconds
	$config["cookie_timeout"] = 2592000; // 60*60*24*30 days => 2592000 seconds

	// full version index - this is the name of the index page for full version of HomeAutomation, no need to change if not changing the filename also.
	$config["full_version_index"] = "index.php";

	// mobile version index - this is the name of the index page for mobile version of HomeAutomation, no need to change if not changing the filename also.
	$config["mobile_version_index"] = "mobile.php";

	// file and directory permission mask
	$config["permission_mask"] = "0700"; // Default
	// $config["permission_mask"] = "0777"; // Less safe, but use this if problems

	// sudo command - ONLY needed for Mac users who have got problems with tdtool
	$config["sudo_command"] = ""; // Default
	// $config["sudo_command"] = "sudo -u webuser"; // Use this if you have problems with tdtool

	// Is force scheduler command available, users using Windows in other language can test changing this to false if experiencing that saving/deleting tasks doesn�t work as expected
	$config["windows_force_scheduler_available"] = true; // Default
	// $config["windows_force_scheduler_available"] = false;

	// If this flag is set true the install folder can be kept intact (for developers)
	$config["skip_install_check"] = false; // Default
	// $config["skip_install_check"] = true;

	// Shows page generation time if true
	$config["showpagegeneratetime"] = false; // Default
	// $config["showpagegeneratetime"] = true;

	// Forcing logging of slow queries, if set to false depends on if DEBUG mode is active
	$config["force_debug_slow_queries"] = false; // Default
	// $config["force_debug_slow_queries"] = true;

/* SESSION defaults */
	$sessionDefaults["tellduspath"] = "TelldusPath";
	$sessionDefaults["iconortext"] = "text";
	$sessionDefaults["title"] = "HomeAutomation v2.0.2";
	$sessionDefaults["alwaysuselastknownstatus"] = "1";
	$sessionDefaults["hoursstatusactive"] = "1";
	$sessionDefaults["houseplanwidth"] = "1000";
	$sessionDefaults["houseplaniconheight"] = "30";
	$sessionDefaults["useexternaltemperature"] = "1";
	$sessionDefaults["externaltempurl"] = "http://www.temperatur.nu/termo/abisko/temp.txt";
	$sessionDefaults["externaltemplocation"] = "Abisko";
	$sessionDefaults["latitude"] = "61.71215";
	$sessionDefaults["longitude"] = "19.59316";
	$sessionDefaults["timezone"] = 2;
	$sessionDefaults["dateformat"] = "j.n.Y";
	$sessionDefaults["timeformat"] = "H:i";
	$sessionDefaults["theme"] = "default";
	$sessionDefaults["defaultpage"] = "houseplan";
	$sessionDefaults["debug"] = 1;
	$sessionDefaults["showinfobox"] = 1;
	$sessionDefaults["ibshowusername"] = 1;
	$sessionDefaults["ibshowcurrenttime"] = 1;
	$sessionDefaults["ibshowsun"] = 1;
	$sessionDefaults["iblogevents"] = 1;
	$sessionDefaults["ibupcomingevents"] = 10;
	$sessionDefaults["sysusername"] = "SERVER\UserName";
	$sessionDefaults["syspassword"] = "Password";
	$sessionDefaults["phpbinpath"] = "C:\xampp\php";
	$sessionDefaults["localip"] = "192.168.1.1/24";
	$sessionDefaults["dimlevelstep"] = 5;
	$sessionDefaults["defaultdimlevel"] = 75;
	$sessionDefaults["dynamiccheckinterval"] = 5;

?>
