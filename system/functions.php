<?php
// HomeAutomation
// Copyright (C) 2021 Tom Rosenback (tom.rosenback@gmail.com), Daniel Malmgren (daniel.malmgren@kolefors.se)
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

/* *********************************************************************
Function toggleSelected

Description: toggles status of selected device(s), will skip device of type 'sauna' if $devicesToToggle is larger than 1 and status true

Params:
$status				- string (one status) or array of statuses, the array must be indexed with [deviceId] (eg $status[1]), if deviceId not found the device will be ignored
$devicesToToggle	- array of devices
$ipaddress			- ipaddress of the user/system toggling the device(s)
$userid				- the userid of the user toggling the device(s)
$doEventLogging		- whether to log the toggling to the database or not

Return: the output from the commandline as array by SystemPlugin
********************************************************************* */
function toggleSelected($status, $devicesToToggle, $ipaddress, $userid, $doEventLogging = true, $activationType = "") {
	$devicesBySystem = array();
	$statusBySystemAndDevice = array();
	$systemPlugins = SystemPlugins::load();

	// arrange the devices by system
	foreach($devicesToToggle as $device) {
		$newStatus = $status;

		if(is_array($status)) {
			if(array_key_exists($device["id"], $status)) {
				$newStatus = $status[$device["id"]];
			} else {
				$newStatus = "";
			}
		}

		$newStatus = trim($newStatus);

		if($newStatus != "") {
			if(count($devicesToToggle) > 1 && $device["type"] == "sauna" && convertToBoolean($newStatus) == true) {
				// skipping device of type sauna when toggling more than 1 device at a time (to ON)
				continue;
			} else {
				$newStatus = $newStatus * 1;

				if($newStatus >= 1) {
					if($newStatus == 1 && $device["type"] == "absdimmer") {
						$event = Events::get($device["id"], 1, "st", "desc", "timestamp DESC, ", "status = 1");

						$newStatus = $event[0]["dimlevel"];

						if($newStatus == "" || $newStatus < 5) {
							$newStatus = $_SESSION[CFG_SESSION_KEY]["settings"]["defaultdimlevel"] * 1;
						}
					}
				}

				$lastEvent = $device["status"];
				$oldStatus = $device["type"] == "absdimmer" && $lastEvent->dimlevel > 1 ? $lastEvent->dimlevel : $lastEvent->state ? 1 : 0;

				if($lastEvent->dimlevel > 1 && $device["type"] != "absdimmer") {
					// we have a long click event in the history, read event from events table instead
					$event = Events::get($device["id"], 1, "st", "desc", "timestamp DESC, ", "dimlevel < 2");
					$oldStatus = $event[0]["status"];
				}

				if(!is_object($systemPlugins[$device["systempluginname"]])) {
					SaveLog("Plugin not installed: ".$device["systempluginname"], true);
					continue;
				}

				$forceEventLogging = $systemPlugins[$device["systempluginname"]]->forceEventLogging($device, $newStatus);

				if(($doEventLogging && $oldStatus != $newStatus) || $forceEventLogging) {
					$saveLog = true;

					if($activationType == "dynamic" && $newStatus == $oldStatus && !$forceEventLogging) {
						$saveLog = false;
					}

					if($saveLog) {
						Events::insert(convertToBoolean($newStatus), $device["id"], $ipaddress, $userid, $newStatus);
					}
				}
			}

			$devicesBySystem[$device["systempluginname"]][] = $device;
			$statusBySystemAndDevice[$device["systempluginname"]][$device["id"]] = $newStatus;
		}

		executeDynamicActivations($device["id"]);
	}

	$output = array();

	if($activationType != "externalUpdate") {
		// doing action by system
		foreach($devicesBySystem as $systemPluginName => $systemDevices) {
			if(array_key_exists($systemPluginName, $systemPlugins)) {
				$output[] = $systemPlugins[$systemPluginName]->toggleDevices($systemDevices, $statusBySystemAndDevice[$systemPluginName]);
			} else {
				SaveLog("SystemPlugin not installed. (PluginName: ".$systemPluginName.").", true);
			}
		}
	} else {
		foreach($devicesBySystem as $systemPluginName => $systemDevices) {
			if(array_key_exists($systemPluginName, $systemPlugins)) {
				$output[] = $systemPlugins[$systemPluginName]->postToggleDevices($systemDevices, $statusBySystemAndDevice[$systemPluginName]);
			} else {
				SaveLog("SystemPlugin not installed. (PluginName: ".$systemPluginName.").", true);
			}
		}
	}

	return $output;
}

function toggleGroupStatus($groupId, $status) {
	$group = DeviceGroups::get($groupId);
	$group = $group[0];

	$devicesToToggle = array();

	if(is_array($group["relations"])) {
		foreach($group["relations"] as $relation) {
			$device = Devices::get($relation["deviceid"]);
			$devicesToToggle[] = $device[0];
		}
	}

	toggleSelected($status, $devicesToToggle, getIpAddress(), $_SESSION[CFG_SESSION_KEY]["userid"]);

	return $devicesToToggle;
}

function readSelectedStatuses($devicesToRead) {
	$devicesBySystem = array();
	$statusBySystemAndDevice = array();
	$outputs = array();
	$systemPlugins = SystemPlugins::load();

	foreach($devicesToRead as $device) {
		$devicesBySystem[$device["systempluginname"]][] = $device;
	}

	foreach($devicesBySystem as $systemPluginName => $systemDevices) {
		if(is_array($systemDevices) && array_key_exists($systemPluginName, $systemPlugins)) {
			//echo "all"; debugVariable($systemDevices, true);
			$pluginDevices = $systemPlugins[$systemPluginName]->readStatuses($systemDevices);

			if(is_array($pluginDevices)) {
				foreach ($pluginDevices as $pluginDevice) {
					//echo "plugin"; debugVariable($pluginDevice, true);
					foreach($systemDevices as $key => $systemDevice) {
						if($systemDevice["id"] == $pluginDevice["id"]) {
							// debugVariable($systemDevice, true);
							if ($systemDevice["status"]->state != $pluginDevice["status"]->state || $systemDevice["status"]->dimlevel != $pluginDevice["status"]->dimlevel) {
								Events::insert($pluginDevice["status"]->state, $pluginDevice["id"], getIpAddress(), $_SESSION[CFG_SESSION_KEY]["userid"], $pluginDevice["status"]->dimlevel);
							} else {
								Devices::updateStatus($pluginDevice["id"], $pluginDevice["status"]->state, $pluginDevice["status"]->dimlevel);
							}

							$outputs[] = $systemDevices[$key];
							unset($systemDevices[$key]);
							break;
						}
					}
				}
			}
		} else {
			SaveLog("SystemPlugin not installed. (PluginName: ".$systemPluginName.").", true);
		}
	}

	return $outputs;
}

function readGroupStatuses($groupId) {
	$group = DeviceGroups::get($groupId);
	$group = $group[0];
	$devicesToRead = array();

	if(is_array($group["relations"])) {
		foreach($group["relations"] as $relation) {
			$device = Devices::get($relation["deviceid"]);
			$devicesToRead[] = $device[0];
		}
	}

	$output = readSelectedStatuses($devicesToRead, getIpAddress(), $_SESSION[CFG_SESSION_KEY]["userid"]);
	return $output;
}

function runMacro($macroId = -1, $preFetchedMacro = array()) {
	$macro = array();
	$devicesToToggle = array();

	if($macroId > -1) {
		$macro = Macros::get($macroId);
		$macro = $macro[0];
	}
	else if(is_array($preFetchedMacro)) {
		$macro = $preFetchedMacro;
	}

	if(is_array($macro)) {
		if($macro["scenario"] != "-1") {
			Settings::update("selectedscenario", $macro["scenario"]);
		}

		if(is_array($macro["relations"])) {
			foreach($macro["relations"] as $relation) {
				$device = Devices::get($relation["deviceid"]);
				$devicesToToggle[] = $device[0];
				$statuses[$device[0]["id"]] = $relation["status"];
			}
		}

		toggleSelected($statuses, $devicesToToggle, getIpAddress(), $_SESSION[CFG_SESSION_KEY]["userid"]);
	}

	return $devicesToToggle;
}

function parseExecOutputToString($output) {
	$string = "";
	$newLine = "";

	foreach($output as $currentRow) {
		$string .= $newLine.$currentRow;

		$newLine = "\n";
	}

	return $string;
}

// custom exec function which outputs the results on the screen if in debug mode else runs in background
function _exec($command) {
	if($_SESSION[CFG_SESSION_KEY]["settings"]["debug"]) {
		exec($command, $output);
		SaveLog("Command: ".$command."\nOutput:\n".parseExecOutputToString($output));
	} else {
		// we are not in debug mode, execute everything in background
		if(strtoupper(substr(PHP_OS, 0, 3)) === "WIN") {
			// $WshShell = new COM("WScript.Shell");
			// $WshShell->Run($command, 0, false);

			// exec($command, $output);
			pclose(popen("start /B ".$command." > nul 2> nul", "r")); // fastest method
		} else {
			exec($command." > /dev/null &");
		}

		$output = true;
	}

	return $output;
}

function runActivation($activationId) {
	$activation = Schedules::getActivations(-1, $activationId);
	$activation = $activation[0];

	if(is_array($activation)) {
		$schedule = Schedules::get($activation["scheduleid"]);
		$schedule = $schedule[0];

		if(is_array($schedule)) {
			$devicesAndStatuses = getActivationDevicesAndStatuses($schedule, $activation);

			$devicesToToggle = array();

			foreach($devicesAndStatuses["deviceids"] as $deviceId) {
				$devices = Devices::get($deviceId);

				if($deviceId == "-1") {
					$devicesToToggle = $devices;
				} else {
					$devicesToToggle[] = $devices[0];
				}
			}

			toggleSelected($devicesAndStatuses["status"], $devicesToToggle, getIpAddress(), $_SESSION[CFG_SESSION_KEY]["userid"], true);
			runScheduleMacros($schedule["macros"]);
		}
	}
}

function runScheduleMacros($macrosIdString) {
	$macros = explode(";", $macrosIdString);
	foreach($macros as $macroId) {
		runMacro($macroId);
	}
}

function updateCommandFile($deviceId = -1, $commandType = "", $commandString = "") {
	if($deviceId != -1 && $commandType != "") {
		$commandFile = "./rawcommands/".$deviceId."_".$commandType.".tdtool";

		deleteFile($commandFile);

		if($commandString != "") {
			$handle = fopen($commandFile, "w");

			if($handle !== false) {
				fwrite($handle, $commandString);
				fclose($handle);
			}
		}
	}
}

function getFileDir($filenameWithPath, $dirSeparator = "/") {
	$filenameWithPath = str_replace("\\", "/", $filenameWithPath);
	$filename = explode("/", $filenameWithPath);

	if(count($filename) > 0) {
		$directory = "";

		for($i = 0; $i < (count($filename) - 1); $i++) {
			$directory .= $filename[$i].$dirSeparator;
		}
	} else {
		$directory = $filenameWithPath;
	}

	return $directory;
}

function checkIfNull($input) {
	$output = $input;

	if(strlen($input) == 0) {
		$output = "null";
	}

	return $output;
}

// Adds scheduled task for scheduler
function addRemovePeriodicTasks() {
	//If there is any activations, ie we need the midnight updater
	$hasActivations = existsActivations();

	if(strtoupper(substr(PHP_OS, 0, 3)) === "WIN") {
		// Windows OS
		addRemoveWindowsPeriodicTasks($hasActivations);
	} else {
		// Linux OS
		addRemoveLinuxPeriodicTasks($hasActivations);
	}
}

// Adds scheduled task for system scheduler
function addSystemScheduledTask($schedule) {
	if(strtoupper(substr(PHP_OS, 0, 3)) === "WIN") {
		// Windows OS
		addWindowsSystemScheduledTask($schedule);
	} else {
		// Linux OS
		addLinuxSystemScheduledTask($schedule);
	}
}

// Function that if needed adds a job to handle the updating of schedules that needs it
function addRemoveWindowsPeriodicTasks($hasActivations) {
	// we have activations configured, add schedule updater
	if($hasActivations) {
		addWindowsScheduledTask("updateschedules");
	} else {
		removeWindowsScheduledTask("updateschedules");
	}

	//Remove dynamic scheduled task if they exist since it's not used anymore
	removeWindowsScheduledTask("dynamic");
}

// Function that if needed adds a job to handle the updating of schedules that needs it
function addRemoveLinuxPeriodicTasks($hasActivations) {
	$tmpFileName = "/tmp/tempcrontab";
	deleteFile($tmpFileName);

	$execStr = "crontab -l > ".$tmpFileName;
	exec($execStr, $output);

	SaveLog("Command: ".$execStr."\nOutput:\n".parseExecOutputToString($output));

	$lines = file($tmpFileName);

	$fh = fopen($tmpFileName, 'w');
	if ($fh === false) {
		die("Could not open '$tmpFileName' for writing");
	}

	foreach($lines as $line) {
		if(strpos($line, "/run.php command=updateschedules") === false && strpos($line, "/run.php command=activation id=dynamic") === false) {
			fwrite($fh, $line);
			SaveLog("Crontab line: ".$line);
		}
	}

	if($hasActivations) {
		$taskUpdaterLine = "01 00 * * * ".$_SESSION[CFG_SESSION_KEY]["settings"]["phpbinpath"]."/php ".HA_ROOT_PATH."/run.php command=updateschedules >/dev/null 2>&1\n";
		fwrite($fh, $taskUpdaterLine);

		SaveLog("Crontab line: ".$taskUpdaterLine);
	}

	fclose($fh);

	$execStr = "crontab ".$tmpFileName;
	exec($execStr, $output);

	SaveLog("Command: ".$execStr."\nOutput:\n".parseExecOutputToString($output));
}

// Function that if needed adds a job to handle the updating of schedules that needs it
function addLinuxSystemScheduledTask($schedule) {
	$tmpFileName = "/tmp/tempcrontab";
	deleteFile($tmpFileName);

	$execStr = "crontab -l > ".$tmpFileName;
	exec($execStr, $output);

	SaveLog("Command: ".$execStr."\nOutput:\n".parseExecOutputToString($output));

	$fh = fopen($tmpFileName, 'a');
	if ($fh === false) {
		die("Could not open '$tmpFileName' for writing");
	}

	$tmp = explode(":", $schedule["settings"][1]);
	$hours = ($tmp[0] * 1);
	$minutes = ($tmp[1] * 1);
	$days = "*";
	$failure = false;

	switch($schedule["settings"][0]) {
		case "daily":
		{
			// getting interval (hh:mm)
			if($hours == 0) {
				$hours = "*";
			}
			else
			{
				$hours = "*/".$hours;
			}

			if($minutes == 0) {
				$minutes = "*";
			}
			else
			{
				$minutes = "*/".$minutes;
			}
			break;
		}
		case "weekly":
		{
			$days = getCrontabDayOfWeekString(explode(",", $schedule["settings"][2]));
			break;
		}
		default:
		{
			$failure = true;
			break;
		}
	}

	if(!$failure) {
		$newScheduleLine = $minutes." ".$hours." * * ".$days." ".trim($schedule["executable"]." ".$schedule["arguments"])." >/dev/null 2>&1\n";
		fwrite($fh, $newScheduleLine);

		SaveLog("Crontab line: ".$newScheduleLine);
	}

	fclose($fh);

	$execStr = "crontab ".$tmpFileName;
	exec($execStr, $output);

	SaveLog("Command: ".$execStr."\nOutput:\n".parseExecOutputToString($output));
}

// Function to add a time to another. Last argument is "+" or "-" for addition or subtraction
function addTimes($time1, $time2, $action) {
	if(strpos($time1, "-") !== false) {
		$time1 = substr($time1, 1);
		if($action == "+") {
			$action = "-";
		} else {
			$action = "+";
		}
	}
	if(strpos($time2, "-") !== false) {
		$time2 = substr($time2, 1);
		if($action == "+") {
			$action = "-";
		} else {
			$action = "+";
		}
	}

	$time1 = explode(":", $time1);
	$time2 = explode(":", $time2);

	$time1[0] = $time1[0] * 1;
	$time1[1] = $time1[1] * 1;
	$time2[0] = $time2[0] * 1;
	$time2[1] = $time2[1] * 1;

	if($action == "+") {
		return date("H:i", mktime(($time1[0] + $time2[0]), ($time1[1] + $time2[1])));
	} else {
		return date("H:i", mktime(($time1[0] - $time2[0]), ($time1[1] - $time2[1])));
	}
}

function getTimeDiffInMinutes($time) {
	$time = explode(":", $time);

	$minutes = ($time[0] * 60) + ($time[1] * 1);

	return $minutes;
}

function deleteAtForActivation($id) {
	$activation = Schedules::getActivations(-1, $id);
	$activation = $activation[0];

	removeScheduledTaskForActivation($activation);
}

function removeScheduledTaskForActivation($activation) {
	if(is_array($activation) && $activation["type"] != "dynamic") {
		if(strtoupper(substr(PHP_OS, 0, 3)) === "WIN") {
			// Windows OS
			removeWindowsScheduledTask("activation", $activation["id"]);
		} else {
			// Linux OS
			deleteAtForActivationLinux($activation);
		}
	}
}

function updateAtForActivation($id) {
	$activation = Schedules::getActivations(-1, $id);
	$activation = $activation[0];

	if(strtoupper(substr(PHP_OS, 0, 3)) === "WIN") {
		// Windows OS
		updateWindowsScheduledTask($activation);
	} else {
		// Linux OS
		updateAtForActivationLinux($activation);
	}
}

function updateAtForSchedule($id = -1) {
	$activations = Schedules::getActivations($id);

	foreach($activations as $activation) {
		if($activation["type"] != "dynamic") {
			if(substr($activation["upcomingruntime"], 0, 10) == date("Y-m-d")) {
				if(strtoupper(substr(PHP_OS, 0, 3)) === "WIN") {
					// Windows OS
					updateWindowsScheduledTask($activation);
				} else {
					// Linux OS
					updateAtForActivationLinux($activation);
				}
			} else {
				removeScheduledTaskForActivation($activation);
			}
		}
	}
}

function deleteAtForSchedule($id) {
	$activations = Schedules::getActivations($id);

	foreach($activations as $activation) {
		removeScheduledTaskForActivation($activation);
	}
}

function updateAtForActivationLinux($activation) {
	deleteAtForActivationLinux($activation);

	$data = $_SESSION[CFG_SESSION_KEY]["settings"]["phpbinpath"]."/php ".HA_ROOT_PATH."/run.php command=activation id=".$activation["id"];
	$time = substr($activation["upcomingruntime"], 11, 5);
	$date = substr($activation["upcomingruntime"], 0, 10);
	$execStr = "echo \"".$data."\" | at ".getLinuxSchedulerNoMailParameter().$time; //." ".$date;
	exec($execStr);
	SaveLog($execStr."\n");

	$execStr = "atq | sort -n";
	$result = exec($execStr);
	SaveLog($execStr."\n");
	$atIdS = explode("\t", $result);
	$atId = $atIdS[0];

	$params = array(
		new dbParam(dbParamType::$INTEGER, "linuxatid", $atId)
	);
	Schedules::saveActivation($params, $activation["id"]);
}

function deleteAtForActivationLinux($activation) {
	$execStr = "atrm ".$activation["linuxatid"];
	exec($execStr);
	SaveLog($execStr."\n");
}

function updateWindowsScheduledTask($activation) {
	removeWindowsScheduledTask("activation", $activation["id"]);
	addWindowsScheduledTask("activation", $activation["upcomingruntime"], $activation["id"]);
}

// Function to add one day to every days in the string.
// 1;4;6 becomes 2;5;7, 3;6;7 becomes 4;7;1
function addDay($days, $separator = ";") {
	if($days != "*" && $days != "-1") {
		$daysArr = explode($separator, $days);

		$days = "";

		foreach($daysArr as $day) {
			$day += 1;

			if($day == 8) {
				$day = 1;
			}

			$days .= $day.$separator;
		}

		$days = trim($days, $separator);
	}

	return $days;
}

// Function to remove schedules connected to a task
function removeWindowsScheduledTask($jobType = "", $id = "") {
	if($id != "") {
		$id = "_".$id;
	}

	$execStr = "schtasks /Delete /TN homeautomation_".$jobType.$id.getWindowsSchedulerForceParameter();

	exec($execStr, $output);

	SaveLog("Command: ".$execStr."\nOutput:\n".parseExecOutputToString($output));
}

// Windows scheduled tasks related function
function addWindowsScheduledTask($jobType, $datetime = "", $id = "") { //$time, $days, $id, $deleteWhenDone = 1, $isScheduleUpdater = false, $hasDynamicActivationsActivationRunner = false) {
	$failure = false;

	$jobName = "";
	$script = "";
	$parameters = "";

	switch($jobType) {
		case "activation":
		{
			$datetime = explode(" ", $datetime);
			$date = explode("-", $datetime[0]);
			$date = $date[2]."/".$date[1]."/".$date[0];

			$time = explode(":", $datetime[1]);
			$time = $time[0].":".$time[1].":".$time[2];

			// Running selected activation at given time
			$jobName = "homeautomation_activation_".$id;
			$script = "run.php command=activation id=".$id;
			$parameters = "/SC ONCE /ST ".$time; //." /SD ".$date;

			break;
		}
		case "updateschedules":
		{
			// Updating schedules every day at 00:01
			$jobName = "homeautomation_updateschedules";
			$script = "run.php command=updateschedules";
			$parameters = "/SC DAILY /ST 00:01:00";

			break;
		}
		default:
		{
			$failure = true;
			break;
		}
	}

	if(!$failure) {
		$application = $_SESSION[CFG_SESSION_KEY]["settings"]["phpbinpath"]."/php.exe";
		$arguments = HA_ROOT_PATH."\\".$script;

		$execStr = 	"schtasks /Create /RU \"".$_SESSION[CFG_SESSION_KEY]["settings"]["sysusername"]."\" /RP ".$_SESSION[CFG_SESSION_KEY]["settings"]["syspassword"]." ".$parameters." /TN ".$jobName." /TR \"".$application." ".$arguments."\"".getWindowsSchedulerForceParameter();

		SaveLog("Command: ".str_replace(array($_SESSION[CFG_SESSION_KEY]["settings"]["sysusername"], $_SESSION[CFG_SESSION_KEY]["settings"]["syspassword"]), "*******", $execStr)."\n");

		_exec($execStr);
	}
}

function addWindowsSystemScheduledTask($schedule) {
	$jobName = "homeautomation_systemschedule_".$schedule["id"];
	$parameters = "";
	// debugVariable($schedule);exit;
	switch($schedule["settings"][0]) {
		case "daily":
		{
			// getting interval (hh:mm)
			$tmp = explode(":", $schedule["settings"][1]);

			// interval = hours * 60 + minutes
			$interval = ($tmp[0] * 60) + ($tmp[1] * 1);

			// Running all days with configured interval
			$parameters = "/SC MINUTE /MO ".$interval;
			break;
		}
		case "weekly":
		{
			// Running all days with configured interval
			$parameters = "/SC WEEKLY /D ".getSchedulerDayOfWeekString(explode(",", $schedule["settings"][2]))." /ST ".$schedule["settings"][1];
			break;
		}
/* At this point we don�t see any use for MONTHLY schedules, leaving them out.
		case "monthly":
		{
			if($schedule["settings"][3] == -1) {
				$schedule["settings"][3] = "*";
			}

			$parameters = "/SC MONTHLY /D ".$schedule["settings"][3]." /M ".getSchedulerMonthNameString(explode(",", $schedule["settings"][2]))." /ST ".$schedule["settings"][1];
			break;
		}
*/
		default:
			break;
	}

	if($parameters != "") {
		$execStr = 	"schtasks /Create /RU \"".$_SESSION[CFG_SESSION_KEY]["settings"]["sysusername"]."\" /RP ".$_SESSION[CFG_SESSION_KEY]["settings"]["syspassword"]." ".$parameters." /TN ".$jobName." /TR \"".$schedule["executable"]." ".$schedule["arguments"]."\"".getWindowsSchedulerForceParameter();

		SaveLog("Command: ".str_replace(array($_SESSION[CFG_SESSION_KEY]["settings"]["sysusername"], $_SESSION[CFG_SESSION_KEY]["settings"]["syspassword"]), "*******", $execStr)."\n");

		_exec($execStr);
	} else {
		SaveLog("No parameters available for system schedule ".$schedule["description"]."(ID: ".$schedule["id"].")");
	}
}

function getWindowsSchedulerForceParameter() {
	$forceParam = "";

	if(defined("CFG_WINDOWS_FORCE_SCHEDULER_AVAILABLE") && CFG_WINDOWS_FORCE_SCHEDULER_AVAILABLE) {
		$forceParam = " /F";
	}

	return $forceParam;
}

function getLinuxSchedulerNoMailParameter() {
	$noMailParam = "";

	if(defined("CFG_LINUX_AT_NOMAIL_AVAILABLE") && CFG_LINUX_AT_NOMAIL_AVAILABLE) {
		$noMailParam = "-M ";
	}

	return $noMailParam;
}

function SaveFile($fileWithPath, $string, $attribute = "a") {
	if($fileWithPath != "" && $string != "") {
		if($attribute == "") {
			$attribute = "a";
		}

		// open file for writing
		$fh = fopen($fileWithPath, $attribute);
		if ($fh === false) {
			die("Could not open '$fileWithPath' for writing");
		}
		fwrite($fh, $string);
		fclose($fh);
	}
}

function deleteFile($fileWithPath) {
	if(file_exists($fileWithPath)) {
		unlink($fileWithPath);
	}
}
/*
function checkInstallation($step, $postData = array()) {
	if(!defined("CFG_SKIP_INSTALL_CHECK") && !CFG_SKIP_INSTALL_CHECK) {
		$installedVersion = getSettings("version");

		if($installedVersion != "-1" && isset($postData["action"]) && $postData["action"] == "clean") {
			$installedVersion = 0;
			Settings::update("version", $installedVersion);
		}

		if($installedVersion == "" || version_compare($installedVersion, THISVERSION, "<")) {
			if(is_dir("install")) {
				// always include english first for fallback, after that include client language.
				include(HA_ROOT_PATH."/resources/languages/install_en.php");
				// missing elements in client language will be in english now.
				if($_SESSION[CFG_SESSION_KEY]["language"] != "en")
				{
					include(HA_ROOT_PATH."/resources/languages/install_".$_SESSION[CFG_SESSION_KEY]["language"].".php");
				}

				defineConstants($installTranslation);

				include(HA_ROOT_PATH."/install/install.php");

				// generateInstallPage($postData, $installedVersion);
			}
			else
			{
				echo LBL_MISSINGINSTALLFOLDER;
			}

			exit;
		}
	}
}
*/
function saveSettingsFile($host, $database, $username = "", $password = "") {
	$data = "<?php

	\$mysqlHost = \"".$host."\";
	\$mysqlDatabase = \"".$database."\";
	\$mysqlUsername = \"".$username."\";
	\$mysqlPassword = \"".$password."\";

?>";

	SaveFile(HA_ROOT_PATH."/system/mysql_settings.php", $data, "w");
}

function doSystemCheck() {
	global $ha;
	global $db;

	$result = "";
	$messageType = SystemMessageType::$ERROR;

	if(is_dir(HA_ROOT_PATH)) {
		if(!defined("CFG_SKIP_INSTALL_CHECK") || !CFG_SKIP_INSTALL_CHECK) {
			$redirectToInstaller = false;

			if(is_dir(HA_ROOT_PATH."/install")) {
				if(!file_exists(HA_ROOT_PATH."/system/mysql_settings.php")) {
					// mysql file is missing and install dir exists, pass the user to installer.
					$redirectToInstaller = true;
				} else {
					$installedVersion = Settings::get("version");

					if($installedVersion !== false && ($installedVersion == "" || version_compare($installedVersion, THISVERSION, "<"))) {
						// nothing or older version installed
						$redirectToInstaller = true;
					}
				}
			} else {
				if(!file_exists(HA_ROOT_PATH."/system/mysql_settings.php")) {
					$result .= LBL_MISSINGINSTALLFOLDER."<br />";
					$result .= "'".HA_ROOT_PATH."/system/mysql_settings.php' file not found.<br />";
				}
			}

			if($redirectToInstaller) {
				resetSession();
				redirectTo("install/install.php");
			}
		}

		if(!file_exists(HA_ROOT_PATH."/system/mysql_settings.php")) {
			$result .= "'".HA_ROOT_PATH."/system/mysql_settings.php' file not found.<br />";
		} else if($db->connect(true) === false) {
			$result .= "Could not establish connection to database, check configuration in '".HA_ROOT_PATH."/system/mysql_settings.php'<br />";
		} else {
			$result .= checkIfFileExists($_SESSION[CFG_SESSION_KEY]["settings"]["phpbinpath"], "php".SYS_FILE_EXT, LBL_SETTINGS_LABELS_PHPPATH);

			if(!defined("CFG_SKIP_INSTALL_CHECK") || !CFG_SKIP_INSTALL_CHECK) {
				if(version_compare(Settings::get("version"), THISVERSION, ">=") && is_dir("install")) {
					$result .= "<form action=\"install/install.php\" method=\"post\"><input type=\"hidden\" name=\"action\" value=\"clean\">".LBL_INSTALLFOLDEREXISTS."<p>".LBL_IFINSTALLCORRUPTED." <input type=\"submit\" class=\"button\" onclick=\"return confirm('".LBL_CONFIRMSTARTCLEANINSTALL."');\" value=\"".LBL_CLICKHERE."\"></form><br />";
					$messageType = SystemMessageType::$WARNING;
				}
			}
		}
	} else {
		$result = "Oops, directory '".HA_ROOT_PATH."' not found";
	}

	if($result != "") {
		$message = new SystemMessage();
		$message->setText($result);
		$message->setType($messageType);
		$ha->addMessage($message);
	}
}

function checkIfFileExists($path, $file, $setting) {
	$result = "";

	if(!is_file($path."/".$file)) {
		$result = str_replace(array("{f}", "{d}", "{s}"), array($file, $path, mb_convert_case($setting, MB_CASE_LOWER, "UTF-8")), LBL_WARNINGFILENOTFOUND);
	}

	return $result;
}

function checkPreRequisities() {
	$result = array("", false);

	$check["php"]["status"] = false;
	$check["php"]["version"] = phpversion();
	$check["php"]["recommended"] = "5.6";
	$check["php"]["text"] = "PHP, ".mb_convert_case(LBL_RECOMMENDEDVERSION, MB_CASE_LOWER, "UTF-8")." ".$check["php"]["recommended"]."+, ".LBL_YOUHAVE." ".$check["php"]["version"];

	if(version_compare($check["php"]["version"], $check["php"]["recommended"], ">=")) {
		$check["php"]["status"] = true;
	}

	$extension = "mysqli";
	$check[$extension] = checkPHPExtension($extension);

	$extension = "mysqlnd";
	$check[$extension] = checkPHPExtension($extension);

	$extension = "gd";
	$check[$extension] = checkPHPExtension($extension);

	// if(strtoupper(substr(PHP_OS, 0, 3)) === "WIN")
	// {
		// $extension = "com_dotnet";
		// $check[$extension] = checkPHPExtension($extension);

		// $setting = "com.allow_dcom";
		// $check[$setting] = checkPHPiniSetting($setting, 1);
	// }

	$check["folder"]["status"] = false;
	$check["folder"]["text"] = str_replace("{folder}", HA_ROOT_PATH, LBL_FOLDERISNOTWRITABLE);

	if(is_writable(HA_ROOT_PATH)) {
		$check["folder"]["status"] = true;
		$check["folder"]["text"] = str_replace("{folder}", HA_ROOT_PATH, LBL_FOLDERISWRITABLE);
	}

	$setting = "date.timezone";
	$check[$setting] = checkPHPiniSetting($setting);

	$setting = "open_basedir";
	$check[$setting] = checkPHPiniSetting($setting, array(""));

	$setting = "allow_url_fopen";
	$check[$setting] = checkPHPiniSetting($setting, array(1, "on"));

	return $check;
}

function checkPHPExtension($extension) {
	$check["status"] = false;
	$check["text"] = str_replace("{ext}", $extension, LBL_PHPEXTENSIONNOTLOADED);

	$extLoaded = extension_loaded($extension);

	if($extLoaded) {
		$check["text"] = str_replace("{ext}", $extension, LBL_PHPEXTENSIONLOADED);
		$check["status"] = true;
	}

	return $check;
}

function checkPHPiniSetting($param, $recommendedValues = "") {
	$check["status"] = false;
	$check["text"] = str_replace("{param}", $param, LBL_SETTINGNOTSETINPHPINI);
	$check["config"] = ini_get($param);

	if($check["config"] !== false) {
		$check["text"] = str_replace("{param}", $param, LBL_SETTINGSETINPHPINI);
		$check["text"] = str_replace("{config}", $check["config"], $check["text"]);

		if(is_array($recommendedValues)) {
			if(in_array(strtolower($check["config"]), $recommendedValues)) {
				$check["status"] = true;
			}
		} else if($recommendedValues == "") { // any setting here is good enough
			$check["status"] = true;
		}
	}

	return $check;
}

// toggles selected devices
// should only be used from run.php
function executeToggleAction($status, $deviceIds = array(), $activationType = "") {
	$devicesToToggle = array();

	foreach($deviceIds as $id) {
		$devices = Devices::get($id);

		if($id == "-1") {
			$devicesToToggle = $devices;
		} else if(count($devices) > 0) {
			$devicesToToggle[] = $devices[0];
		}
	}

	$output = toggleSelected($status, $devicesToToggle, getIpAddress(), $_SESSION[CFG_SESSION_KEY]["userid"], true, $activationType);

	return $devicesToToggle;
}

function SaveLog($message = "", $forceSaveLog = false, $logFile = "debug") {
	if(($_SESSION[CFG_SESSION_KEY]["settings"]["debug"] || $forceSaveLog) && $message != "") {
		SaveFile(
				HA_ROOT_PATH."/logs/".$logFile."_".date("Y-m-d").".txt",
				date($_SESSION[CFG_SESSION_KEY]["settings"]["dateformat"].
				"\t".$_SESSION[CFG_SESSION_KEY]["settings"]["timeformat"]).
				"\t".$_SESSION[CFG_SESSION_KEY]["user"].
				"\t".getIpAddress().
				"\t".preg_replace(array("/\n/", "/\r/", "/\t/"), " ", $message)."\n"
			);
	}
}

// Removes all scheduled tasks
function removeAllScheduledTasks() {
	if(strtoupper(substr(PHP_OS, 0, 3)) === "WIN") {
		// Windows OS
		removeAllWindowsScheduledTasks();
	} else {
		// Linux OS
		removeAllLinuxScheduledTasks();
	}
}

// Removes specific system schedule
function removeSystemScheduledTask($schedule) {
	if(strtoupper(substr(PHP_OS, 0, 3)) === "WIN") {
		// Windows OS
		removeWindowsScheduledTask("systemschedule", $schedule["id"]);
	} else {
		// Linux OS
		removeAllLinuxScheduledTasks(trim($schedule["executable"]." ".$schedule["arguments"]));
	}
}

// Deletes all scheduled tasks for HomeAutomation except systemschedules
// That is only schedules from scheduler are deleted
function removeAllWindowsScheduledTasks() {
	$tasks = array();
	$execStr = "schtasks";
	exec($execStr, $tasks);

	SaveLog("Command: ".$execStr."\nOutput:\n".parseExecOutputToString($tasks), true);

	// echo "<pre>";
	// print_r($tasks);
	// echo "</pre>";

	foreach($tasks as $task) {
		// echo $task."<br />";

		if(strpos($task, "homeautomation_") !== false && strpos($task, "homeautomation_systemschedule_") === false) {
			$taskName = explode(" ", $task);
			$taskName = trim($taskName[0]);

			$execStr = "schtasks /Delete /TN ".$taskName.getWindowsSchedulerForceParameter();
			$result = _exec($execStr);

			SaveLog($execStr."\t".$result."\t(".$task.")\n", true);
		}
	}
}

function removeAllLinuxScheduledTasks($taskToDelete = "/run.php") {
	$tmpFileName = "/tmp/tempcrontab";
	deleteFile($tmpFileName);

	$execStr = "crontab -l > ".$tmpFileName;
	exec($execStr, $output);

	SaveLog("Command: ".$execStr."\nOutput:\n".parseExecOutputToString($output), true);

	$lines = file($tmpFileName);

	$fh = fopen($tmpFileName, "w");
	if ($fh === false) {
		die("Could not open '$tmpFileName' for writing");
	}

	foreach($lines as $line) {
		if(strpos($line, $taskToDelete) === false) {
			fwrite($fh, $line);
		} else {
			SaveLog("Crontab line removed: ".$line, true);
		}
	}

	fclose($fh);

	$execStr = "crontab ".$tmpFileName;
	exec($execStr, $output);

	SaveLog("Command: ".$execStr."\nOutput:\n".parseExecOutputToString($output), true);
}

function rebuildSchedules() {
	removeAllScheduledTasks();
	resetRuntimes();
	addRemovePeriodicTasks();
	updateAtForSchedule();

	$result = LBL_SCHEDULESREBUILT;

	return $result;
}

function resetRuntimes($scheduleId = -1) {
	$activations = Schedules::getActivations($scheduleId);

	foreach($activations as $activation) {
		$schedule = Schedules::get($activation["scheduleid"]);
		$schedule = $schedule[0];

		$params = array(
			new dbParam(dbParamType::$STRING, "upcomingruntime", calculateNextRunTime($schedule["days"], $activation["type"], $activation["params"], true)),
			new dbParam(dbParamType::$STRING, "nextdaysruntime", null)
		);
		Schedules::saveActivation($params, $activation["id"]);
	}

	Schedules::updateRealActivationTimes($scheduleId);
}

function checkVersions() {
	$json = "";
	$updates = array();

	if($debug) {
		error_reporting(E_ALL);
		ini_set('display_errors',1);
	} else {
		ini_set('display_errors',0);
		ini_set('log_errors',1);
	}
	// allow_url_fopen needs to be enabled
	$url = VERSIONSURL."?os=".PHP_OS."&ha=".THISVERSION;
	$handle = @fopen($url, "r");

	if($handle !== false) {
		while(!feof($handle)) {
			$json .= fread($handle, 8192);
		}

		fclose($handle);
	}

	$json = json_decode($json, true);

	if($json !== NULL && array_key_exists("versions", $json)) {
		$versions = $json["versions"];
		if(version_compare($versions["ha"], THISVERSION, ">")) {
			//echo "New version of HA found: ".$versions["ha"]." (".THISVERSION." installed)\n";
			Versions::updateChecks("ha", $versions["ha"]);
			$updates[] = "HomeAutomation v".$versions["ha"];
		}

		$installedPlugins = SystemPlugins::getInstalled();

		foreach($versions["systemplugins"] as $plugin) {
			foreach($plugin as $pluginName => $version) {
				if(array_key_exists($pluginName, $installedPlugins))
				{
					if(version_compare($version, $installedPlugins[$pluginName]["version"], ">"))
					{
						//echo "New version of ".$pluginName." found: ".$version." (".$installedPlugins[$pluginName]["version"]." installed)\n";
						Versions::updateChecks($pluginName, $version);
						$updates[] = $pluginName." v".$version;
					}
				}
			}
		}
	}

	return $updates;
}

?>
