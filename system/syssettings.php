<?php
// HomeAutomation
// Copyright (C) 2021 Tom Rosenback (tom.rosenback@gmail.com), Daniel Malmgren (daniel.malmgren@kolefors.se)
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

///////////////////////////////////////////////////////////////////////////////////////////
// This file handles it settings by it own, this file should not be edited manually.
///////////////////////////////////////////////////////////////////////////////////////////
define("THISVERSION", "3.3.2");
define("THISRELEASEDATE", "2016-08-30");
define("VERSIONSURL", "https://rosenback.me/ha/versions");
define("SCHEMAVERSION", 320);

if(strtoupper(substr(PHP_OS, 0, 3)) === "WIN") {
	// Windows OS
	define("SYS_FILE_EXT", ".exe");
} else {
	define("SYS_FILE_EXT", "");
}

define("SLOW_QUERY_THRESHOLD", 0.02);


?>
