<?php

// HomeAutomation
// Copyright (C) 2021 Tom Rosenback (tom.rosenback@gmail.com), Daniel Malmgren (daniel.malmgren@kolefors.se)
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
if(!defined("HomeAutomationIncluded")) { die("HomeAutomation: Direct access not premitted"); }

// disabling notice messages
error_reporting(E_ALL ^ E_NOTICE);

$username = getFormVariable("login_username", "");
$password = getFormVariable("login_password", "");

$msg = "";
$sysMsg = false;

$autologin = "";

if($username != "" && $password != "") {
	if(Users::authenticate($username, $password)) {
		registerSession($username, getFormVariable("rememberme", false));
	} else {
		$autologin = "&autologin=0";

		$sysMsg = new SystemMessage();
		$sysMsg->setText(LBL_INCORRECTCREDENTIALS);
		$sysMsg->setType(SystemMessageType::$ERROR);
	}
}
else
{
	if(isIpLocal()) {
		registerSession("local");
	} else {
		$sysMsg = new SystemMessage();
		$sysMsg->setText(LBL_USERNAMEANDPASSWORDCANNOTBEEMPTY);
		$sysMsg->setType(SystemMessageType::$ERROR);
	}
}

if($sysMsg !== false) {
	$msg = "&msg=".$sysMsg->encode();
}

redirectTo(THISPAGE."?page=".$this->currentPage["name"].$autologin.$msg);

?>
