<?php
// HomeAutomation
// Copyright (C) 2021 Tom Rosenback (tom.rosenback@gmail.com), Daniel Malmgren (daniel.malmgren@kolefors.se)
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
if(!defined("HomeAutomationIncluded")) { die("HomeAutomation: Direct access not premitted"); }

// disabling notice messages
error_reporting(E_ALL ^ E_NOTICE);

$form = "<div class=\"bold\" style=\"width: 100%;\">
			<form method=\"post\" action=\"".THISPAGE."?action=login\">
				<table style=\"margin: 0 auto;\">
					<tr>
						<td>".LBL_USERNAME.":</td>
						<td><input class=\"text\" type=\"text\" size=\"15\" name=\"login_username\" tabindex=\"1\"></td>
						<td>".LBL_PASSWORD.":</td>
						<td><input class=\"text\" type=\"password\" size=\"15\" name=\"login_password\" tabindex=\"2\"></td>
						<td align=\"center\"><input class=\"button\" type=\"submit\" value=\"".LBL_LOGIN."\" name=\"dologin\" tabindex=\"4\"></td>
					</tr>
					<tr>
						<td colspan=\"5\" align=\"center\">
							<label><input type=\"checkbox\" name=\"rememberme\" align=\"middle\" tabindex=\"3\" />".LBL_REMEMBERME."</label>
							<!--<a href=\"".THISPAGE."?page=logout&action=forgotpassword\">".LBL_FORGOTPASSWORD."</a>-->
						</td>
					</tr>
				</table>
			</form>
		</div>";
		
$this->addBody($form);

?>