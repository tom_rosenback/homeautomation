<?php
// HomeAutomation
// Copyright (C) 2021 Tom Rosenback (tom.rosenback@gmail.com), Daniel Malmgren (daniel.malmgren@kolefors.se)
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
if(!defined("HomeAutomationIncluded")) { die("HomeAutomation: Direct access not premitted"); }

// disabling notice messages
error_reporting(E_ALL ^ E_NOTICE);
	
$form = "<form method=\"post\" action=\"".THISPAGE."?action=login\" data-ajax=\"false\">
			
			<div data-role=\"fieldcontain\" class=\"ui-hide-label\" class=\"no-field-separator\">
				<label for=\"login_username\">".LBL_USERNAME."</label>
				<input type=\"text\" name=\"login_username\" id=\"login_username\" value=\"\" placeholder=\"".LBL_USERNAME."\"/>			
			</div>
			
			<div data-role=\"fieldcontain\" class=\"ui-hide-label\" class=\"no-field-separator\">
				<label for=\"login_password\">".LBL_PASSWORD."</label>
				<input type=\"password\" name=\"login_password\" id=\"login_password\" value=\"\" placeholder=\"".LBL_PASSWORD."\"/>
			</div>

			<div data-role=\"fieldcontain\" class=\"no-field-separator\">
				<input type=\"checkbox\" name=\"rememberme\" id=\"rememberme\" />
				<label for=\"rememberme\">".LBL_REMEMBERME."</label>
			</div>
			
			<div data-role=\"fieldcontain\" class=\"no-field-separator\">
				<button type=\"submit\" data-theme=\"b\" name=\"dologin\" value=\"".LBL_LOGIN."\">".LBL_LOGIN."</button>	
			</div>
		</form>";

$this->addBody($form);

?>