<?php

// HomeAutomation
// Copyright (C) 2021 Tom Rosenback (tom.rosenback@gmail.com), Daniel Malmgren (daniel.malmgren@kolefors.se)
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
if(!defined("HomeAutomationIncluded")) { die("HomeAutomation: Direct access not premitted"); }

// disabling notice messages
error_reporting(E_ALL ^ E_NOTICE);


$list = "
		<div class=\"bold\">".LBL_FORGOTPASSWORD."</div>
		<div>".LBL_FORGOTPASSWORDDESC."</div>
		<div>
			<form method=\"post\" action=\"".THISPAGE."?page=".$this->currentPage["name"]."&action=sendrecovery\">
				<label>".LBL_USERNAME.": <input type=\"text\" name=\"username\" class=\"text3\" /></label>
				<label>".LBL_OREMAIL.": <input type=\"text\" name=\"email\" class=\"text3\" /></label>
				<input type=\"submit\" value=\"".LBL_SUBMIT." name=\"submit\" />
			</form>
		</div>
			";
	
$this->addBody($list);

?>