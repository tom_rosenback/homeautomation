<?php
// HomeAutomation
// Copyright (C) 2021 Tom Rosenback (tom.rosenback@gmail.com), Daniel Malmgren (daniel.malmgren@kolefors.se)
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
if(!defined("HomeAutomationIncluded")) { die("HomeAutomation: Direct access not premitted"); }

// disabling notice messages
error_reporting(E_ALL ^ E_NOTICE);

// user logged in with user level 1 or higher (demo)
if($_SESSION[CFG_SESSION_KEY]["userlevel"] >= 1)
{
	$list = "<table class=\"nowrap\">
				<tr>
					<td class=\"bold\">".LBL_DESCRIPTION."</td>
					<td class=\"bold\">&nbsp;</td>
				</tr>";

	$macros = Macros::get();
	$i = 0;

	foreach($macros as $macro) {
		$id = $macro["id"];
		$desc = $macro["name"];

		$macroImage = getStatusImage("macro", true, $macro["id"], "macros");
		
		$list .= "<tr>
					<td>".$desc."</td>
					<td align=\"center\">
						<a href=\"api.php?do=macros/run&macroid=".$id."&redirect=".THISPAGE."?page=".$this->currentPage["name"]."\" class=\"macro-run\" data-macroid=\"".$id."\">
							<img src=\"".$macroImage."\" border=\"0\" height=\"".$_SESSION[CFG_SESSION_KEY]["settings"]["houseplaniconheight"]."px\" alt=\"".LBL_MACRO.": ".$desc."\" title=\"".LBL_MACRO.": ".$desc."\" />
						</a>
					</td>
				</tr>";

		$i++;
	}

	$list .= "</table>";

	$this->addBody($list);
}

?>
