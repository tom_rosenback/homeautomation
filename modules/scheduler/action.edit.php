<?php
// HomeAutomation
// Copyright (C) 2021 Tom Rosenback (tom.rosenback@gmail.com), Daniel Malmgren (daniel.malmgren@kolefors.se)
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
if(!defined("HomeAutomationIncluded")) { die("HomeAutomation: Direct access not premitted"); }

// disabling notice messages
error_reporting(E_ALL ^ E_NOTICE);

$scheduleId = getFormVariable("scheduleid", 0);

// user logged in and not with user level 1 (demo)
if($_SESSION[CFG_SESSION_KEY]["userlevel"] > 1 && ($scheduleId == -1 || $scheduleId > 0)) {
	$this->addHeader("js-scheduler", "<script language=\"javascript\" type=\"text/javascript\" src=\"modules/scheduler/script.js\"></script>");

	include(HA_ROOT_PATH."/modules/scheduler/generateActivationsList.php");
	include("scripts/sliders.js.php");
	$this->addHeader("js-sliders", $sliders);

	$schedule = array();

	$devices = Devices::get();
	$devices = removeDevicesOfType($devices, "sauna");
	$deviceGroups = DeviceGroups::get();
	$macros = Macros::get();

	$timezone = $_SESSION[CFG_SESSION_KEY]["settings"]["timezone"];
	$latitude = $_SESSION[CFG_SESSION_KEY]["settings"]["latitude"];
	$longitude = $_SESSION[CFG_SESSION_KEY]["settings"]["longitude"];

	$dawn = getTimeOfSunAtPosition("dawn", time(), $latitude, $longitude, $timezone, $schedule["offset_sunrise"]);
	$sunrise = getTimeOfSunAtPosition("sunrise", time(), $latitude, $longitude, $timezone, $schedule["offset_sunrise"]);
	$dusk = getTimeOfSunAtPosition("dusk", time(), $latitude, $longitude, $timezone, $schedule["offset_sunset"]);
	$sunset = getTimeOfSunAtPosition("sunset", time(), $latitude, $longitude, $timezone, $schedule["offset_sunset"]);

	$list = "<table width=\"100%\" cellspacing=\"0\">";

	$resetSortingLink .= "&nbsp;-&nbsp;<a href=\"".THISPAGE."?page=".$this->currentPage["name"]."\" id=\"resetSorting\" title=\"".LBL_CONFIRMRESETSORTING."\">".LBL_RESETSORTING."</a>";

	$deviceListDisplay = " style=\"display: none;\"";
	$allDevicesChecked = " checked=\"checked\"";

	$deviceGroupListDisplay = " style=\"display: none;\"";
	$macroListDisplay = " style=\"display: none;\"";

	$dayListDisplay = "";
	$allDaysChecked = "";
	$enabledChecked = "";

	if($scheduleId == -1) {
		$schedule["id"] = $scheduleId;
		$schedule["name"] = "";
		$schedule["comment"] = "";
		$schedule["scenario"] = -100;
		$schedule["devices"] = array();
		$schedule["devicegroups"] = array();
		$schedule["macros"] = array();
		$schedule["days"] = array();
		$schedule["activations"][0] = array("id" => -1, "params" => getDefaultActivationParams());
		$enabledChecked = " checked=\"checked\"";
		$deviceListDisplay = "";
	} else {
		$schedule = Schedules::get($scheduleId);
		$schedule = $schedule[0];
		$schedule["devices"] = explode(";", $schedule["devices"]);
		$schedule["devicegroups"] = explode(";", $schedule["devicegroups"]);
		$schedule["macros"] = explode(";", $schedule["macros"]);
		$schedule["days"] = explode(";", $schedule["days"]);
		$schedule["activations"] = Schedules::getActivations($schedule["id"]);

		if($schedule["devices"][0] != -1) {
			$deviceListDisplay = "";
			$allDevicesChecked = "";
		}

		if($schedule["days"][0] == -1) {
			$dayListDisplay = " style=\"display: none;\"";
			$allDaysChecked = " checked=\"checked\"";
		}

		if($schedule["devicegroups"][0] != "") {
			$deviceGroupListDisplay = "";
		}

		if($schedule["macros"][0] != "") {
			$macroListDisplay = "";
		}

		if($schedule["enabled"] == 1) {
			$enabledChecked = " checked=\"checked\"";
		}
	}

	$list .= "	<tr>
					<td>
						<form id=\"scheduler\" method=\"post\" action=\"".THISPAGE."?page=".$this->currentPage["name"]."\">
							<input type=\"hidden\" name=\"action\" id=\"action\" value=\"\" />
							<table style=\"width: 100%;\">
								<tr>
									<td style=\"width: 100px;\" class=\"bold\">
										<label for=\"enabled\">".LBL_ENABLED.":</label>
									</td>
									<td colspan=\"3\">
										<input type=\"checkbox\" id=\"enabled\" name=\"enabled\" value=\"1\"".$enabledChecked.">
									</td>
								</tr>
								<tr>
									<td class=\"bold\">
										<label for=\"name\">".LBL_NAME.":</label>
									</td>
									<td colspan=\"3\">
										<input type=\"text\" id=\"name\" name=\"name\" class=\"text3\" value=\"".$schedule["name"]."\">
										<div id=\"noNameGiven\" style=\"display: none;\" class=\"error\">".LBL_ENTERSCHEDULENAME."</div>
									</td>
								</tr>
								<tr>
									<td style=\"width: 100px;\" class=\"bold\">
										<label for=\"scenario\">".LBL_SCENARIO.":</label>
									</td>
									<td colspan=\"3\">
										".generateSelector($schedule["scenario"], "scenario", "text2", "id", "name", Scenarios::get(), -1, "")."
									</td>
								</tr>
								<tr>
									<td valign=\"top\" class=\"bold\">
										<label for=\"comment\">".LBL_COMMENT.":</label>
									</td>
									<td colspan=\"3\">
										<textarea name=\"comment\" class=\"textarea\">".$schedule["comment"]."</textarea>
									</td>
								</tr>
								<tr>
									<td valign=\"top\" class=\"bold\" style=\"height: 10px;\">
										<label for=\"allDevicesSelector\">".LBL_ACTIVEDEVICES.":</label>
									</td>
									<td valign=\"top\" style=\"width: 250px;\" class=\"bold\">
										<label for=\"allDevicesSelector\"><input type=\"checkbox\" id=\"allDevicesSelector\"".$allDevicesChecked.">".LBL_ALLDEVICES."</label> <a href=\"#\" onclick=\"return false;\" id=\"deviceListToggler\">".LBL_SHOWHIDE."</a>
										<ul id=\"deviceList\"".$deviceListDisplay." style=\"width: 100%\">";
	$i = 0;

	foreach($devices as $device) {
		$deviceChecked = "";

		if($schedule["devices"][0] == -1 || in_array($device["id"], $schedule["devices"])) {
			$deviceChecked = " checked=\"checked\"";
		}

		$list .= "<li>
					<label><input type=\"checkbox\" name=\"devices[".$i."]\" id=\"devices_".$i."\" value=\"".$device["id"]."\"".$deviceChecked.">".$device["description"]."</label>
				</li>";

		$i++;
	}


	$list .= "							</ul>
									</td>
									<td valign=\"top\" style=\"width: 100px;\" class=\"bold\">
										<label for=\"allDaysSelector\">".LBL_ACTIVEDAYS.":</label>
									</td>
									<td valign=\"top\" class=\"bold\" rowspan=\"2\">
										<label for=\"allDaysSelector\"><input type=\"checkbox\" id=\"allDaysSelector\"".$allDaysChecked.">".LBL_ALLDAYS."</label> <a href=\"#\" onclick=\"return false;\" id=\"dayListToggler\">".LBL_SHOWHIDE."</a>
										<ul id=\"dayList\"".$dayListDisplay.">";

	for($i = 1; $i <= 7; $i++) {
		$dayChecked = "";

		if($schedule["days"][0] == -1 || in_array($i, $schedule["days"])) {
			$dayChecked = " checked=\"checked\"";
		}

		$list .= "<li>
					<label><input type=\"checkbox\" name=\"days[".$i."]\" id=\"days_".$i."\" value=\"".$i."\"".$dayChecked.">".getDayName($i)."</label>
				</li>";
	}

	$list .= "							</ul>
										<div id=\"noDaysSelected\" style=\"display: none;\" class=\"error\">".LBL_ONEDAYMUSTBESELECTED."</div>
									</td>
								</tr>
								<tr>
									<td valign=\"top\" class=\"bold\">
										".LBL_ACTIVEGROUPS.":
									</td>
									<td valign=\"top\" style=\"width: 250px;\" class=\"bold\">
										<a href=\"#\" onclick=\"return false;\" id=\"deviceGroupListToggler\">".LBL_SHOWHIDE."</a>
										<ul id=\"deviceGroupList\"".$deviceGroupListDisplay.">";
	$i = 0;

	foreach($deviceGroups as $group) {
		$groupChecked = "";
		$deviceGroupRelations = "";
		$separator = "";

		foreach($group["relations"] as $relation) {
			$deviceGroupRelations .= $separator.$relation["deviceid"];
			$separator = ";";
		}

		if(is_array($schedule["devicegroups"])) {
			if(in_array($group["id"], $schedule["devicegroups"])) {
				$groupChecked = " checked=\"checked\"";
			}
		}

		$list .= "<li>
					<label><input type=\"checkbox\" name=\"devicegroups[".$i."]\" id=\"devicegroups_".$i."\" value=\"".$group["id"]."\"".$groupChecked.">".$group["name"]."</label>
					<input type=\"hidden\" id=\"devicegrouprelations_".$i."\" value=\"".$deviceGroupRelations."\" />
				</li>";

		$i++;
	}


	$list .= "							</ul>
										<div id=\"noDevicesOrGroupsSelected\" style=\"display: none;\" class=\"error\">".LBL_DEVICEORGROUPMUSTBESELECTED."</div>
									</td>
								</tr>
								<tr>
									<td valign=\"top\" class=\"bold\">
										".LBL_ACTIVEMACROS.":
									</td>
									<td valign=\"top\" style=\"width: 250px;\" class=\"bold\">
										<a href=\"#\" onclick=\"return false;\" id=\"macroListToggler\">".LBL_SHOWHIDE."</a>
										<ul id=\"macroList\"".$macroListDisplay.">";
	$i = 0;

	foreach($macros as $macro) {
		$macroChecked = "";

		if(is_array($schedule["macros"])) {
			if(in_array($macro["id"], $schedule["macros"])) {
				$macroChecked = " checked=\"checked\"";
			}
		}

		$list .= "<li>
					<label><input type=\"checkbox\" name=\"macros[".$i."]\" id=\"macros_".$i."\" value=\"".$macro["id"]."\"".$macroChecked.">".$macro["name"]."</label>
				</li>";

		$i++;
	}


	$list .= "				</ul>
									</td>
								</tr>
							</table>
							<table style=\"width: 100%;\">
								<tr>
									<td class=\"bold\">
										<br />
										".LBL_ACTIVATIONS."
									</td>
									<td align=\"right\" valign=\"bottom\" class=\"bold\">
										<a href=\"#\" id=\"addActivation\" onclick=\"return false;\">".LBL_ADDACTIVATION."</a>
									</td>
								</tr>
								<tr>
									<td colspan=\"2\">
										<ul id=\"activationList\">
											<!-- list of activations for current schedule -->
											".generateActivationsList($schedule["activations"])."
										</ul>
									</td>
								</tr>
								<tr>
									<td>
										<ul>
											<li id=\"scheduleErrors\" style=\"display: none;\"><b>".LBL_CORRECTERRORSTOCONTINUE."</b>
												<ul>
													<li id=\"noNameError\" style=\"display: none;\"><b>- ".LBL_ENTERSCHEDULENAME."</b></li>
													<li id=\"noDevicesOrGroupsError\" style=\"display: none;\"><b>- ".LBL_DEVICEORGROUPMUSTBESELECTED."</b></li>
													<li id=\"noDaysError\" style=\"display: none;\"><b>- ".LBL_ONEDAYMUSTBESELECTED."</b></li>
												</ul>
											</li>
											<li id=\"noActivations\" class=\"error\" style=\"display: none;\">".LBL_ACTIVATIONSMUSTBECONFIGURED."</li>
										</ul>
									</td>
									<td align=\"right\">";
	if($schedule["id"] != -1) {
		$list .= "						<label><input type=\"checkbox\" name=\"saveascopy\" id=\"saveascopy\">".LBL_SAVEASCOPY."</label>
										<input type=\"submit\" name=\"delete\" value=\"".LBL_DELETE."\" class=\"button setActionByButton\" onclick=\"return confirm('".LBL_CONFIRMDELETE."?');\" tabindex=\"1000\" />";
	}

	$list .= "							<input type=\"submit\" name=\"cancel\" value=\"".LBL_CANCEL."\" class=\"button setActionByButton\" />&nbsp;<input type=\"submit\" id=\"saveBtn\" name=\"save\" value=\"".LBL_SAVE."\" class=\"button setActionByButton\" />
									</td>
								</tr>
							</table>
							<input type=\"hidden\" id=\"dawn\" value=\"".$dawn."\" />
							<input type=\"hidden\" id=\"sunrise\" value=\"".$sunrise."\" />
							<input type=\"hidden\" id=\"dusk\" value=\"".$dusk."\" />
							<input type=\"hidden\" id=\"sunset\" value=\"".$sunset."\" />
							<input type=\"hidden\" id=\"errorTime\" value=\"".LBL_INCORRECTTIMEFORMAT."\" />
							<input type=\"hidden\" name=\"scheduleid\" value=\"".$schedule["id"]."\" />
						</form>
					</td>
				</tr>
			</table>";

	$this->addBody($list);
}
else
{
	redirectTo(THISPAGE."?page=".$this->currentPage["name"]);
}

?>
