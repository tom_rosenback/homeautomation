<?php
// HomeAutomation
// Copyright (C) 2021 Tom Rosenback (tom.rosenback@gmail.com), Daniel Malmgren (daniel.malmgren@kolefors.se)
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
if(!defined("HomeAutomationIncluded")) { die("HomeAutomation: Direct access not premitted"); }

// disabling notice messages
error_reporting(E_ALL ^ E_NOTICE);

// user logged in with user level 1 or higher (demo)
if($_SESSION[CFG_SESSION_KEY]["userlevel"] >= 1) {
	$list = "<table width=\"100%\" cellspacing=\"0\">";

	$resetSortingLink .= "&nbsp;-&nbsp;<a href=\"".THISPAGE."?page=".$this->currentPage["name"]."\" id=\"resetSorting\" title=\"".LBL_CONFIRMRESETSORTING."\">".LBL_RESETSORTING."</a>";

	$list .= "<tr>
				<td class=\"bold\">
					".LBL_SCHEDULES."
				</td>
			</tr>
			<tr>
				<td>
					<table cellspacing=\"0\" class=\"tablelist\" id=\"sortableList\">
						<thead>
							<tr>
								<td class=\"bold\" width=\"20px\">
									&nbsp;
								</td>
								<td class=\"bold\" width=\"50px\">
									".LBL_SCENARIO."
								</td>
								<td class=\"bold\" width=\"250px\">
									".LBL_NAME."
								</td>
								<td class=\"bold\" width=\"250px\">
									".LBL_COMMENT."
								</td>
								<td class=\"bold\" width=\"80px\">
									".LBL_DAYS."
								</td>
								<td class=\"bold\" width=\"250px\">
									".LBL_DEVICESGROUPS."
								</td>
							</tr>
						</thead>";

	$schedules = Schedules::get();

	if(count($schedules) > 0) {
		$list .= "<tbody>";

		foreach($schedules as $schedule) {
			$activeDevicesGroupsMacros = "";
			$days = "";
			$linebreak = "";

			if($schedule["scenario"] == -100) {
				$schedule["scenarioname"] = LBL_ALL;
			}

			if($schedule["devices"] == -1) {
				$activeDevicesGroupsMacros = LBL_ALLDEVICES;
				$linebreak = "<br />";
			} else {
				$tmp = explode(";", $schedule["devices"]);
				$linebreak = "";

				foreach($tmp as $deviceId) {
					if($deviceId != "") {
						$device = Devices::get($deviceId);

						$activeDevicesGroupsMacros .= $linebreak."- ".$device[0]["description"];
						$linebreak = "<br />";
					}
				}
			}

			$tmp = explode(";", $schedule["devicegroups"]);

			foreach($tmp as $groupId) {
				if($groupId != "") {
					$group = DeviceGroups::get($groupId);

					$activeDevicesGroupsMacros .= $linebreak."- ".LBL_GROUP.": ".$group[0]["name"];
					$linebreak = "<br />";
				}
			}

			$tmp = explode(";", $schedule["macros"]);

			foreach($tmp as $macroId) {
				if($macroId != "") {
					$macro = Macros::get($macroId);

					$activeDevicesGroupsMacros .= $linebreak."- ".LBL_MACRO.": ".$macro[0]["name"];
					$linebreak = "<br />";
				}
			}

			if($schedule["days"] == "1;2;3;4;5") {
				$days = LBL_WEEKDAYS;
			} else if($schedule["days"] == "6;7") {
				$days = LBL_WEEKENDS;
			} else {
				$tmp = explode(";", $schedule["days"]);
				$linebreak = "";

				foreach($tmp as $day) {
					$days .= $linebreak;

					if($day != -1) {
						$days .= "- ";
					}

					$days .= getDayName($day);
					$linebreak = "<br>";
				}
			}

			$stateImg = "disabled.png";
			$action = "enable";
			$enableDisableText = LBL_ENABLE;

			if($schedule["enabled"] == "1") {
				$stateImg = "enabled.png";
				$action = "disable";
				$enableDisableText = LBL_DISABLE;
			}

			$list .= "<tr id=\"schedules_".$schedule["id"]."\">
									<td style=\"padding-left: 2px; padding-right: 2px;\">
										<a href=\"".THISPAGE."?page=".$this->currentPage["name"]."&action=".$action."&scheduleid=".$schedule["id"]."\"><img src=\"./resources/".$stateImg."\" alt=\"\" title=\"".$enableDisableText."\" border=\"0\" width=\"12px\"></a>
									</td>
									<td>".$schedule["scenarioname"]."</td>
									<td>".$schedule["name"]."<br>(".$schedule["numactivations"]." ".LBL_ACTIVATIONSSUFFIX.")</td>
									<td>".$schedule["comment"]."</td>
									<td>".$days."</td>
									<td valign=\"middle\">".$activeDevicesGroupsMacros."</td>
									<td valign=\"middle\">
										<a href=\"".THISPAGE."?page=".$this->currentPage["name"]."&action=copy&scheduleid=".$schedule["id"]."\" alt=\"".LBL_COPY."\" title=\"".LBL_COPY."\"><img src=\"./resources/copy.png\" border=\"0\" width=\"20\"></a>
										<a href=\"".THISPAGE."?page=".$this->currentPage["name"]."&action=edit&scheduleid=".$schedule["id"]."\"><img src=\"./resources/edit.png\" border=\"0\" width=\"20\"></a>
										<a href=\"".THISPAGE."?page=".$this->currentPage["name"]."&action=delete&scheduleid=".$schedule["id"]."\" onclick=\"return confirm('".LBL_CONFIRMDELETE." ".$schedule["name"]."?');\"><img src=\"./resources/delete.png\" border=\"0\" width=\"20\"></a>
									</td>
								</tr>";
		}

		$list .= "</tbody>";
	} else {
		$list .= "<tfoot>
						<tr>
						<td colspan=\"7\" class=\"bold\">
							<br><br>
							".LBL_NOSCHEDULESCREATED."
						</td>
					</tr>
				</tfoot>";

		$resetSortingLink = "";
	}

	$list .= "</table>
		</td>
	</tr>
	<tr>
		<td class=\"bold\">
			<a href=\"".THISPAGE."?page=".$this->currentPage["name"]."&action=edit&scheduleid=-1\">".LBL_CREATENEWSCHEDULE."</a>&nbsp;-&nbsp;
			<a href=\"".THISPAGE."?page=".$this->currentPage["name"]."&action=rebuild\" onclick=\"return confirm('".LBL_CONFIRMREBUILDSCHEDULES."');\">".LBL_REBUILDSCHEDULES."</a>
			".$resetSortingLink."
		</td>
	</tr>";

	$list .= "</table>";

	$this->addBody($list);
}

?>
