function initialize() {
	initializeSlidersWithoutAction(true);

	$('#activationList li:visible:odd').removeClass().addClass('odd');
	$('#activationList li:visible:even').removeClass().addClass('even');

	$('a[id^="deleteActivation_"]').unbind("click").click(function() {
		thisIdNumber = getIdNumber(this.id);

		$("#activationDeleted_" + thisIdNumber).val("1");

		$("#activationListItem_" + thisIdNumber).fadeOut("slow", function()
		{
			$('#activationList li:visible:odd').removeClass().addClass('odd');
			$('#activationList li:visible:even').removeClass().addClass('even');

			if($("#activationList li:visible").length == 0) {
				$("#noActivations").show();
			}
		});
	});

	$("select[id^='activationType_']").unbind("change").change(function() {
		thisIdNumber = getIdNumber(this.id);

		showActivationTypeConfiguration(thisIdNumber, $(this).val());
	}).change();

	$("input[id^='activationStatus_']").unbind("change").change(function() {
		thisIdNumber = getIdNumber(this.id);
		selectedActivationType = $("#activationType_" + thisIdNumber).val();

		if($(this).val() == "1" && selectedActivationType != "engineheater") {
			checkAll("devices_", "-1");
			$("#activationAbsDimDevices_" + thisIdNumber).slideDown();
		} else {
			$("#activationAbsDimDevices_" + thisIdNumber).slideUp();
		}
	});

	$("#activationList .validateTime").unbind("keyup").keyup(function() {
		thisIdNumber = getIdNumber(this.id);
		validateTime($(this).val(), thisIdNumber);
	});

	$("select[id^='sunActivity_']").unbind("change").change(function() {
		if($(this).is(":visible")) {
			thisIdNumber = getIdNumber(this.id);
			calculcateSunActivation(thisIdNumber);
		}
	}).change();

	$("input[id^='sunOffset_']").unbind("keyup").keyup(function() {
		if($(this).is(":visible")) {
			thisIdNumber = getIdNumber(this.id);
			calculcateSunActivation(thisIdNumber);
		}
	});
}

function validateTime(time, currentIdNumber) {
	var regex = new RegExp("^[-+]?(([0-1]?[0-9])|([2][0-3])):([0-5]?[0-9])?$");

	if(regex.test(time) == true) {
		$("#activationError_" + currentIdNumber).html("");
		validationOk = true;
	} else {
		$("#activationError_" + currentIdNumber).html($("#errorTime").val());
		validationOk = false;
	}

	return validationOk;
}

function calculcateSunActivation(currentIdNumber) {
	sunOffset = $("#sunOffset_" + currentIdNumber).val();
	sunActivity = $("#sunActivity_" + currentIdNumber).val();
	time = $("#" + sunActivity).val().split(":");
	offset = new Array(0,0);

	if(validateTime(sunOffset, currentIdNumber)) {
		offset = sunOffset.split(":");

		if(offset[0].match("-")) {
			offset[1] = -offset[1];
		}
	}

	var sunActivation = new Date();
	sunActivation.setHours((time[0] * 1) + (offset[0] * 1));
	sunActivation.setMinutes((time[1] * 1) + (offset[1] * 1));

	$("#sunActivationToday_" + currentIdNumber).html(padTime(sunActivation.getHours()) + ':' + padTime(sunActivation.getMinutes()));
}

function getIdNumber(idString) {
	idNumber = idString.substr(idString.lastIndexOf("_") + 1);

	return idNumber;
}

function padTime(value) {
	if(value < 10) {
		value = "0" + value;
	}

	return value;
}

function showActivationTypeConfiguration(currentIdNumber, selectedActivationType) {
	hideAllActivationConfigurations(currentIdNumber);

	switch(selectedActivationType) {
		case "static": {
			$("#activationStatusDiv_" + currentIdNumber).show();
			$("#activationTime_" + currentIdNumber).show();

			break;
		}
		case "suncontrolled": {
			$("#activationStatusDiv_" + currentIdNumber).show();
			$("#activationSun_" + currentIdNumber).show();

			break;
		}
		case "engineheater": {
			$("#activationEngineHeater_" + currentIdNumber).show();

			break;
		}
		case "random": {
			$("#activationStatusDiv_" + currentIdNumber).show();
			$("#activationRandom_" + currentIdNumber).show();

			break;
		}
		case "dynamic": {
			$("#activationStatusDiv_" + currentIdNumber).show();
			$("#activationDynamic_" + currentIdNumber).show();
			$("#activationError_" + currentIdNumber).html("");

			break;
		}
		case "gcal": {
			$("#activationGCal_" + currentIdNumber).show();
			$("#activationStatusDiv_" + currentIdNumber).show();
			break;
		}
		default: {
			break;
		}
	}

	if($("#activationStatus_" + currentIdNumber + ":checked").val() == "1" && selectedActivationType != "engineheater") {
		$("#activationAbsDimDevices_" + currentIdNumber).slideDown();
	} else {
		$("#activationAbsDimDevices_" + currentIdNumber).slideUp();
	}

	$("#activationList .validateTime:visible").each(function(index) {
		thisIdNumber = getIdNumber(this.id);
		validateTime($(this).val(), thisIdNumber);
	});
}

function hideAllActivationConfigurations(currentIdNumber) {
	$("#activationStatusDiv_" + currentIdNumber).hide();
	$("#activationTime_" + currentIdNumber).hide();
	$("#activationSun_" + currentIdNumber).hide();
	$("#activationRandom_" + currentIdNumber).hide();
	$("#activationEngineHeater_" + currentIdNumber).hide();
	$("#activationDynamic_" + currentIdNumber).hide();
	$("#activationGCal_" + currentIdNumber).hide();
}

function toggleAbsDimDevices(currentDevice) {
	if(currentDevice.is(":checked")) {
		$("tr[id^='activationAbsDimLevel_" + currentDevice.val() + "']").each(function() {
			$(this).show();
		});
	} else {
		$("tr[id^='activationAbsDimLevel_" + currentDevice.val() + "']").each(function() {
			$(this).hide();
		});
	}
}

function checkValidation() {
	validationErrors = 0;
	validationOk = false;

	$(".error", "#scheduler").each(function(index) {
		if($(this).is(":visible") && $(this).html() != "") {
			validationErrors++;
		}
	});

	scheduleNameError = validateScheduleName();

	if(scheduleNameError == 1) {
		$("#noNameError").show();
	} else {
		$("#noNameError").hide();
	}

	validationErrors += scheduleNameError;
	selectedDays = checkSelections("days_");

	if(!selectedDays[0]) {
		$("#noDaysError").show();
		validationErrors++;
	} else {
		$("#noDaysError").hide();
	}

	result = checkDeviceAndGroupSelections();
	deviceSelections = result[0];
	deviceGroupsSelections = result[1];
	macrosSelections = result[2];

	if(!deviceSelections[0] && !deviceGroupsSelections[0] && !macrosSelections[0]) {
		validationErrors++;
		$("#noDevicesOrGroupsError").show();
	} else {
		$("#noDevicesOrGroupsError").hide();
	}

	if(validationErrors > 0) {
		validationOk = false;
		$("#scheduleErrors").show();
	} else {
		validationOk = true;
		$("#scheduleErrors").hide();
	}

	return validationOk;
}

function validateScheduleName() {
	error = 0;

	if($("#name").val() == "")
	{
		error = 1;
		$("#noNameGiven").show();
	}
	else
	{
		$("#noNameGiven").hide();
	}

	return error;
}

function checkAll(selector, selected) {
	$("input[id^='" + selector + "']").each(function(index)
	{
		if(selected != "-1")
		{
			$("input[id^='" + selector + "']").attr("checked", selected);
		}

		if(selector == "devices_")
		{
			toggleAbsDimDevices($(this));
		}
	});
}

function checkDeviceAndGroupSelections() {
	deviceSelections = checkSelections("devices_");
	deviceGroupsSelections = checkSelections("devicegroups_");
	macroSelections = checkSelections("macros_");

	if(!deviceSelections[0] && !deviceGroupsSelections[0] && !macroSelections[0]) {
		$("#noDevicesOrGroupsSelected").show();
	} else {
		$("#noDevicesOrGroupsSelected").hide();
	}

	return new Array(deviceSelections, deviceGroupsSelections, macroSelections);
}

$(document).ready(function() {
	$(".setActionByButton").click(function()
	{
		var action = this.name;

		$("#action").val(action)
	});

	$("#saveBtn").click(function()
	{
		return checkValidation();
	});

	$("#name").blur(function()
	{
		validateScheduleName();
	});

	$("#addActivation").click(function()
	{
		$.get("api.php", { "do": "activations/add", "ts": (new Date()) }, function(data)
		{
			// add new activation
			$("#activationList").append(data.replace(/{n}/g, ($("#activationList").children().length + 1)));
			// bind functions to dynamic content
			initialize();
			// display correct configuration inputs and validate activation type
			currentIdNumber = getIdNumber($("#activationList li:last").attr("id"));
			showActivationTypeConfiguration(currentIdNumber, $("#activationType_" + currentIdNumber).val());
			$("#activationList li:last").animate({ opacity: "1"}, {	duration: "slow" });
			$('#noActivations').hide();
		});
	});

	$("#allDevicesSelector").change(function()
	{
		allSelected = $(this).is(":checked");

		// $("input[id^='devicegroups_']").attr("checked", allSelected);

		if(!allSelected)
		{
			$("#deviceList").slideDown();
			$("#noDevicesSelected").show();
		}
		else
		{
			$("#noDevicesSelected").hide();
		}

		checkAll("devices_", allSelected);
		checkDeviceAndGroupSelections();
	});

	$("input[id^='devices_']").change(function()
	{
		if(!$(this).is(":checked"))
		{
			$("#allDevicesSelector").attr("checked", false);
		}

		result = checkDeviceAndGroupSelections();
		deviceSelections = result[0];

		$("#allDevicesSelector").attr("checked", deviceSelections[1]);

		toggleAbsDimDevices($(this));

	}).change();

	$("input[id^='devicegroups_']").change(function()
	{
		checkDeviceAndGroupSelections();
	}).change();

	$("input[id^='days_']").change(function()
	{
		if(!$(this).is(":checked"))
		{
			$("#allDaysSelector").attr("checked", false);
		}

		selections = checkSelections("days_");

		if(!selections[0])
		{
			$("#noDaysSelected").show();
		}
		else
		{
			$("#noDaysSelected").hide();
		}

		$("#allDaysSelector").attr("checked", selections[1]);
	}).change();

	$("#allDaysSelector").change(function()
	{
		allSelected = $(this).is(":checked");

		if(!allSelected) {
			$("#dayList").slideDown();
			$("#noDaysSelected").show();
		} else {
			$("#noDaysSelected").hide();
		}

		checkAll("days_", allSelected);
	});

	$("#deviceListToggler").click(function () {
		$("#deviceList").slideToggle();
	});

	$("#dayListToggler").click(function () {
		$("#dayList").slideToggle();
	});

	$("#deviceGroupListToggler").click(function () {
		$("#deviceGroupList").slideToggle();
	});

	$("#macroListToggler").click(function () {
		$("#macroList").slideToggle();
	});



	initialize();

	$(function()
	{
		$("#activationList").sortable(
			{
				axis: 'y',
				cursor: 'move',
				stop: function(event, ui)
				{
					$('#activationList li:visible:odd').removeClass().addClass('odd');
					$('#activationList li:visible:even').removeClass().addClass('even');
				}
			}
		);

	});
});
