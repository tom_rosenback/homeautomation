<?php

// HomeAutomation
// Copyright (C) 2021 Tom Rosenback (tom.rosenback@gmail.com), Daniel Malmgren (daniel.malmgren@kolefors.se)
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
if(!defined("HomeAutomationIncluded")) { die("HomeAutomation: Direct access not premitted"); }

// disabling notice messages
error_reporting(E_ALL ^ E_NOTICE);

function generateActivationsList($activations = array(), $newItem = false)
{
	$activationTypes = getActivationTypes();

	$dimlevels = getDimlevels();

	$devices = Devices::get();
	$devicesAndTempSensors = array();

	if(count($devices) > 0) {
		$devicesAndTempSensors[] = array("id" => "optgroup_start", "description" => LBL_DEVICES);
		$devicesAndTempSensors = array_merge($devicesAndTempSensors, $devices);
		$devicesAndTempSensors[] = array("id" => "optgroup_end", "description" => "");
	}

	$tempSensors = Sensors::get();

	if($_SESSION[CFG_SESSION_KEY]["settings"]["useexternaltemperature"]) {
		$tempSensors[] = array("id" => "external", "description" => LBL_EXTTEMP.": ".$_SESSION[CFG_SESSION_KEY]["settings"]["externaltemplocation"]);
	}

	if(count($tempSensors) > 0) {
		$devicesAndTempSensors[] = array("id" => "optgroup_start", "description" => LBL_SENSORS);
		$devicesAndTempSensors = array_merge($devicesAndTempSensors, $tempSensors);
		$devicesAndTempSensors[] = array("id" => "optgroup_end", "description" => "");
	}

	$dimmableDevices = Devices::get(-1, 1, true);

	$operators = getOperators();

	$activationList = "";
	$itemNumber = 0;

	if($newItem == true) {
		$itemNumber = "{n}";
		$activations[] = array();
		$activations[0]["id"] = -1;
		$activations[0]["params"] = getDefaultActivationParams();
		$opacity = " style=\"opacity: 0;\"";
	}

	if(is_array($activations)) {
		foreach($activations as $activation) {
			$statusChecked = array("", "");

			if($activation["params"]["status"] == "")
			{
				$statusChecked[0] = " checked=\"checked\"";
			}
			else
			{
				$statusChecked[$activation["params"]["status"]] = " checked=\"checked\"";
			}

			$sunActivity = array("", "", "", "");
			$sunActivity[$activation["params"]["sunactivity"]] = " selected=\"selected\"";
			$latitude = $_SESSION[CFG_SESSION_KEY]["settings"]["latitude"];
			$longitude = $_SESSION[CFG_SESSION_KEY]["settings"]["longitude"];
			$timezone = $_SESSION[CFG_SESSION_KEY]["settings"]["timezone"];

			$activationList .= "
						<li id=\"activationListItem_".$itemNumber."\"".$opacity.">
							<div class=\"activation\">
								<!--<div class=\"left\">
									".$activationId."
								</div>-->

								<div class=\"left\">
									".generateSelector($activation["type"], "activation[".$itemNumber."][type]", "text2", "id", "text", $activationTypes, -1, "", false, "", "activationType_".$itemNumber)."
								</div>

								<div id=\"activationStatusDiv_".$itemNumber."\" class=\"left\"> <!-- static, sun, random, dynamic -->
									<label><input type=\"radio\" value=\"1\" name=\"activation[".$itemNumber."][status]\" id=\"activationStatus_".$itemNumber."\"".$statusChecked[1].">".LBL_ON."</label>
									<label><input type=\"radio\" value=\"0\" name=\"activation[".$itemNumber."][status]\" id=\"activationStatus_".$itemNumber."\"".$statusChecked[0].">".LBL_OFF."</label>
								</div>

								<div id=\"activationTime_".$itemNumber."\" class=\"left\"> <!-- static -->
									<label for=\"time_".$itemNumber."\">".LBL_TIME.":</label><input type=\"text\" name=\"activation[".$itemNumber."][static][time]\" id=\"time_".$itemNumber."\" class=\"validateTime text\" value=\"".$activation["params"]["time"]."\" maxlength=\"5\" style=\"z-index: 1000;\">
								</div>

								<div id=\"activationSun_".$itemNumber."\" class=\"left\" style=\"display: none;\"> <!-- sun -->
									<label>".ucfirst(LBL_AT).":</label>
									<select class=\"text\" name=\"activation[".$itemNumber."][sun][activity]\" id=\"sunActivity_".$itemNumber."\">
										<option value=\"dawn\"".$sunActivity["dawn"].">".LBL_DAWN."</option>
										<option value=\"sunrise\"".$sunActivity["sunrise"].">".LBL_SUNRISE."</option>
										<option value=\"dusk\"".$sunActivity["dusk"].">".LBL_DUSK."</option>
										<option value=\"sunset\"".$sunActivity["sunset"].">".LBL_SUNSET."</option>
									</select>
									<label>".ucfirst(LBL_OFFSET).":</label><input type=\"text\" name=\"activation[".$itemNumber."][sun][offset]\" id=\"sunOffset_".$itemNumber."\" class=\"validateTime text\" value=\"".$activation["params"]["offset"]."\" maxlength=\"6\">
									<label>".LBL_ACTIVATIONTODAY.":</label><span id=\"sunActivationToday_".$itemNumber."\">".getTimeOfSunAtPosition($activation["params"]["sunactivity"], time(), $latitude, $longitude, $timezone, $activation["params"]["offset"])."</span>
								</div>

								<div id=\"activationRandom_".$itemNumber."\" class=\"left\" style=\"display: none;\"> <!-- random -->
									<label>".LBL_EARLIEST.":</label><input type=\"text\" name=\"activation[".$itemNumber."][random][earliest]\" id=\"randomEarliest_".$itemNumber."\" class=\"validateTime text\" value=\"".$activation["params"]["earliest"]."\" maxlength=\"5\">
									<label>".LBL_LATEST.":</label><input type=\"text\" name=\"activation[".$itemNumber."][random][latest]\" id=\"randomLatest_".$itemNumber."\" class=\"validateTime text\" value=\"".$activation["params"]["latest"]."\" maxlength=\"5\">
								</div>

								<div id=\"activationEngineHeater_".$itemNumber."\" class=\"left\" style=\"display: none;\"> <!-- engineheater -->
									<label for=\"departure_".$itemNumber."\">".LBL_TIMEFORDEPATURE.":</label><input type=\"text\" name=\"activation[".$itemNumber."][engineheater][departure]\" id=\"departure_".$itemNumber."\" class=\"validateTime text\" value=\"".$activation["params"]["departure"]."\" maxlength=\"5\">
									<label>".LBL_TEMPSENSOR.":</label>
									".generateSelector($activation["params"]["tempsensor"], "activation[".$itemNumber."][engineheater][tempsensor]", "text2", "id", "description", $tempSensors, -1, LBL_SELECTTEMPSENSOR, false, "", "tempsensor_".$itemNumber)."
								</div>

								<div id=\"activationDynamic_".$itemNumber."\" class=\"left\" style=\"display: none;\"> <!-- dynamic -->
									<label>".LBL_DEVICE.":</label>
									".generateSelector($activation["params"]["dyndevice"], "activation[".$itemNumber."][dynamic][device]", "text2", "id", "description", $devicesAndTempSensors, -1, LBL_SELECTDEVICE, false, "", "dyndevice_".$itemNumber)."
									".generateSelector($activation["params"]["dynoperator"], "activation[".$itemNumber."][dynamic][operator]", "text2", "0", "1", $operators, -1, LBL_SELECTOPERATOR, false, "", "dynoperator_".$itemNumber)."
									<input type=\"text\" name=\"activation[".$itemNumber."][dynamic][value]\" class=\"text\" value=\"".$activation["params"]["dynvalue"]."\">
								</div>";

		if(count(getPlugins("", "gcal")) != 0) {
			$activationList .= "<div id=\"activationGCal_".$itemNumber."\" class=\"left\" style=\"display: none;\"> <!-- google calendar -->
				<label>".ucfirst(LBL_AT).":</label>
				<select class=\"text\" name=\"activation[".$itemNumber."][gcal][activity]\" id=\"gcalActivity_".$itemNumber."\">
					<option value=\"beginning\"".$gcalActivity["beginning"].">".LBL_BEGINNING."</option>
					<option value=\"end\"".$gcalActivity["end"].">".LBL_END."</option>
				</select>
				<label>".LBL_KEYWORD.":</label>
				<input type=\"text\" name=\"activation[".$itemNumber."][gcal][keyword]\" class=\"text\" value=\"".$activation["params"]["gcalkeyword"]."\">
			</div>";
		}

		$activationList .= "	<!-- show abs dimmable devices here also if status = 1 selected -->
								<div id=\"activationAbsDimDevices_".$itemNumber."\" class=\"left\" style=\"display: none;\">
									<table cellspacing=\"0\" cellpadding=\"0\">";

			foreach($dimmableDevices as $device)
			{
				if($activation["params"]["dimlevel"][$device["id"]] == "")
				{
					$activation["params"]["dimlevel"][$device["id"]] = $_SESSION[CFG_SESSION_KEY]["settings"]["dimlevelstep"];
				}

				$activationList .= "	<tr id=\"activationAbsDimLevel_".$device["id"]."_".$itemNumber."\">
											<td>
												<label>".$device["description"]."</label>
											</td>
											<td>
												<div style=\"margin-top: 2px; margin-left: 5px;\">
													<div class=\"device-slider2\" id=\"status-slider".$device["id"]."-".$itemNumber."\" data-sliderid=\"".$device["id"]."-".$itemNumber."\" style=\"z-index: 1000;\"></div>
													<div id=\"slider-val-".$device["id"]."-".$itemNumber."\" style=\"margin-left: 10px; float: right;\">".$activation["params"]["dimlevel"][$device["id"]]."</div>
													<input type=\"hidden\" value=\"".$activation["params"]["dimlevel"][$device["id"]]."\" id=\"status-val-".$device["id"]."-".$itemNumber."\" name=\"activation[".$itemNumber."][absdimlevel][".$device["id"]."]\" />
												</div>
											</td>
										</tr>";
			}

			$activationList .=		"</table>
								</div>

								<div id=\"activationError_".$itemNumber."\" class=\"left error\">
								</div>

								<div class=\"right\" style=\"margin-right: 3px;\">
									<a href=\"#\" id=\"deleteActivation_".$itemNumber."\" onclick=\"return false;\"><img src=\"./resources/delete.png\" border=\"0\" width=\"15\" alt=\"".LBL_DELETE."\" title=\"".LBL_DELETE."\"></a>
								</div>
								<input type=\"hidden\" name=\"activation[".$itemNumber."][id]\" value=\"".$activation["id"]."\" />
								<input type=\"hidden\" name=\"activation[".$itemNumber."][deleted]\" id=\"activationDeleted_".$itemNumber."\" value=\"0\" />
							</div>
						</li>";

			if($newItem == false)
			{
				$itemNumber++;
			}
		}
	}

	return $activationList;
}

?>
