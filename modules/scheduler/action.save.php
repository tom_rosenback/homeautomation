<?php

// HomeAutomation
// Copyright (C) 2021 Tom Rosenback (tom.rosenback@gmail.com), Daniel Malmgren (daniel.malmgren@kolefors.se)
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
if(!defined("HomeAutomationIncluded")) { die("HomeAutomation: Direct access not premitted"); }

// disabling notice messages
error_reporting(E_ALL ^ E_NOTICE);

$scheduleId = getFormVariable("scheduleid", 0);

// user logged in and not with user level 1 (demo)
if($_SESSION[CFG_SESSION_KEY]["userlevel"] > 1 && ($scheduleId == -1 || $scheduleId > 0)) {
	$devices = getFormVariable("devices", array());
	$deviceGroups = getFormVariable("devicegroups", array());
	$macros = getFormVariable("macros", array());
	$days = getArrayAsString(getFormVariable("days", array()), 7);

	$params = array(
		new dbParam(dbParamType::$STRING, "name", getFormVariable("name", "")),
		new dbParam(dbParamType::$STRING, "comment", getFormVariable("comment", "")),
		new dbParam(dbParamType::$INTEGER, "scenario", getFormVariable("scenario", -100)),
		new dbParam(dbParamType::$INTEGER, "enabled", getFormVariable("enabled", 0)),
		new dbParam(dbParamType::$STRING, "devices", getArrayAsString($devices, count(removeDevicesOfType(Devices::get(), "sauna")))),
		new dbParam(dbParamType::$STRING, "devicegroups", getArrayAsString($deviceGroups, (count($deviceGroups) + 1))), // +1 because we don´´t use "all groups"
		new dbParam(dbParamType::$STRING, "macros", getArrayAsString($macros, count($macros) + 1)),
		new dbParam(dbParamType::$STRING, "days", $days)
	);

	$saveAsCopy = convertToBoolean(getFormVariable("saveascopy", "off"));

	if($saveAsCopy) {
		$schedule = Schedules::get($scheduleId);
		$schedule = $schedule[0];

		$scheduleId = -1;

		$params[] = new dbParam(dbParamType::$STRING, "name", $schedule["name"]." (".LBL_COPY.")");
		$params[] = new dbParam(dbParamType::$INTEGER, "sort", $schedule["sort"]);
	}

	$scheduleId = Schedules::save($params, $scheduleId);

	$i = 0;

	$activations = getFormVariable("activation", array());

	if(is_array($activations)) {
		foreach($activations as $activation) {
			$params = array();

			if($saveAsCopy && $activation["deleted"] != "0") {
				$activation["id"] = -1;
			}

			$type = $activation["type"];
			$status = $activation["status"];
			$absdimlevel = "";
			$activationParams = "";

			if($activation["deleted"] == "0") {
				if($status == 1 && is_array($devices)) {
					$separator = "";
					//deviceid#dimlevel|deviceid#dimlevel

					if(is_array($activation["absdimlevel"])) {
						foreach($activation["absdimlevel"] as $deviceId => $dimlevel) {
							if(in_array($deviceId, $devices)) {
								$absdimlevel .= $separator.$deviceId."#".$dimlevel;
								$separator = "|";
							}
						}

						if($absdimlevel != "") {
							$absdimlevel = ";".$absdimlevel;
						}
					}
				}

				$params[] = new dbParam(dbParamType::$INTEGER, "scheduleid", $scheduleId);
				$params[] = new dbParam(dbParamType::$STRING, "type", $type);
				$params[] = new dbParam(dbParamType::$INTEGER, "sort", $i);

				switch($type) {
					case "static": {
						// status;time;deviceid#dimlevel|deviceid#dimlevel
						$activationParams = $status.";".
											$activation["static"]["time"].
											$absdimlevel;
						break;
					}
					case "suncontrolled": {
						// status;sunactivity;offset;deviceid#dimlevel|deviceid#dimlevel
						$activationParams = $status.";".
											$activation["sun"]["activity"].";".
											$activation["sun"]["offset"].
											$absdimlevel;
						break;
					}
					case "engineheater": {
						// status;departure;sensor
						$activationParams = "1;". // status ON for engineheater
											$activation["engineheater"]["departure"].";".
											$activation["engineheater"]["tempsensor"];
						break;
					}
					case "random": {
						// status;earliest;latest;deviceid#dimlevel|deviceid#dimlevel
						$activationParams = $status.";".
											$activation["random"]["earliest"].";".
											$activation["random"]["latest"].
											$absdimlevel;
						break;
					}
					case "dynamic": {
						// status;device;operator;value;deviceid#dimlevel|deviceid#dimlevel
						$activationParams = $status.";".
											$activation["dynamic"]["device"].";".
											$activation["dynamic"]["operator"].";".
											$activation["dynamic"]["value"].
											$absdimlevel;
						break;
					}
					case "gcal": {
						// status;gcalactivity;gcalkeyword;deviceid#dimlevel|deviceid#dimlevel
						$activationParams = $status.";".
											$activation["gcal"]["activity"].";".
											$activation["gcal"]["keyword"].
											$absdimlevel;
						break;
					}
					default:
						break;
				}

				$params[] = new dbParam(dbParamType::$STRING, "params", $activationParams);
				$params[] = new dbParam(dbParamType::$STRING, "upcomingruntime", calculateNextRunTime($days, $type, generateActivationParamArray($type, $activationParams), true));

				$activationId = Schedules::saveActivation($params, $activation["id"]);
				$i++;
			} else {
				if(!$saveAsCopy) { // we don't need to delete activations if we are copying a schedule as they don�t exist yet
					deleteAtForActivation($activation["id"]);
					Schedules::deleteActivation($activation["id"]);
				}
			}
		}
	}

	//if($scheduleId > 0)
	//{
	//	removeSchedules($scheduleId);
	//}

	// echo "<pre>";
	// print_r($data);
	// echo "</pre>";

	resetRuntimes($scheduleId);
	updateAtForSchedule($scheduleId);
	addRemovePeriodicTasks();
}

redirectTo(THISPAGE."?page=".$this->currentPage["name"]);

?>
