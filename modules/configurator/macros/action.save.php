<?php

// HomeAutomation
// Copyright (C) 2021 Tom Rosenback (tom.rosenback@gmail.com), Daniel Malmgren (daniel.malmgren@kolefors.se)
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
if(!defined("HomeAutomationIncluded")) { die("HomeAutomation: Direct access not premitted"); }

// disabling notice messages
error_reporting(E_ALL ^ E_NOTICE);

$id = getFormVariable("id", -1);

// user logged in with user level 3 (admin)
if($_SESSION[CFG_SESSION_KEY]["userlevel"] >= 3 && ($id == -1 || $id > 0)) {
	$params = array(
		new dbParam(dbParamType::$STRING, "name", getFormVariable("name", "")),
		new dbParam(dbParamType::$STRING, "comment", getFormVariable("comment", "")),
		new dbParam(dbParamType::$INTEGER, "scenario", getFormVariable("scenario", -100))
	);
	
	$id = Macros::save($params, $id);
	Macros::deleteRelations($id);

	$devices = getFormVariable("devices", array());
	$statuses = getFormVariable("statuses", array());

	foreach($devices as $key => $device) {
		$params = array(
			new dbParam(dbParamType::$INTEGER, "macroid", $id),
			new dbParam(dbParamType::$INTEGER, "deviceid", $device),
			new dbParam(dbParamType::$INTEGER, "status", isset($statuses[$key]) ? $statuses[$key] : 0)
		);

		Macros::saveRelations($params);
	}

	if($_POST["reseticons"] == "on") {
		deleteFile(HA_ROOT_PATH."/resources/icons/macros/".$id."_on.png");
	}
	else if($_POST["uploadicons"] == "on") {
		saveUploadedFile($_FILES["icon_on"], HA_ROOT_PATH."/resources/icons/macros/".$id."_on.png", "jpg|png|gif|jpeg");
	}

	$id = -1;
}

redirectTo(THISPAGE."?page=".$this->currentPage["name"]);

?>
