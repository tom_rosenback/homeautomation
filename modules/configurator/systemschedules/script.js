$(document).ready(function()
{
	$(".allSelector").change(function()
	{
		tmp = this.id.split("_");
		childClass = tmp[1];
		
		allSelected = $(this).is(":checked");
		
		if(allSelected)
		{	
			$("." + childClass).attr("checked", "checked");
			//$("#noDevicesSelected").hide();
		}
		else
		{	
			$("." + childClass).removeAttr("checked");
			$("#" + childClass + "List").slideDown();
			//$("#noDevicesSelected").show();
		}
	});
	
	$(".selector").change(function()
	{
		tmp = this.id.split("_");
		id = tmp[0] + "_";
		selections = checkSelections(id);
		
		if(!selections[1])
		{
			$("#allSelector_" + tmp[0]).removeAttr("checked");
		}
		else
		{
			$("#allSelector_" + tmp[0]).attr("checked", "checked");
		}
	});
	
	$(".listToggler").click(function()
	{
		tmp = this.id.split("_");
		childClass = tmp[1];
		
		$("#" + childClass + "List").slideToggle();
		
	});
	
	
});