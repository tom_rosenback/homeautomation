<?php
// HomeAutomation
// Copyright (C) 2021 Tom Rosenback (tom.rosenback@gmail.com), Daniel Malmgren (daniel.malmgren@kolefors.se)
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
if(!defined("HomeAutomationIncluded")) { die("HomeAutomation: Direct access not premitted"); }

// disabling notice messages
error_reporting(E_ALL ^ E_NOTICE);

$resetSortingLink = "<a href=\"".THISPAGE."?page=".$this->currentPage["name"]."\" id=\"resetSorting\" title=\"".LBL_CONFIRMRESETSORTING."\">".LBL_RESETSORTING."</a>";

// user logged in with user level 1 or higher (demo)
if($_SESSION[CFG_SESSION_KEY]["userlevel"] >= 3)
{
	$schedules = SystemSchedules::get();
		
	$list = "<table width=\"100%\">
				<tr>
					<td class=\"bold\">
						".$this->currentPage["translation"]."
						<br />
					</td>
				</tr>
				<tr>
					<td>
						<table cellspacing=\"0\" cellpadding=\"3\" width=\"90%\" class=\"tablelist\" id=\"sortableList\">
							<thead>
								<tr>
									<td class=\"bold\">&nbsp;</td>
									<td class=\"bold\">".LBL_DESCRIPTION."</td>
									<td class=\"bold\">".LBL_EXECUTABLE."</td>
									<td class=\"bold\">".LBL_ARGUMENTS."</td>
									<td class=\"bold\">".LBL_SETTINGS."</td>
									<td>&nbsp;</td>
								</tr>
							</thead>";
				
	if(count($schedules) > 0) {
		$list .= "<tbody>";
		
		foreach($schedules as $schedule) {
			$stateImg = "disabled.png";
			$action = "enable";
			$enableDisableText = LBL_ENABLE;

			if($schedule["enabled"] == "1")
			{
				$stateImg = "enabled.png";
				$action = "disable";
				$enableDisableText = LBL_DISABLE;
			}
			
			
			$constArray = get_defined_constants(true);
			$constArray = $constArray["user"];

			$settings = $constArray["LBL_".strtoupper($schedule["settings"][0])];
			
			switch($schedule["settings"][0])
			{
				case "daily":
				{
					$settings .= ", ".mb_convert_case(LBL_INTERVAL, MB_CASE_LOWER, "UTF-8")." ".$schedule["settings"][1];
					break;
				}
				case "weekly":
				{
					$prefix = mb_convert_case(LBL_EVERY, MB_CASE_LOWER, "UTF-8");
					if($schedule["settings"][2] == -1)
					{
						$prefix = mb_convert_case(LBL_ALL, MB_CASE_LOWER, "UTF-8");
					}
					
					$settings .= " ".LBL_AT." ".$schedule["settings"][1].", ".$prefix." ".getDayNames(explode(",", $schedule["settings"][2]));
					break;
				}
				case "monthly":
				{
					$settings .= " ".LBL_AT." ".$schedule["settings"][1].", ".mb_convert_case(LBL_EVERY, MB_CASE_LOWER, "UTF-8")." ".replaceLastOccurance(",", " ".LBL_AND, preg_replace("/,/", ", ", $schedule["settings"][3]))." ".LBL_IN." ".getMonthNames(explode(",", $schedule["settings"][2]));
					break;
				}
				default:
					break;
			}
			
			$list .= "<tr id=\"systemschedules_".$schedule["id"]."\">
						<td><a href=\"".THISPAGE."?page=".$this->currentPage["name"]."&action=".$action."&id=".$schedule["id"]."\"><img src=\"./resources/".$stateImg."\" alt=\"\" title=\"".$enableDisableText."\" border=\"0\" width=\"12px\"></a></td>
						<td>".$schedule["description"]."</td>
						<td style=\"white-space: nowrap;\">".$schedule["executable"]."</td>
						<td style=\"white-space: nowrap;\">".$schedule["arguments"]."</td>
						<td>".$settings."</td>
						<td align=\"right\" width=\"45px\">";
						
			
				$list .= "	<a href=\"".THISPAGE."?page=".$this->currentPage["name"]."&action=edit&id=".$schedule["id"]."\"><img src=\"./resources/edit.png\" border=\"0\" width=\"20\" alt=\"".LBL_EDIT."\" title=\"".LBL_EDIT."\"></a>
							<a href=\"".THISPAGE."?page=".$this->currentPage["name"]."&action=delete&id=".$schedule["id"]."\" onclick=\"return confirm('".LBL_CONFIRMDELETE." ".$schedule["description"]."?');\"><img src=\"./resources/delete.png\" border=\"0\" width=\"20\" alt=\"".LBL_DELETE."\" title=\"".LBL_DELETE."\"></a>";
			
			$list .="		&nbsp;</td>
					</tr>";
		}
		
		$list .= "</tbody>";
		$resetSortingLink = "&nbsp;-&nbsp;".$resetSortingLink;
	}
	else
	{
		$list .= "	<tfoot>
						<tr>
							<td colspan=\"3\" class=\"bold\">".LBL_NOSYSTEMSCHEDULESCONFIGURED."</td>
						</tr>
					</tfoot>";
		
		$resetSortingLink = "";
	}
	
	$list .= "</table>
			</td>
		</tr>
		<tr>
			<td></td>
		</tr>
		<tr>
			<td class=\"bold\">
				<a href=\"".THISPAGE."?page=".$this->currentPage["name"]."&action=edit&id=-1\">".LBL_CREATENEWSYSTEMSCHEDULE."</a>
				".$resetSortingLink."
			</td>
		</tr>
	</table>";
		
	$this->addBody($list);
}
else
{
	redirectTo(THISPAGE);
}

?>