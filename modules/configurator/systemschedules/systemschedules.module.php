<?php
// HomeAutomation
// Copyright (C) 2021 Tom Rosenback (tom.rosenback@gmail.com), Daniel Malmgren (daniel.malmgren@kolefors.se)
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
if(!defined("HomeAutomationIncluded")) { die("HomeAutomation: Direct access not premitted"); }

$this->addHeader("js-configurator", "<script language=\"javascript\" type=\"text/javascript\" src=\"modules/configurator/script.js\"></script>");
$this->addHeader("js-systemschedules", "<script language=\"javascript\" type=\"text/javascript\" src=\"modules/configurator/systemschedules/script.js\"></script>");
$this->addHeader("js-jquery-ui-timepicker", "<script type=\"text/javascript\" type=\"text/javascript\" src=\"plugins/jquery/jquery-ui/jquery-ui-timepicker-addon.js\"></script>");
$this->addHeader("css-systemschedules", "<link rel=\"stylesheet\" href=\"modules/configurator/systemschedules/systemschedules.css\" type=\"text/css\" media=\"screen\" />");

include(HA_ROOT_PATH."/modules/configurator/systemschedules/script.js.php");
$this->addHeader("js-systemschedule-php", $script);

?>
