<?php
// HomeAutomation
// Copyright (C) 2021 Tom Rosenback (tom.rosenback@gmail.com), Daniel Malmgren (daniel.malmgren@kolefors.se)
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

$script = "
<script type=\"text/javascript\">
	$(document).ready(function() {	
		if($(\"#systemschedulemanager\").length) {
			$(\"#time\").timepicker({
				timeFormat: \"hh:mm\",
				stepHour: 1,
				stepMinute: 1,
				ampm: false,
				showButtonPanel: false,
				timeOnlyTitle: '".LBL_CHOOSETIME."',
				timeText: '".LBL_TIME."',
				hourText: '".LBL_HOUR."',
				minuteText: '".LBL_MINUTE."'
			});
			
			$(\"#interval\").timepicker({
				timeFormat: \"hh:mm\",
				stepHour: 1,
				stepMinute: 1,
				hourMax: 11,
				ampm: false,
				showButtonPanel: false,
				timeOnlyTitle: '".LBL_CHOOSEINTERVAL."',
				timeText: '".LBL_INTERVAL."',
				hourText: '".LBL_HOUR."',
				minuteText: '".LBL_MINUTE."'
			});
			
			$(\"#period\").change(function()
			{
				$(\".interval\").hide();
				$(\".\" + $(this).val()).show();
				
			}).change();
		}
	});
</script>";

?>