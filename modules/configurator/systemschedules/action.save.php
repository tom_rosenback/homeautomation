<?php

// HomeAutomation
// Copyright (C) 2021 Tom Rosenback (tom.rosenback@gmail.com), Daniel Malmgren (daniel.malmgren@kolefors.se)
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
if(!defined("HomeAutomationIncluded")) { die("HomeAutomation: Direct access not premitted"); }

// disabling notice messages
error_reporting(E_ALL ^ E_NOTICE);

$id = getFormVariable("id", -1);

$msg = "";
$sysMsg = false;

$action = "";

// user logged in with user level 3 (admin)
if($_SESSION[CFG_SESSION_KEY]["userlevel"] >= 3 && ($id == -1 || $id > 0)) {
	// save schedule

	if($id > 0 && strtoupper(substr(PHP_OS, 0, 3)) !== "WIN") {
		$schedule = SystemSchedules::get($id);
		$schedule = $schedule[0];

		removeSystemScheduledTask($schedule);
	}

	$executable = getFormVariable("executable", "");

	if(file_exists($executable)) {
		$period = getFormVariable("period", "");
		$settings = $period.";";

		switch($period) {
			case "daily": {
				// type;interval => daily;00:05
				$settings .= getFormVariable("interval", "00:05");
				break;
			}
			case "weekly": {
				// type;time;days => weekly;12:45;1,2,3,7
				$settings .= getFormVariable("time", "12:00").";".getArrayAsString($_POST["days"], 7, ",");
				break;
			}
/* At this point we don´t see any use for MONTHLY schedules, leaving them out.
			case "monthly":
			{
				// type;time;months;days => monthly;12:45;3,5,6;1,5,10,11,30
				$settings .= getFormVariable("time", "12:00").";".getArrayAsString($_POST["months"], 12, ",").";".getArrayAsString($_POST["monthdays"], 31, ",");
				break;
			}
*/
			default:
				break;
		}

		// save schedule to database

		$params = array(
			new dbParam(dbParamType::$INTEGER, "enabled", getFormVariable("enabled", 0)),
			new dbParam(dbParamType::$STRING, "description", getFormVariable("description", "")),
			new dbParam(dbParamType::$STRING, "executable", $executable),
			new dbParam(dbParamType::$STRING, "arguments", getFormVariable("arguments", "")),
			new dbParam(dbParamType::$STRING, "settings", $settings)
		);

		$id = SystemSchedules::save($params, $id);

		if($id !== false) {
			$schedule = SystemSchedules::get($id);
			$schedule = $schedule[0];

			// add scheduled task
			addSystemScheduledTask($schedule);

			$sysMsg = new SystemMessage();
			$sysMsg->setText(LBL_SAVED_SUCCESSFULLY);
			$sysMsg->setType(SystemMessageType::$SUCCESS);
			$sysMsg->setAutoClose(true);
		} else {
			$sysMsg = new SystemMessage();
			$sysMsg->setText(LBL_FAILED_SAVING);
			$sysMsg->setType(SystemMessageType::$ERROR);
			$sysMsg->setAutoClose(true);
		}

	} else {
		$sysMsg = new SystemMessage();
		$sysMsg->setText(LBL_EXECUTABLEDOESNTEXIST);
		$sysMsg->setType(SystemMessageType::$ERROR);
	}

	if($sysMsg !== false) {
		$msg = "&msg=".$sysMsg->encode();
	}
}

redirectTo(THISPAGE."?page=".$this->currentPage["name"].$msg.$action);

?>
