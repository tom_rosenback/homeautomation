<?php

// HomeAutomation
// Copyright (C) 2021 Tom Rosenback (tom.rosenback@gmail.com), Daniel Malmgren (daniel.malmgren@kolefors.se)
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
if(!defined("HomeAutomationIncluded")) { die("HomeAutomation: Direct access not premitted"); }

// disabling notice messages
error_reporting(E_ALL ^ E_NOTICE);

$id = getFormVariable("id", -1);

// user logged in with user level 3 (admin)
if($_SESSION[CFG_SESSION_KEY]["userlevel"] >= 3 && ($id == -1 || $id > 0))
{
	$schedule = array();

	if($id > 0) {
		$schedule = SystemSchedules::get($id);
		$schedule = $schedule[0];
	}
	else
	{
		$schedule["id"] 			= -1;
		$schedule["enabled"]		= 1;
		$schedule["description"]	= "";
		$schedule["executable"] 	= "";
		$schedule["arguments"] 		= "";
		$schedule["time"] 			= date($_SESSION[CFG_SESSION_KEY]["settings"]["timeformat"]);
		$schedule["settings"]		= array("daily");
	}
	
	/*
		type;interval => daily;00:05
		type;time;days => weekly;12:45;1,2,3,7
		type;time;months;days => monthly;12:45;3,5,6;1,5,10,11,30
	*/
	
	$selectedPeriod = array("daily" => "", "weekly" => "", "monthly" => "");
	$selectedPeriod[$schedule["settings"][0]] = " selected=\"selected\"";
	
	$time = "";
	$interval = "";
	
	if($schedule["settings"][0] == "daily") {
		$interval = $schedule["settings"][1];
	}
	else
	{
		$time = $schedule["settings"][1];
	}
	
	$enabledChecked = "";
	if($schedule["enabled"] == 1) {
		$enabledChecked = " checked=\"checked\"";
	}

	$list .= "<table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">
				<tr>
					<td class=\"bold\">".$this->currentPage["translation"]."</td>
				</tr>
				<tr>
					<td>
						<form id=\"systemschedulemanager\" method=\"post\" action=\"".THISPAGE."?page=".$this->currentPage["name"]."\">
							<input type=\"hidden\" name=\"id\" id=\"id\" value=\"".$schedule["id"]."\">
							<input type=\"hidden\" name=\"action\" id=\"action\" value=\"\">
							<table width=\"700px\">
								<tr>
									<td class=\"bold\" width=\"150px\"><label for=\"enabled\">".LBL_ENABLED.":</label></td>
									<td><input type=\"checkbox\" id=\"enabled\" name=\"enabled\" value=\"1\"".$enabledChecked."></td>
								</tr>
								<tr>
									<td class=\"bold\" valign=\"top\"><label for=\"description\">".LBL_DESCRIPTION.":</label></td>
									<td><textarea name=\"description\" class=\"textarea\">".$schedule["description"]."</textarea></td>
								</tr>
								<tr>
									<td class=\"bold\"><label for=\"executable\">".LBL_EXECUTABLE.":</label></td>
									<td><input type=\"text\" name=\"executable\" class=\"text3\" value=\"".$schedule["executable"]."\"></td>
								</tr>
								<tr>
									<td class=\"bold\"><label for=\"arguments\">".LBL_ARGUMENTS.":</label></td>
									<td><input type=\"text\" name=\"arguments\" class=\"text3\" value=\"".$schedule["arguments"]."\"></td>
								</tr>
								<tr>
									<td class=\"bold\"><label for=\"period\">".LBL_PERIOD.":</label></td>
									<td>
										<select name=\"period\" id=\"period\" class=\"text2\">
											<option value=\"daily\"".$selectedPeriod["daily"].">".LBL_DAILY."</option>
											<option value=\"weekly\"".$selectedPeriod["weekly"].">".LBL_WEEKLY."</option>
											<!--<option value=\"monthly\"".$selectedPeriod["monthly"].">".LBL_MONTHLY."</option>-->
										</select>
									</td>
								</tr>
								<tr class=\"interval daily\">
									<td class=\"bold\" valign=\"top\">".LBL_INTERVAL.":</td>
									<td><input type=\"text\" name=\"interval\" id=\"interval\" class=\"text\" value=\"".$interval."\" />
								</tr>
								<tr class=\"interval weekly monthly\" style=\"display: none;\">
									<td class=\"bold\"><label for=\"time\">".LBL_TIME.":</label></td>
									<td>
										<input type=\"text\" name=\"time\" id=\"time\" class=\"text\" value=\"".$time."\" />
									</td>
								</tr>								
								<tr class=\"interval weekly monthly\" style=\"display: none;\">
									<td class=\"bold\" valign=\"top\">".LBL_ACTIVEFOR.":</td>
									<td class=\"bold\">
										<div class=\"interval weekly\">
											<label for=\"allSelector_day\"><input type=\"checkbox\" id=\"allSelector_day\" class=\"allSelector\"".$allDaysChecked.">".LBL_ALLDAYS."</label> <a href=\"#\" onclick=\"return false;\" id=\"listToggler_day\" class=\"listToggler\">".LBL_SHOWHIDE."</a>
											<ul id=\"dayList\">";
											
										$selections = explode(",", $schedule["settings"][2]);
										
										for($i = 1; $i <= 7; $i++)
										{
											$checked = "";

											if($schedule["settings"][0] == "weekly" && ($schedule["settings"][2] == -1 || in_array($i, $selections)))
											{
												$checked = " checked=\"checked\"";
											}

											$list .= "<li>
														<label><input type=\"checkbox\" name=\"days[".$i."]\" id=\"day_".$i."\" class=\"selector day\" value=\"".$i."\"".$checked.">".getDayName($i)."</label>
													</li>";
										}
											
	$list .= "								</ul>
										</div>";

/* At this point we don�t see any use for MONTHLY schedules, leaving them out.										
										<div class=\"interval monthly\" style=\"clear: both; display: none;\">
											<label for=\"allSelector_month\"><input type=\"checkbox\" class=\"allSelector\" id=\"allSelector_month\"".$allMonthsChecked.">".LBL_ALLMONTHS."</label> <a href=\"#\" onclick=\"return false;\" class=\"listToggler\" id=\"listToggler_month\">".LBL_SHOWHIDE."</a>
											<ul id=\"monthList\">";
										
										$selections = explode(",", $schedule["settings"][2]);										
										
										for($i = 1; $i <= 12; $i++)
										{
											$checked = "";

											if($schedule["settings"][0] == "monthly" && ($schedule["settings"][2] == -1 || in_array($i, $selections)))
											{
												$checked = " checked=\"checked\"";
											}

											$list .= "<li>
														<label><input type=\"checkbox\" name=\"months[".$i."]\" id=\"month_".$i."\" class=\"selector month\" value=\"".$i."\"".$checked.">".getMonthName($i)."</label>
													</li>";
										}
											
		$list .= "							</ul>
											<div style=\"clear: both; padding-top: 10px;\">
												<label for=\"allSelector_monthDay\"><input type=\"checkbox\" class=\"allSelector\" id=\"allSelector_monthDay\"".$allMonthDaysChecked.">".LBL_ALLMONTHDAYS."</label> <a href=\"#\" onclick=\"return false;\" class=\"listToggler\" id=\"listToggler_monthDay\">".LBL_SHOWHIDE."</a>
												<ul id=\"monthDayList\">";
										
										$selections = explode(",", $schedule["settings"][3]);
										
										for($i = 1; $i <= 31; $i++)
										{
											$checked = "";

											if($schedule["settings"][0] == "monthly" && ($schedule["settings"][3] == -1 || in_array($i, $selections)))
											{
												$checked = " checked=\"checked\"";
											}

											$list .= "<li>
														<label><input type=\"checkbox\" name=\"monthdays[".$i."]\" id=\"monthDay_".$i."\" class=\"selector monthDay\" value=\"".$i."\"".$checked.">".$i."</label>
													</li>";
										}
											
	$list .= "									</ul>
											</div>
										</div>
*/
	$list .= "						</td>
								</tr>
								<tr>
									<td colspan=\"2\">&nbsp;</td>
								</tr>
								<tr>
									<td colspan=\"2\" align=\"right\">
										<input type=\"submit\" name=\"cancel\" value=\"".LBL_CANCEL."\" class=\"button setActionByButton\">&nbsp;<input type=\"submit\" name=\"delete\" value=\"".LBL_DELETE."\" class=\"button setActionByButton\" onclick=\"return confirm('".LBL_CONFIRMDELETE."?');\">&nbsp;<input type=\"submit\" value=\"".LBL_SAVE."\" name=\"save\" class=\"button setActionByButton\">
									</td>
								</tr>
								<tr>
									<td colspan=\"2\">
										&nbsp;
									</td>
								</tr>
							</table>
						</form>
					</td>
				</tr>
			</table>";
			
	$this->addBody($list);
}
else
{
	redirectTo(THISPAGE."?page=".$this->currentPage["name"]);
}

?>