<?php
// HomeAutomation
// Copyright (C) 2021 Tom Rosenback (tom.rosenback@gmail.com), Daniel Malmgren (daniel.malmgren@kolefors.se)
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
if(!defined("HomeAutomationIncluded")) { die("HomeAutomation: Direct access not premitted"); }

// disabling notice messages
error_reporting(E_ALL ^ E_NOTICE);

// user logged in with user level 1 or higher (demo)
if($_SESSION[CFG_SESSION_KEY]["userlevel"] >= 1)
{
	$this->addHeader("js-conf-houseplan", "<script language=\"javascript\" type=\"text/javascript\" src=\"modules/configurator/houseplan/script.js\"></script>");

	$list = "<table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">
				<tr>
					<td class=\"bold\">
						".$this->currentPage["translation"]."
						<br />
					</td>
				</tr>
				<tr>
					<td>";

	if(!file_exists("resources/houseplan.png")) {
		$list .= "<form method=\"post\" action=\"".THISPAGE."?page=".$this->currentPage["name"]."&action=uploadhouseplan\" name=\"uploadform\" enctype=\"multipart/form-data\">
					<table>
						<tr>
							<td colspan=\"2\" class=\"bold\">".LBL_UPLOADHOUSEPLANTOCONFIGURE."</td>
						</tr>
						<tr>
							<td class=\"bold\">".LBL_IMAGE.":</td>
							<td align=\"left\"><input type=\"file\" name=\"houseplanimage\" /></td>
						</tr>
						<tr>
							<td colspan=\"2\" align=\"right\">
								<input type=\"submit\" name=\"uploadhouseplan\" value=\"".LBL_UPLOAD."\" class=\"button\" />
							</td>
						</tr>
					</table>
				</form>";
	}
	else
	{
		$houseplan = Houseplan::get();

		$list .= "<table width=\"100%\" height=\"100%\">
					<tr>
						<td valign=\"top\" width=\"100%\">
							<div style=\"position: relative; top: 0; left: 0; width: 100%; height: 100%;\">
								<form id=\"houseplanmanager\" method=\"post\" action=\"".THISPAGE."?page=".$this->currentPage["name"]."\" enctype=\"multipart/form-data\">
									<input type=\"hidden\" name=\"action\" id=\"action\" value=\"add\">
									<input type=\"hidden\" name=\"hpid\" id=\"hpid\" value=\"-1\">
									<input type=\"image\" style=\"position: relative; top: 0; left: 0; cursor: crosshair;\" src=\"resources/houseplan.png?".filemtime("resources/houseplan.png")."\" width=\"".$_SESSION[CFG_SESSION_KEY]["settings"]["houseplanwidth"]."px\" />";
		$i = 1;

		$pic_info = getimagesize(HA_ROOT_PATH."/resources/houseplan.png");
		$ratio = $pic_info[0] / $_SESSION[CFG_SESSION_KEY]["settings"]["houseplanwidth"];
		$currHeight = $pic_info[1] / $ratio;

		foreach($houseplan as $hpDevice) {
			$hpDevice["xposition"] = round($hpDevice["xposition"] / 100 * $_SESSION[CFG_SESSION_KEY]["settings"]["houseplanwidth"]);
			$hpDevice["yposition"] = round($hpDevice["yposition"] / 100 * $currHeight);

			// echo "x: ".$hpDevice["xposition"]."<br>";
			// echo "y: ".$hpDevice["yposition"]."<br>";

			if($hpDevice["deviceid"] != -1)
			{
				$device = Devices::get($hpDevice["deviceid"]);
				$device = $device[0];

				$list .= "<img src=\"".getStatusImage($device["type"], 0, $device["id"])."\" style=\"position: absolute; left: ".$hpDevice["xposition"]."px; top: ".$hpDevice["yposition"]."px; z-index: 10;\" border=\"0\" id=\"img".$i."\" height=\"".$_SESSION[CFG_SESSION_KEY]["settings"]["houseplaniconheight"]."px\" alt=\"".$device["description"].", ".mb_convert_case(LBL_CLICKTODELETE, MB_CASE_LOWER, "UTF-8")."\" title=\"".$device["description"].", ".mb_convert_case(LBL_CLICKTODELETE, MB_CASE_LOWER, "UTF-8")."\" onclick=\"deleteHouseplanDevice(".$hpDevice["id"].", '".LBL_DELETEDEVICEFROMHOUSEPLAN."');\" />";
			}
			else if($hpDevice["groupid"] != -1)
			{
				$tmp = explode("_", $hpDevice["groupid"]);
				$group = DeviceGroups::get($tmp[0]);
				$group = $group[0];

				$list .= "<img src=\"".getStatusImage("group", $tmp[1], $tmp[0])."\" style=\"position: absolute; left: ".$hpDevice["xposition"]."px; top: ".$hpDevice["yposition"]."px; z-index: 10;\" border=\"0\" id=\"img".$i."\" height=\"".$_SESSION[CFG_SESSION_KEY]["settings"]["houseplaniconheight"]."px\" alt=\"".$group["name"].", ".mb_convert_case(LBL_CLICKTODELETE, MB_CASE_LOWER, "UTF-8")."\" title=\"".$group["name"]." ".getStatusText($tmp[1]).", ".mb_convert_case(LBL_CLICKTODELETE, MB_CASE_LOWER, "UTF-8")."\" onclick=\"deleteHouseplanDevice(".$hpDevice["id"].", '".LBL_DELETEDEVICEFROMHOUSEPLAN."');\" />";
			}
			else if($hpDevice["macroid"] != -1)
			{
				$macro = Macros::get($hpDevice["macroid"]);
				$macro = $macro[0];

				$list .= "<img src=\"./resources/run.png\" style=\"position: absolute; left: ".$hpDevice["xposition"]."px; top: ".$hpDevice["yposition"]."px; z-index: 10;\" border=\"0\" id=\"img".$i."\" height=\"".$_SESSION[CFG_SESSION_KEY]["settings"]["houseplaniconheight"]."px\" alt=\"".LBL_MACRO.": ".$macro["name"].", ".mb_convert_case(LBL_CLICKTODELETE, MB_CASE_LOWER, "UTF-8")."\" title=\"".LBL_MACRO.": ".$macro["name"].", ".mb_convert_case(LBL_CLICKTODELETE, MB_CASE_LOWER, "UTF-8")."\" onclick=\"deleteHouseplanDevice(".$hpDevice["id"].", '".LBL_DELETEDEVICEFROMHOUSEPLAN."');\" />";
			}
			else if($hpDevice["tempsensor"] != -1 || $_SESSION[CFG_SESSION_KEY]["settings"]["useexternaltemperature"])
			{
				$tempSensor = Sensors::get($hpDevice["tempsensor"]);

				if($_SESSION[CFG_SESSION_KEY]["settings"]["useexternaltemperature"] && $hpDevice["tempsensor"] == "external") {
					$tempSensor[0]["name"] = $_SESSION[CFG_SESSION_KEY]["settings"]["externaltemplocation"];
				}

				$sensorName = "Temp ".mb_convert_case($tempSensor[0]["name"], MB_CASE_LOWER, "UTF-8");

				$list .= "<img src=\"resources/temp_high.png\" border=\"0\" height=\"".$_SESSION[CFG_SESSION_KEY]["settings"]["houseplaniconheight"]."px\" style=\"position: absolute; left: ".$hpDevice["xposition"]."px; top: ".$hpDevice["yposition"]."px; z-index: 10; vertical-align: middle;\" alt=\"".$sensorName.", ".mb_convert_case(LBL_CLICKTODELETE, MB_CASE_LOWER, "UTF-8")."\" title=\"".$sensorName.", ".mb_convert_case(LBL_CLICKTODELETE, MB_CASE_LOWER, "UTF-8")."\" onclick=\"deleteHouseplanDevice(".$hpDevice["id"].", '".LBL_DELETEDEVICEFROMHOUSEPLAN."');\" />";
			}

			$i++;
		}

		$devices = Devices::get();
		$allDevices = array();

		if(count($devices) > 0) {
			$allDevices[] = array("id" => "optgroup_start", "description" => LBL_DEVICES);
			$allDevices = array_merge($allDevices, $devices);
			$allDevices[] = array("id" => "optgroup_end", "description" => "");
		}

		$tempSensors = Sensors::get();

		if($_SESSION[CFG_SESSION_KEY]["settings"]["useexternaltemperature"]) {
			$tempSensors[] = array("id" => "external", "sensor_serial" => "external", "description" => "Ext. temp: ".$_SESSION[CFG_SESSION_KEY]["settings"]["externaltemplocation"]);
		}

		if(count($tempSensors) > 0) {
			$allDevices[] = array("id" => "optgroup_start", "description" => LBL_SENSORS);
			$allDevices = array_merge($allDevices, $tempSensors);
			$allDevices[] = array("id" => "optgroup_end", "description" => "");
		}

		$deviceGroups = DeviceGroups::get();
		$groups = array();

		foreach($deviceGroups as $key => $value) {
			$id = "dg_".$value["id"];
			$groups[$key."_on"] = array("id" => $id."_1", "description" => $value["name"]." (".LBL_ON.")");
			$groups[$key."_off"] = array("id" => $id."_0", "description" => $value["name"]." (".LBL_OFF.")");
		}

		if(count($groups) > 0) {
			$allDevices[] = array("id" => "optgroup_start", "description" => LBL_GROUPS);
			$allDevices = array_merge($allDevices, $groups);
			$allDevices[] = array("id" => "optgroup_end", "description" => "");
		}

		$macros = Macros::get();

		if(count($macros) > 0) {
			foreach($macros as $key => $value)
			{
				$id = "m_".$value["id"];
				$macros[$key] = array("id" => $id, "description" => $value["name"]);
			}

			$allDevices[] = array("id" => "optgroup_start", "description" => LBL_MACROS);
			$allDevices = array_merge($allDevices, $macros);
			$allDevices[] = array("id" => "optgroup_end", "description" => "");
		}

		// echo "<pre>";
		// print_r($allDevices);
		// echo "</pre>";
		$deleteHouseplanImageBtn = "";

		if(file_exists("resources/houseplan.png")) {
			$deleteHouseplanImageBtn = "<input type=\"submit\" name=\"deletehouseplan\" value=\"".LBL_DELETE."\" class=\"button setActionByButton\" onclick=\"return confirm('".LBL_CONFIRMDELETE."?');\" /> ";
		}

		$list .= "			<table style=\"position: absolute; top: 0; right: 0\">
								<tr>
									<td>
										<table class=\"outline2\">
											<tr>
												<td class=\"bold\" colspan=\"2\">".LBL_HOUSEPLANCONFIGURATIONDESCRIPTION."</td>
											</tr>
											<tr>
												<td class=\"bold\">".LBL_DEVICE.":</td>
												<td>".generateSelector(-1, "deviceid", "text2", "id", "description", $allDevices, -1, LBL_SELECTDEVICE)."</td>
											</tr>
											<tr>
												<td colspan=\"2\">&nbsp;</td>
											</tr>
											<tr>
												<td colspan=\"2\" align=\"right\">
													<input type=\"submit\" name=\"deleteall\" onclick=\"return confirm('".LBL_DELETEALLDEVICESFROMHOUSEPLAN."');\" value=\"".LBL_DELETEALL."\" class=\"button setActionByButton\" />
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td>
										<table class=\"outline2\">
											<tr>
												<td colspan=\"2\" class=\"bold\">".LBL_UPLOADNEWHOUSEPLAN."</td>
											</tr>
											<tr>
												<td class=\"bold\">".LBL_IMAGE.":</td>
												<td align=\"left\"><input type=\"file\" name=\"houseplanimage\" /></td>
											</tr>
											<tr>
												<td colspan=\"2\" align=\"right\">
													".$deleteHouseplanImageBtn."<input type=\"submit\" name=\"uploadhouseplan\" value=\"".LBL_UPLOAD."\" class=\"button setActionByButton\" />
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</form>
					</div>
				</td>
			</tr>
		</table>";
	}

	$list .= "			</td>
					</tr>
				</table>";

	$this->addBody($list);
}
else
{
	redirectTo(THISPAGE);
}

?>
