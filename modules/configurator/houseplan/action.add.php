<?php
// HomeAutomation
// Copyright (C) 2021 Tom Rosenback (tom.rosenback@gmail.com), Daniel Malmgren (daniel.malmgren@kolefors.se)
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
if(!defined("HomeAutomationIncluded")) { die("HomeAutomation: Direct access not premitted"); }

// disabling notice messages
error_reporting(E_ALL ^ E_NOTICE);

// user logged in with user level 3 (admin)
if($_SESSION[CFG_SESSION_KEY]["userlevel"] >= 3)
{
	if($_POST["deviceid"] != "-1") {
		$deviceId = -1;
		$groupId = -1;
		$macroId = -1;
		$tempSensor = -1;

		if(substr($_POST["deviceid"], 0, 3) == "dg_") {
			$groupId = substr($_POST["deviceid"], 3);
		}
		else if(substr($_POST["deviceid"], 0, 3) == "t@@") {
			$tempSensor = substr($_POST["deviceid"], 3);
		}
		else if(substr($_POST["deviceid"], 0, 2) == "m_") {
			$macroId = substr($_POST["deviceid"], 2);
		}
		else if($_POST["deviceid"] == "external") {
			$tempSensor = $_POST["deviceid"];
		}
		else
		{
			$deviceId = $_POST["deviceid"];
		}

		$pic_info = getimagesize(HA_ROOT_PATH."/resources/houseplan.png");
		$ratio = $pic_info[0] / $_SESSION[CFG_SESSION_KEY]["settings"]["houseplanwidth"];
		$currHeight = $pic_info[1] / $ratio;

		$xPos = (round($_POST["x"] / 10, 2) * 10) - 1;
		$yPos = (round($_POST["y"] / 10, 2) * 10); // - 8;

		$xPercent = round((($xPos / $_SESSION[CFG_SESSION_KEY]["settings"]["houseplanwidth"]) * 100), 2);
		$yPercent = round((($yPos / $currHeight) * 100), 2);

		$params = array(
			new dbParam(dbParamType::$INTEGER, "deviceid", $deviceId),
			new dbParam(dbParamType::$STRING, "tempsensor", $tempSensor),
			new dbParam(dbParamType::$STRING, "groupid", $groupId),
			new dbParam(dbParamType::$INTEGER, "macroid", $macroId),
			new dbParam(dbParamType::$INTEGER, "xposition", $xPercent),
			new dbParam(dbParamType::$INTEGER, "yposition", $yPercent)
		);

		Houseplan::save($params);
	}
}

redirectTo(THISPAGE."?page=".$this->currentPage["name"]);

?>
