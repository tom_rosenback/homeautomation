<?php

// HomeAutomation
// Copyright (C) 2021 Tom Rosenback (tom.rosenback@gmail.com), Daniel Malmgren (daniel.malmgren@kolefors.se)
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
if(!defined("HomeAutomationIncluded")) { die("HomeAutomation: Direct access not premitted"); }

// disabling notice messages
error_reporting(E_ALL ^ E_NOTICE);

$id = getFormVariable("id", 0);

// user logged in with user level 3 (admin)
if($_SESSION[CFG_SESSION_KEY]["userlevel"] >= 3 && ($id == -1 || $id > 0))
{
	$group = array();
		
	if($id == -1) {
		$group["id"] = $id;
		$group["name"] = "";
		$group["relations"] = array();
	}
	else
	{
		$group = DeviceGroups::get($id);
		$group = $group[0];
	}
	
	$devices = Devices::get();
	
	$list .= "<table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">
				<tr>
					<td>
						<form id=\"devicegroupmanager\" method=\"post\" action=\"".THISPAGE."?page=".$this->currentPage["name"]."\" enctype=\"multipart/form-data\">
							<input type=\"hidden\" name=\"id\" value=\"".$group["id"]."\">
							<input type=\"hidden\" name=\"action\" id=\"action\" value=\"\">
							<table>
								<tr>
									<td class=\"bold\">".LBL_GROUP.":</td>
									<td><input type=\"text\" name=\"name\" class=\"text2\" value=\"".$group["name"]."\"></td>
								</tr>
								<tr>
									<td class=\"bold\">".LBL_ICONS.":</td>
									<td>
										<table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">
											<tr>
												<td colspan=\"4\" class=\"bold\">
													
												</td>
											</tr>
											<tr>
												<td class=\"bold\">".LBL_OFF.":</td>
												<td><img src=\"".getStatusImage("group", 0, $group["id"], "groups")."\" alt=\"".$group["name"]." ".getStatusText(false)."\" title=\"".$group["name"]." ".getStatusText(false)."\" height=\"20px\"></td>
												<td class=\"bold\"><label><input type=\"checkbox\" name=\"reseticons\" id=\"reseticons\" onclick=\"iconAction(this.id);\" />".LBL_RESETICONS."</label></td>
											</tr>
											<tr>
												<td class=\"bold\">".LBL_ON.":</td>
												<td><img src=\"".getStatusImage("group", 1, $group["id"], "groups")."\" alt=\"".$group["name"]." ".getStatusText(true)."\" title=\"".$group["name"]." ".getStatusText(true)."\" height=\"20px\"></td>
												<td class=\"bold\"><label><input type=\"checkbox\" name=\"uploadicons\" id=\"uploadicons\" onclick=\"iconAction(this.id);\" />".LBL_UPLOADNEWICONS."</label></td>
											</tr>
										</table>	
										<table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" style=\"display: none;\" id=\"newIconsTable\">
											<tr>
												<td class=\"bold\" colspan=\"2\">&nbsp;</td>
											</tr>
											<tr>
												<td class=\"bold\">".LBL_OFF.":</td>
												<td align=\"left\"><input type=\"file\" name=\"icon_off\" class=\"text2\" /></td>
											</tr>
											<tr>
												<td class=\"bold\">".LBL_ON.":</td>
												<td align=\"left\"><input type=\"file\" name=\"icon_on\" class=\"text2\" /></td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td class=\"bold\" valign=\"top\">".LBL_DEVICES.":</td>
									<td>
										<ul>";
		$i = 0;
		
		foreach($devices as $device) {			
			$deviceChecked = "";
			
			if(is_array($group["relations"]))
			{
				foreach($group["relations"] as $relation)
				{
					if($relation["deviceid"] == $device["id"])
					{
						$deviceChecked = " checked=\"checked\"";
						break;
					}
				}
			}

			$list .= "<li>
						<label><input type=\"checkbox\" name=\"devices[".$i."]\" value=\"".$device["id"]."\"".$deviceChecked.">".$device["description"]."</label>
					</li>";

			$i++;
		}		
		
		$list .= "							</ul>
										</td>
									</tr>
									<tr>
										<td colspan=\"2\">
											&nbsp;
										</td>
									</tr>
									<tr>
										<td colspan=\"2\" align=\"right\">
											<input type=\"submit\" name=\"cancel\" value=\"".LBL_CANCEL."\" class=\"button setActionByButton\" /> <input type=\"submit\" id=\"saveBtn\" name=\"save\" value=\"".LBL_SAVE."\" class=\"button setActionByButton\" />
										</td>
									</tr>
								</table>
							</form>
						</td>
					</tr>
				</table>";
			
	$this->addBody($list);
}
else
{
	redirectTo(THISPAGE."?page=".$this->currentPage["name"]);
}

?>