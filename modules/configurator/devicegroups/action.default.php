<?php
// HomeAutomation
// Copyright (C) 2021 Tom Rosenback (tom.rosenback@gmail.com), Daniel Malmgren (daniel.malmgren@kolefors.se)
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
if(!defined("HomeAutomationIncluded")) { die("HomeAutomation: Direct access not premitted"); }

// disabling notice messages
error_reporting(E_ALL ^ E_NOTICE);

$resetSortingLink = "<a href=\"".THISPAGE."?page=".$this->currentPage["name"]."\" id=\"resetSorting\" title=\"".LBL_CONFIRMRESETSORTING."\">".LBL_RESETSORTING."</a>";

// user logged in with user level 1 or higher (demo)
if($_SESSION[CFG_SESSION_KEY]["userlevel"] >= 3)
{
	$list = "<table width=\"100%\">
				<tr>
					<td class=\"bold\">
						".$this->currentPage["translation"]."
						<br />
					</td>
				</tr>
				<tr>
					<td>
						<table cellspacing=\"0\" cellpadding=\"3\" class=\"tablelist\" id=\"sortableList\">
							<thead>
								<tr>
									<td class=\"bold\">".LBL_ID."</td>
									<td class=\"bold\">".LBL_GROUP."</td>
									<td class=\"bold\" align=\"center\">".LBL_ICONS."</td>
									<td class=\"bold\">".LBL_DEVICES."</td>
									<td class=\"bold\">&nbsp;</td>
								</tr>
							</thead>";
	
	$allGroups = DeviceGroups::get();
	if(count($allGroups) > 0) {
		$list .= "<tbody>";
		
		foreach($allGroups as $group) {
			$relations = "";
			
			if(is_array($group["relations"]))
			{
				$relations = "<ul>";
				
				foreach($group["relations"] as $relation)
				{
					$relations .=" <li>- ".$relation["devicedesc"]."</li>";
				}
				
				$relations .= "</ul>";
			}
			
			$list .= "<tr id=\"devicegroups_".$group["id"]."\">		
						<td valign=\"top\">".$group["id"]."</td>
						<td valign=\"top\">".$group["name"]."</td>
						<td valign=\"top\" align=\"center\">
							<img src=\"".getStatusImage("group", 0, $group["id"], "groups")."\" alt=\"".$group["name"]." ".getStatusText(false)."\" title=\"".$group["name"]." ".getStatusText(false)."\" height=\"20px\">
							<img src=\"".getStatusImage("group", 1, $group["id"], "groups")."\" alt=\"".$group["name"]." ".getStatusText(true)."\" title=\"".$group["name"]." ".getStatusText(true)."\" height=\"20px\">
						</td>
						<td valign=\"top\">".$relations."</td>
						<td align=\"right\">					
							<a href=\"".THISPAGE."?page=".$this->currentPage["name"]."&action=edit&id=".$group["id"]."\"><img src=\"./resources/edit.png\" border=\"0\" width=\"20\" alt=\"".LBL_EDIT."\" title=\"".LBL_EDIT."\"></a>
							<a href=\"".THISPAGE."?page=".$this->currentPage["name"]."&action=delete&id=".$group["id"]."\" onclick=\"return confirm('".LBL_CONFIRMDELETE." ".$group["name"]."?');\"><img src=\"./resources/delete.png\" border=\"0\" width=\"20\" alt=\"".LBL_DELETE."\" title=\"".LBL_DELETE."\"></a>
						</td>
					</tr>";			

			$i++;
		}
		
		$list .= "</tbody>";
		$resetSortingLink = "&nbsp;-&nbsp;".$resetSortingLink;
	}
	else
	{
		$list .= "<tfoot>
					<tr>
						<td colspan=\"4\" class=\"bold\">".LBL_NOGROUPSCONFIGURED."</td>
					</tr>
				</tfoot>";
		
		$resetSortingLink = "";
	}
	
	$list .= "				</table>
						</td>
					</tr>
					<tr>
						<td class=\"bold\">
							<a href=\"".THISPAGE."?page=".$this->currentPage["name"]."&action=edit&id=-1\">".LBL_CREATENEWGROUP."</a>
							".$resetSortingLink."
						</td>
					</tr>
				</table>";
		
	$this->addBody($list);
}
else
{
	redirectTo(THISPAGE);
}

?>