<?php
// HomeAutomation
// Copyright (C) 2021 Tom Rosenback (tom.rosenback@gmail.com), Daniel Malmgren (daniel.malmgren@kolefors.se)
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
if(!defined("HomeAutomationIncluded")) { die("HomeAutomation: Direct access not premitted"); }

// disabling notice messages
error_reporting(E_ALL ^ E_NOTICE);

// user logged in with user level 1 or higher (demo)
if($_SESSION[CFG_SESSION_KEY]["userlevel"] >= 1) {
	$list = "<table width=\"100%\">
				<tr>
					<td class=\"bold\">
						".$this->currentPage["translation"]."
						<br />
					</td>
				</tr>
				<tr>
					<td>
						<table cellspacing=\"0\" cellpadding=\"3\" class=\"tablelist\">
							<thead>
								<tr>
									<td class=\"bold\">".LBL_USERNAME."</td>
									<td class=\"bold\">".LBL_FIRSTNAME."</td>
									<td class=\"bold\">".LBL_LASTNAME."</td>
									<td class=\"bold\" colspan=\"2\">".LBL_USERLEVEL."</td>
								</tr>
							</thead>
							<tbody>";
		
		foreach($users as $user) {
			if($user["editable"] > 0) {
				$list .= "<tr>
							<td>".$user["username"]."</td>
							<td>".$user["firstname"]."</td>
							<td>".$user["lastname"]."</td>
							<td>".$user["userleveltext"]."</td>
							<td align=\"right\">
								<a href=\"".THISPAGE."?page=".$this->currentPage["name"]."&action=edit&id=".$user["id"]."\"><img src=\"./resources/edit.png\" border=\"0\" width=\"20\" alt=\"".LBL_EDIT."\" title=\"".LBL_EDIT."\"></a>";
								
				if($user["editable"] == 2) {
					$list .= "	<a href=\"".THISPAGE."?page=".$this->currentPage["name"]."&action=delete&id=".$user["id"]."&editable=".$user["editable"]."\" onclick=\"return confirm('".LBL_CONFIRMDELETE." ".$user["username"]."?');\"><img src=\"./resources/delete.png\" border=\"0\" width=\"20\" alt=\"".LBL_DELETE."\" title=\"".LBL_DELETE."\"></a>";
				}
				
				$list .="		</td>
						</tr>";
			}
		}

		$list .= "</tbody>
				</table>
			</td>
		</tr>
		<tr>
			<td></td>
		</tr>
		<tr>
			<td class=\"bold\">
				<a href=\"".THISPAGE."?page=".$this->currentPage["name"]."&action=edit&id=-1\">".LBL_CREATENEWUSER."</a>
			</td>
		</tr>
	</table>";
		
	$this->addBody($list);
}
else
{
	redirectTo(THISPAGE);
}

?>