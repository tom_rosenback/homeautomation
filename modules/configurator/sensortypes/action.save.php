<?php

// HomeAutomation
// Copyright (C) 2021 Tom Rosenback (tom.rosenback@gmail.com), Daniel Malmgren (daniel.malmgren@kolefors.se)
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
if(!defined("HomeAutomationIncluded")) { die("HomeAutomation: Direct access not premitted"); }

// disabling notice messages
error_reporting(E_ALL ^ E_NOTICE);

$id = getFormVariable("id", -1);
$message = new SystemMessage();

// user logged in with user level 3 (admin)
if($_SESSION[CFG_SESSION_KEY]["userlevel"] >= 3 && ($id == -1 || $id > 0)) {
	$params = array(
		new dbParam(dbParamType::$STRING, "description", getFormVariable("description", "")),
		new dbParam(dbParamType::$STRING, "unit", getFormVariable("unit", "")),
		new dbParam(dbParamType::$STRING, "graphtype", getFormVariable("graphtype", "")),
		new dbParam(dbParamType::$INTEGER, "min", getFormVariable("min", -1000)),
		new dbParam(dbParamType::$INTEGER, "max", getFormVariable("max", 1000)),
		new dbParam(dbParamType::$INTEGER, "low", getFormVariable("low", -1000)),
		new dbParam(dbParamType::$INTEGER, "high", getFormVariable("high", 1000))
	);

	$id = Sensors::saveType($params, $id);

	if(getFormVariable("reseticons", "") == "on") {
		deleteFile(HA_ROOT_PATH."/resources/icons/sensortypes/".$id."_high.png");
		deleteFile(HA_ROOT_PATH."/resources/icons/sensortypes/".$id."_low.png");
	} else if(getFormVariable("uploadicons", "") == "on") {
		$sensor = Sensors::get("", $id);
		saveUploadedFile($_FILES["icon_low"], HA_ROOT_PATH."/resources/icons/sensortypes/".$id."_low.png", "jpg|png|gif|jpeg");
		saveUploadedFile($_FILES["icon_high"], HA_ROOT_PATH."/resources/icons/sensortypes/".$id."_high.png", "jpg|png|gif|jpeg");
	}

	$message->setText(LBL_SAVED_SUCCESSFULLY);
	$message->setType(SystemMessageType::$SUCCESS);
	$message->setAutoClose(true);
} else {
}

redirectTo(THISPAGE."?page=".$this->currentPage["name"]."&msg=".$message->encode());

?>
