<?php

// HomeAutomation
// Copyright (C) 2021 Tom Rosenback (tom.rosenback@gmail.com), Daniel Malmgren (daniel.malmgren@kolefors.se)
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
if(!defined("HomeAutomationIncluded")) { die("HomeAutomation: Direct access not premitted"); }

// disabling notice messages
error_reporting(E_ALL ^ E_NOTICE);

$id = getFormVariable("id", -1);

// user logged in with user level 3 (admin)
if($_SESSION[CFG_SESSION_KEY]["userlevel"] >= 3 && ($id == -1 || $id > 0))
{
	$currentType = array();

	if($id != -1 && $id > 0) {
		$currentType = Sensors::getTypes($id);
		$currentType = $currentType[0];
	}
	else
	{
		$currentType["id"] 		= -1;
		$currentType["description"]	= "";
		$currentType["unit"] 	= "";
	}

	$list .= "<table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">
				<tr>
					<td>
						<form id=\"sensortypemanager\" method=\"post\" action=\"".THISPAGE."?page=".$this->currentPage["name"]."\" enctype=\"multipart/form-data\">
							<input type=\"hidden\" name=\"id\" id=\"id\" value=\"".$currentType["id"]."\">
							<input type=\"hidden\" name=\"action\" id=\"action\" value=\"\">
							<table>
								<tr>
									<td class=\"bold\">".LBL_DESCRIPTION.":</td>
									<td><input type=\"text\" name=\"description\" class=\"text2\" value=\"".$currentType["description"]."\"></td>
								</tr>
								<tr>
									<td class=\"bold\">".LBL_UNIT.":</td>
									<td><input type=\"text\" name=\"unit\" class=\"text2\" value=\"".$currentType["unit"]."\"></td>
								</tr>
								<tr>
									<td class=\"bold\">".LBL_GAUGETYPE.":</td>
									<td>".generateSelector($currentType["gaugetype"], "gaugetype", "text2", 0, 1, getGaugeTypes(), 1, LBL_CHOOSETYPE)."</td>
								</tr>
								<tr>
									<td class=\"bold\">".LBL_GRAPHTYPE.":</td>
									<td>".generateSelector($currentType["graphtype"], "graphtype", "text2", 0, 1, getGraphTypes(), 1, LBL_CHOOSETYPE)."</td>
								</tr>
								<tr>
									<td class=\"bold\">".LBL_MIN.":</td>
									<td><input type=\"text\" name=\"min\" class=\"text\" value=\"".$currentType["min"]."\"></td>
								</tr>
								<tr>
									<td class=\"bold\">".LBL_MAX.":</td>
									<td><input type=\"text\" name=\"max\" class=\"text\" value=\"".$currentType["max"]."\"></td>
								</tr>
								<tr>
									<td class=\"bold\">".LBL_LOWLIMIT.":</td>
									<td><input type=\"text\" name=\"low\" class=\"text\" value=\"".$currentType["low"]."\"></td>
								</tr>
								<tr>
									<td class=\"bold\">".LBL_HIGHLIMIT.":</td>
									<td><input type=\"text\" name=\"high\" class=\"text\" value=\"".$currentType["high"]."\"></td>
								</tr>
								<tr>
									<td class=\"bold\">".LBL_ICONS.":</td>
									<td>
										<table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">
											<tr>
												<td colspan=\"4\" class=\"bold\">
												</td>
											</tr>
											<tr>
												<td class=\"bold\">".LBL_LOW.":</td>
												<td><img src=\"".getSensorImage("", -1000, $id)."\" alt=\"".$device["description"]." ".getStatusText(false)."\" title=\"".$device["description"]." ".getStatusText(false)."\" height=\"20px\"></td>
												<td class=\"bold\"><label><input type=\"checkbox\" name=\"reseticons\" id=\"reseticons\" onclick=\"iconAction(this.id);\" />".LBL_RESETICONS."</label></td>
											</tr>
											<tr>
												<td class=\"bold\">".LBL_HIGH.":</td>
												<td><img src=\"".getSensorImage("", 1000, $id)."\" alt=\"".$device["description"]." ".getStatusText(true)."\" title=\"".$device["description"]." ".getStatusText(true)."\" height=\"20px\"></td>
												<td class=\"bold\"><label><input type=\"checkbox\" name=\"uploadicons\" id=\"uploadicons\" onclick=\"iconAction(this.id);\" />".LBL_UPLOADNEWICONS."</label></td>
											</tr>
										</table>
										<table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" style=\"display: none;\" id=\"newIconsTable\">
											<tr>
												<td class=\"bold\" colspan=\"2\">&nbsp;</td>
											</tr>
											<tr>
												<td class=\"bold\">".LBL_LOW.":</td>
												<td align=\"left\"><input type=\"file\" name=\"icon_low\" class=\"text2\" /></td>
											</tr>
											<tr>
												<td class=\"bold\">".LBL_HIGH.":</td>
												<td align=\"left\"><input type=\"file\" name=\"icon_high\" class=\"text2\" /></td>
											</tr>
										</table>
									</td>
								</tr><tr>
									<td colspan=\"2\">
										&nbsp;
									</td>
								</tr>
								<tr>
									<td colspan=\"2\" align=\"right\">
										<input type=\"submit\" value=\"".LBL_CANCEL."\" name=\"cancel\" class=\"button setActionByButton\">&nbsp;<input type=\"submit\" value=\"".LBL_SAVE."\" name=\"save\" class=\"button setActionByButton\">
									</td>
								</tr>
								<tr>
									<td colspan=\"2\">
										&nbsp;
									</td>
								</tr>
							</table>
						</form>
					</td>
				</tr>
			</table>";
			
	$this->addBody($list);
}
else
{
	redirectTo(THISPAGE."?page=".$this->currentPage["name"]);
}

?>
