// HomeAutomation
// Copyright (C) 2021 Tom Rosenback (tom.rosenback@gmail.com), Daniel Malmgren (daniel.malmgren@kolefors.se)
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

$(document).ready(function()
{
	$(".devices").change(function()
	{
		value = $(this).val();
		
		if($(this).is(":checked"))
		{	
			$("#status_selection" + value).show();
		}
		else
		{	
			$("#status_selection" + value).hide();
		}
		
	}).change();
	
	$(".setActionByButton").click(function()
	{
		var action = this.name;
		
		$("#action").val(action)
	});
	
	initializeSlidersWithoutAction(false);
		
	$("#gcallogin").click(function()
	{
		getGoogleCalendarList();
	});

	if($("#gcallogin").length)
	{
		if($("#username").val() != "" && $("#password").val() != "")
		{
			// alert("google");
			getGoogleCalendarList();
		}
	}
});

function getGoogleCalendarList()
{
	$.ajax(
	{
		type: 'GET',
		url: 'api.php',
		data: 
		{
			'do': 'gcal/getCalendarList',
			'username': $("#username").val(),
			'password': $("#password").val(),
			'ts': new Date()
		},
		beforeSend: function(jqXHR, settings) {
			$("#availablecalendars").html("");
			$("#saveBtn").hide();
			$("#availablecalendars").hide();
			$("#loading").show();
			
			return true;
		},
		error: function(jqXHR, textStatus, errorThrown) {
			//alert(textStatus);
		},
		success: function(html) {
			// alert(html);
			$("#loading").hide();
			
			data = $.parseJSON(html);
			if(data.errmsg == "" && data.list != "")
			{
				$("#availablecalendars").html(data.list);
				colorizeTable();
			}
			else
			{
				$("#availablecalendars").html(data.errmsg);
			}
			
			$("#saveBtn").show();
			$("#availablecalendars").show();			
		}
	});
}