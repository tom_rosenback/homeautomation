<?php
// HomeAutomation
// Copyright (C) 2021 Tom Rosenback (tom.rosenback@gmail.com), Daniel Malmgren (daniel.malmgren@kolefors.se)
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
if(!defined("HomeAutomationIncluded")) { die("HomeAutomation: Direct access not premitted"); }

// disabling notice messages
error_reporting(E_ALL ^ E_NOTICE);

$resetSortingLink = "<a href=\"".THISPAGE."?page=".$this->currentPage["name"]."\" id=\"resetSorting\" title=\"".LBL_CONFIRMRESETSORTING."\">".LBL_RESETSORTING."</a>";

// user logged in with user level 1 or higher (demo)
if($_SESSION[CFG_SESSION_KEY]["userlevel"] >= 3)
{
	$sensors = Sensors::get("", -1, "sort", -1);

	$list = "<table width=\"100%\">
				<tr>
					<td class=\"bold\">
						".$this->currentPage["translation"]."
						<br />
					</td>
				</tr>
				<tr>
					<td>
						<table cellspacing=\"0\" cellpadding=\"3\" class=\"tablelist\" width=\"80%\" id=\"sortableList\">
							<thead>
								<tr>
									<td class=\"bold\">".LBL_ID."</td>
									<td class=\"bold\">".LBL_SENSORNAME."</td>
									<td class=\"bold\">".LBL_UNIQUEID."</td>
									<td class=\"bold\">".LBL_TYPE."</td>
									<td class=\"bold\">".LBL_GAUGETYPE."</td>
									<td class=\"bold\">".LBL_MIN."</td>
									<td class=\"bold\">".LBL_MAX."</td>
									<td class=\"bold\">".LBL_LOW."</td>
									<td class=\"bold\">".LBL_HIGH."</td>
									<td class=\"bold\">".LBL_ACTIVE."</td>
									<td class=\"bold\">
										&nbsp;
									</td>
								</tr>
							</thead>";

	if(count($sensors) > 0) {
		$list .= "<tbody>";
		$i = 0;
		$gaugeTypes = getGaugeTypes();

		foreach($sensors as $sensor) {
			$list .= "<tr id=\"tempsensors_".$sensor["realid"]."\">
						<td>".$sensor["realid"]."</td>
						<td>".$sensor["name"]."</td>
						<td>".$sensor["sensor_serial"]."</td>
						<td>".$sensor["sensortypedesc"]."</td>
						<td>".$gaugeTypes[$sensor["gaugetype"]][1]."</td>
						<td>".$sensor["min"]." ".trim($sensor["unit"])."</td>
						<td>".$sensor["max"]." ".trim($sensor["unit"])."</td>
						<td>".$sensor["low"]." ".trim($sensor["unit"])."</td>
						<td>".$sensor["high"]." ".trim($sensor["unit"])."</td>
						<td>".getBooleanText($sensor["active"])."</td>
						<td align=\"right\" height=\"25px\">";

			if($sensor["realid"] > 0)
			{
				$list .= "	<a href=\"".THISPAGE."?page=".$this->currentPage["name"]."&action=edit&id=".$sensor["realid"]."\"><img src=\"./resources/edit.png\" border=\"0\" width=\"20\" alt=\"".LBL_EDIT."\" title=\"".LBL_EDIT."\"></a>
							<a href=\"".THISPAGE."?page=".$this->currentPage["name"]."&action=delete&id=".$sensor["realid"]."\" onclick=\"return confirm('".LBL_CONFIRMDELETE." ".$sensor["name"]."?');\"><img src=\"./resources/delete.png\" border=\"0\" width=\"20\" alt=\"".LBL_DELETE."\" title=\"".LBL_DELETE."\"></a>";
			}

			$list .="		&nbsp;</td>
					</tr>";

			$i++;
		}

		$list .= "</tbody>";
		$resetSortingLink = "&nbsp;-&nbsp;".$resetSortingLink;
	}
	else
	{
		$list .= "<tfoot>
					<tr>
						<td colspan=\"5\" class=\"bold\">".LBL_NOSENSORSAVAILABLE."</td>
					</tr>
				</tfoot>";

		$resetSortingLink = "";
	}

	$list .= "</table>
			</td>
		</tr>
		<tr>
			<td></td>
		</tr>
		<tr>
			<td class=\"bold\">
				<a href=\"".THISPAGE."?page=".$this->currentPage["name"]."&action=update\">".LBL_UPDATESENSORS."</a>
				".$resetSortingLink."
			</td>
		</tr>
	</table>";	
		
	$this->addBody($list);
}
else
{
	redirectTo(THISPAGE);
}

?>
