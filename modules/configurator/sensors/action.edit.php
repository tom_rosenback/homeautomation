<?php

// HomeAutomation
// Copyright (C) 2021 Tom Rosenback (tom.rosenback@gmail.com), Daniel Malmgren (daniel.malmgren@kolefors.se)
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
if(!defined("HomeAutomationIncluded")) { die("HomeAutomation: Direct access not premitted"); }

// disabling notice messages
error_reporting(E_ALL ^ E_NOTICE);

$id = getFormVariable("id", -1);

// user logged in with user level 3 (admin)
if($_SESSION[CFG_SESSION_KEY]["userlevel"] >= 3 && ($id > 0))
{
	$currentSensor = array();

	if($id != -1 && $id > 0) {
		$currentSensor = Sensors::get("", $id, "sort", -1);
		$currentSensor = $currentSensor[0];
	}

	$sensorTypes = Sensors::getTypes();

	$list .= "<table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">
				<tr>
					<td>
						<form id=\"sensormanager\" method=\"post\" action=\"".THISPAGE."?page=".$this->currentPage["name"]."\">
							<input type=\"hidden\" name=\"id\" id=\"id\" value=\"".$currentSensor["realid"]."\">
							<input type=\"hidden\" name=\"action\" id=\"action\" value=\"\">
							<table>
								<tr>
									<td class=\"bold\">".LBL_SENSORNAME.":</td>
									<td><input type=\"text\" name=\"name\" class=\"text2\" value=\"".$currentSensor["name"]."\"></td>
								</tr>
								<tr>
									<td class=\"bold\">".LBL_UNIQUEID.":</td>
									<td><input type=\"text\" name=\"serial\" class=\"text2\" value=\"".$currentSensor["sensor_serial"]."\"></td>
								</tr>
								<tr>
									<td class=\"bold\">".LBL_TYPE.":</td>
									<td>".generateSelector($currentSensor["sensortype"], "sensortype", "text2", "id", "descwithunit", $sensorTypes, 1, LBL_CHOOSETYPE)."</td>
								</tr>
								<tr>
									<td class=\"bold\">".LBL_ACTIVE.":</td>
									<td>".generateSelector($currentSensor["active"], "active", "text", 0, 1, array(array("0", mb_convert_case(LBL_NO, MB_CASE_LOWER, "UTF-8")), array("1", mb_convert_case(LBL_YES, MB_CASE_LOWER, "UTF-8"))), 0, "")."</td>
								</tr>
								<tr>
									<td colspan=\"2\">
										&nbsp;
									</td>
								</tr>
								<tr>
									<td colspan=\"2\" align=\"right\">
										<input type=\"submit\" value=\"".LBL_CANCEL."\" name=\"cancel\" class=\"button setActionByButton\">&nbsp;<input type=\"submit\" value=\"".LBL_SAVE."\" name=\"save\" class=\"button setActionByButton\">
									</td>
								</tr>
								<tr>
									<td colspan=\"2\">
										&nbsp;
									</td>
								</tr>
							</table>
						</form>
					</td>
				</tr>
			</table>";

	$this->addBody($list);
}
else
{
	redirectTo(THISPAGE."?page=".$this->currentPage["name"]);
}

?>
