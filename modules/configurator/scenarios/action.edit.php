<?php

// HomeAutomation
// Copyright (C) 2021 Tom Rosenback (tom.rosenback@gmail.com), Daniel Malmgren (daniel.malmgren@kolefors.se)
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
if(!defined("HomeAutomationIncluded")) { die("HomeAutomation: Direct access not premitted"); }

// disabling notice messages
error_reporting(E_ALL ^ E_NOTICE);

$id = getFormVariable("id", -1);

// user logged in with user level 3 (admin)
if($_SESSION[CFG_SESSION_KEY]["userlevel"] >= 3 && ($id == -1 || $id > 0))
{
	$currentScenario = array();
	
	if($id != -1 && $id > 0) {
		$currentScenario = Scenarios::get($id);
		$currentScenario = $currentScenario[0];
	}
	else
	{
		$currentScenario["id"] 		= -1;
		$currentScenario["name"] 	= "";
	}
		
	$list .= "<table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">
				<tr>
					<td>
						<form id=\"scenariomanager\" method=\"post\" action=\"".THISPAGE."?page=".$this->currentPage["name"]."\">
							<input type=\"hidden\" name=\"id\" id=\"id\" value=\"".$currentScenario["id"]."\">
							<input type=\"hidden\" name=\"action\" id=\"action\" value=\"\">
						
							<table>
								<tr>
									<td class=\"bold\">".LBL_SCENARIO.":</td>
									<td><input type=\"text\" name=\"name\" class=\"text2\" value=\"".$currentScenario["name"]."\"></td>
								</tr>
								<tr>
									<td colspan=\"2\">
										&nbsp;
									</td>
								</tr>
								<tr>
									<td colspan=\"2\" align=\"right\">
										<input type=\"submit\" value=\"".LBL_CANCEL."\" name=\"cancel\" class=\"button setActionByButton\">&nbsp;<input type=\"submit\" name=\"save\" value=\"".LBL_SAVE."\" class=\"button setActionByButton\">
									</td>
								</tr>
								<tr>
									<td colspan=\"2\">
										&nbsp;
									</td>
								</tr>
							</table>
						</form>
					</td>
				</tr>
			</table>";
			
	$this->addBody($list);
}
else
{
	redirectTo(THISPAGE."?page=".$this->currentPage["name"]);
}

?>
