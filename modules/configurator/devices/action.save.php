<?php

// HomeAutomation
// Copyright (C) 2021 Tom Rosenback (tom.rosenback@gmail.com), Daniel Malmgren (daniel.malmgren@kolefors.se)
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
if(!defined("HomeAutomationIncluded")) { die("HomeAutomation: Direct access not premitted"); }

// disabling notice messages
error_reporting(E_ALL ^ E_NOTICE);

$deviceId = getFormVariable("device", 0);

// user logged in with user level 3 (admin)
if($_SESSION[CFG_SESSION_KEY]["userlevel"] >= 3 && ($deviceId == -1 || $deviceId > 0)) {
	$description = getFormVariable("description", "");
	$systemPluginName = getFormVariable("systempluginname", "");
	$rawdevice = getFormVariable("rawdevice", 0);

	$params = array(
		new dbParam(dbParamType::$STRING, "description", $description),
		new dbParam(dbParamType::$STRING, "systempluginname", $systemPluginName),
		new dbParam(dbParamType::$INTEGER, "type", getFormVariable("typeid", 1)),
		new dbParam(dbParamType::$INTEGER, "active", getFormVariable("active", 1)),
		new dbParam(dbParamType::$INTEGER, "consumption", getFormVariable("consumption", 0)),
		new dbParam(dbParamType::$INTEGER, "rawdevice", $rawdevice)
	);

	$rawlearncmd = getFormVariable("rawlearncmd", "");
	$rawoffcmd = getFormVariable("rawoffcmd", "");
	$rawoncmd = getFormVariable("rawoncmd", "");

	if($rawdevice == 1) {
		$params[] = new dbParam(dbParamType::$STRING, "rawlearncmd", $rawlearncmd);
		$params[] = new dbParam(dbParamType::$STRING, "rawoffcmd", $rawoffcmd);
		$params[] = new dbParam(dbParamType::$STRING, "rawoncmd", $rawoncmd);
	}

	$deviceId = Devices::save($params, $deviceId);

	$systemDeviceId = getFormVariable("systemdeviceid", "");
	$systemPlugin = SystemPlugins::load($systemPluginName);

	if(is_object($systemPlugin) && $systemDeviceId > 0) {
		$systemPlugin->changeDeviceName($systemDeviceId, $description);
	}

	updateCommandFile($deviceId, "learn", $rawlearncmd);
	updateCommandFile($deviceId, "off", $rawoffcmd);
	updateCommandFile($deviceId, "on", $rawoncmd);

	if(getFormVariable("reseticons", "") == "on") {
		deleteFile(HA_ROOT_PATH."/resources/icons/".$deviceId."_off.png");
		deleteFile(HA_ROOT_PATH."/resources/icons/".$deviceId."_on.png");
	} else if(getFormVariable("uploadicons", "") == "on") {
		saveUploadedFile($_FILES["icon_off"], HA_ROOT_PATH."/resources/icons/".$deviceId."_off.png", "jpg|png|gif|jpeg");
		saveUploadedFile($_FILES["icon_on"], HA_ROOT_PATH."/resources/icons/".$deviceId."_on.png", "jpg|png|gif|jpeg");
	}
}

redirectTo(THISPAGE."?page=".$this->currentPage["name"]);

?>
