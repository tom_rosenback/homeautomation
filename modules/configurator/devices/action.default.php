<?php
// HomeAutomation
// Copyright (C) 2021 Tom Rosenback (tom.rosenback@gmail.com), Daniel Malmgren (daniel.malmgren@kolefors.se)
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
if(!defined("HomeAutomationIncluded")) { die("HomeAutomation: Direct access not premitted"); }

// disabling notice messages
error_reporting(E_ALL ^ E_NOTICE);

$resetSortingLink = "<a href=\"".THISPAGE."?page=".$this->currentPage["name"]."\" id=\"resetSorting\" title=\"".LBL_CONFIRMRESETSORTING."\">".LBL_RESETSORTING."</a>";

// user logged in with user level 1 or higher (demo)
if($_SESSION[CFG_SESSION_KEY]["userlevel"] >= 1)
{
	$list = "<table width=\"100%\">
				<tr>
					<td class=\"bold\">
						".$this->currentPage["translation"]."
						<br />
					</td>
				</tr>
				<tr>
					<td>
						<table cellspacing=\"0\" cellpadding=\"3\" class=\"tablelist\" id=\"sortableList\">
							<thead>
								<tr>
									<td class=\"bold\">".LBL_ID."</td>
									<td class=\"bold\" align=\"center\">".LBL_ICONS."</td>
									<td class=\"bold\">".LBL_DESCRIPTION."</td>
									<td class=\"bold\">".LBL_SYSTEM."</td>
									<td class=\"bold\">".LBL_DEVICETYPE."</td>
									<td class=\"bold\">".LBL_POWERCONSUMPTION." (W)</td>
									<td class=\"bold\">".LBL_ACTIVE."</td>
									<td class=\"bold\">".LBL_RAWDEVICE."</td>
									<!--<td class=\"bold\">".LBL_RAWCOMMANDS."</td>-->
									<td class=\"bold\">&nbsp;</td>
								</tr>
							</thead>";
	
	$allDevices = Devices::get(-1, -1);
	$systemPlugins = SystemPlugins::load();
		
	$i = 0;
	
	if(count($allDevices) > 0) {
		$list .= "<tbody>";
		
		foreach($allDevices as $device) {
			$systemPluginName = $device["systempluginname"];
			$systemDisplayName = LBL_NOTINSTALLED." (".$device["systempluginname"].")";

			if(array_key_exists($systemPluginName, $systemPlugins) && is_object($systemPlugins[$systemPluginName]))
			{
				$systemDisplayName = $systemPlugins[$systemPluginName]->getDisplayName();
			}

			$list .= "<tr id=\"devices_".$device["id"]."\">
						<td>".$device["id"]."</td>
						<td align=\"center\"><img src=\"".getStatusImage($device["type"], 0, $device["id"])."\" alt=\"".$device["description"]." ".getStatusText(false)."\" title=\"".$device["description"]." ".getStatusText(false)."\" height=\"20px\">&nbsp;&nbsp;<img src=\"".getStatusImage($device["type"], 1, $device["id"])."\" alt=\"".$device["description"]." ".getStatusText(true)."\" title=\"".$device["description"]." ".getStatusText(true)."\" height=\"20px\"></td>
						<td>".$device["description"]."</td>
						<td>".$systemDisplayName."</td>
						<td>".$device["typetext"]."</td>
						<td style=\"padding-left: 5%;\">".$device["consumption"]." W</td>
						<td>".getBooleanText($device["active"])."</td>
						<td>".getBooleanText($device["rawdevice"])."</td>
						<!--<td>".checkRAWcommands($device["id"], $device["rawdevice"], $device["rawlearncmd"], $device["rawoffcmd"], $device["rawoncmd"])."</td>-->
						<td align=\"right\">";

			if($device["rawdevice"] == 1 || $device["rawdevice"] == true)
			{
				$list .= "<a href=\"".THISPAGE."?page=".$this->currentPage["name"]."&action=learn&device=".$device["id"]."\"><img src=\"./resources/run.png\" border=\"0\" width=\"20\" alt=\"".LBL_TEACH."\" title=\"".LBL_TEACH."\"></a>";
			}
					
			if($_SESSION[CFG_SESSION_KEY]["userlevel"] >= 3)
			{
				$list .= "<a href=\"".THISPAGE."?page=".$this->currentPage["name"]."&action=edit&device=".$device["id"]."\"><img src=\"./resources/edit.png\" border=\"0\" width=\"20\" alt=\"".LBL_EDIT."\" title=\"".LBL_EDIT."\"></a>
						<a href=\"".THISPAGE."?page=".$this->currentPage["name"]."&action=delete&device=".$device["id"]."\" onclick=\"return confirm('".LBL_CONFIRMDELETE." ".$device["description"]."?');\"><img src=\"./resources/delete.png\" border=\"0\" width=\"20\" alt=\"".LBL_DELETE."\" title=\"".LBL_DELETE."\"></a>";
			}
			
			$list .= "</td>
					</tr>";
		}

		$list .= "</tbody>";
	}
	else
	{
		$list .= "<tfoot>
					<tr>
						<td colspan=\"9\" class=\"bold\">".LBL_NODEVICESCONFIGURED."</td>
					</tr>
				</tfoot>";

		$resetSortingLink = "";
	}

	$list .= "			</table>
					</td>
				</tr>									
				<tr>
				<td class=\"bold\">";
	
	if($_SESSION[CFG_SESSION_KEY]["userlevel"] == 1 || $_SESSION[CFG_SESSION_KEY]["userlevel"] >= 3) {
		$list .= "<a href=\"".THISPAGE."?page=".$this->currentPage["name"]."&action=importdevices\">".LBL_UPDATEFROMSYSTEMPLUGINS."</a>&nbsp;-&nbsp;
					<a href=\"".THISPAGE."?page=".$this->currentPage["name"]."&action=edit&device=-1\">".LBL_NEWRAWDEVICE."</a>";
					
		if(count($allDevices) > 0) {
			$resetSortingLink = "&nbsp;-&nbsp;".$resetSortingLink;
		}
	}
	
	$list .= 		$resetSortingLink."
				</td>
			</tr>
		</table>";
		
	$this->addBody($list);
}
else
{
	redirectTo(THISPAGE);
}

?>