<?php

// HomeAutomation
// Copyright (C) 2021 Tom Rosenback (tom.rosenback@gmail.com), Daniel Malmgren (daniel.malmgren@kolefors.se)
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
if(!defined("HomeAutomationIncluded")) { die("HomeAutomation: Direct access not premitted"); }

// disabling notice messages
error_reporting(E_ALL ^ E_NOTICE);

$deviceId = getFormVariable("device", 0);

// user logged in with user level 3 (admin)
if($_SESSION[CFG_SESSION_KEY]["userlevel"] >= 3 && ($deviceId == -1 || $deviceId > 0)) {
	if($deviceId == -1) {
		$device["id"] = $deviceId;
		$device["description"] = "";
		$device["systempluginname"] = "";
		$device["type"] = "light";
		$device["typeid"] = 1;
		$device["active"] = 1;
		$device["rawdevice"] = 1;
		$device["consumption"] = 0;
		$device["showRawCommands"] = true;
	} else {
		$device = Devices::get($deviceId, -1);
		$device = $device[0];

		$device["showRawCommands"] = convertToBoolean($device["rawdevice"]);
	}

	$deviceTypes = Devices::getTypes();

	if($device["showRawCommands"] == true) {
		$deviceTypes = removeDeviceType($deviceTypes, array("absdimmer"));
	}

	$list .= '<table width="100%" cellpadding="0" cellspacing="0">
				<tr>
					<td>
						<form id="devicemanager" method="post" action="'.THISPAGE.'?page='.$this->currentPage["name"].'" enctype="multipart/form-data">
							<input type="hidden" name="device" value="'.$device["id"].'">
							<input type="hidden" name="action" id="action" value="">
							<input type="hidden" name="rawdevice" value="'.$device["rawdevice"].'">
							<input type="hidden" name="systemdeviceid" value="'.$device["systemdeviceid"].'">
							<table>
								<tr>
									<td class="bold">'.LBL_DESCRIPTION.':</td>
									<td><input type="text" name="description" class="text3" value="'.$device["description"].'"></td>
								</tr>
								<tr>
									<td class="bold">'.LBL_SYSTEM.':</td>
									<td>'.generateSelector($device["systempluginname"], "systempluginname", "text3", "getPluginName", "getDisplayName", SystemPlugins::load(), "", LBL_CHOOSESYSTEM).'</td>
								</tr>
								<tr>
									<td class="bold">'.LBL_SYSTEMDEVICEID.':</td>
									<td><input type="text" class="text3" value="'.$device["systemdeviceid"].'" disabled="disabled"></td>
								</tr>


								<tr>
									<td class="bold">'.LBL_NOTIFICATIONPLUGINS.':</td></tr>
								<tr><td><ul id="npList" style="width: 100%">';


	$notificationPlugins = SystemPlugins::loadNotificationPlugins($device["id"], true);

    foreach($notificationPlugins as $np) {
        $npChecked = "";

        if($np["isconnected"] == 1) {
            $npChecked = " checked=\"checked\"";
        }

        $list .= "<li>
                    <label><input type=\"checkbox\" name=\"np[".$np["pluginname"]."]\" id=\"np_".$np["pluginname"]."\" value=\"".$np["pluginname"]."\"".$npChecked.">".$np["plugin"]->getDisplayName()."</label>
                </li>";
    }


								$list .= '</ul></td></tr>
								<tr>
									<td class="bold">'.LBL_DEVICETYPE.':</td>
									<td>'.generateSelector($device["typeid"], "typeid", "text3", "id", "text", $deviceTypes, 1, LBL_CHOOSETYPE).'</td>
								</tr>
								<tr>
									<td class="bold">'.LBL_ICONS.':</td>
									<td>
										<table width="100%" cellpadding="0" cellspacing="0">
											<tr>
												<td colspan="4" class="bold">

												</td>
											</tr>
											<tr>
												<td class="bold">'.LBL_OFF.':</td>
												<td><img src="'.getStatusImage($device["type"], 0, $device["id"]).'" alt="'.$device["description"].' '.getStatusText(false).'" title="'.$device["description"].' '.getStatusText(false).'" height="20px"></td>
												<td class="bold"><label><input type="checkbox" name="reseticons" id="reseticons" onclick="iconAction(this.id);" />'.LBL_RESETICONS.'</label></td>
											</tr>
											<tr>
												<td class="bold">'.LBL_ON.':</td>
												<td><img src="'.getStatusImage($device["type"], 1, $device["id"]).'" alt="'.$device["description"].' '.getStatusText(true).'" title="'.$device["description"].' '.getStatusText(true).'" height="20px"></td>
												<td class="bold"><label><input type="checkbox" name="uploadicons" id="uploadicons" onclick="iconAction(this.id);" />'.LBL_UPLOADNEWICONS.'</label></td>
											</tr>
										</table>
										<table width="100%" cellpadding="0" cellspacing="0" style="display: none;" id="newIconsTable">
											<tr>
												<td class="bold" colspan="2">&nbsp;</td>
											</tr>
											<tr>
												<td class="bold">'.LBL_OFF.':</td>
												<td align="left"><input type="file" name="icon_off" class="text2" /></td>
											</tr>
											<tr>
												<td class="bold">'.LBL_ON.':</td>
												<td align="left"><input type="file" name="icon_on" class="text2" /></td>
											</tr>
										</table>
									</td>
								</tr>

								<tr>
									<td class="bold">'.LBL_POWERCONSUMPTION.' (W):</td>
									<td><input type="text" name="consumption" class="text" value="'.$device["consumption"].'"></td>
								</tr>
								<tr>
									<td class="bold">'.LBL_ACTIVE.':</td>
									<td>'.generateSelector($device["active"], "active", "text", 0, 1, array(array("0", mb_convert_case(LBL_NO, MB_CASE_LOWER, "UTF-8")), array("1", mb_convert_case(LBL_YES, MB_CASE_LOWER, "UTF-8"))), 0, "").'</td>
								</tr>
								<tr>
									<td class="bold">'.LBL_RAWDEVICE.':</td>
									<td><input type="text" name="tdid" class="text" value="'.getBooleanText($device["rawdevice"]).'" disabled="disabled"></td>
								</tr>';

	if($device["showRawCommands"]) {
		$list .= '	<tr>
									<td class="bold" colspan="2"><br />'.LBL_RAWCOMMANDS.'</td>
								</tr>
								<tr>
									<td class="bold">'.LBL_LEARN.':</td>
									<td><input type="text" name="rawlearncmd" class="text4" value="'.$device["rawlearncmd"].'"></td>
								</tr>
								<tr>
									<td class="bold">'.LBL_OFF.':</td>
									<td><input type="text" name="rawoffcmd" class="text4" value="'.$device["rawoffcmd"].'"></td>
								</tr>
								<tr>
									<td class="bold">'.LBL_ON.':</td>
									<td><input type="text" name="rawoncmd" class="text4" value="'.$device["rawoncmd"].'"></td>
								</tr>';
	}

	$list .= '		<tr>
									<td colspan="2">
										&nbsp;
									</td>
								</tr>
								<tr>
									<td colspan="2" align="right">
										<input type="submit" value="'.LBL_CANCEL.'" name="cancel" class="button setActionByButton">
										<input type="submit" value="'.LBL_DELETE.'" name="delete" class="button setActionByButton">
										<input type="submit" value="'.LBL_SAVE.'" name="save" class="button setActionByButton">
									</td>
								</tr>
								<tr>
									<td colspan="2">
										&nbsp;
									</td>
								</tr>
							</table>
						</form>
					</td>
				</tr>
			</table>';

	$this->addBody($list);
}
else
{
	redirectTo(THISPAGE."?page=".$this->currentPage["name"]);
}

?>
