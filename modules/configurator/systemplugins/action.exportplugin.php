<?php

// HomeAutomation
// Copyright (C) 2021 Tom Rosenback (tom.rosenback@gmail.com), Daniel Malmgren (daniel.malmgren@kolefors.se)
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
if(!defined("HomeAutomationIncluded")) { die("HomeAutomation: Direct access not premitted"); }

// disabling notice messages
error_reporting(E_ALL ^ E_NOTICE);

$pluginName = getFormVariable("plugin", "");

// user logged in with user level 3 (admin)
if($_SESSION[CFG_SESSION_KEY]["userlevel"] >= 3 && $pluginName != "")
{
	$systemPlugin = SystemPlugins::load($pluginName);
	if(is_object($systemPlugin)) {
		$pluginRootDir = HA_ROOT_PATH."/system/systemplugins/".$systemPlugin->getPluginName();

		$xml  = "<?xml version=\"1.0\" encoding=\"utf-8\"?>"."\n";
		$xml .= "<systemplugin>\n";
		$xml .= "	<pluginname>".$systemPlugin->getPluginName()."</pluginname>\n";
		$xml .= "	<displayname>".$systemPlugin->getDisplayName()."</displayname>\n";
		$xml .= "	<version>".$systemPlugin->getVersion()."</version>\n";
		$xml .= "	<author>".$systemPlugin->getAuthor()."</author>\n";
		$xml .= "	<authoremail>".$systemPlugin->getAuthorEmail()."</authoremail>\n";

		foreach(getRecursiveFileList($pluginRootDir) as $file) {
			$xml .= "	<file>\n";
			$xml .= "		<filename>".$file["filename"]."</filename>\n";
			$xml .= "		<isdir>".$file["isdir"]."</isdir>\n";
			
			if(!$file["isdir"])
			{
				$data = base64_encode(file_get_contents($pluginRootDir.$file["filename"]));
				$xml .= "		<data><![CDATA[".$data."]]></data>\n";
			}
			
			$xml .= "	</file>\n";
		}

		$xml .= "</systemplugin>\n";
		$xmlFileName = $systemPlugin->getPluginName()."-".$systemPlugin->getVersion().".xml";

		// and send the file
		ob_end_clean();
		header("Content-Description: File Transfer");
		header("Content-Type: application/force-download");
		header("Content-Disposition: attachment; filename=".$xmlFileName);
		//     header('Content-Type: text/xml');
		echo $xml;
		exit();
	}
}
else
{
	redirectTo(THISPAGE);
}

?>