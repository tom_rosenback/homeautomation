<?php
// HomeAutomation
// Copyright (C) 2021 Tom Rosenback (tom.rosenback@gmail.com), Daniel Malmgren (daniel.malmgren@kolefors.se)
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
if(!defined("HomeAutomationIncluded")) { die("HomeAutomation: Direct access not premitted"); }

// disabling notice messages
error_reporting(E_ALL ^ E_NOTICE);

// user logged in with user level 1 or higher (demo)
if($_SESSION[CFG_SESSION_KEY]["userlevel"] >= 3)
{
	$list = "<table width=\"100%\">
				<tr>
					<td class=\"bold\">
						".$this->currentPage["translation"]."
						<br />
					</td>
				</tr>";
	$list .= "	<tr>
					<td>
						<table cellspacing=\"0\" cellpadding=\"5\" class=\"tablelist\">
							<thead>
								<tr>
									<td class=\"bold\">".LBL_SYSTEMPLUGIN."</td>
									<td class=\"bold\">".LBL_AUTHOR."</td>
									<td class=\"bold\">".LBL_INSTALLEDVERSION."</td>
									<td class=\"bold\">&nbsp;</td>
									<td class=\"bold\">&nbsp;</td>
									<td class=\"bold\">&nbsp;</td>
								</tr>
							</thead>";

	$systemPlugins = SystemPlugins::load("", false, "action");

	if(count($systemPlugins) > 0) {
		$list .= "<tbody>";

		foreach($systemPlugins as $systemPlugin) {
			$upgradeInstallEditSettingsLink = "<a href=\"".THISPAGE."?page=".$this->currentPage["name"]."&action=installplugin&plugin=".$systemPlugin->getPluginName()."\" onclick=\"return confirm('".LBL_CONFIRMINSTALL." ".$systemPlugin->getDisplayName()."?');\" class=\"link\">".LBL_INSTALL." (".$systemPlugin->getVersion().")</a>";
			$uninstallRemoveLink = "<a href=\"".THISPAGE."?page=".$this->currentPage["name"]."&action=removeplugin&plugin=".$systemPlugin->getPluginName()."\" onclick=\"return confirm('".LBL_CONFIRMREMOVE." ".$systemPlugin->getDisplayName()."?');\" class=\"link\">".LBL_REMOVE."</a>";

			if($systemPlugin->installedVersion != -1)
			{
				$upgradeInstallEditSettingsLink = "";

				$uninstallRemoveLink = "<a href=\"".THISPAGE."?page=".$this->currentPage["name"]."&action=uninstallplugin&plugin=".$systemPlugin->getPluginName()."\" onclick=\"return confirm('".LBL_CONFIRMUNINSTALL." ".$systemPlugin->getDisplayName()."?');\" class=\"link\">".LBL_UNINSTALL."</a>";

				if(version_compare($systemPlugin->getVersion(), $systemPlugin->installedVersion, "!=")) {
					$upgradeInstallEditSettingsLink = "<a href=\"".THISPAGE."?page=".$this->currentPage["name"]."&action=installplugin&plugin=".$systemPlugin->getPluginName()."\" onclick=\"return confirm('".LBL_CONFIRMUPGRADE." ".$systemPlugin->getDisplayName()."?');\" class=\"link\">".LBL_UPGRADE." (".$systemPlugin->getVersion().")</a>";
				} else if(version_compare($systemPlugin->getVersion(), $systemPlugin->installedVersion, "==") && ($systemPlugin->hasSettings == 1 || $systemPlugin->hasPluginPage)) {
					$upgradeInstallEditSettingsLink = "<a href=\"".THISPAGE."?page=".$this->currentPage["name"]."&action=editsettings&plugin=".$systemPlugin->getPluginName()."\" class=\"link\">".$systemPlugin->getPluginPageLinkText()."</a>";
				}
			}

			$installedInfo = "-";
			$exportLink = "";

			if($systemPlugin->installedVersion != -1) {
				$installedInfo = $systemPlugin->installedVersion." (".convertDateTimeToLocal($systemPlugin->dateInstalled, false, false, true).")";
				$exportLink = "<a href=\"".THISPAGE."?page=".$this->currentPage["name"]."&action=exportplugin&plugin=".$systemPlugin->getPluginName()."\" class=\"link\"><img src=\"resources/xml.png\" alt=\"".LBL_EXPORTTOXML."\" title=\"".LBL_EXPORTTOXML."\"></a>";
			}

			$list .= "<tr>
						<td>".$systemPlugin->getDisplayName()."</td>
						<td>".$systemPlugin->getAuthor()."</td>
						<td align=\"center\">".$installedInfo."</td>
						<td align=\"center\">
							".$upgradeInstallEditSettingsLink."
						</td>
						<td align=\"right\">
							".$uninstallRemoveLink."
						</td>
						<td align=\"right\">
							".$exportLink."
						</td>
					</tr>";

			$i++;
		}

		$list .= "</tbody>";
	}
	else
	{
		$list .= "<tfoot>
					<tr>
						<td colspan=\"6\" class=\"bold\">".LBL_NOSYSTEMPLUGINSINSTALLED."</td>
					</tr>
				</tfoot>";
	}
	$list .= "				</table>
						</td>
					</tr>
				</table>";

	$list .= "<table width=\"100%\">
				<tr>
					<td class=\"bold\">
						".LBL_NOTIFICATIONPLUGINS."
						<br />
					</td>
				</tr>";
	$list .= "	<tr>
					<td>
						<table cellspacing=\"0\" cellpadding=\"5\" class=\"tablelist\">
							<thead>
								<tr>
									<td class=\"bold\">".LBL_SYSTEMPLUGIN."</td>
									<td class=\"bold\">".LBL_AUTHOR."</td>
									<td class=\"bold\">".LBL_INSTALLEDVERSION."</td>
									<td class=\"bold\">&nbsp;</td>
									<td class=\"bold\">&nbsp;</td>
									<td class=\"bold\">&nbsp;</td>
								</tr>
							</thead>";


	$systemPlugins = SystemPlugins::load("", false, "notification");

	if(count($systemPlugins) > 0) {
		$list .= "<tbody>";

		foreach($systemPlugins as $systemPlugin) {
			$upgradeInstallEditSettingsLink = "<a href=\"".THISPAGE."?page=".$this->currentPage["name"]."&action=installplugin&plugin=".$systemPlugin->getPluginName()."\" onclick=\"return confirm('".LBL_CONFIRMINSTALL." ".$systemPlugin->getDisplayName()."?');\" class=\"link\">".LBL_INSTALL." (".$systemPlugin->getVersion().")</a>";
			$uninstallRemoveLink = "<a href=\"".THISPAGE."?page=".$this->currentPage["name"]."&action=removeplugin&plugin=".$systemPlugin->getPluginName()."\" onclick=\"return confirm('".LBL_CONFIRMREMOVE." ".$systemPlugin->getDisplayName()."?');\" class=\"link\">".LBL_REMOVE."</a>";

			if($systemPlugin->installedVersion != -1)
			{
				$upgradeInstallEditSettingsLink = "";

				$uninstallRemoveLink = "<a href=\"".THISPAGE."?page=".$this->currentPage["name"]."&action=uninstallplugin&plugin=".$systemPlugin->getPluginName()."\" onclick=\"return confirm('".LBL_CONFIRMUNINSTALL." ".$systemPlugin->getDisplayName()."?');\" class=\"link\">".LBL_UNINSTALL."</a>";

				if(version_compare($systemPlugin->getVersion(), $systemPlugin->installedVersion, "!=")) {
					$upgradeInstallEditSettingsLink = "<a href=\"".THISPAGE."?page=".$this->currentPage["name"]."&action=installplugin&plugin=".$systemPlugin->getPluginName()."\" onclick=\"return confirm('".LBL_CONFIRMUPGRADE." ".$systemPlugin->getDisplayName()."?');\" class=\"link\">".LBL_UPGRADE." (".$systemPlugin->getVersion().")</a>";
				} else if(version_compare($systemPlugin->getVersion(), $systemPlugin->installedVersion, "==") && ($systemPlugin->hasSettings == 1 || $systemPlugin->hasPluginPage)) {
					$upgradeInstallEditSettingsLink = "<a href=\"".THISPAGE."?page=".$this->currentPage["name"]."&action=editsettings&plugin=".$systemPlugin->getPluginName()."\" class=\"link\">".$systemPlugin->getPluginPageLinkText()."</a>";
				}
			}

			$installedInfo = "-";
			$exportLink = "";

			if($systemPlugin->installedVersion != -1) {
				$installedInfo = $systemPlugin->installedVersion." (".convertDateTimeToLocal($systemPlugin->dateInstalled, false, false, true).")";
				$exportLink = "<a href=\"".THISPAGE."?page=".$this->currentPage["name"]."&action=exportplugin&plugin=".$systemPlugin->getPluginName()."\" class=\"link\"><img src=\"resources/xml.png\" alt=\"".LBL_EXPORTTOXML."\" title=\"".LBL_EXPORTTOXML."\"></a>";
			}

			$list .= "<tr>
						<td>".$systemPlugin->getDisplayName()."</td>
						<td>".$systemPlugin->getAuthor()."</td>
						<td align=\"center\">".$installedInfo."</td>
						<td align=\"center\">
							".$upgradeInstallEditSettingsLink."
						</td>
						<td align=\"right\">
							".$uninstallRemoveLink."
						</td>
						<td align=\"right\">
							".$exportLink."
						</td>
					</tr>";

			$i++;
		}

		$list .= "</tbody>";
	}
	else
	{
		$list .= "<tfoot>
					<tr>
						<td colspan=\"6\" class=\"bold\">".LBL_NOSYSTEMPLUGINSINSTALLED."</td>
					</tr>
				</tfoot>";
	}
	$list .= "				</table>
						</td>
					</tr>
				</table>";

	$list .= "<table width=\"100%\">
				<tr>
					<td class=\"bold\">
						".LBL_AVAILABLEPLUGINS."
						<br />
					</td>
				</tr>";
	$list .= "	<tr>
					<td>
						<table cellspacing=\"0\" cellpadding=\"5\" class=\"tablelist\">
							<thead>
								<tr>
									<td class=\"bold\">".LBL_SYSTEMPLUGIN."</td>
									<td class=\"bold\">".LBL_AUTHOR."</td>
									<td class=\"bold\">&nbsp;</td>
									<td class=\"bold\">&nbsp;</td>
									<td class=\"bold\">&nbsp;</td>
								</tr>
							</thead>";

	$systemPlugins = SystemPlugins::load("", true, "");

	if(count($systemPlugins) > 0) {
		$list .= "<tbody>";

		foreach($systemPlugins as $systemPlugin) {
			$upgradeInstallEditSettingsLink = "<a href=\"".THISPAGE."?page=".$this->currentPage["name"]."&action=installplugin&plugin=".$systemPlugin->getPluginName()."\" onclick=\"return confirm('".LBL_CONFIRMINSTALL." ".$systemPlugin->getDisplayName()."?');\" class=\"link\">".LBL_INSTALL." (".$systemPlugin->getVersion().")</a>";
			$uninstallRemoveLink = "<a href=\"".THISPAGE."?page=".$this->currentPage["name"]."&action=removeplugin&plugin=".$systemPlugin->getPluginName()."\" onclick=\"return confirm('".LBL_CONFIRMREMOVE." ".$systemPlugin->getDisplayName()."?');\" class=\"link\">".LBL_REMOVE."</a>";

			if($systemPlugin->installedVersion != -1)
			{
				$upgradeInstallEditSettingsLink = "";

				$uninstallRemoveLink = "<a href=\"".THISPAGE."?page=".$this->currentPage["name"]."&action=uninstallplugin&plugin=".$systemPlugin->getPluginName()."\" onclick=\"return confirm('".LBL_CONFIRMUNINSTALL." ".$systemPlugin->getDisplayName()."?');\" class=\"link\">".LBL_UNINSTALL."</a>";

				if(version_compare($systemPlugin->getVersion(), $systemPlugin->installedVersion, "!=")) {
					$upgradeInstallEditSettingsLink = "<a href=\"".THISPAGE."?page=".$this->currentPage["name"]."&action=installplugin&plugin=".$systemPlugin->getPluginName()."\" onclick=\"return confirm('".LBL_CONFIRMUPGRADE." ".$systemPlugin->getDisplayName()."?');\" class=\"link\">".LBL_UPGRADE." (".$systemPlugin->getVersion().")</a>";
				} else if(version_compare($systemPlugin->getVersion(), $systemPlugin->installedVersion, "==") && ($systemPlugin->hasSettings == 1 || $systemPlugin->hasPluginPage)) {
					$upgradeInstallEditSettingsLink = "<a href=\"".THISPAGE."?page=".$this->currentPage["name"]."&action=editsettings&plugin=".$systemPlugin->getPluginName()."\" class=\"link\">".LBL_EDITSETTINGS."</a>";
				}
			}

			$installedInfo = "-";
			$exportLink = "";

			if($systemPlugin->installedVersion != -1) {
				$installedInfo = $systemPlugin->installedVersion." (".convertDateTimeToLocal($systemPlugin->dateInstalled, false, false, true).")";
				$exportLink = "<a href=\"".THISPAGE."?page=".$this->currentPage["name"]."&action=exportplugin&plugin=".$systemPlugin->getPluginName()."\" class=\"link\"><img src=\"resources/xml.png\" alt=\"".LBL_EXPORTTOXML."\" title=\"".LBL_EXPORTTOXML."\"></a>";
			}

			$list .= "<tr>
						<td>".$systemPlugin->getDisplayName()."</td>
						<td>".$systemPlugin->getAuthor()."</td>
						<td align=\"center\">
							".$upgradeInstallEditSettingsLink."
						</td>
						<td align=\"right\">
							".$uninstallRemoveLink."
						</td>
						<td align=\"right\">
							".$exportLink."
						</td>
					</tr>";

			$i++;
		}

		$list .= "</tbody>";
	}
	else
	{
		$list .= "<tfoot>
					<tr>
						<td colspan=\"6\" class=\"bold\">".LBL_NOSYSTEMPLUGINSINSTALLED."</td>
					</tr>
				</tfoot>";
	}

	$list .= "				</table>
						</td>
					</tr>
					<tr>
						<td>
							<form action=\"".THISPAGE."?page=".$this->currentPage["name"]."&action=importplugin\" method=\"post\"  enctype=\"multipart/form-data\">
								<table>
									<tr>
										<td class=\"bold\" colspan=\"2\">".LBL_INSTALLPLUGINFROMXML."</td>
									<tr>
									<tr>
										<td class=\"bold\" width=\"20%\"><label for=\"importfile\">".LBL_FILE.":</label></td>
										<td><input type=\"file\" name=\"importfile\" id=\"importfile\" class=\"text4\" size=\"40\" /></td>
									</tr>
									<tr>
										<td colspan=\"2\" align=\"right\"><input type=\"submit\" value=\"".LBL_INSTALL."\" class=\"button\" onclick=\"return confirm('".LBL_CONFIRMINSTALLPLUGINFROMXML."');\"/></td>
									</tr>
								</table>
							</form>
						</td>
					</tr>
				</table>";

	$this->addBody($list);
}
else
{
	redirectTo(THISPAGE);
}

?>
