<?php

// HomeAutomation
// Copyright (C) 2021 Tom Rosenback (tom.rosenback@gmail.com), Daniel Malmgren (daniel.malmgren@kolefors.se)
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
if(!defined("HomeAutomationIncluded")) { die("HomeAutomation: Direct access not premitted"); }

// disabling notice messages
error_reporting(E_ALL ^ E_NOTICE);

$importFile = $_FILES["importfile"];

// user logged in with user level 3 (admin)
if($_SESSION[CFG_SESSION_KEY]["userlevel"] >= 3 && $importFile != -1)
{
/*	
	<?xml version="1.0" encoding="utf-8"?>
	<systemplugin>
		<name>SystemPluginName</name>
		<version>Version</version>
		<author>Author</author>
		<authoremail>AuthorEmail</authoremail>
		<file>
			<filename>/filename.php</filename>
			<isdir>0</isdir>
			<data><![CDATA[base64encoded file content]]></data>
		</file>
		<file>
			<filename>/dirname</filename>
			<isdir>1</isdir>
		</file>
	</systemplugin>
*/
	installPluginFromXML($importFile["tmp_name"]);
	unlink($importFile["tmp_name"]);
	
	redirectTo(THISPAGE."?page=".$this->currentPage["name"]);
}
else
{
	redirectTo(THISPAGE);
}

function installPluginFromXML($fileToImport)
{
	 // read the XML contents
    $parser = xml_parser_create();
    xml_parser_set_option($parser, XML_OPTION_CASE_FOLDING, 0);
    xml_parser_set_option($parser, XML_OPTION_SKIP_WHITE, 1);
    xml_parse_into_struct($parser, file_get_contents($fileToImport), $values, $tags);
    xml_parser_free($parser);

	// debugVariable($values);
	
	$pluginsRootDir = HA_ROOT_PATH."/system/systemplugins";
	$pluginDir = "";
	$pluginName = "";
	
	$file = array();
	
	foreach($values as $element) {
		// echo $element["tag"]." ".$element["type"]."<br />";
		
		switch($element["tag"]) {
			case "pluginname":
			{
				if($element["type"] == "complete")
				{
					$pluginName = $element["value"];
					$pluginDir = $pluginsRootDir."/".$pluginName;
					
					if(!is_dir($pluginDir))
					{
						@mkdir($pluginDir);
					}
				}
				
				break;
			}
			case "file":
			{
				if($element["type"] == "open")
				{
					$file = array();
				}
				else if($element["type"] == "close")
				{
					// debugVariable($file);
				
					// save file
					if(isset($file["isdir"]) && isset($file["filename"]) && $pluginDir != "")
					{
						$filename = $pluginDir.$file["filename"];
						
						if($file["isdir"] == 1)
						{
							if(!is_dir($filename))
							{
								@mkdir($filename);
							}
						}
						else
						{
							$data = "";
								
							if(isset($file["data"]))
							{
								$data = base64_decode($file["data"]);
							}
							
							$fh = fopen($filename, "w");
							if ($fh === false)
							{
								die("Could not open '$filename' for writing");
							}
							fwrite($fh, $data);
							fclose($fh);
						}
					}
				}
				
				break;
			}
			case "filename":
			case "isdir":
			case "data":
			{
				if($element["type"] == "complete")
				{
					$file[$element["tag"]] = $element["value"];
				}
				
				break;
			}
			default:
			{
				break;
			}
		}
	}
	
	if($pluginName != "") {
		installSystemPlugin($pluginName, true);
	}
}

?>