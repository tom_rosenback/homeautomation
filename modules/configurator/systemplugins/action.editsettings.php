<?php

// HomeAutomation
// Copyright (C) 2021 Tom Rosenback (tom.rosenback@gmail.com), Daniel Malmgren (daniel.malmgren@kolefors.se)
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
if(!defined("HomeAutomationIncluded")) { die("HomeAutomation: Direct access not premitted"); }

// disabling notice messages
error_reporting(E_ALL ^ E_NOTICE);

$this->addHeader("js-configurator", "<script language=\"javascript\" type=\"text/javascript\" src=\"modules/configurator/script.js\"></script>");

$pluginName = getFormVariable("plugin", "");

// user logged in with user level 3 (admin)
if($_SESSION[CFG_SESSION_KEY]["userlevel"] >= 3 && $pluginName != "")
{
	$systemPlugin = SystemPlugins::load($pluginName);
	if(is_object($systemPlugin)) {
		$list = "<table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">
					<tr>
						<td>
							<form id=\"systempluginsettingmanager\" method=\"post\" action=\"".THISPAGE."?page=".$this->currentPage["name"]."\">
								<input type=\"hidden\" name=\"plugin\" value=\"".$systemPlugin->getPluginName()."\">
								<input type=\"hidden\" name=\"action\" id=\"action\" value=\"\">
								<table>
									<tr>
										<td class=\"bold\" colspan=\"2\">
											".LBL_SYSTEMPLUGINSETTINGS." - ".$systemPlugin->getDisplayName()." (v".$systemPlugin->getVersion().")
										</td>
									</tr>";
		$i = 0;

		foreach($systemPlugin->getSettings() as $setting) {
			$translation = $systemPlugin->getTranslation($setting["name"]);

			$list .= "
						<tr>
							<td valign=\"top\" class=\"bold\">
								<label for=\"set_".$setting["name"]."\">".$translation.":</label>
							</td>
							<td>
								<input type=\"text\" class=\"text3\" name=\"set_".$setting["name"]."\" id=\"set_".$setting["name"]."\" value=\"".str_replace("\"", "&quot;", $setting["value"])."\" />
							</td>
						</tr>";

			$i++;
		}

		$pluginPage = $systemPlugin->pluginPage();

		if($pluginPage != "") {
			$list .= "<tr><td colspan=\"2\">".$pluginPage."</tr>";
		}

		$list .= "
									<tr>
										<td colspan=\"2\" align=\"right\">
											<input type=\"submit\" name=\"cancel\" value=\"".LBL_CANCEL."\" class=\"button setActionByButton\" /> <input type=\"submit\" id=\"saveBtn\" name=\"savesettings\" value=\"".LBL_SAVE."\" class=\"button setActionByButton\" />
										</td>
									</tr>
								</table>
							</form>
						</td>
					</tr>";

		$list .= "</table>";

		$this->addBody($list);
	}
	else
	{
		redirectTo(THISPAGE."?page=".$this->currentPage["name"]);
	}
}
else
{
	redirectTo(THISPAGE."?page=".$this->currentPage["name"]);
}

?>
