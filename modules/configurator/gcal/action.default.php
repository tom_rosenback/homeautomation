<?php
// HomeAutomation
// Copyright (C) 2021 Tom Rosenback (tom.rosenback@gmail.com), Daniel Malmgren (daniel.malmgren@kolefors.se)
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
if(!defined("HomeAutomationIncluded")) { die("HomeAutomation: Direct access not premitted"); }

// disabling notice messages
error_reporting(E_ALL ^ E_NOTICE);

// user logged in with user level 1 or higher (demo)
if($_SESSION[CFG_SESSION_KEY]["userlevel"] >= 3)
{
	$gcalSettings = GoogleCalendar::getSettings();
		
	$list = "<table width=\"100%\">
				<tr>
					<td class=\"bold\">
						".$this->currentPage["translation"]."
						<br />
					</td>
				</tr>
				<tr>
					<td>
						<form method=\"post\" action=\"".THISPAGE."?page=".$this->currentPage["name"]."\" class=\"enterInputDisabled\">
							<input type=\"hidden\" name=\"action\" id=\"action\" value=\"\">
							<table>
								<tr>
									<td class=\"bold\"><label for=\"username\">".LBL_GUSERNAME.":</label></td>
									<td><input type=\"text\" name=\"username\" id=\"username\" class=\"text2\" value=\"".$gcalSettings["username"]."\"></td>
								</tr>
								<tr>
									<td class=\"bold\"><label for=\"password\">".LBL_GPASSWORD.":</label></td>
									<td><input type=\"password\" name=\"password\" id=\"password\" class=\"text2\" value=\"".$gcalSettings["password"]."\"></td>
								</tr>
								<tr>
									<td align=\"right\" colspan=\"2\">
										<input id=\"gcallogin\" type=\"button\" value=\"".LBL_LOGIN."\" class=\"button\" />
									</td>
								</tr>
								<tr>
									<td class=\"bold\" colspan=\"2\">".LBL_AVAILABLECALENDARS."</label></td>
								</tr>
								<tr>
									<td colspan=\"2\" id=\"availablecalendars\">
										".LBL_LOGINFIRST."
									</td>
								</tr>
								<tr>
									<td colspan=\"2\">
										<span id=\"loading\" style=\"display: none;\"><img src=\"resources/loading.gif\" width=\"20px\" align=\"middle\" />&nbsp;".LBL_LOADING."</span>
									</td>
								</tr>									
								<tr>
									<td colspan=\"2\" align=\"right\">
										<input type=\"submit\" value=\"".LBL_SAVE."\" id=\"saveBtn\" name=\"save\" class=\"button setActionByButton\" />
									</td>
								</tr>
							</table>
						</form>
					</td>
				</tr>
			</table>";
		
	$this->addBody($list);
}
else
{
	redirectTo(THISPAGE);
}

?>