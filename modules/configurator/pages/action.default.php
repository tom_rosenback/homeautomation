<?php
// HomeAutomation
// Copyright (C) 2021 Tom Rosenback (tom.rosenback@gmail.com), Daniel Malmgren (daniel.malmgren@kolefors.se)
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
if(!defined("HomeAutomationIncluded")) { die("HomeAutomation: Direct access not premitted"); }

// disabling notice messages
error_reporting(E_ALL ^ E_NOTICE);

$resetSortingLink = "<a href=\"".THISPAGE."?page=".$this->currentPage["name"]."\" id=\"resetSorting\" title=\"".LBL_CONFIRMRESETSORTING."\">".LBL_RESETSORTING."</a>";

// user logged in with user level 1 or higher (demo)
if($_SESSION[CFG_SESSION_KEY]["userlevel"] >= 3)
{
	$userlevels = Users::getUserlevels();
	
	$list = "<table width=\"100%\">
				<tr>
					<td class=\"bold\">
						".$this->currentPage["translation"]."
						<br />
					</td>
				</tr>
				<tr>
					<td>
						<table cellspacing=\"0\" cellpadding=\"5\" class=\"tablelist\" id=\"sortableList\">
							<thead>
								<tr>
									<td class=\"bold\">&nbsp;</td>
									<td class=\"bold\">".LBL_PAGE."</td>";
									
	if($userlevels !== false) {
		foreach($userlevels as $userlevel) {
			$list .= 					"<td class=\"bold\">".ucfirst($userlevel["description"])."</td>";
		}
	}
	
	$list .=						"</tr>
							</thead>";

	$pages = Pages::get();
	
	if(count($pages) > 0) {
		$list .= "<tbody>".generatePageList($userlevels, $pages)."</tbody>";
		$resetSortingLink = "&nbsp;-&nbsp;".$resetSortingLink;
	}
	else
	{
		$list .= "<tfoot>
					<tr>
						<td colspan=\"3\" class=\"bold\">".LBL_NOPAGESCONFIGURED."</td>
					</tr>
				</tfoot>";
				
		$resetSortingLink = "";
	}
	
	$list .= "				</table>
						</td>
					</tr>
				</table>";
		
	$this->addBody($list);
}
else
{
	redirectTo(THISPAGE);
}

?>