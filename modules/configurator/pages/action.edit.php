<?php

// HomeAutomation
// Copyright (C) 2021 Tom Rosenback (tom.rosenback@gmail.com), Daniel Malmgren (daniel.malmgren@kolefors.se)
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
if(!defined("HomeAutomationIncluded")) { die("HomeAutomation: Direct access not premitted"); }

// disabling notice messages
error_reporting(E_ALL ^ E_NOTICE);

$id = getFormVariable("id", -1);

// user logged in with user level 3 (admin)
if($_SESSION[CFG_SESSION_KEY]["userlevel"] >= 3 && ($id == -1 || $id > 0))
{
	$list = "";
	
	if($id == -1) {
		$macro["id"] = $id;
		$macro["name"] = "";
		$macro["comment"] = "";
		$macro["scenario"] = -1;
		$macro["relations"] = array();
	}
	else
	{
		$macro = Macros::get($id);
		$macro = $macro[0];
	}
	
	$devices = Devices::get();
	
	$list .= "<table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">
				<tr>
					<td>
						<form id=\"macromanager\" method=\"post\" action=\"".THISPAGE."?page=".$this->currentPage["name"]."\" enctype=\"multipart/form-data\">
							<input type=\"hidden\" name=\"id\" value=\"".$macro["id"]."\">
							<input type=\"hidden\" name=\"action\" id=\"action\" value=\"\">
							<table>
								<tr>
									<td class=\"bold\">
										<label for=\"name\">".LBL_MACRO.":</label>
									</td>
									<td>
										<input type=\"text\" name=\"name\" id=\"name\" class=\"text2\" value=\"".$macro["name"]."\">
									</td>
								</tr>
								<tr>
									<td valign=\"top\" class=\"bold\">
										<label for=\"comment\">".LBL_COMMENT.":</label>
									</td>
									<td>
										<textarea name=\"comment\" id=\"comment\" class=\"textarea\">".$macro["comment"]."</textarea>
									</td>
								</tr>
								<tr>
									<td class=\"bold\">".LBL_ICON.":</td>
									<td>
										<table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">
											<tr>
												<td>
													<img src=\"".getStatusImage("macro", 1, $macro["id"], "macros")."\" alt=\"".$macro["name"]."\" title=\"".$macro["name"]."\" height=\"20px\">
												</td>
												<td class=\"bold\">
													<label><input type=\"checkbox\" name=\"reseticons\" id=\"reseticons\" onclick=\"iconAction(this.id);\" />".LBL_RESETICON."</label><br />
													<label><input type=\"checkbox\" name=\"uploadicons\" id=\"uploadicons\" onclick=\"iconAction(this.id);\" />".LBL_UPLOADNEWICON."</label>
												</td>
											</tr>
										</table>	
										<table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" style=\"display: none;\" id=\"newIconsTable\">
											<tr>
												<td class=\"bold\">&nbsp;</td>
											</tr>
											<tr>
												<td align=\"left\"><input type=\"file\" name=\"icon_on\" class=\"text2\" /></td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td valign=\"top\" class=\"bold\" colspan=\"2\">
										<br />".LBL_MACRODESCRIPTION."
									</td>
								</tr>
								<tr>
									<td class=\"bold\">
										<label for=\"scenario\">".LBL_SCENARIO.":</label>
									</td>
									<td>
										".generateSelector($macro["scenario"], "scenario", "text2", "id", "name", Scenarios::get(-1, true), -1, LBL_NOCHANGE)."
									</td>
								</tr>
								<tr>
									<td valign=\"top\" colspan=\"2\">
										<table cellspacing=\"0\" cellpadding=\"3\" class=\"tablelist\">
											<thead>
												<tr>
													<td class=\"bold\">".LBL_DEVICE."</td>
													<td class=\"bold\" align=\"center\">".LBL_TOGGLE."</td>
													<td class=\"bold\" align=\"center\">".LBL_STATUS."</td>
												</tr>
											</thead>
											<tbody>";
	$i = 0;
	
	foreach($devices as $device) {			
		$deviceChecked = "";
		$statusChecked = array("", "");
		$status = 0;
		
		if(is_array($macro["relations"])) {
			foreach($macro["relations"] as $relation)
			{
				if($relation["deviceid"] == $device["id"])
				{
					$deviceChecked = " checked=\"checked\"";
					$status = $relation["status"];
					
					break;
				}
			}
		}

		$list .= "		<tr>
							<td><label for=\"devices".$i."\">".$device["description"]."</label></td>
							<td align=\"center\">
								<label><input type=\"checkbox\" class=\"devices\" name=\"devices[".$i."]\" id=\"devices".$i."\" value=\"".$device["id"]."\"".$deviceChecked."></label>
							</td>
							<td align=\"center\" valign=\"middle\" width=\"130px\">
								<div id=\"status_selection".$device["id"]."\" style=\"display: none;\">";
							
							if($device["type"] == "absdimmer")
							{
								$list .= "<div class=\"status_slider2\" id=\"status_slider".$device["id"]."\"></div>
											<div id=\"status_slider_val".$device["id"]."\" style=\"float: right;\">".$status."</div>
											<input type=\"hidden\" value=\"".$status."\" id=\"status_val".$device["id"]."\" name=\"statuses[".$i."]\" />";
							}
							else
							{
								if($status == "0")
								{
									$statusChecked[0] = " checked=\"checked\"";
								}
								else
								{
									$statusChecked[1] = " checked=\"checked\"";
								}
								
								$list .= "<label><input type=\"radio\" value=\"1\" name=\"statuses[".$i."]\"".$statusChecked[1].">".LBL_ON."</label>
											<label><input type=\"radio\" value=\"0\" name=\"statuses[".$i."]\"".$statusChecked[0].">".LBL_OFF."</label>";									
							}
						
		$list .= "				</div>	
							</td>
						</tr>";

		$i++;
	}		
	
	$list .= "								</tbody>
										</table>
									</td>
								</tr>
								<tr>
									<td colspan=\"2\">
										&nbsp;
									</td>
								</tr>
								<tr>
									<td colspan=\"2\" align=\"right\">
										<input type=\"submit\" name=\"cancel\" value=\"".LBL_CANCEL."\" class=\"button setActionByButton\" /> <input type=\"submit\" id=\"saveBtn\" name=\"save\" value=\"".LBL_SAVE."\" class=\"button setActionByButton\" />
									</td>
								</tr>
							</table>
						</form>
					</td>
				</tr>
			</table>";
			
	$this->addBody($list);
}
else
{
	redirectTo(THISPAGE."?page=".$this->currentPage["name"]);
}

?>