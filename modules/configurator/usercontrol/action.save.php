<?php

// HomeAutomation
// Copyright (C) 2021 Tom Rosenback (tom.rosenback@gmail.com), Daniel Malmgren (daniel.malmgren@kolefors.se)
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
if(!defined("HomeAutomationIncluded")) { die("HomeAutomation: Direct access not premitted"); }

// disabling notice messages
error_reporting(E_ALL ^ E_NOTICE);

$id = getFormVariable("id", -1);

// user logged in with user level 3 (admin)
if($_SESSION[CFG_SESSION_KEY]["userlevel"] >= 3 && ($id == -1 || $id > 0)) {
	$id = getFormVariable("id", -1);
	$userEditable = getFormVariable("editable", 0);

	if($userEditable > 0) {
		$params = array(
			new dbParam(dbParamType::$STRING, "salt", substr(uniqid(CFG_SESSION_KEY, true), 0, 150)) // Limiting length of salt to 150 characters, DB limit atm too.
		);
		
		if($userEditable == 2) {
			$params[] = new dbParam(dbParamType::$STRING, "username", getFormVariable("username", ""));
			$params[] = new dbParam(dbParamType::$STRING, "firstname", getFormVariable("firstname", ""));
			$params[] = new dbParam(dbParamType::$STRING, "lastname", getFormVariable("lastname", ""));
			$params[] = new dbParam(dbParamType::$STRING, "email", getFormVariable("email", ""));

			if(getFormVariable("password", "") != "") {
				$params[] = new dbParam(dbParamType::$STRING, "password", md5(getFormVariable("password", "")));
			}
		}

		if($userEditable >= 1) {
			if($_POST["userlevel"] != -1) {
				$params[] = new dbParam(dbParamType::$STRING, "userlevel", $_POST["userlevel"]);
			} else {
				$params[] = new dbParam(dbParamType::$STRING, "userlevel", 1);
			}
		}

		$userExists = false;

		if($id == -1) {
			if(is_array(Users::getDetails(getFormVariable("username", "")))) {
				$userExists = true;
			}
		}

		if(!$userExists) {
			Users::save($params, $id);
		}
	}
}

redirectTo(THISPAGE."?page=".$this->currentPage["name"]);

?>
