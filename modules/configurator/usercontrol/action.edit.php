<?php

// HomeAutomation
// Copyright (C) 2021 Tom Rosenback (tom.rosenback@gmail.com), Daniel Malmgren (daniel.malmgren@kolefors.se)
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
if(!defined("HomeAutomationIncluded")) { die("HomeAutomation: Direct access not premitted"); }

// disabling notice messages
error_reporting(E_ALL ^ E_NOTICE);

$id = getFormVariable("id", -1);

// user logged in with user level 3 (admin)
if($_SESSION[CFG_SESSION_KEY]["userlevel"] >= 3 && ($id == -1 ||$id > 0)) {
	$currentUser = array();
	
	if($id != -1) {
		$users = Users::get($id);
		$currentUser = $users[$id];
		
		if($currentUser["editable"] == 2) {
			$currentUser["password"] = "********";
		}
		else if($currentUser["editable"] == 0) {
			$currentUser = array();
			$currentUser["id"] 			= -1;
			$currentUser["userlevel"] 	= -1;
			$currentUser["editable"] 	= 2;
		}
	}
	else
	{
		$currentUser["id"] 			= -1;
		$currentUser["userlevel"] 	= -1;
		$currentUser["editable"] 	= 2;
	}

	$list .= "<table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">
				<tr>
					<td>
						<form id=\"usermanager\" action=\"".THISPAGE."?page=".$this->currentPage["name"]."\" method=\"post\">
							<input type=\"hidden\" name=\"id\" id=\"id\" value=\"".$currentUser["id"]."\">
							<input type=\"hidden\" name=\"action\" id=\"action\" value=\"\">
							<input type=\"hidden\" name=\"editable\" id=\"editable\" value=\"".$currentUser["editable"]."\">

							<table>
								<tr>
									<td class=\"bold\">".LBL_USERNAME.":</td>
									<td><input type=\"text\" name=\"username\" class=\"text2\" value=\"".$currentUser["username"]."\" autocomplete=\"off\" /></td>
								</tr>";

	if($currentUser["editable"] == 2) {

		$list .= "				<tr>
									<td class=\"bold\">".LBL_PASSWORD.":</td>
									<td><input type=\"password\" name=\"password\" class=\"text2\" value=\"".$currentUser["password"]."\" autocomplete=\"off\" /></td>
								</tr>
								<tr>
									<td class=\"bold\">".LBL_FIRSTNAME.":</td>
									<td><input type=\"text\" name=\"firstname\" class=\"text2\" value=\"".$currentUser["firstname"]."\"></td>
								</tr>
								<tr>
									<td class=\"bold\">".LBL_LASTNAME.":</td>
									<td><input type=\"text\" name=\"lastname\" class=\"text2\" value=\"".$currentUser["lastname"]."\"></td>
								</tr>
								<tr>
									<td class=\"bold\">".LBL_EMAILADDRESS.":</td>
									<td><input type=\"text\" name=\"email\" class=\"text2\" value=\"".$currentUser["email"]."\"></td>
								</tr>";
	}

	$list .= "					<tr>
									<td class=\"bold\">".LBL_USERLEVEL.":</td>
									<td>".generateSelector($currentUser["userlevel"], "userlevel", "text2", "id", "description", Users::getUserLevels(), -1, LBL_SELECTUSERLEVEL)."</td>
								</tr>";

	$list .= "					<tr>
									<td colspan=\"2\">
										&nbsp;
									</td>
								</tr>
								<tr>
									<td colspan=\"2\" align=\"right\">
										<input type=\"submit\" value=\"".LBL_CANCEL."\" class=\"button setActionByButton\" name=\"cancel\">&nbsp;<input type=\"submit\" value=\"".LBL_SAVE."\" name=\"save\" class=\"button setActionByButton\">
									</td>
								</tr>
								<tr>
									<td colspan=\"2\">
										&nbsp;
									</td>
								</tr>
							</table>
						</form>
					</td>
				</tr>
			</table>";

	$this->addBody($list);
}
else
{
	redirectTo(THISPAGE."?page=".$this->currentPage["name"]);
}

?>
