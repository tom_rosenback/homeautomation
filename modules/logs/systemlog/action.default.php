<?php
// HomeAutomation
// Copyright (C) 2021 Tom Rosenback (tom.rosenback@gmail.com), Daniel Malmgren (daniel.malmgren@kolefors.se)
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
if(!defined("HomeAutomationIncluded")) { die("HomeAutomation: Direct access not premitted"); }

// disabling notice messages
error_reporting(E_ALL ^ E_NOTICE);

// user logged in with user level 1 or higher (demo)
if($_SESSION[CFG_SESSION_KEY]["userlevel"] >= 1)
{
	$id = getFormVariable("id", -1);
	$limit = getFormVariable("limit", 50);
	$sort = getFormVariable("sort", "ts");
	$sortDirection = getFormVariable("dir", "desc");

	$newSortDirection = array();
	$newSortDirection["dev"] = "asc";
	$newSortDirection["st"] = "asc";
	$newSortDirection["ts"] = "desc";
	$newSortDirection["usr"] = "asc";
	$newSortDirection["ip"] = "asc";
	$newSortDirection[$sort] = $sortDirection;

	$events = Events::get($id, $limit, $sort, $newSortDirection[$sort]);

	// echo "<pre>";
	// print_r($sort."\n");
	// print_r($sortDirection."\n");
	// print_r($newSortDirection);
	// echo "</pre>";

	$newSortDirection[$sort] = toggleSortDirection($sortDirection);

	$list = "<table width=\"100%\" cellspacing=\"0\">
				<tr>
					<td colspan=\"3\" class=\"bold\">
						<form id=\"logdevicechooser\" method=\"post\" action=\"".THISPAGE."?page=".$this->currentPage["name"]."\">
							".generateSelector($id, "id", "text2", "id", "description", Devices::get(), -1, LBL_ALLDEVICES, false, " onchange=\"document.getElementById('logdevicechooser').submit();\"")."&nbsp;&nbsp;&nbsp;
							".generateSelector($limit, "limit", "text", 0, 0, array(array(10), array(20), array(50), array(100), array(200), array(1000)), -1, LBL_SHOWALL, false, " onchange=\"document.getElementById('logdevicechooser').submit();\"")."&nbsp;&nbsp;&nbsp;
							<a href=\"".THISPAGE."?page=".$this->currentPage["name"]."\">".LBL_RESETFILTER."</a>&nbsp;&nbsp;&nbsp;
							<a href=\"".THISPAGE."?page=".$this->currentPage["name"]."&id=".$id."&sort=".$sort."&dir=".toggleSortDirection($newSortDirection[$sort])."&limit=".$limit."\">".LBL_UPDATE."</a>&nbsp;&nbsp;&nbsp;
						</form>
					</td>
					<td colspan=\"3\" class=\"bold\" align=\"right\">
						<a href=\"".THISPAGE."?page=".$this->currentPage["name"]."&action=clear\" onclick=\"return confirm('".LBL_CONFIRMCLEARLOG."');\">".LBL_CLEARLOG."</a>
					</td>
				</tr>
				<tr>
					<td colspan=\"6\" class=\"bold\">";

	if(count($events) >= $limit) {
		$list .= LBL_SHOWING." ".$limit." ".mb_convert_case(LBL_EVENTS, MB_CASE_LOWER, "UTF-8");
	}
	else
	{
		$list .= LBL_SHOWINGALLACTIVITIES;
	}

	$list .= "	</td>
			</tr>
			<tr>
				<td class=\"bold\"><a href=\"".THISPAGE."?page=".$this->currentPage["name"]."\">#</a></td>
				<td class=\"bold\"><a href=\"".THISPAGE."?page=".$this->currentPage["name"]."&id=".$id."&sort=dev&dir=".$newSortDirection["dev"]."&limit=".$limit."\">".LBL_DEVICE."</a></td>
				<td class=\"bold\"><a href=\"".THISPAGE."?page=".$this->currentPage["name"]."&id=".$id."&sort=st&dir=".$newSortDirection["st"]."&limit=".$limit."\">".LBL_STATUS."</td>
				<td class=\"bold\"><a href=\"".THISPAGE."?page=".$this->currentPage["name"]."&id=".$id."&sort=ts&dir=".$newSortDirection["ts"]."&limit=".$limit."\">".LBL_DATE."</td>
				<td class=\"bold\"><a href=\"".THISPAGE."?page=".$this->currentPage["name"]."&id=".$id."&sort=usr&dir=".$newSortDirection["usr"]."&limit=".$limit."\">".LBL_USER."</td>
				<td class=\"bold\"><a href=\"".THISPAGE."?page=".$this->currentPage["name"]."&id=".$id."&sort=ip&dir=".$newSortDirection["ip"]."&limit=".$limit."\">".LBL_IP."</td>
			</tr>";

	$i = 1;
	$trclass = "";

	$systemPlugins = SystemPlugins::load();

	foreach($events as $event) {
		if($trclass == "evenrow") {
			$trclass = "oddrow";
		}
		else
		{
			$trclass = "evenrow";
		}

		$statusText = getStatusText($event["status"]);

		if(is_object($systemPlugins[$event["systempluginname"]])) {
			$statusText = $systemPlugins[$event["systempluginname"]]->getStatusText($event["status"], $event["dimlevel"]);
		}

		$list .= "<tr class=\"".$trclass."\" onMouseOver=\"this.className='markedrow';\" onMouseOut=\"this.className='".$trclass."';\">
					<td>".$i++."</td>
					<td>".$event["description"]."</td>
					<td>
						".$statusText;

		if($event["device_type"] == "absdimmer" && $event["dimlevel"] > 0) {
			$list .= " ".$event["dimlevel"]."%";
		}

		$list .= "	</td>
					<td>".convertDateTimeToLocal($event["timestamp"], false, true)."</td>";

		if($_SESSION[CFG_SESSION_KEY]["userlevel"] > 1) {
			$list .= "<td>".$event["username"]."</td>
						<td>".$event["ipaddress"]."</td>";
		}
		else
		{
			$list .= "<td>".LBL_DEMOUSER."</td>
						<td>".LBL_DEMOUSERIP."</td>";
		}

		$list .= "</tr>";
	}

	$list .= "</table>";

	$this->addBody($list);
}

?>
