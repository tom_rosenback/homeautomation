<?php
// HomeAutomation
// Copyright (C) 2021 Tom Rosenback (tom.rosenback@gmail.com), Daniel Malmgren (daniel.malmgren@kolefors.se)
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
if(!defined("HomeAutomationIncluded")) { die("HomeAutomation: Direct access not premitted"); }

// disabling notice messages
error_reporting(E_ALL ^ E_NOTICE);

// user logged in with user level 1 or higher (demo)
if($_SESSION[CFG_SESSION_KEY]["userlevel"] >= 1)
{
	$list = "<div class=\"mobile-center\">";
	
	$groups = DeviceGroups::get();
	$i = 0;
	
	foreach($groups as $group) {
		$id = $group["id"];
		$desc = $group["name"];	
		
		$list .= "
				<fieldset class=\"ui-grid-a\">
					<div class=\"ui-block-a\"><button type=\"button\" data-theme=\"a\" class=\"group-toggle\" id=\"group-".$id."-0\" data-groupid=\"".$id."\" data-status=\"0\">".$desc."&nbsp;".LBL_OFF."</button></div>
					<div class=\"ui-block-b\"><button type=\"button\" data-theme=\"b\" class=\"group-toggle\" id=\"group-".$id."-1\" data-groupid=\"".$id."\" data-status=\"1\">".$desc."&nbsp;".LBL_ON."</button></div>	   
				</fieldset>";			
		$i++;
	}
	
	$list .= "</div>";
		
	$this->addBody($list);
}

?>