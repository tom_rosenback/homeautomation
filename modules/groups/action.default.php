<?php
// HomeAutomation
// Copyright (C) 2021 Tom Rosenback (tom.rosenback@gmail.com), Daniel Malmgren (daniel.malmgren@kolefors.se)
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
if(!defined("HomeAutomationIncluded")) { die("HomeAutomation: Direct access not premitted"); }

// disabling notice messages
error_reporting(E_ALL ^ E_NOTICE);

// user logged in with user level 1 or higher (demo)
if($_SESSION[CFG_SESSION_KEY]["userlevel"] >= 1)
{
	$list = "<table class=\"nowrap\">
				<tr>
					<td class=\"bold\">".LBL_DESCRIPTION."</td>
					<td align=\"center\" class=\"bold\">".LBL_TOGGLE."</td>
				</tr>";

	$groups = DeviceGroups::get();
	$i = 0;

	foreach($groups as $group) {
		$id = $group["id"];
		$desc = $group["name"];

		$list .= "<tr>
					<td>".$desc."</td>
					<td align=\"center\">
						<a href=\"api.php?do=groups/toggle&groupid=".$id."&status=0&redirect=".THISPAGE."?page=".$this->currentPage["name"]."\" class=\"group-toggle\" data-groupid=\"".$id."\" data-status=\"0\">
							<img src=\"".getStatusImage("group", false, $group["id"], "groups")."\" border=\"0\" alt=\"".$desc." ".getStatusText(false)."\" title=\"".$desc." ".getStatusText(false)."\" height=\"".$_SESSION[CFG_SESSION_KEY]["settings"]["houseplaniconheight"]."px\" />
						</a>
						<a href=\"api.php?do=groups/toggle&groupid=".$id."&status=1&redirect=".THISPAGE."?page=".$this->currentPage["name"]."\" class=\"group-toggle\" data-groupid=\"".$id."\" data-status=\"1\">
							<img src=\"".getStatusImage("group", true, $group["id"], "groups")."\" border=\"0\" alt=\"".$desc." ".getStatusText(true)."\" title=\"".$desc." ".getStatusText(true)."\" height=\"".$_SESSION[CFG_SESSION_KEY]["settings"]["houseplaniconheight"]."px\"/>
						</a>
					</td>
				</tr>";

		$i++;
	}

	$list .= "</table>";

	$this->addBody($list);
}

?>
