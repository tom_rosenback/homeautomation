<?php
// HomeAutomation
// Copyright (C) 2021 Tom Rosenback (tom.rosenback@gmail.com), Daniel Malmgren (daniel.malmgren@kolefors.se)
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
if(!defined("HomeAutomationIncluded")) { die("HomeAutomation: Direct access not premitted"); }

// disabling notice messages
error_reporting(E_ALL ^ E_NOTICE);

// user logged in with user level 1 or higher (demo)
if($_SESSION[CFG_SESSION_KEY]["userlevel"] >= 1) {
	$start = microtime(true);
	$this->addHeader("js-houseplan", "<script language=\"javascript\" type=\"text/javascript\" src=\"modules/houseplan/script.js\"></script>");
	
	$list = "";
	if(file_exists(HA_ROOT_PATH."/resources/houseplan.png")) {
		$pic_info = getimagesize(HA_ROOT_PATH."/resources/houseplan.png");
		$houseplanWidth = $pic_info[0];
		$houseplanHeight = $pic_info[1];
		$ratio = $houseplanWidth / $houseplanHeight;
		$houseplanSize = " width: 100%;";
		$divHeight = "";
		
		if($ratio < 1.5) {
			$houseplanSize = " height: 100%;";
			$divHeight = " height: 90%;";
		}
		
		$list = "<div id=\"houseplancontainer\" style=\"position: relative; width: 92%;".$divHeight."\">
					<img id=\"houseplanimage\" style=\"position: relative; top: 0px; left: 0px;".$houseplanSize."\" src=\"resources/houseplan.png?".filemtime("resources/houseplan.png")."\" />";

		$houseplan = Houseplan::get();
		
		$i = 1;

		foreach($houseplan as $hpDevice) {				
			// user logged in and not with user level 1 (demo)
			if($hpDevice["deviceid"] != -1 && $_SESSION[CFG_SESSION_KEY]["userlevel"] >= 1)
			{
				$device = Devices::get($hpDevice["deviceid"]);
				$device = $device[0];

				$event = $device["status"];

				$imagesize = getimagesize(getStatusImage($device["type"], $event->state, $device["id"]));
				$iconheight = $imagesize[1];

				if($device["type"] == "absdimmer") {
					$list .= "<div style=\"position: absolute; left: ".$hpDevice["xposition"]."%; top: ".$hpDevice["yposition"]."%; z-index: 10;\">
								<div class=\"device-img-box\">
									<a href=\"api.php?do=devices/toggle&deviceid=".$device["id"]."&status=-1&redirect=".THISPAGE."?page=".$this->currentPage["name"]."\" class=\"device-toggle\" data-deviceid=\"".$device["id"]."\" data-status=\"-1\"><img src=\"".getStatusImage($device["type"], $event->state, $device["id"])."\" border=\"0\" class=\"device-img-".$device["id"]."\" height=\"".$_SESSION[CFG_SESSION_KEY]["settings"]["houseplaniconheight"]."px\" alt=\"".$device["description"]."\" title=\"".$device["description"]."\" /></a>
								</div>
								<div class=\"device-slider-box device-slider-box-".$hpDevice["deviceid"]."\">
									<div class=\"device-slider-val device-slider-val-".$hpDevice["deviceid"]."\">".$event->dimlevel." %</div>
									<div class=\"device-slider device-slider-".$hpDevice["deviceid"]."\" data-deviceid=\"".$hpDevice["deviceid"]."\" data-status=\"".$event->dimlevel."\"></div>
								</div>
							</div>";
				} else {
					$list .= "<a href=\"api.php?do=devices/toggle&deviceid=".$device["id"]."&status=-1&redirect=".THISPAGE."?page=".$this->currentPage["name"]."\" class=\"device-toggle\" data-deviceid=\"".$device["id"]."\" data-status=\"-1\" style=\"position: absolute; left: ".$hpDevice["xposition"]."%; top: ".$hpDevice["yposition"]."%; z-index: 10;\">
								<img src=\"".getStatusImage($device["type"], $event->state, $device["id"])."\" border=\"0\" class=\"device-img-".$device["id"]."\" height=\"".$_SESSION[CFG_SESSION_KEY]["settings"]["houseplaniconheight"]."px\" alt=\"".$device["description"]."\" title=\"".$device["description"]."\" />
							</a>";
				}
			} else if($hpDevice["tempsensor"] != -1) {
				$sensorId = "";
				$image = "";

				if($hpDevice["tempsensor"] != "external") {
					$sensorData = Sensors::getCurrentReadings($hpDevice["tempsensor"]);

					$sensorId = $sensorData[0]["id"];
					$currentValue = $sensorData[0]["last_reading"];
					$currentUnit = $sensorData[0]["unit"];

					$sensorDisp = "<a href=\"#graph\" graph-type=\"".$sensorData[0]["graphtype"]."\" data-sensor=\"".$hpDevice["tempsensor"]."\" class=\"sensors sensor-value-".$sensorId."\" target=\"_blank\" id=\"sensor-".$hpDevice["id"]."-".$i."\" rel=\"graph_".$hpDevice["id"]."\">".$currentValue.$currentUnit."</a>";

					$sensorName = $sensorData[0]["name"];
					$image = $sensorData[0]["image"];
				} else if($_SESSION[CFG_SESSION_KEY]["settings"]["useexternaltemperature"] && $hpDevice["tempsensor"] == "external") {
					$sensorId = "external";
					// due to user agreements temperature needs to be a linked to host site
					preg_match('@^(?:http://)?([^/]+)@i', $_SESSION[CFG_SESSION_KEY]["settings"]["externaltempurl"], $host);

					$currentValue = trim(getTemperatureFromExternalUrl());
					$currentUnit = "&deg;C";

					$sensorDisp = "<a href=\"".$host[0]."\" target=\"_blank\" class=\"bigBold\" id=\"sensor-".$hpDevice["id"]."-".$i."\" class=\"sensor-value-".$hpDevice["tempsensor"]."\">".$currentValue.$currentUnit."</a>";
					$sensorName = $_SESSION[CFG_SESSION_KEY]["settings"]["externaltemplocation"];
					$image = getSensorImage(-1, $currentValue, "temp");
				}

				$list .= "<div style=\"position: absolute; left: ".$hpDevice["xposition"]."%; top: ".$hpDevice["yposition"]."%; z-index: 10; white-space: nowrap;\" id=\"sensor-".$hpDevice["id"]."-".$i."\">
						<label alt=\"".$sensorName."\" title=\"".$sensorName."\"><img src=\"".$image."\" border=\"0\" height=\"".$_SESSION[CFG_SESSION_KEY]["settings"]["houseplaniconheight"]."px\" style=\"vertical-align: middle;\" class=\"sensor-img sensor-img-".$sensorId."\" id=\"sensor-img-".$hpDevice["id"]."-".$i."\" />&nbsp;".$sensorDisp."</label>
					</div>";
			} else if($hpDevice["groupid"] != -1) {
				$tmp = explode("_", $hpDevice["groupid"]);
				$group = DeviceGroups::get($tmp[0]);
				$group = $group[0];

				$list .= "<a href=\"api.php?do=groups/toggle&groupid=".$tmp[0]."&status=".$tmp[1]."&redirect=".THISPAGE."?page=".$this->currentPage["name"]."\" class=\"group-toggle\" data-groupid=\"".$tmp[0]."\" data-status=\"".$tmp[1]."\" style=\"position: absolute; left: ".$hpDevice["xposition"]."%; top: ".$hpDevice["yposition"]."%; z-index: 10;\">
							<img src=\"".getStatusImage("group", $tmp[1], $tmp[0], "groups")."\" border=\"0\" height=\"".$_SESSION[CFG_SESSION_KEY]["settings"]["houseplaniconheight"]."px\" alt=\"".$group["name"]."\" title=\"".$group["name"]." ".getStatusText($tmp[1])."\" />
						</a>";
			} else if($hpDevice["macroid"] != -1) {
				$macro = Macros::get($hpDevice["macroid"]);
				$macro = $macro[0];

				$list .= "<a href=\"api.php?do=macros/run&macroid=".$macro["id"]."&redirect=".THISPAGE."?page=".$this->currentPage["name"]."\" class=\"macro-run\" data-macroid=\"".$macro["id"]."\" style=\"position: absolute; left: ".$hpDevice["xposition"]."%; top: ".$hpDevice["yposition"]."%; z-index: 10;\">
							<img src=\"".getStatusImage("macro", true, $macro["id"], "macros")."\" border=\"0\" height=\"".$_SESSION[CFG_SESSION_KEY]["settings"]["houseplaniconheight"]."px\" alt=\"".LBL_MACRO.": ".$macro["name"]."\" title=\"".LBL_MACRO.": ".$macro["name"]."\" />
						</a>";
			}

			$i++;
		}

		$list .= "		</div>
						<div style=\"position: relative; top: 2px;\">
							<a href=\"api.php?do=devices/toggle&deviceid=-1&status=0&redirect=".THISPAGE."?page=".$this->currentPage["name"]."\" class=\"device-toggle\" data-deviceid=\"-1\" data-status=\"0\">
								<img src=\"".getStatusImage("all", false)."\" border=\"0\" class=\"device-status-all-0\" alt=\"".LBL_TURNOFFALL."\" title=\"".LBL_TURNOFFALL."\" height=\"".$_SESSION[CFG_SESSION_KEY]["settings"]["houseplaniconheight"]."px\" />
							</a>

							<a href=\"api.php?do=devices/toggle&deviceid=-1&status=1&redirect=".THISPAGE."?page=".$this->currentPage["name"]."\" class=\"device-toggle\" data-deviceid=\"-1\" data-status=\"1\">
								<img src=\"".getStatusImage("all", true)."\" border=\"0\" class=\"device-status-all-1\" alt=\"".LBL_TURNONALL."\" title=\"".LBL_TURNONALL."\" height=\"".$_SESSION[CFG_SESSION_KEY]["settings"]["houseplaniconheight"]."px\" />
							</a>
						</div>

						<div style=\"display: none;\">
							<div id=\"graph\"></div>
						</div>";
	} else {
		$list = LBL_MISSINGHOUSEPLANIMAGE;
	}
	
	$message = new SystemMessage();
	$message->setText(str_replace(array("{module}", "{time}"), array("houseplan.action.default", number_format((microtime(true) - $start), 4)), LBL_MODULELOADEDIN));
	$message->setAutoClose(true);
	$message->setShowOnlyInDebug(true);
	
	$this->addMessage($message);
	$this->addBody($list);
}

?>
