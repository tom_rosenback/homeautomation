$(window).load(function()
{	
	// fixes issues with div container around houseplan image not being correct size
	$("#houseplanimage").ready(function()
	{
		var varWidth = $("#houseplanimage").width();
		var varHeight = $("#houseplanimage").height();
		var varRatio = varWidth / varHeight;
		
		if(varRatio < 1.5)
		{
			$("#houseplancontainer").width(varWidth);
		}
	});
});