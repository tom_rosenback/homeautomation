<?php
// HomeAutomation
// Copyright (C) 2021 Tom Rosenback (tom.rosenback@gmail.com), Daniel Malmgren (daniel.malmgren@kolefors.se)
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
if(!defined("HomeAutomationIncluded")) { die("HomeAutomation: Direct access not premitted"); }

// disabling notice messages
error_reporting(E_ALL ^ E_NOTICE);

// user logged in with user level 1 or higher (demo)
if($_SESSION[CFG_SESSION_KEY]["userlevel"] >= 1)
{
	// $this->addBody("<div id=\"jsdebug\">Javascript problems</div>");
	
	$list = "<div class=\"mobile-center\">";

	$devices = Devices::get();
	$i = 0;
	
	foreach($devices as $device) {
		$id = $device["id"];
		$desc = $device["description"];	
		$lastEvent = $device["status"];
		
		if($device["type"] == "absdimmer") {
			$list .= "<div data-role=\"fieldcontain\" class=\"no-field-separator slider-box-".$device["id"]." device-slider\">
							<label for=\"device-".$id."-1\">".$desc."</label>
							<input type=\"range\" name=\"device-".$device["id"]."-1\" id=\"device-".$device["id"]."-1\" class=\"device-slider device-slider-".$device["id"]." addunit\" data-deviceid=\"".$device["id"]."\" data-status=\"".$lastEvent->dimlevel."\" value=\"".$lastEvent->dimlevel."\" min=\"0\" max=\"100\" step=\"".$_SESSION[CFG_SESSION_KEY]["settings"]["dimlevelstep"]."\" data-theme=\"a\" data-highlight=\"true\" data-track-theme=\"c\" />
						</div>";						
		}
		else
		{
			$list .= "<div data-role=\"fieldcontain\" class=\"no-field-separator\">
						<label for=\"device-".$id."-1\" class=\"ui-hidden-accessible\">".$desc."</label>
						".generateSelector(convertToNumeric($lastEvent->state), "device-".$id."-1", "device-toggle device-toggler-".$device["id"]."", 0, 1, array(array(0, $desc." ".getStatusText(false)), array(1, $desc." ".getStatusText(true))), 0, "", false, "", "", " data-status=\"".convertToNumeric($lastEvent->state)."\" data-role=\"slider\" data-track-theme=\"c\" data-deviceid=\"".$id."\"")."
					</div>";
		}
				
		$i++;
	}

	$list .= "	<fieldset class=\"ui-grid-a\">
					<div class=\"ui-block-a\">
						<a href=\"api.php?do=devices/toggle&deviceid=-1&status=0&redirect=".THISPAGE."?page=".$this->currentPage["name"]."\" class=\"device-toggle\" data-deviceid=\"-1\" data-status=\"0\" data-role=\"button\" data-theme=\"a\">
							".LBL_TURNOFFALL."
						</a>
					</div>
					<div class=\"ui-block-b\">
						<a href=\"api.php?do=devices/toggle&deviceid=-1&status=1&redirect=".THISPAGE."?page=".$this->currentPage["name"]."\" class=\"device-toggle\" data-deviceid=\"-1\" data-status=\"1\" data-role=\"button\" data-theme=\"b\">
							".LBL_TURNONALL."
						</a>
					</div>	   
				</fieldset>";
	
	$list .= "</div>";

	$this->addBody($list);
}

?>