<?php
// HomeAutomation
// Copyright (C) 2021 Tom Rosenback (tom.rosenback@gmail.com), Daniel Malmgren (daniel.malmgren@kolefors.se)
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
if(!defined("HomeAutomationIncluded")) { die("HomeAutomation: Direct access not premitted"); }

// disabling notice messages
error_reporting(E_ALL ^ E_NOTICE);
	
// user logged in with user level 1 or higher (demo)
if($_SESSION[CFG_SESSION_KEY]["userlevel"] >= 1)
{
	$list = "<table class=\"nowrap\">
				<tr>
					<td class=\"bold\">".LBL_DESCRIPTION."</td>
					<td class=\"bold\">".LBL_LASTKNOWNSTATUS."</td>
					<td align=\"center\" class=\"bold\">".LBL_TOGGLE."</td>
				</tr>";
	$devices = Devices::get();
	$i = 0;
	
	foreach($devices as $device) {
		$id = $device["id"];
		$desc = $device["description"];	
		$lastEvent = $device["status"];
	
		$lastEventText = "senast ".getStatusText($lastEvent->state). " ".mb_convert_case(convertDateTimeToLocal($lastEvent->timestamp, false, true), MB_CASE_LOWER, "UTF-8");
		
		$image = getStatusImage($device["type"], $lastEvent->state, $device["id"]);

		$list .= "<tr>
					<td>".$desc."</td>
					<td align=\"center\">
						<a href=\"api.php?do=devices/toggle&deviceid=".$device["id"]."&status=-1&redirect=".THISPAGE."?page=".$this->currentPage["name"]."\" class=\"device-toggle\" data-deviceid=\"".$device["id"]."\" data-status=\"-1\"><img src=\"".$image."\" border=\"0\" class=\"device-img-".$device["id"]."\" alt=\"".$device["description"]."\" title=\"".$device["description"]."\" height=\"".$_SESSION[CFG_SESSION_KEY]["settings"]["houseplaniconheight"]."px\" /></a>
					</td>
					<td align=\"center\">";
		
		if($device["type"] == "absdimmer") {
			$list .= "<div class=\"device-slider-val device-slider-val-".$device["id"]."\">".$lastEvent->dimlevel." %</div>  
						<div class=\"device-slider device-slider-".$device["id"]."\" data-deviceid=\"".$device["id"]."\" data-status=\"".$lastEvent->dimlevel."\"></div>";
		}
		else
		{
			$list .= "	<a href=\"api.php?do=devices/toggle&deviceid=".$device["id"]."&status=0&redirect=".THISPAGE."?page=".$this->currentPage["name"]."\" class=\"device-toggle\" data-deviceid=\"".$device["id"]."\" data-status=\"0\"><img src=\"".getStatusImage($device["type"], false, $device["id"])."\" border=\"0\" alt=\"".$device["typetext"]." ".getStatusText(false)."\" title=\"".$device["typetext"]." ".getStatusText(false)."\" height=\"".$_SESSION[CFG_SESSION_KEY]["settings"]["houseplaniconheight"]."px\" /></a>
						<a href=\"api.php?do=devices/toggle&deviceid=".$device["id"]."&status=1&redirect=".THISPAGE."?page=".$this->currentPage["name"]."\" class=\"device-toggle\" data-deviceid=\"".$device["id"]."\" data-status=\"1\"><img src=\"".getStatusImage($device["type"], true, $device["id"])."\" border=\"0\" alt=\"".$device["typetext"]." ".getStatusText(true)."\" title=\"".$device["typetext"]." ".getStatusText(true)."\" height=\"".$_SESSION[CFG_SESSION_KEY]["settings"]["houseplaniconheight"]."px\" /></a>";
						
		}
		
		$list .= "</td>
				</tr>";		
				
		$i++;
	}

	$list .= "<tr>
				<td colspan=\"3\" align=\"center\">
					<a href=\"api.php?do=devices/toggle&deviceid=-1&status=0&redirect=".THISPAGE."?page=".$this->currentPage["name"]."\" class=\"device-toggle\" data-deviceid=\"-1\" data-status=\"0\">
						<img src=\"".getStatusImage("all", false)."\" border=\"0\" class=\"device-status-all-0\" alt=\"".LBL_TURNOFFALL."\" title=\"".LBL_TURNOFFALL."\" height=\"".$_SESSION[CFG_SESSION_KEY]["settings"]["houseplaniconheight"]."px\" />
					</a>
					
					<a href=\"api.php?do=devices/toggle&deviceid=-1&status=1&redirect=".THISPAGE."?page=".$this->currentPage["name"]."\" class=\"device-toggle\" data-deviceid=\"-1\" data-status=\"1\">
						<img src=\"".getStatusImage("all", true)."\" border=\"0\" class=\"device-status-all-1\" alt=\"".LBL_TURNONALL."\" title=\"".LBL_TURNONALL."\" height=\"".$_SESSION[CFG_SESSION_KEY]["settings"]["houseplaniconheight"]."px\" />
					</a>
				</td>
			</tr>
		</table>";
		
	$this->addBody($list);
}

?>