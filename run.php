<?php
// HomeAutomation
// Copyright (C) 2021 Tom Rosenback (tom.rosenback@gmail.com), Daniel Malmgren (daniel.malmgren@kolefors.se)
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

session_start();

$executionStart = microtime(true);
$forceLog = false;

if(php_sapi_name() == "cli" && empty($_SERVER["REMOTE_ADDR"])) {
	chdir(dirname(__FILE__));

	include("includes.php");

	if($_SESSION[CFG_SESSION_KEY]["settings"]["debug"]) {
		echo "\n\n<pre>";
		print_r($_POST);
		print_r($_GET);
		print_r($_SESSION[CFG_SESSION_KEY]);
		print_r($_SERVER);
		echo "</pre>";
	}

	$usage = "\nIncorrect arguments, correct usage is: 'php run.php arg1=x arg2=y arg3=z'
	Accepted arguments:
	command\t= activation | device | group | macro | updateschedules
	id\t= id of the command, can be several ids separated by a ';', activation command only accepts one id, updateschedules takes no id argument, activation command also supports 'dynamic' as id, this will execute the dynamic activations
	status\t= 0 | 1 | off | on | dimlevel, can be several ids separated by a ';' (only valid for device and group commands)\n
	If several statuses are given they must match the amount of id´s
	Each argument is only allowed once per execution\n";

	$args = getCommandLineArguments($_SERVER["argv"]);

	if($args["failure"] == true) {
		// user either entered wrong arguments or something else failed
		echo $usage;
	} else {
		// doing the action here
		if(registerSession($args["user"])) {
			switch($args["command"]) {
				case "activation": {
					$selectedScenario = Settings::get("selectedscenario");
					$activations = array();

					if($args["id"][0] == "dynamic") {
						$activations = Schedules::getActivations(-1, -1, "dynamic");
					} else {
						$activations = Schedules::getActivations(-1, $args["id"][0]);
					}

					if(count($activations) == 0) {
						echo "Nothing to do, quit.\n";
					} else {
						$schedule = -1;

						foreach($activations as $activation) {
							$schedule = Schedules::get($activation["scheduleid"]);
							$schedule = $schedule[0];

							// schedule is enabled and scenario ALL or selected scenario is matching configured scenario in schedule
							if(convertToBoolean($schedule["enabled"]) && ($schedule["scenario"] == -100 || ($selectedScenario !== false && $schedule["scenario"] == $selectedScenario))) {
								// checking activation (if dynamic should run, if enginheater should be on...)
								$result = validateActivationForExecution($activation);

								$msg = "\nRunning schedule: ".$schedule["name"]."\n";
								echo $msg;
								SaveLog($msg, $forceLog);

								if($result["msg"] != "") {
									echo $result["msg"];
									SaveLog($result["msg"], $forceLog);
								}

								// validation result
								if($result["execute"]) {
									if(!Schedules::isActivationTriggered($activation["id"])) {
										$msg = $numDevicesToggled." devices toggled\n";
										echo $msg;

										SaveLog($msg, $forceLog);
										$params = array(
											new dbParam(dbParamType::$INTEGER, "linuxatid", null)
										);

										if($activation["type"] == "dynamic") {
											$params[] = new dbParam(dbParamType::$STRING, "upcomingruntime", dbParam::now());
											$params[] = new dbParam(dbParamType::$INTEGER, "triggered", 1);
										}

										Schedules::saveActivation($params, $activation["id"]);

										$devicesAndStatuses = getActivationDevicesAndStatuses($schedule, $activation);
										$numDevicesToggled = count(executeToggleAction($devicesAndStatuses["status"], $devicesAndStatuses["deviceids"], $activation["type"]));
										runScheduleMacros($schedule["macros"]);

										Schedules::updateActivationsUpcomingRuntime($activation["id"]);
									} else {
										echo "Activation is still in triggered mode, nothing to do.\n";
									}
								} else {
									$params = array(
										new dbParam(dbParamType::$INTEGER, "triggered", 0)
									);

									Schedules::saveActivation($params, $activation["id"]);
								}
							}
						}
					}
					break;
				}
				case "group": {
					$i = 0;

					foreach($args["id"] as $groupId) {
						$group = DeviceGroups::get($groupId);
						$group = $group[0];

						$msg = "\nRunning group: ".$group["name"]."\n";
						echo $msg;
						SaveLog($msg, $forceLog);

						$status = $args["status"];

						if(is_array($args["status"])) {
							$status = $args["status"][$i];
						}

						$deviceIds = array();

						foreach($group["relations"] as $rel) {
							$deviceIds[] = $rel["deviceid"];
						}

						$numDevicesToggled = count(executeToggleAction($status, $deviceIds));

						$msg = $numDevicesToggled." devices toggled\n";
						echo $msg;
						SaveLog($msg, $forceLog);
						$i++;
					}

					break;
				}
				case "device": {
					$msg = "\nToggling devices\n";
					echo $msg;
					SaveLog($msg, $forceLog);

					$numDevicesToggled = count(executeToggleAction($args["status"], $args["id"]));
					$msg = $numDevicesToggled." devices toggled\n";
					echo $msg;
					SaveLog($msg, $forceLog);

					break;
				}
				case "macro": {
					foreach($args["id"] as $id) {
						$macro = Macros::get($id);
						$macro = $macro[0];

						if(is_array($macro)) {
							$msg = "\nRunning macro: ".$macro["name"]."\n";
							echo $msg;
							SaveLog($msg);

							if($macro["scenario"] != "-1") {
								$scenario = Scenarios::get($macro["scenario"]);
								$scenario = $scenario[0];

								Settings::update("selectedscenario", $macro["scenario"]);

								$selectedScenario = "Selected scenario: ".$scenario["name"]."\n";
								echo $selectedScenario;
								SaveLog($selectedScenario, $forceLog);
							}

							$deviceIds = array();
							$status = array();

							foreach($macro["relations"] as $rel) {
								$deviceIds[] = $rel["deviceid"];
								$status[$rel["deviceid"]] = $rel["status"];
							}

							$numDevicesToggled = count(executeToggleAction($status, $deviceIds));
							$msg = $numDevicesToggled." devices toggled\n";
							echo $msg;
							SaveLog($msg, $forceLog);
						}
					}
					break;
				}
				case "updateschedules": {
					$msg = "\nUpdating schedules\n";
					echo $msg;
					SaveLog($msg, $forceLog);

					Schedules::updateRealActivationTimes();
					updateAtForSchedule();

					break;
				}
				default: {
					// should not be possible to be here due to previous failure check
					break;
				}
			}

			$executionTime = microtime(true) - $executionStart;
			$msg = "\nCompleted in ".number_format($executionTime, 4)." seconds\n";
			echo $msg;
			SaveLog($msg, $forceLog);
		}
		else {
			$msg = "run.php: Not a valid user\n";
			echo $msg;
			SaveLog($msg, $forceLog);
		}
	}
} else {
	echo "<b>You are not allowed here</b>";
}

?>
