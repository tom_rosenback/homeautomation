<?php
global $ha;

// META

$ha->addHeader("meta-author", "<meta name=\"author\" content=\"Tom Rosenback - Daniel Malmgren\" />");
$ha->addHeader("meta-description", "<meta name=\"description\" content=\"HomeAutomation\" />");
$ha->addHeader("meta-charset", "<meta http-equiv=\"Content-type\" content=\"text/html; charset=UTF-8\" />");

// SCRIPTS

$ha->addHeader("js-jquery", "<script type=\"text/javascript\" src=\"plugins/jquery/jquery.min.js\"></script>");
$ha->addHeader("js-jquery-ui", "<script type=\"text/javascript\" src=\"plugins/jquery/jquery-ui/jquery-ui.min.js\"></script>");
$ha->addHeader("js-jquery-datatables", "<script type=\"text/javascript\" src=\"plugins/jquery/jquery.dataTables.min.js\"></script>");

$ha->addHeader("js-highcharts", "<script type=\"text/javascript\" src=\"plugins/highcharts/highstock.js\"></script>");

// HA JS
if($_SESSION[CFG_SESSION_KEY]["userlevel"] == "" || $_SESSION[CFG_SESSION_KEY]["userlevel"] == "1" || $_SESSION[CFG_SESSION_KEY]["settings"]["debug"]) {
	$ha->addHeader("js-autoupdates", "<script type=\"text/javascript\">var disableAutoUpdates=true;</script>");
}

include(HA_ROOT_PATH."/scripts/localization.js.php");
$ha->addHeader("js-localization", $HA_Localization);

$ha->addHeader("js-network", "<script type=\"text/javascript\" src=\"scripts/network.js\"></script>");
$ha->addHeader("js-ha", "<script type=\"text/javascript\" src=\"scripts/ha.js\"></script>");
$ha->addHeader("js-common", "<script type=\"text/javascript\" src=\"scripts/common.js\"></script>");
$ha->addHeader("js-script", "<script type=\"text/javascript\" src=\"scripts/script.js\"></script>");

$ha->addHeader("js-fancybox", "<script type=\"text/javascript\" src=\"plugins/fancybox2/jquery.fancybox.js\"></script>");
$ha->addHeader("js-fancybox-pack", "<script type=\"text/javascript\" src=\"plugins/fancybox2/jquery.fancybox.pack.js\"></script>");

include(HA_ROOT_PATH."/scripts/sliders.js.php");
$ha->addHeader("js-sliders", $sliders);

// CSS

$ha->addHeader("css-fancybox", "<link rel=\"stylesheet\" href=\"plugins/fancybox2/jquery.fancybox.css\" type=\"text/css\" media=\"screen\" />"); // 1.3.4
$ha->addHeader("css-jquery-ui", "<link href=\"plugins/jquery/jquery-ui/jquery-ui.min.css\" rel=\"stylesheet\" type=\"text/css\" />");
$ha->addHeader("css-common", "<link rel=\"stylesheet\" href=\"resources/common.css\" type=\"text/css\" media=\"screen\" />");
$ha->addHeader("css-theme", "<link rel=\"stylesheet\" href=\"resources/themes/".$_SESSION[CFG_SESSION_KEY]["settings"]["theme"].".css\" type=\"text/css\" media=\"screen\" />");
$ha->addHeader("css-sensors", "<link href=\"sensors/styles.css\" rel=\"stylesheet\">");
$ha->addHeader("webkit-text-size-adjust", "<style>html { -webkit-text-size-adjust: 100%; }</style>");

// META

$ha->addHeader("title", "<title>".$_SESSION[CFG_SESSION_KEY]["settings"]["title"]."</title>");
$ha->addHeader("icon", "<link rel=\"icon\" href=\"resources/homeautomation.ico\" />");

$ha->addHeader("format-detection", "<meta name=\"format-detection\" content=\"telephone=no\">");
$ha->addHeader("apple-mobile-web-app-capable", "<meta name=\"apple-mobile-web-app-capable\" content=\"yes\" />");
$ha->addHeader("apple-mobile-web-app-status-bar-style", "<meta name=\"apple-mobile-web-app-status-bar-style\" content=\"black\" />");
$ha->addHeader("apple-mobile-web-app-title", "<meta name=\"apple-mobile-web-app-title\" content=\"".$_SESSION[CFG_SESSION_KEY]["settings"]["title"]."\" />");

// Launchers
$ha->addHeader("launch-icon-precomposed", "<link href=\"resources/webapp/default-icon-144.png\" rel=\"apple-touch-icon-precomposed\"/>");
$ha->addHeader("launch-icon-ios7-ipad-retina", "<link href=\"resources/webapp/default-icon-152.png\" sizes=\"152x152\" rel=\"apple-touch-icon\" />");
$ha->addHeader("launch-icon-ios6-ipad-retina", "<link href=\"resources/webapp/default-icon-144.png\" sizes=\"144x144\" rel=\"apple-touch-icon\" />");
$ha->addHeader("launch-icon-ios7-iphone-retina", "<link href=\"resources/webapp/default-icon-120.png\" sizes=\"120x120\" rel=\"apple-touch-icon\" />");
$ha->addHeader("launch-icon-ios6-iphone-retina", "<link href=\"resources/webapp/default-icon-114.png\" sizes=\"114x114\" rel=\"apple-touch-icon\" />");
$ha->addHeader("launch-icon-ios7-ipad", "<link href=\"resources/webapp/default-icon-76.png\" sizes=\"76x76\" rel=\"apple-touch-icon\" />");
$ha->addHeader("launch-icon-ios6-ipad", "<link href=\"resources/webapp/default-icon-72.png\" sizes=\"72x72\" rel=\"apple-touch-icon\" />");
$ha->addHeader("launch-icon-ios6-iphone", "<link href=\"resources/webapp/default-icon-57.png\" sizes=\"57x57\" rel=\"apple-touch-icon\" />");

// Splashscreens
$ha->addHeader("splashscreen-ios6-7-ipad-retina-portrait", "<link href=\"resources/webapp/splashscreen-1536x2008.png\" media=\"(device-width: 768px) and (device-height: 1024px) and (orientation: portrait) and (-webkit-device-pixel-ratio: 2)\" rel=\"apple-touch-startup-image\" />");
$ha->addHeader("splashscreen-ios6-7-ipad-retina-landscape", "<link href=\"resources/webapp/splashscreen-2048x1496.png\" media=\"(device-width: 768px) and (device-height: 1024px) and (orientation: landscape) and (-webkit-device-pixel-ratio: 2)\" rel=\"apple-touch-startup-image\" />");
$ha->addHeader("splashscreen-ios6-ipad-portrait", "<link href=\"resources/webapp/splashscreen-768x1004.png\" media=\"(device-width: 768px) and (device-height: 1024px) and (orientation: portrait) and (-webkit-device-pixel-ratio: 1)\" rel=\"apple-touch-startup-image\" />");
$ha->addHeader("splashscreen-ios6-ipad-landscape", "<link href=\"resources/webapp/splashscreen-1024x748.png\" media=\"(device-width: 768px) and (device-height: 1024px) and (orientation: landscape) and (-webkit-device-pixel-ratio: 1)\" rel=\"apple-touch-startup-image\" />");
$ha->addHeader("splashscreen-ios6-7-iphone5", "<link href=\"resources/webapp/splashscreen-640x1096.png\" media=\"(device-width: 320px) and (device-height: 568px) and (-webkit-device-pixel-ratio: 2)\" rel=\"apple-touch-startup-image\" />");
$ha->addHeader("splashscreen-ios6-7-iphone-retina", "<link href=\"resources/webapp/splashscreen-640x920.png\" media=\"(device-width: 320px) and (device-height: 480px) and (-webkit-device-pixel-ratio: 2)\" rel=\"apple-touch-startup-image\" />");
$ha->addHeader("splashscreen-ios6-iphone", "<link href=\"resources/webapp/splashscreen-320x460.png\" media=\"(device-width: 320px) and (device-height: 480px) and (-webkit-device-pixel-ratio: 1)\" rel=\"apple-touch-startup-image\" />");

?>
