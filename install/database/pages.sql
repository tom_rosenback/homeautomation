/*
SQLyog Enterprise - MySQL GUI v6.07
Host - 5.5.16 : Database - homeautomation
*********************************************************************
Server version : 5.5.16
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

/*Table structure for table `pages` */

CREATE TABLE `pages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parentid` int(10) DEFAULT '-1',
  `name` varchar(30) NOT NULL,
  `translation` varchar(20) NOT NULL,
  `modulegroup` varchar(30) DEFAULT NULL,
  `module` varchar(30) NOT NULL,
  `menuaction` varchar(20) DEFAULT NULL,
  `sort` int(11) DEFAULT '0',
  `userlevel` tinyint(4) DEFAULT '3',
  `isavailableformobile` tinyint(1) DEFAULT '0',
  `dateadded` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM AUTO_INCREMENT=28 DEFAULT CHARSET=utf8;

/*Data for the table `pages` */

insert  into `pages`(`id`,`parentid`,`name`,`translation`,`modulegroup`,`module`,`menuaction`,`sort`,`userlevel`,`isavailableformobile`,`dateadded`) values (1,-1,'houseplan','LBL_HOUSEPLAN',NULL,'houseplan',NULL,1,1,0,'2012-02-24 21:36:44');
insert  into `pages`(`id`,`parentid`,`name`,`translation`,`modulegroup`,`module`,`menuaction`,`sort`,`userlevel`,`isavailableformobile`,`dateadded`) values (2,-1,'devices','LBL_DEVICES',NULL,'devices',NULL,2,1,0,'2012-02-24 21:36:44');
insert  into `pages`(`id`,`parentid`,`name`,`translation`,`modulegroup`,`module`,`menuaction`,`sort`,`userlevel`,`isavailableformobile`,`dateadded`) values (3,-1,'groups','LBL_GROUPS',NULL,'groups',NULL,3,1,0,'2011-11-12 14:02:44');
insert  into `pages`(`id`,`parentid`,`name`,`translation`,`modulegroup`,`module`,`menuaction`,`sort`,`userlevel`,`isavailableformobile`,`dateadded`) values (4,-1,'macros','LBL_MACROS',NULL,'macros',NULL,4,1,0,'2011-11-12 14:02:44');
insert  into `pages`(`id`,`parentid`,`name`,`translation`,`modulegroup`,`module`,`menuaction`,`sort`,`userlevel`,`isavailableformobile`,`dateadded`) values (5,-1,'scheduler','LBL_SCHEDULER',NULL,'scheduler',NULL,5,1,0,'2011-11-12 14:02:44');
insert  into `pages`(`id`,`parentid`,`name`,`translation`,`modulegroup`,`module`,`menuaction`,`sort`,`userlevel`,`isavailableformobile`,`dateadded`) values (6,-1,'logs','LBL_LOGS','logs','systemlogs',NULL,6,1,0,'2011-11-12 14:02:44');
insert  into `pages`(`id`,`parentid`,`name`,`translation`,`modulegroup`,`module`,`menuaction`,`sort`,`userlevel`,`isavailableformobile`,`dateadded`) values (7,6,'logs-measurements','LBL_MEASUREMENTS','logs','measurements',NULL,7,1,0,'2012-06-18 22:24:21');
insert  into `pages`(`id`,`parentid`,`name`,`translation`,`modulegroup`,`module`,`menuaction`,`sort`,`userlevel`,`isavailableformobile`,`dateadded`) values (8,6,'logs-systemlog','LBL_LOGS','logs','systemlog',NULL,8,1,0,'2012-06-18 22:24:21');
insert  into `pages`(`id`,`parentid`,`name`,`translation`,`modulegroup`,`module`,`menuaction`,`sort`,`userlevel`,`isavailableformobile`,`dateadded`) values (9,6,'logs-consumption','LBL_POWERCONSUMPTION','logs','consumption',NULL,9,4,0,'2012-06-18 22:24:21');
insert  into `pages`(`id`,`parentid`,`name`,`translation`,`modulegroup`,`module`,`menuaction`,`sort`,`userlevel`,`isavailableformobile`,`dateadded`) values (10,-1,'configurator','LBL_CONFIGURATION','configurator','devices',NULL,10,3,0,'2012-02-24 20:30:47');
insert  into `pages`(`id`,`parentid`,`name`,`translation`,`modulegroup`,`module`,`menuaction`,`sort`,`userlevel`,`isavailableformobile`,`dateadded`) values (11,10,'conf-devices','LBL_DEVICES','configurator','devices',NULL,11,3,0,'2012-06-18 22:24:21');
insert  into `pages`(`id`,`parentid`,`name`,`translation`,`modulegroup`,`module`,`menuaction`,`sort`,`userlevel`,`isavailableformobile`,`dateadded`) values (12,10,'conf-devicegroups','LBL_DEVICEGROUPS','configurator','devicegroups',NULL,12,3,0,'2012-06-18 22:24:21');
insert  into `pages`(`id`,`parentid`,`name`,`translation`,`modulegroup`,`module`,`menuaction`,`sort`,`userlevel`,`isavailableformobile`,`dateadded`) values (13,10,'conf-houseplan','LBL_HOUSEPLAN','configurator','houseplan',NULL,13,3,0,'2012-06-18 22:24:21');
insert  into `pages`(`id`,`parentid`,`name`,`translation`,`modulegroup`,`module`,`menuaction`,`sort`,`userlevel`,`isavailableformobile`,`dateadded`) values (14,10,'conf-scenarios','LBL_SCENARIOS','configurator','scenarios',NULL,14,3,0,'2012-06-18 22:24:21');
insert  into `pages`(`id`,`parentid`,`name`,`translation`,`modulegroup`,`module`,`menuaction`,`sort`,`userlevel`,`isavailableformobile`,`dateadded`) values (15,10,'conf-gcal','LBL_GCAL','configurator','gcal',NULL,15,3,0,'2012-06-18 22:24:21');
insert  into `pages`(`id`,`parentid`,`name`,`translation`,`modulegroup`,`module`,`menuaction`,`sort`,`userlevel`,`isavailableformobile`,`dateadded`) values (16,10,'conf-usercontrol','LBL_USERMANAGEMENT','configurator','usercontrol',NULL,16,3,0,'2012-06-18 22:24:21');
insert  into `pages`(`id`,`parentid`,`name`,`translation`,`modulegroup`,`module`,`menuaction`,`sort`,`userlevel`,`isavailableformobile`,`dateadded`) values (17,10,'conf-sensors','LBL_SENSORS','configurator','sensors',NULL,17,3,0,'2012-06-18 22:24:21');
insert  into `pages`(`id`,`parentid`,`name`,`translation`,`modulegroup`,`module`,`menuaction`,`sort`,`userlevel`,`isavailableformobile`,`dateadded`) values (18,10,'conf-sensortypes','LBL_SENSORTYPES','configurator','sensortypes',NULL,18,3,0,'2012-06-18 22:24:21');
insert  into `pages`(`id`,`parentid`,`name`,`translation`,`modulegroup`,`module`,`menuaction`,`sort`,`userlevel`,`isavailableformobile`,`dateadded`) values (19,10,'conf-macros','LBL_MACROS','configurator','macros',NULL,19,3,0,'2012-06-18 22:24:21');
insert  into `pages`(`id`,`parentid`,`name`,`translation`,`modulegroup`,`module`,`menuaction`,`sort`,`userlevel`,`isavailableformobile`,`dateadded`) values (20,10,'conf-plugins','LBL_PLUGINS','configurator','plugins',NULL,20,4,0,'2012-06-18 22:24:21');
insert  into `pages`(`id`,`parentid`,`name`,`translation`,`modulegroup`,`module`,`menuaction`,`sort`,`userlevel`,`isavailableformobile`,`dateadded`) values (21,10,'conf-systemschedules','LBL_SYSTEMSCHEDULES','configurator','systemschedules',NULL,21,3,0,'2012-06-18 22:24:21');
insert  into `pages`(`id`,`parentid`,`name`,`translation`,`modulegroup`,`module`,`menuaction`,`sort`,`userlevel`,`isavailableformobile`,`dateadded`) values (22,10,'conf-pages','LBL_PAGES','configurator','pages',NULL,22,3,0,'2012-06-18 22:24:21');
insert  into `pages`(`id`,`parentid`,`name`,`translation`,`modulegroup`,`module`,`menuaction`,`sort`,`userlevel`,`isavailableformobile`,`dateadded`) values (23,10,'conf-settings','LBL_SETTINGS','configurator','settings',NULL,23,3,0,'2012-06-18 22:24:21');
insert  into `pages`(`id`,`parentid`,`name`,`translation`,`modulegroup`,`module`,`menuaction`,`sort`,`userlevel`,`isavailableformobile`,`dateadded`) values (24,-1,'help','LBL_HELP',NULL,'help',NULL,25,1,0,'2012-02-24 20:30:47');
insert  into `pages`(`id`,`parentid`,`name`,`translation`,`modulegroup`,`module`,`menuaction`,`sort`,`userlevel`,`isavailableformobile`,`dateadded`) values (25,24,'about','LBL_ABOUT',NULL,'about',NULL,26,1,0,'2012-02-24 20:30:47');
insert  into `pages`(`id`,`parentid`,`name`,`translation`,`modulegroup`,`module`,`menuaction`,`sort`,`userlevel`,`isavailableformobile`,`dateadded`) values (26,-1,'logout','LBL_LOGOUT',NULL,'login','logout',27,1,0,'2012-02-24 20:30:47');
insert  into `pages`(`id`,`parentid`,`name`,`translation`,`modulegroup`,`module`,`menuaction`,`sort`,`userlevel`,`isavailableformobile`,`dateadded`) values (27,10,'conf-systemplugins','LBL_SYSTEMPLUGINS','configurator','systemplugins',NULL,24,3,0,'2012-06-18 22:24:21');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;