UPDATE settings SET name = 'houseplaniconheight' WHERE name = 'houseplaniconwidth';
UPDATE settings SET label = 'Dimnivå stegstorlek (%)' WHERE name = 'dimlevelstep';
UPDATE settings SET label = 'Standard dimnivå (%)' WHERE name = 'defaultdimlevel';
UPDATE settings SET label = 'Planlösningsbredd (px)' WHERE name = 'houseplanwidth';
UPDATE settings SET label = 'Planlösnings ikon höjd (px)' WHERE name = 'houseplaniconheight';
UPDATE settings SET type = 'dropdown' WHERE name = 'dimlevelstep';
UPDATE settings SET type = 'dropdown' WHERE name = 'defaultdimlevel';