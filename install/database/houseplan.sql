/*
SQLyog Enterprise - MySQL GUI v6.07
Host - 5.0.51b-community-nt : Database - homeautomation
*********************************************************************
Server version : 5.0.51b-community-nt
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

USE `homeautomation`;

/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

/*Table structure for table `houseplan` */

CREATE TABLE `houseplan` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `deviceid` int(11) default '-1',
  `tempsensor` int(11) default '-1',
  `xposition` int(10) unsigned default '0',
  `yposition` int(10) unsigned default '0',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;