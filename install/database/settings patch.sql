ALTER TABLE settings CHANGE isboolean type VARCHAR(10) DEFAULT 'text' COMMENT 'text, radio, checkbox, password...';
UPDATE settings SET type = "radio" WHERE type = 1;
UPDATE settings SET type = "password" WHERE name LIKE "%password%";
UPDATE settings SET type = "text" WHERE type = "0";