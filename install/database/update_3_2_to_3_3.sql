alter table tempsensors add column active tinyint(1) default 1;
delete from settings where name = 'dynamiccheckinterval';

alter table tempsensors add column last_reading float, add column last_reading_id int(10) unsigned, add column previous_reading float;
update tempsensors s set last_reading=(SELECT temp_c FROM temps t WHERE t.sensor_serial = s.serial ORDER BY t.id DESC LIMIT 1);

alter table systempluginsettings modify value varchar(4096);
