INSERT INTO settings (name, grouping, label, value, type, sort) VALUES ('showinfobox', 'infobox', 'showinfobox', '1', 'radio', 119);
INSERT INTO settings (name, grouping, label, value, type, sort) VALUES ('theme', 'userinterface', 'theme', 'default', 'dropdown', 112);
INSERT INTO settings (name, grouping, label, value, type, sort) VALUES ('defaultdimlevel', 'telldus', 'defaultdimlevel', '50', 'dropdown', 304);
ALTER TABLE tasks DROP COLUMN time;
UPDATE settings SET name = 'selectedscenario', type = 'text', value = -100 WHERE name = 'athome';
UPDATE tasks SET active = -100 WHERE active = 1;
ALTER TABLE tasks CHANGE active active INT(11) DEFAULT -100 COMMENT '0 = disabled, -100 = always active, 1..n = custom scenario';

DROP TABLE IF EXISTS `scenarios`;

CREATE TABLE `scenarios` (                                 
             `id` int(10) unsigned NOT NULL auto_increment,           
             `name` varchar(30) NOT NULL,                             
             PRIMARY KEY  (`id`)                                      
           ) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

INSERT INTO settings (name, grouping, label, value, type, sort) VALUES ('localip', 'system', 'localipaddresses', '192.168.1.1/24', 'text', 204);
INSERT INTO settings (name, grouping, label, value, type, sort) VALUES ('defaultpage', 'userinterface', 'defaultpage', NULL, 'dropdown', 112);
UPDATE settings SET grouping = 'userinterface' WHERE grouping = 'Anv�ndargr�nssnitt';
UPDATE settings SET grouping = 'infobox' WHERE grouping = 'Infobox';
UPDATE settings SET grouping = 'system' WHERE grouping = 'System';
UPDATE settings SET grouping = 'telldus' WHERE grouping = 'Telldus';
UPDATE settings SET label = 'showloggedinuser' WHERE label = 'Visa inloggad anv�ndare';
UPDATE settings SET label = 'showcurrenttime' WHERE label = 'Visa nuvarande tid';
UPDATE settings SET label = 'showtimeforsun' WHERE label = 'Visa tid f�r soluppg�ng/soln';
UPDATE settings SET label = 'numberoflogevents' WHERE label = 'Antal loggh�ndelser';
UPDATE settings SET label = 'numberofcomingevents' WHERE label = 'Antal kommande h�ndelser';
UPDATE settings SET label = 'timelimitforcomingevents' WHERE label = 'Tidsgr�ns f�r kommande h�nd';
UPDATE settings SET label = 'latitude' WHERE label = 'Latitude';
UPDATE settings SET label = 'longitude' WHERE label = 'Longitude';
UPDATE settings SET label = 'version' WHERE label = 'Version';
UPDATE settings SET label = 'dimlevelstep' WHERE label = 'Dimniv� stegstorlek (%)';
UPDATE settings SET label = 'alwaysshowlaststatus' WHERE label = 'Visa alltid senaste status';
UPDATE settings SET label = 'hoursstatusactive' WHERE label = 'Timmar status aktivt';
UPDATE settings SET label = 'houseplanwidthpx' WHERE label = 'Planl�sningsbredd (px)';
UPDATE settings SET label = 'houseplaniconheightpx' WHERE label = 'Planl�snings ikon h�jd (px)';
UPDATE settings SET label = 'useonewire' WHERE label = 'Anv�nd 1-wire temperatur';
UPDATE settings SET label = 'useexterntemperature' WHERE label = 'Anv�nd extern temperatur';
UPDATE settings SET label = 'externtemperatureurl' WHERE label = 'Extern temperatur URL';
UPDATE settings SET label = 'externtemperaturelocation' WHERE label = 'Plats f�r extern temperatur';
UPDATE settings SET label = 'debugmode' WHERE label = 'Debug mode';
UPDATE settings SET label = 'timezone' WHERE label = 'Tidzon';
UPDATE settings SET label = 'title' WHERE label = 'Titel';
UPDATE settings SET label = 'username' WHERE label = 'Anv�ndarnamn';
UPDATE settings SET label = 'password' WHERE label = 'L�senord';
UPDATE settings SET label = 'phppath' WHERE label = 'PHP bin path';
UPDATE settings SET label = 'tdtoolpath' WHERE label = 'Tdtool path';
ALTER TABLE settings DROP COLUMN description;
INSERT INTO settings (name, grouping, label, value, type, sort) VALUES ('ibshowusername', 'infobox', 'showloggedinuser', '0', 'radio', 120);
INSERT INTO settings (name, grouping, label, value, type, sort) VALUES ('ibshowcurrenttime', 'infobox', 'showcurrenttime', '1', 'radio', 121);
INSERT INTO settings (name, grouping, label, value, type, sort) VALUES ('ibshowsun', 'infobox', 'showtimeforsun', '1', 'radio', 122);
INSERT INTO settings (name, grouping, label, value, type, sort) VALUES ('iblogevents', 'infobox', 'numberoflogevents', '3', 'dropdown', 123);
INSERT INTO settings (name, grouping, label, value, type, sort) VALUES ('ibupcomingevents', 'infobox', 'numberofcomingevents', '3', 'dropdown', 124);
INSERT INTO settings (name, grouping, label, value, type, sort) VALUES ('ibupcomingeventstime', 'infobox', 'timelimitforcomingevents', '02:00', 'text', 125);
UPDATE settings SET type = 'dropdown' WHERE name = 'hoursstatusactive';
UPDATE settings SET type = 'dropdown' WHERE name = 'houseplanwidth';
UPDATE settings SET type = 'dropdown' WHERE name = 'houseplaniconheight';
