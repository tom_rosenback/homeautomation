ALTER TABLE devices ALTER telldus_id SET DEFAULT -1;

ALTER TABLE devices ADD `rawdevice` tinyint(4) default '0' AFTER type;
ALTER TABLE devices ADD `rawlearncmd` varchar(150) default NULL AFTER rawdevice;
ALTER TABLE devices ADD `rawoffcmd` varchar(150) default NULL AFTER rawlearncmd;
ALTER TABLE devices ADD `rawoncmd` varchar(150) default NULL AFTER rawoffcmd;

INSERT INTO `devicetypes`(`type`,`text`) VALUES ('absdimmer','dimmer (abs)');

ALTER TABLE events ADD `dimlevel` int(11) default '100' AFTER status;
