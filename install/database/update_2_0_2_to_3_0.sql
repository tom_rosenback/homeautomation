alter table devicegroups ADD COLUMN sort INT(11) UNSIGNED DEFAULT 0;
alter table devices ADD COLUMN systempluginname VARCHAR (30) NOT NULL AFTER id, ADD COLUMN consumption int(11) UNSIGNED DEFAULT '0' NULL COMMENT 'Power consumption in watt (W)' AFTER rawoncmd, ADD COLUMN sort int(11) UNSIGNED DEFAULT '0' NULL AFTER date_added, change telldus_id systemdeviceid varchar(50) DEFAULT '-1' NOT NULL;
alter table macros ADD COLUMN sort INT(11) UNSIGNED DEFAULT 0;
alter table scenarios ADD COLUMN sort INT(11) UNSIGNED DEFAULT 0;
alter table schedules ADD COLUMN sort INT(11) UNSIGNED DEFAULT 0;

CREATE TABLE sensortypes (
  id int(10) unsigned NOT NULL AUTO_INCREMENT,
  description varchar(50) DEFAULT NULL,
  unit varchar(10) DEFAULT NULL,
  sort int(11) unsigned DEFAULT '0',
  PRIMARY KEY (id)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

alter table tempsensors change sensortype sensortype int (10) UNSIGNED  DEFAULT '1' NOT NULL;

insert into `sensortypes`(`description`,`unit`) values ('Temperature','&deg;C');
insert into `sensortypes`(`description`,`unit`) values ('Humidity','%');
insert into `sensortypes`(`description`,`unit`) values ('Rainfall',' mm');
insert into `sensortypes`(`description`,`unit`) values ('Speed',' m/s');
insert into `sensortypes`(`description`,`unit`) values ('Bearing','&deg;');
insert into `sensortypes`(`description`,`unit`) values ('Speed',' km/h');
insert into `sensortypes`(`description`,`unit`) values ('Rainfall',' mm/h');
insert into `sensortypes`(`description`,`unit`) values ('Pressure','mb');
insert into `sensortypes`(`description`,`unit`) values ('Digital','');

CREATE TABLE gcalsettings (
  id int(10) unsigned NOT NULL AUTO_INCREMENT,
  name varchar(30) NOT NULL,
  value text,
  PRIMARY KEY (id)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

insert into gcalsettings(id,name,value) values (1,'username',NULL);
insert into gcalsettings(id,name,value) values (2,'password',NULL);
insert into gcalsettings(id,name,value) values (3,'selectedcalendars',NULL);

insert into settings (name, grouping, label, value, type, sort) values ('iconortext', 'userinterface', 'iconortext', 'text', 'dropdown', '99');

alter table users ADD COLUMN salt VARCHAR(32) DEFAULT '';

CREATE TABLE `pages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parentid` int(10) DEFAULT '-1',
  `name` varchar(30) NOT NULL,
  `translation` varchar(20) NOT NULL,
  `modulegroup` varchar(30) DEFAULT NULL,
  `module` varchar(30) NOT NULL,
  `menuaction` varchar(20) DEFAULT NULL,
  `sort` int(11) DEFAULT '0',
  `userlevel` tinyint(4) DEFAULT '3',
  `isavailableformobile` tinyint(1) DEFAULT '0',
  `isavailablefordesktop` tinyint(1) DEFAULT '1',
  `dateadded` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

insert  into `pages`(`id`,`parentid`,`name`,`translation`,`modulegroup`,`module`,`menuaction`,`sort`,`userlevel`,`isavailableformobile`,`isavailablefordesktop`,`dateadded`) values (1,-1,'houseplan','LBL_HOUSEPLAN',NULL,'houseplan',NULL,1,1,1,1,'2012-11-28 18:35:36');
insert  into `pages`(`id`,`parentid`,`name`,`translation`,`modulegroup`,`module`,`menuaction`,`sort`,`userlevel`,`isavailableformobile`,`isavailablefordesktop`,`dateadded`) values (2,-1,'devices','LBL_DEVICES',NULL,'devices',NULL,2,1,1,1,'2012-11-28 18:35:37');
insert  into `pages`(`id`,`parentid`,`name`,`translation`,`modulegroup`,`module`,`menuaction`,`sort`,`userlevel`,`isavailableformobile`,`isavailablefordesktop`,`dateadded`) values (3,-1,'groups','LBL_GROUPS',NULL,'groups',NULL,3,1,1,1,'2012-11-28 18:35:37');
insert  into `pages`(`id`,`parentid`,`name`,`translation`,`modulegroup`,`module`,`menuaction`,`sort`,`userlevel`,`isavailableformobile`,`isavailablefordesktop`,`dateadded`) values (4,-1,'macros','LBL_MACROS',NULL,'macros',NULL,4,1,1,1,'2012-11-28 18:35:38');
insert  into `pages`(`id`,`parentid`,`name`,`translation`,`modulegroup`,`module`,`menuaction`,`sort`,`userlevel`,`isavailableformobile`,`isavailablefordesktop`,`dateadded`) values (5,-1,'scheduler','LBL_SCHEDULER',NULL,'scheduler',NULL,5,1,0,1,'2011-11-12 14:02:44');
insert  into `pages`(`id`,`parentid`,`name`,`translation`,`modulegroup`,`module`,`menuaction`,`sort`,`userlevel`,`isavailableformobile`,`isavailablefordesktop`,`dateadded`) values (6,-1,'logs','LBL_LOGS','logs','systemlog',NULL,6,1,1,1,'2012-11-28 18:35:41');
insert  into `pages`(`id`,`parentid`,`name`,`translation`,`modulegroup`,`module`,`menuaction`,`sort`,`userlevel`,`isavailableformobile`,`isavailablefordesktop`,`dateadded`) values (7,6,'logs-measurements','LBL_MEASUREMENTS','logs','measurements',NULL,1,1,1,1,'2012-11-28 19:15:21');
insert  into `pages`(`id`,`parentid`,`name`,`translation`,`modulegroup`,`module`,`menuaction`,`sort`,`userlevel`,`isavailableformobile`,`isavailablefordesktop`,`dateadded`) values (8,6,'logs-systemlog','LBL_LOGS','logs','systemlog',NULL,2,1,1,1,'2012-11-28 19:15:22');
insert  into `pages`(`id`,`parentid`,`name`,`translation`,`modulegroup`,`module`,`menuaction`,`sort`,`userlevel`,`isavailableformobile`,`isavailablefordesktop`,`dateadded`) values (9,6,'logs-consumption','LBL_POWERCONSUMPTION','logs','consumption',NULL,3,4,0,1,'2012-06-18 22:24:21');
insert  into `pages`(`id`,`parentid`,`name`,`translation`,`modulegroup`,`module`,`menuaction`,`sort`,`userlevel`,`isavailableformobile`,`isavailablefordesktop`,`dateadded`) values (10,-1,'configurator','LBL_CONFIGURATION','configurator','devices',NULL,7,3,1,1,'2012-11-28 21:04:27');
insert  into `pages`(`id`,`parentid`,`name`,`translation`,`modulegroup`,`module`,`menuaction`,`sort`,`userlevel`,`isavailableformobile`,`isavailablefordesktop`,`dateadded`) values (11,10,'conf-devices','LBL_DEVICES','configurator','devices',NULL,1,3,1,1,'2012-11-28 21:04:28');
insert  into `pages`(`id`,`parentid`,`name`,`translation`,`modulegroup`,`module`,`menuaction`,`sort`,`userlevel`,`isavailableformobile`,`isavailablefordesktop`,`dateadded`) values (12,10,'conf-devicegroups','LBL_DEVICEGROUPS','configurator','devicegroups',NULL,2,3,1,1,'2012-11-28 21:04:28');
insert  into `pages`(`id`,`parentid`,`name`,`translation`,`modulegroup`,`module`,`menuaction`,`sort`,`userlevel`,`isavailableformobile`,`isavailablefordesktop`,`dateadded`) values (13,10,'conf-houseplan','LBL_HOUSEPLAN','configurator','houseplan',NULL,3,3,1,1,'2012-11-28 21:04:28');
insert  into `pages`(`id`,`parentid`,`name`,`translation`,`modulegroup`,`module`,`menuaction`,`sort`,`userlevel`,`isavailableformobile`,`isavailablefordesktop`,`dateadded`) values (14,10,'conf-scenarios','LBL_SCENARIOS','configurator','scenarios',NULL,4,3,1,1,'2012-11-28 21:04:28');
/*iinsert  into `pages`(`id`,`parentid`,`name`,`translation`,`modulegroup`,`module`,`menuaction`,`sort`,`userlevel`,`isavailableformobile`,`isavailablefordesktop`,`dateadded`) values (15,10,'conf-gcal','LBL_GCAL','configurator','gcal',NULL,5,3,1,1,'2012-11-28 21:04:28'); */;
insert  into `pages`(`id`,`parentid`,`name`,`translation`,`modulegroup`,`module`,`menuaction`,`sort`,`userlevel`,`isavailableformobile`,`isavailablefordesktop`,`dateadded`) values (16,10,'conf-usercontrol','LBL_USERMANAGEMENT','configurator','usercontrol',NULL,6,3,1,1,'2012-11-28 21:04:29');
insert  into `pages`(`id`,`parentid`,`name`,`translation`,`modulegroup`,`module`,`menuaction`,`sort`,`userlevel`,`isavailableformobile`,`isavailablefordesktop`,`dateadded`) values (17,10,'conf-sensors','LBL_SENSORS','configurator','sensors',NULL,7,3,1,1,'2012-11-28 21:04:29');
insert  into `pages`(`id`,`parentid`,`name`,`translation`,`modulegroup`,`module`,`menuaction`,`sort`,`userlevel`,`isavailableformobile`,`isavailablefordesktop`,`dateadded`) values (18,10,'conf-sensortypes','LBL_SENSORTYPES','configurator','sensortypes',NULL,8,3,1,1,'2012-11-28 21:04:29');
insert  into `pages`(`id`,`parentid`,`name`,`translation`,`modulegroup`,`module`,`menuaction`,`sort`,`userlevel`,`isavailableformobile`,`isavailablefordesktop`,`dateadded`) values (19,10,'conf-macros','LBL_MACROS','configurator','macros',NULL,9,3,1,1,'2012-11-28 21:04:29');
insert  into `pages`(`id`,`parentid`,`name`,`translation`,`modulegroup`,`module`,`menuaction`,`sort`,`userlevel`,`isavailableformobile`,`isavailablefordesktop`,`dateadded`) values (20,10,'conf-plugins','LBL_PLUGINS','configurator','plugins',NULL,10,4,1,1,'2012-11-28 21:04:30');
insert  into `pages`(`id`,`parentid`,`name`,`translation`,`modulegroup`,`module`,`menuaction`,`sort`,`userlevel`,`isavailableformobile`,`isavailablefordesktop`,`dateadded`) values (21,10,'conf-systemschedules','LBL_SYSTEMSCHEDULES','configurator','systemschedules',NULL,11,3,1,1,'2012-11-28 21:04:30');
/*iinsert  into `pages`(`id`,`parentid`,`name`,`translation`,`modulegroup`,`module`,`menuaction`,`sort`,`userlevel`,`isavailableformobile`,`isavailablefordesktop`,`dateadded`) values (22,10,'conf-pages','LBL_PAGES','configurator','pages',NULL,12,3,1,1,'2012-11-28 21:04:30');*/
insert  into `pages`(`id`,`parentid`,`name`,`translation`,`modulegroup`,`module`,`menuaction`,`sort`,`userlevel`,`isavailableformobile`,`isavailablefordesktop`,`dateadded`) values (23,10,'conf-settings','LBL_SETTINGS','configurator','settings',NULL,13,3,1,1,'2012-11-28 21:04:31');
insert  into `pages`(`id`,`parentid`,`name`,`translation`,`modulegroup`,`module`,`menuaction`,`sort`,`userlevel`,`isavailableformobile`,`isavailablefordesktop`,`dateadded`) values (24,-1,'help','LBL_HELP',NULL,'help',NULL,8,1,1,1,'2012-11-28 18:35:54');
insert  into `pages`(`id`,`parentid`,`name`,`translation`,`modulegroup`,`module`,`menuaction`,`sort`,`userlevel`,`isavailableformobile`,`isavailablefordesktop`,`dateadded`) values (25,24,'help-about','LBL_ABOUT',NULL,'about',NULL,1,1,1,1,'2012-11-28 18:35:55');
insert  into `pages`(`id`,`parentid`,`name`,`translation`,`modulegroup`,`module`,`menuaction`,`sort`,`userlevel`,`isavailableformobile`,`isavailablefordesktop`,`dateadded`) values (26,-1,'logout','LBL_LOGOUT',NULL,'login','logout',27,1,1,1,'2012-11-28 18:35:56');
insert  into `pages`(`id`,`parentid`,`name`,`translation`,`modulegroup`,`module`,`menuaction`,`sort`,`userlevel`,`isavailableformobile`,`isavailablefordesktop`,`dateadded`) values (27,10,'conf-systemplugins','LBL_SYSTEMPLUGINS','configurator','systemplugins',NULL,14,3,0,1,'2012-06-18 22:24:21');

CREATE TABLE systemschedules (
                   id int(10) unsigned NOT NULL AUTO_INCREMENT,
                   enabled tinyint(1) DEFAULT '1',
                   description varchar(200) DEFAULT NULL,
                   executable varchar(200) DEFAULT NULL,
                   arguments varchar(200) DEFAULT NULL,
                   settings varchar(50) DEFAULT NULL,
                   sort int(11) DEFAULT '0',
                   datecreated timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                   PRIMARY KEY (id)
                 );

CREATE TABLE systemplugins (
  id int(10) unsigned NOT NULL AUTO_INCREMENT,
  pluginname varchar(30) NOT NULL,
  version varchar(10) DEFAULT NULL,
  dateinstalled timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (id),
  UNIQUE KEY pluginname (pluginname)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

insert  into `systemplugins`(`pluginname`,`version`) values ('tdtool','1.0');
insert  into `systemplugins`(`pluginname`,`version`) values ('sendmail','0.1');
insert  into `systemplugins`(`pluginname`,`version`) values ('dummy','0.0');

CREATE TABLE systempluginsettings (
  id int(10) unsigned NOT NULL AUTO_INCREMENT,
  pluginname varchar(30) NOT NULL,
  name varchar(30) NOT NULL,
  value varchar(100) DEFAULT NULL,
  type varchar(20) NOT NULL DEFAULT 'text',
  sort int(11) DEFAULT '0',
  PRIMARY KEY (id)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

insert  into `systempluginsettings`(`pluginname`,`name`,`value`,`type`,`sort`) values ('sendmail','destinationaddress',NULL,'text',0);
insert  into `systempluginsettings`(`pluginname`,`name`,`value`,`type`,`sort`) values ('tdtool','tellduspath','','text',0);

update `systempluginsettings` SET value = (SELECT value FROM settings WHERE name = 'tellduspath' LIMIT 1) WHERE pluginname = 'tdtool' AND name = 'tellduspath';

UPDATE devices SET systempluginname = 'tdtool';
UPDATE settings SET sort = -1 WHERE name = 'tellduspath';
update settings set sort = 115, grouping = 'userinterface' where name = 'dimlevelstep';

insert into `devicetypes`(`type`,`text`) values ('bell','klocka');
insert  into `devicetypes`(`type`,`text`) values ('dummy','dummy');
alter table `events` add index `timestamp` (`timestamp`);

create table `userlevelpermissions` (  `pageid` int , `userlevelid` int );
alter table `userlevelpermissions` add unique `pageid` (`pageid`, `userlevelid`);

ALTER TABLE devicegrouprelations CONVERT TO CHARACTER SET utf8;
ALTER TABLE events CONVERT TO CHARACTER SET utf8;
ALTER TABLE gcalsettings CONVERT TO CHARACTER SET utf8;
ALTER TABLE macrorelations CONVERT TO CHARACTER SET utf8;
ALTER TABLE scheduleactivations CONVERT TO CHARACTER SET utf8;
ALTER TABLE settings CONVERT TO CHARACTER SET utf8;
ALTER TABLE systemplugins CONVERT TO CHARACTER SET utf8;
ALTER TABLE userlevelpermissions CONVERT TO CHARACTER SET utf8;

ALTER TABLE devicegroups CHANGE name name varchar(30) CHARACTER SET utf8;
ALTER TABLE devicegroups CONVERT TO CHARACTER SET utf8;

ALTER TABLE devices CHANGE systempluginname systempluginname varchar(30) CHARACTER SET utf8 NOT NULL;
ALTER TABLE devices CHANGE description description varchar(50) CHARACTER SET utf8 NOT NULL;
ALTER TABLE devices CHANGE rawlearncmd rawlearncmd varchar(150) CHARACTER SET utf8;
ALTER TABLE devices CHANGE rawoncmd rawoncmd varchar(150) CHARACTER SET utf8;
ALTER TABLE devices CHANGE rawoffcmd rawoffcmd varchar(150) CHARACTER SET utf8;
ALTER TABLE devices CONVERT TO CHARACTER SET utf8;

ALTER TABLE devicetypes CHANGE type type varchar(50) CHARACTER SET utf8 NOT NULL;
ALTER TABLE devicetypes CHANGE text text varchar(50) CHARACTER SET utf8 NOT NULL;
ALTER TABLE devicetypes CONVERT TO CHARACTER SET utf8;

ALTER TABLE houseplan CHANGE tempsensor tempsensor varchar(24) CHARACTER SET utf8 DEFAULT '-1';
ALTER TABLE houseplan CHANGE groupid groupid varchar(20) CHARACTER SET utf8 DEFAULT '-1';
ALTER TABLE houseplan CONVERT TO CHARACTER SET utf8;

ALTER TABLE macros CHANGE name name varchar(30) CHARACTER SET utf8;
ALTER TABLE macros CHANGE comment comment varchar(200) CHARACTER SET utf8;
ALTER TABLE macros CONVERT TO CHARACTER SET utf8;

ALTER TABLE pages CHANGE name name varchar(30) CHARACTER SET utf8 NOT NULL;
ALTER TABLE pages CHANGE translation translation varchar(20) CHARACTER SET utf8 NOT NULL;
ALTER TABLE pages CHANGE modulegroup modulegroup varchar(30) CHARACTER SET utf8;
ALTER TABLE pages CHANGE module module varchar(30) CHARACTER SET utf8 NOT NULL;
ALTER TABLE pages CHANGE menuaction menuaction varchar(20) CHARACTER SET utf8;
ALTER TABLE pages CONVERT TO CHARACTER SET utf8;

ALTER TABLE scenarios CHANGE name name varchar(30) CHARACTER SET utf8 NOT NULL;
ALTER TABLE scenarios CONVERT TO CHARACTER SET utf8;

ALTER TABLE schedules CHANGE name name varchar(100) CHARACTER SET utf8 NOT NULL;
ALTER TABLE schedules CHANGE comment comment varchar(200) CHARACTER SET utf8;
ALTER TABLE schedules CONVERT TO CHARACTER SET utf8;

ALTER TABLE sensortypes CHANGE description description varchar(50) CHARACTER SET utf8;
ALTER TABLE sensortypes CHANGE unit unit varchar(10) CHARACTER SET utf8;
ALTER TABLE sensortypes CONVERT TO CHARACTER SET utf8;

ALTER TABLE settings CHANGE value value varchar(100) CHARACTER SET utf8;
ALTER TABLE settings CONVERT TO CHARACTER SET utf8;

ALTER TABLE systempluginsettings CHANGE value value varchar(100) CHARACTER SET utf8;
ALTER TABLE systempluginsettings CONVERT TO CHARACTER SET utf8;

ALTER TABLE systemschedules CHANGE description description varchar(200) CHARACTER SET utf8;
ALTER TABLE systemschedules CHANGE executable executable varchar(200) CHARACTER SET utf8;
ALTER TABLE systemschedules CHANGE arguments arguments varchar(200) CHARACTER SET utf8;
ALTER TABLE systemschedules CHANGE settings settings varchar(50) CHARACTER SET utf8;
ALTER TABLE systemschedules CONVERT TO CHARACTER SET utf8;

ALTER TABLE temps CHANGE sensor_serial sensor_serial varchar(24) CHARACTER SET utf8;
ALTER TABLE temps CONVERT TO CHARACTER SET utf8;

ALTER TABLE tempsensors CHANGE serial serial varchar(24) CHARACTER SET utf8;
ALTER TABLE tempsensors CHANGE name name varchar(30) CHARACTER SET utf8;
ALTER TABLE tempsensors CONVERT TO CHARACTER SET utf8;

ALTER TABLE userlevels CHANGE description description varchar(50) CHARACTER SET utf8;
ALTER TABLE userlevels CONVERT TO CHARACTER SET utf8;

ALTER TABLE users CHANGE username username varchar(20) CHARACTER SET utf8 NOT NULL;
ALTER TABLE users CHANGE password password char(32) CHARACTER SET utf8;
ALTER TABLE users CHANGE firstname firstname varchar(50) CHARACTER SET utf8;
ALTER TABLE users CHANGE lastname lastname varchar(50) CHARACTER SET utf8;
ALTER TABLE users CHANGE salt salt varchar(32) CHARACTER SET utf8 DEFAULT '';
ALTER TABLE users CONVERT TO CHARACTER SET utf8;

alter table `users` add column `email` varchar (50)  NULL  after `userlevel`;
update pages set module='systemlog' where name = 'logs';

alter table `houseplan` change `tempsensor` `tempsensor` varchar (50) DEFAULT '-1' NULL  COLLATE utf8_general_ci;
alter table `temps` change `sensor_serial` `sensor_serial` varchar (50)  NULL  COLLATE utf8_general_ci;
alter table `tempsensors` change `serial` `serial` varchar (50)  NULL  COLLATE utf8_general_ci;

insert into settings(name, grouping, label, value, type, sort) values('ibsensor', 'infobox', 'usethissensor', 'nosensor', 'dropdown', 122);

alter table `sensortypes` add column `gaugetype` varchar(20) DEFAULT 'speedometer' COLLATE utf8_general_ci after `unit`;
alter table `sensortypes` add column `min` double after `gaugetype`;
alter table `sensortypes` add column `max` double after `min`;
alter table `sensortypes` add column `low` double after `max`;
alter table `sensortypes` add column `high` double after `low`;

alter table `tempsensors` add column `gaugetype` varchar(20) DEFAULT 'speedometer' COLLATE utf8_general_ci;
alter table `tempsensors` add column `min` double;
alter table `tempsensors` add column `max` double;
alter table `tempsensors` add column `low` double;
alter table `tempsensors` add column `high` double;

update settings set label = 'houseplaniconheight' where name = 'houseplaniconheight';
update settings set label = 'houseplanwidth' where name = 'houseplanwidth';

/*ALTER DATABASE homeautomation CHARACTER SET utf8;*/

/*Table structure for table `usermailnotification` */

CREATE TABLE `usermailnotification` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userid` int(11) NOT NULL,
  `passkey` varchar(64) NOT NULL,
  `enabled` tinyint(1) DEFAULT '0',
  `sent` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `usermailnotification` */
