alter table `devices` add column `last_state` tinyint  DEFAULT '0' NULL  after `date_added`, add column `last_dimlevel` tinyint   NULL  after `last_state`, add column `last_updated` timestamp   NULL  after `last_dimlevel`;

insert into `sensortypes`(`description`,`unit`, min, max) values ('Ambient light','lux', 0, 10000);
insert into `sensortypes`(`description`,`unit`, min, max) values ('Power','W', 0, 1000);
