DROP TABLE IF EXISTS `schedules`;

CREATE TABLE `schedules` (                                                                                                
             `id` int(10) unsigned NOT NULL AUTO_INCREMENT,                                                                          
             `name` varchar(100) NOT NULL,                                                                                           
             `comment` varchar(200) DEFAULT NULL,                                                                                    
             `devices` varchar(100) DEFAULT NULL COMMENT '-1 = all devices otherwise deviceids separated by semicolon',
			 `devicegroups` varchar(100) DEFAULT NULL COMMENT 'groupids separated by semicolon',                  
             `days` varchar(20) DEFAULT NULL COMMENT '-1 = all days otherwise days separated by semicolon (1=monday ... 7=sunday)',
             `scenario` int(11) DEFAULT '-100' COMMENT '-100 = all, 1..n = custom scenario',                               
             `enabled` tinyint(1) DEFAULT '1' COMMENT '0 = disabled, 1 = enabled',                                                   
             `dateadded` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,                                                               
             PRIMARY KEY (`id`)                                                                                                      
           ) ENGINE=MyISAM DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `scheduleactivations`;

CREATE TABLE `scheduleactivations` (                         
                       `id` int(10) unsigned NOT NULL AUTO_INCREMENT,             
                       `scheduleid` int(11) NOT NULL,                             
                       `type` varchar(30) DEFAULT 'static',                          
                       `params` varchar(200) DEFAULT NULL,                        
                       `sort` int(10) unsigned DEFAULT NULL,
						`lastruntime` datetime default NULL,
						`upcomingruntime` datetime default NULL,			
						`nextdaysruntime` datetime default NULL,
						`linuxatid` int(10) unsigned DEFAULT NULL,
                       `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,  
                       PRIMARY KEY (`id`)                                         
                     ) ENGINE=MyISAM DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `devicegroups`;
					 
CREATE TABLE `devicegroups` (                            
                `id` int(10) unsigned NOT NULL AUTO_INCREMENT,         
                `name` varchar(30) DEFAULT NULL,                       
                `dateadded` timestamp NULL DEFAULT CURRENT_TIMESTAMP,  
                PRIMARY KEY (`id`)                                     
              ) ENGINE=MyISAM DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `devicegrouprelations`;
		  
CREATE TABLE `devicegrouprelations` (                        
                        `id` int(10) unsigned NOT NULL AUTO_INCREMENT,             
                        `groupid` int(10) unsigned NOT NULL,                       
                        `deviceid` int(10) unsigned NOT NULL,                      
                        `dateadded` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,  
                        PRIMARY KEY (`id`)                                         
                      ) ENGINE=MyISAM DEFAULT CHARSET=latin1;

UPDATE settings SET sort = '108' WHERE name = 'latitude';
UPDATE settings SET sort = '109' WHERE name = 'longitude';

ALTER TABLE houseplan ADD COLUMN groupid varchar(20) DEFAULT '-1' AFTER tempsensor;
INSERT INTO settings (name, grouping, label, value, type, sort) VALUES ('numsignalrepetitions', 'telldus', 'numsignalrepetitions', 1, 'dropdown', 305);
ALTER TABLE houseplan ADD COLUMN macroid int(11) DEFAULT '-1' AFTER groupid;

DROP TABLE IF EXISTS `macros`;

CREATE TABLE `macros` (                                  
          `id` int(10) unsigned NOT NULL AUTO_INCREMENT,         
          `name` varchar(30) DEFAULT NULL,                       
          `comment` varchar(200) DEFAULT NULL,                   
          `scenario` int(11) DEFAULT '-1',                       
          `dateadded` timestamp NULL DEFAULT CURRENT_TIMESTAMP,  
          PRIMARY KEY (`id`)                                     
        ) ENGINE=MyISAM DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `macrorelations`;

CREATE TABLE `macrorelations` (                           
                  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,          
                  `macroid` int(10) unsigned NOT NULL,                    
                  `deviceid` int(10) unsigned NOT NULL,                   
                  `status` int(10) unsigned NOT NULL,                     
                  PRIMARY KEY (`id`)                                      
                ) ENGINE=MyISAM DEFAULT CHARSET=latin1;

UPDATE settings SET sort = '-1' WHERE name = 'jtbinpath';
UPDATE settings SET sort = '-1' WHERE name = 'ibupcomingeventstime';
