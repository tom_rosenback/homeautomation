alter table `users` change `salt` `salt` varchar (150) DEFAULT '' NULL  COLLATE utf8_general_ci;

ALTER TABLE systemplugins ADD lastupdated DATETIME;

ALTER TABLE scheduleactivations ADD triggered BOOLEAN DEFAULT FALSE;

DELETE FROM settings WHERE name = 'numsignalrepetitions';

UPDATE settings SET grouping = 'dimlevels' WHERE grouping = 'telldus';
