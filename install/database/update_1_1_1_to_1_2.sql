UPDATE settings SET sort = 115 WHERE name = 'theme';
UPDATE settings SET sort = 116 WHERE name = 'defaultpage';
UPDATE settings SET sort = 118 WHERE name = 'debug';

INSERT INTO settings (name, grouping, label, value, type, sort) VALUES ('dateformat', 'userinterface', 'dateformat', 'd.n.Y', 'text', 111);
INSERT INTO settings (name, grouping, label, value, type, sort) VALUES ('timeformat', 'userinterface', 'timeformat', 'H:i', 'text', 112);

