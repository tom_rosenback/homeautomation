TRUNCATE table users;
ALTER TABLE users ADD COLUMN editable tinyint(1) DEFAULT 2 COMMENT '0 = not editable, 1 = userlevel editable, 2 = fully editable';
INSERT INTO users (username, firstname, userlevel_id, editable) VALUES ('system', 'system', 3, 0);
INSERT INTO users (username, firstname, userlevel_id, editable) VALUES ('remote', 'remote', 3, 0);
INSERT INTO users (username, firstname, userlevel_id, editable) VALUES ('local', 'local', 3, 1);
INSERT INTO users (username, firstname, password, userlevel_id, editable) VALUES ('demo', 'demo', md5('d3m0'), 1, 2);