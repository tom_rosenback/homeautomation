/*
SQLyog Enterprise - MySQL GUI v6.07
Host - 5.0.51b-community-nt : Database - homeautomation
*********************************************************************
Server version : 5.0.51b-community-nt
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

/*Table structure for table `settings` */

CREATE TABLE `settings` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `name` varchar(30) NOT NULL,
  `grouping` varchar(30) default NULL,
  `label` varchar(30) default NULL,
  `description` text,
  `value` varchar(100) default NULL,
  `isboolean` tinyint(1) default '0',
  `sort` int(11) default '-1' COMMENT '-1 = do not display setting on settings page',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;

/*Data for the table `settings` */

insert  into `settings`(`id`,`name`,`grouping`,`label`,`description`,`value`,`isboolean`,`sort`) values (1,'athome','Användargränssnitt',NULL,NULL,'1',1,-1);
insert  into `settings`(`id`,`name`,`grouping`,`label`,`description`,`value`,`isboolean`,`sort`) values (24,'latitude','Användargränssnitt','Latitude','','58.029',0,109);
insert  into `settings`(`id`,`name`,`grouping`,`label`,`description`,`value`,`isboolean`,`sort`) values (23,'longitude','Användargränssnitt','Longitude','','15.652',0,108);
insert  into `settings`(`id`,`name`,`grouping`,`label`,`description`,`value`,`isboolean`,`sort`) values (6,'sysusername','System','Användarnamn',NULL,'COMPUTER\\username',0,200);
insert  into `settings`(`id`,`name`,`grouping`,`label`,`description`,`value`,`isboolean`,`sort`) values (7,'syspassword','System','Lösenord',NULL,'password',0,201);
insert  into `settings`(`id`,`name`,`grouping`,`label`,`description`,`value`,`isboolean`,`sort`) values (8,'systaskspath','System','Scheduledtasks path',NULL,'C:\\WINDOWS\\TASKS\\',0,202);
insert  into `settings`(`id`,`name`,`grouping`,`label`,`description`,`value`,`isboolean`,`sort`) values (9,'phpbinpath','System','PHP bin path',NULL,'C:\\Program Files\\xampp\\php\\',0,203);
insert  into `settings`(`id`,`name`,`grouping`,`label`,`description`,`value`,`isboolean`,`sort`) values (10,'tellduspath','Telldus','Tdtool path',NULL,'C:\\Progra~1\\Telldus\\',0,301);
insert  into `settings`(`id`,`name`,`grouping`,`label`,`description`,`value`,`isboolean`,`sort`) values (11,'rawcommandspath','Telldus','RAW-kommando path',NULL,'X:\\wwwroot\\homeautomation\\rawcommands\\',0,302);
insert  into `settings`(`id`,`name`,`grouping`,`label`,`description`,`value`,`isboolean`,`sort`) values (12,'dimlevelstep','Telldus','Dimnivå steg',NULL,'5',0,303);
insert  into `settings`(`id`,`name`,`grouping`,`label`,`description`,`value`,`isboolean`,`sort`) values (13,'defaultdimlevel','Telldus','Default dimnivå',NULL,'80',0,304);
insert  into `settings`(`id`,`name`,`grouping`,`label`,`description`,`value`,`isboolean`,`sort`) values (14,'alwaysuselastknownstatus','Användargränssnitt','Visa alltid senaste status',NULL,'1',1,100);
insert  into `settings`(`id`,`name`,`grouping`,`label`,`description`,`value`,`isboolean`,`sort`) values (15,'hoursstatusactive','Användargränssnitt','Timmar status aktivt',NULL,'1',0,101);
insert  into `settings`(`id`,`name`,`grouping`,`label`,`description`,`value`,`isboolean`,`sort`) values (16,'houseplanwidth','Användargränssnitt','Planlösningsbredd',NULL,'800',0,102);
insert  into `settings`(`id`,`name`,`grouping`,`label`,`description`,`value`,`isboolean`,`sort`) values (17,'houseplaniconwidth','Användargränssnitt','Planlösnings ikon bredd',NULL,'20',0,103);
insert  into `settings`(`id`,`name`,`grouping`,`label`,`description`,`value`,`isboolean`,`sort`) values (19,'useexternaltemperature','Användargränssnitt','Använd extern temperatur',NULL,'0',1,105);
insert  into `settings`(`id`,`name`,`grouping`,`label`,`description`,`value`,`isboolean`,`sort`) values (20,'externaltempurl','Användargränssnitt','Extern temperatur URL',NULL,'http://www.temperatur.nu/termo/globen/temp.txt',0,106);
insert  into `settings`(`id`,`name`,`grouping`,`label`,`description`,`value`,`isboolean`,`sort`) values (21,'externaltemplocation','Användargränssnitt','Plats för extern temperatur',NULL,'Globen',0,107);
insert  into `settings`(`id`,`name`,`grouping`,`label`,`description`,`value`,`isboolean`,`sort`) values (22,'debug','Användargränssnitt','Debug mode',NULL,'0',1,110);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
