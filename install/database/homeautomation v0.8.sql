/*
SQLyog Enterprise - MySQL GUI v6.07
Host - 5.0.51b-community-nt : Database - homeautomation
*********************************************************************
Server version : 5.0.51b-community-nt
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

create database if not exists `homeautomation`;

USE `homeautomation`;

/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

/*Table structure for table `devices` */

CREATE TABLE `devices` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `telldus_id` int(11) NOT NULL,
  `description` varchar(50) NOT NULL,
  `active` tinyint(1) default '0',
  `type` tinyint(3) unsigned default '1',
  `date_added` timestamp NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

/*Table structure for table `devicetypes` */

CREATE TABLE `devicetypes` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `type` varchar(50) NOT NULL,
  `text` varchar(50) NOT NULL,
  `dateadded` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `devicetypes` */

insert  into `devicetypes`(`id`,`type`,`text`,`dateadded`) values (1,'light','lampa','2008-11-11 07:17:06');
insert  into `devicetypes`(`id`,`type`,`text`,`dateadded`) values (2,'dimmer','dimmer','2008-11-11 07:17:12');
insert  into `devicetypes`(`id`,`type`,`text`,`dateadded`) values (3,'heating','v�rme','2008-11-11 07:17:18');

/*Table structure for table `events` */

CREATE TABLE `events` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `device_id` int(10) unsigned NOT NULL,
  `status` tinyint(1) NOT NULL,
  `userid` int(11) NOT NULL,
  `ipaddress` varchar(50) default NULL,
  `timestamp` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9216 DEFAULT CHARSET=latin1;

/*Table structure for table `settings` */

CREATE TABLE `settings` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `name` varchar(20) NOT NULL,
  `value` varchar(50) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `settings` */

insert  into `settings`(`id`,`name`,`value`) values (1,'athome','1');

/*Table structure for table `tasks` */

CREATE TABLE `tasks` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `name` varchar(100) NOT NULL,
  `devices` varchar(100) NOT NULL COMMENT '-1 = all devices otherwise deviceids separated by ;',
  `days` varchar(20) default NULL COMMENT '-1 = all days otherwise days separated by ; (1=monday ... 7=sunday)',
  `time` time default NULL COMMENT 'format 12:00',
  `status` tinyint(4) default '0' COMMENT '0 = off, 1 = on',
  `active` tinyint(1) default '1' COMMENT '0 = disabled, 1 = enabled, 2 = home, 3 = away',
  `timestamp` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;

/*Table structure for table `userlevels` */

CREATE TABLE `userlevels` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `userlevel` int(10) unsigned NOT NULL,
  `description` varchar(50) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `userlevels` */

insert  into `userlevels`(`id`,`userlevel`,`description`) values (1,1,'demo');
insert  into `userlevels`(`id`,`userlevel`,`description`) values (2,2,'user');
insert  into `userlevels`(`id`,`userlevel`,`description`) values (3,3,'admin');

/*Table structure for table `users` */

CREATE TABLE `users` (
  `id` int(11) NOT NULL auto_increment,
  `username` varchar(20) collate latin1_general_ci NOT NULL,
  `password` char(32) collate latin1_general_ci default NULL,
  `firstname` varchar(50) collate latin1_general_ci default NULL,
  `lastname` varchar(50) collate latin1_general_ci default NULL,
  `userlevel_id` int(11) default '1',
  PRIMARY KEY  (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci ROW_FORMAT=DYNAMIC;

/*Data for the table `users` */

insert  into `users`(`id`,`username`,`password`,`firstname`,`lastname`,`userlevel_id`) values (-1,'local',NULL,'local',NULL,3);
insert  into `users`(`id`,`username`,`password`,`firstname`,`lastname`,`userlevel_id`) values (-2,'system',NULL,'system',NULL,3);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
