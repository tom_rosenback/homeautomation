DROP TABLE IF EXISTS `devices`;

CREATE TABLE `devices` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `telldus_id` int(11) NOT NULL default '-1',
  `description` varchar(50) NOT NULL,
  `active` tinyint(1) default '0',
  `type` tinyint(3) unsigned default '1',
  `rawdevice` tinyint(4) default '0',
  `rawlearncmd` varchar(150) default NULL,
  `rawoffcmd` varchar(150) default NULL,
  `rawoncmd` varchar(150) default NULL,
  `date_added` timestamp NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

/*Table structure for table `devicetypes` */

DROP TABLE IF EXISTS `devicetypes`;

CREATE TABLE `devicetypes` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `type` varchar(50) NOT NULL,
  `text` varchar(50) NOT NULL,
  `dateadded` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

/*Data for the table `devicetypes` */

insert  into `devicetypes`(`type`,`text`) values ('light','lampa');
insert  into `devicetypes`(`type`,`text`) values ('dimmer','dimmer');
insert  into `devicetypes`(`type`,`text`) values ('sauna','bastu');
insert  into `devicetypes`(`type`,`text`) values ('absdimmer','dimmer (abs)');
insert  into `devicetypes`(`type`,`text`) values ('engineheater','motorvärmare');

/*Table structure for table `events` */

DROP TABLE IF EXISTS `events`;

CREATE TABLE `events` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `device_id` int(10) unsigned NOT NULL,
  `status` tinyint(1) NOT NULL,
  `dimlevel` int(11) default '0',
  `userid` int(11) NOT NULL,
  `ipaddress` varchar(50) default NULL,
  `timestamp` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

/*Table structure for table `houseplan` */

DROP TABLE IF EXISTS `houseplan`;

CREATE TABLE `houseplan` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `deviceid` int(11) default '-1',
  `tempsensor` varchar(24) default '-1',
  `xposition` int(10) unsigned default '0',
  `yposition` int(10) unsigned default '0',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

/*Table structure for table `settings` */

DROP TABLE IF EXISTS `settings`;

CREATE TABLE `settings` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `name` varchar(30) NOT NULL,
  `grouping` varchar(30) default NULL,
  `label` varchar(30) default NULL,
  `description` text,
  `value` varchar(100) default NULL,
  `type` varchar(10) default 'text' COMMENT 'text, radio, checkbox, password...',
  `sort` int(11) default '-1' COMMENT '-1 = do not display setting on settings page',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

/*Data for the table `settings` */

insert  into `settings`(`name`,`grouping`,`label`,`description`,`value`,`type`,`sort`) values ('athome','Användargränssnitt',NULL,NULL,'1','radio',-1);
insert  into `settings`(`name`,`grouping`,`label`,`description`,`value`,`type`,`sort`) values ('latitude','Användargränssnitt','Latitude',NULL,'61.689872','text',109);
insert  into `settings`(`name`,`grouping`,`label`,`description`,`value`,`type`,`sort`) values ('longitude','Användargränssnitt','Longitude',NULL,'19.511719','text',108);
insert  into `settings`(`name`,`grouping`,`label`,`description`,`value`,`type`,`sort`) values ('version','System','Version',NULL,'0','text',-1);
insert  into `settings`(`name`,`grouping`,`label`,`description`,`value`,`type`,`sort`) values ('dimlevelstep','Telldus','Dimnivå stegstorlek (%)',NULL,'5','dropdown',303);
insert  into `settings`(`name`,`grouping`,`label`,`description`,`value`,`type`,`sort`) values ('alwaysuselastknownstatus','Användargränssnitt','Visa alltid senaste status',NULL,'1','radio',100);
insert  into `settings`(`name`,`grouping`,`label`,`description`,`value`,`type`,`sort`) values ('hoursstatusactive','Användargränssnitt','Timmar status aktivt',NULL,'1','text',101);
insert  into `settings`(`name`,`grouping`,`label`,`description`,`value`,`type`,`sort`) values ('houseplanwidth','Användargränssnitt','Planlösningsbredd (px)',NULL,'600','text',102);
insert  into `settings`(`name`,`grouping`,`label`,`description`,`value`,`type`,`sort`) values ('houseplaniconheight','Användargränssnitt','Planlösnings ikon höjd (px)',NULL,'30','text',103);
insert  into `settings`(`name`,`grouping`,`label`,`description`,`value`,`type`,`sort`) values ('useexternaltemperature','Användargränssnitt','Använd extern temperatur',NULL,'1','radio',105);
insert  into `settings`(`name`,`grouping`,`label`,`description`,`value`,`type`,`sort`) values ('externaltempurl','Användargränssnitt','Extern temperatur URL',NULL,'http://www.temperatur.nu/termo/globen/temp.txt','text',106);
insert  into `settings`(`name`,`grouping`,`label`,`description`,`value`,`type`,`sort`) values ('externaltemplocation','Användargränssnitt','Plats för extern temperatur',NULL,'Globen','text',107);
insert  into `settings`(`name`,`grouping`,`label`,`description`,`value`,`type`,`sort`) values ('debug','Användargränssnitt','Debug mode',NULL,'0','radio',113);
insert  into `settings`(`name`,`grouping`,`label`,`description`,`value`,`type`,`sort`) values ('timezone','Användargränssnitt','Tidzon',NULL,'0','dropdown',110);
insert  into `settings`(`name`,`grouping`,`label`,`description`,`value`,`type`,`sort`) values ('title','Användargränssnitt','Titel',NULL,'HomeAutomation','text',99);

/*Table structure for table `tasks` */

DROP TABLE IF EXISTS `tasks`;

CREATE TABLE `tasks` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `name` varchar(100) NOT NULL,
  `devices` varchar(100) NOT NULL COMMENT '-1 = all devices otherwise deviceids separated by semicolon',
  `days` varchar(20) default NULL COMMENT '-1 = all days otherwise days separated by semicolon (1=monday ... 7=sunday)',
  `time` time default NULL COMMENT 'format 12:00',
  `time_on` time default NULL COMMENT 'format 12:00',
  `time_off` time default NULL COMMENT 'format 12:00',
  `status` tinyint(4) default '0' COMMENT '0 = off, 1 = on',
  `active` tinyint(1) default '1' COMMENT '0 = disabled, 1 = enabled, 2 = home, 3 = away',
  `type` tinyint(4) default '0' COMMENT '0 = static, 1 = dynamic with sun, 2 = dynamic with temperature',
  `offset_sunrise` time default NULL COMMENT 'offset for sunrise in minutes',
  `offset_sunset` time default NULL COMMENT 'offset for sunset in minutes',
  `tempsensor` varchar(24) default '-1' COMMENT '0 = disabled, otherwise serial or external temperature in use',
  `timestamp` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

/*Table structure for table `userlevels` */

DROP TABLE IF EXISTS `userlevels`;

CREATE TABLE `userlevels` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `userlevel` int(10) unsigned NOT NULL,
  `description` varchar(50) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

/*Data for the table `userlevels` */

insert  into `userlevels`(`userlevel`,`description`) values (1,'demo');
insert  into `userlevels`(`userlevel`,`description`) values (2,'user');
insert  into `userlevels`(`userlevel`,`description`) values (3,'admin');

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(11) NOT NULL auto_increment,
  `username` varchar(20) collate latin1_general_ci NOT NULL,
  `password` char(32) collate latin1_general_ci default NULL,
  `firstname` varchar(50) collate latin1_general_ci default NULL,
  `lastname` varchar(50) collate latin1_general_ci default NULL,
  `userlevel` int(11) default '1',
  `editable` tinyint(1) default '2' COMMENT '0 = not editable, 1 = userlevel editable, 2 = fully editable',
  PRIMARY KEY  (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci ROW_FORMAT=DYNAMIC;

/*Data for the table `users` */

insert  into `users`(`username`,`password`,`firstname`,`lastname`,`userlevel`,`editable`) values ('system',NULL,'system',NULL,3,0);
insert  into `users`(`username`,`password`,`firstname`,`lastname`,`userlevel`,`editable`) values ('remote',NULL,'remote',NULL,3,0);
insert  into `users`(`username`,`password`,`firstname`,`lastname`,`userlevel`,`editable`) values ('local',NULL,'local',NULL,2,1);

CREATE TABLE IF NOT EXISTS`temps` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `sensor_serial` char(24) collate latin1_general_ci default NULL,
  `temp_c` float default NULL,
  `date` datetime default NULL,
  PRIMARY KEY  (`id`),
  KEY `date` (`date`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

/*Table structure for table `tempsensors` */

CREATE TABLE IF NOT EXISTS `tempsensors` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `serial` char(24) collate latin1_general_ci default NULL,
  `name` varchar(30) collate latin1_general_ci default NULL,
  `sensortype` int(10) unsigned NOT NULL default '0',
  `sort` int(10) unsigned default '0',
  `dateadded` timestamp NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
