/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

/*Table structure for table `devicegrouprelations` */

DROP TABLE IF EXISTS `devicegrouprelations`;

CREATE TABLE `devicegrouprelations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `groupid` int(10) unsigned NOT NULL,
  `deviceid` int(10) unsigned NOT NULL,
  `dateadded` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Table structure for table `devicegroups` */

DROP TABLE IF EXISTS `devicegroups`;

CREATE TABLE `devicegroups` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(30) DEFAULT NULL,
  `dateadded` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `sort` int(11) unsigned DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Table structure for table `devices` */

DROP TABLE IF EXISTS `devices`;

CREATE TABLE `devices` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `systempluginname` varchar(30) NOT NULL,
  `systemdeviceid` varchar(50) NOT NULL DEFAULT '-1',
  `description` varchar(50) NOT NULL,
  `active` tinyint(1) DEFAULT '0',
  `type` tinyint(3) unsigned DEFAULT '1',
  `rawdevice` tinyint(4) DEFAULT '0',
  `rawlearncmd` varchar(150) DEFAULT NULL,
  `rawoffcmd` varchar(150) DEFAULT NULL,
  `rawoncmd` varchar(150) DEFAULT NULL,
  `consumption` int(11) unsigned DEFAULT '0' COMMENT 'Power consumption in watt (W)',
  `date_added` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `last_state` tinyint(4) DEFAULT '0',
  `last_dimlevel` tinyint(4) DEFAULT NULL,
  `last_updated` timestamp NULL DEFAULT NULL,
  `sort` int(11) unsigned DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Table structure for table `devicetypes` */

DROP TABLE IF EXISTS `devicetypes`;

CREATE TABLE `devicetypes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(50) NOT NULL,
  `text` varchar(50) NOT NULL,
  `dateadded` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Table structure for table `events` */

DROP TABLE IF EXISTS `events`;

CREATE TABLE `events` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `device_id` int(10) unsigned NOT NULL,
  `status` tinyint(1) NOT NULL,
  `dimlevel` int(11) DEFAULT '0',
  `userid` int(11) NOT NULL,
  `ipaddress` varchar(50) DEFAULT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `timestamp` (`timestamp`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Table structure for table `gcalsettings` */

DROP TABLE IF EXISTS `gcalsettings`;

CREATE TABLE `gcalsettings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  `value` mediumtext,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

/*Table structure for table `houseplan` */

DROP TABLE IF EXISTS `houseplan`;

CREATE TABLE `houseplan` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `deviceid` int(11) DEFAULT '-1',
  `tempsensor` varchar(50) DEFAULT '-1',
  `groupid` varchar(20) DEFAULT '-1',
  `macroid` int(11) DEFAULT '-1',
  `xposition` int(10) unsigned DEFAULT '0',
  `yposition` int(10) unsigned DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Table structure for table `macrorelations` */

DROP TABLE IF EXISTS `macrorelations`;

CREATE TABLE `macrorelations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `macroid` int(10) unsigned NOT NULL,
  `deviceid` int(10) unsigned NOT NULL,
  `status` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Table structure for table `macros` */

DROP TABLE IF EXISTS `macros`;

CREATE TABLE `macros` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(30) DEFAULT NULL,
  `comment` varchar(200) DEFAULT NULL,
  `scenario` int(11) DEFAULT '-1',
  `dateadded` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `sort` int(11) unsigned DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Table structure for table `pages` */

DROP TABLE IF EXISTS `pages`;

CREATE TABLE `pages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parentid` int(10) DEFAULT '-1',
  `name` varchar(30) NOT NULL,
  `translation` varchar(20) NOT NULL,
  `modulegroup` varchar(30) DEFAULT NULL,
  `module` varchar(30) NOT NULL,
  `menuaction` varchar(20) DEFAULT NULL,
  `sort` int(11) DEFAULT '0',
  `userlevel` tinyint(4) DEFAULT '3',
  `isavailableformobile` tinyint(1) DEFAULT '0',
  `isavailablefordesktop` tinyint(1) DEFAULT '1',
  `dateadded` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Table structure for table `scenarios` */

DROP TABLE IF EXISTS `scenarios`;

CREATE TABLE `scenarios` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  `sort` int(11) unsigned DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

/*Table structure for table `scheduleactivations` */

DROP TABLE IF EXISTS `scheduleactivations`;

CREATE TABLE `scheduleactivations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `scheduleid` int(11) NOT NULL,
  `type` varchar(30) DEFAULT 'static',
  `params` varchar(200) DEFAULT NULL,
  `sort` int(10) unsigned DEFAULT NULL,
  `lastruntime` datetime DEFAULT NULL,
  `upcomingruntime` datetime DEFAULT NULL,
  `nextdaysruntime` datetime DEFAULT NULL,
  `linuxatid` int(10) unsigned DEFAULT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `triggered` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Table structure for table `schedules` */

DROP TABLE IF EXISTS `schedules`;

CREATE TABLE `schedules` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `comment` varchar(200) DEFAULT NULL,
  `devices` varchar(100) DEFAULT NULL COMMENT '-1 = all devices otherwise deviceids separated by semicolon',
  `devicegroups` varchar(100) DEFAULT NULL COMMENT 'groupids separated by semicolon',
  `macros` varchar(100) DEFAULT NULL COMMENT 'macroids separated by semicolon',
  `days` varchar(20) DEFAULT NULL COMMENT '-1 = all days otherwise days separated by semicolon (1=monday ... 7=sunday)',
  `scenario` int(11) DEFAULT '-100' COMMENT '-100 = all, 1..n = custom scenario',
  `enabled` tinyint(1) DEFAULT '1' COMMENT '0 = disabled, 1 = enabled',
  `dateadded` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `sort` int(11) unsigned DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Table structure for table `sensortypes` */

DROP TABLE IF EXISTS `sensortypes`;

CREATE TABLE `sensortypes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `description` varchar(50) DEFAULT NULL,
  `unit` varchar(10) DEFAULT NULL,
  `gaugetype` varchar(20) DEFAULT 'speedometer',
  `min` double DEFAULT NULL,
  `max` double DEFAULT NULL,
  `low` double DEFAULT NULL,
  `high` double DEFAULT NULL,
  `sort` int(11) unsigned DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Table structure for table `settings` */

DROP TABLE IF EXISTS `settings`;

CREATE TABLE `settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  `grouping` varchar(30) DEFAULT NULL,
  `label` varchar(30) DEFAULT NULL,
  `value` varchar(100) DEFAULT NULL,
  `type` varchar(10) DEFAULT 'text' COMMENT 'text, radio, checkbox, password...',
  `sort` int(11) DEFAULT '-1' COMMENT '-1 = do not display setting on settings page',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Table structure for table `systemplugins` */

DROP TABLE IF EXISTS `systemplugins`;

CREATE TABLE `systemplugins` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pluginname` varchar(30) NOT NULL,
  `version` varchar(10) DEFAULT NULL,
  `dateinstalled` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `lastupdated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `pluginname` (`pluginname`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Table structure for table `systempluginsettings` */

DROP TABLE IF EXISTS `systempluginsettings`;

CREATE TABLE `systempluginsettings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pluginname` varchar(30) NOT NULL,
  `name` varchar(30) NOT NULL,
  `value` varchar(4096) DEFAULT NULL,
  `type` varchar(20) NOT NULL DEFAULT 'text',
  `sort` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Table structure for table `systemschedules` */

DROP TABLE IF EXISTS `systemschedules`;

CREATE TABLE `systemschedules` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `enabled` tinyint(1) DEFAULT '1',
  `description` varchar(200) DEFAULT NULL,
  `executable` varchar(200) DEFAULT NULL,
  `arguments` varchar(200) DEFAULT NULL,
  `settings` varchar(50) DEFAULT NULL,
  `sort` int(11) DEFAULT '0',
  `datecreated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Table structure for table `temps` */

DROP TABLE IF EXISTS `temps`;

CREATE TABLE `temps` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sensor_serial` varchar(50) DEFAULT NULL,
  `temp_c` float DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `date` (`date`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Table structure for table `tempsensors` */

DROP TABLE IF EXISTS `tempsensors`;

CREATE TABLE `tempsensors` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `serial` varchar(50) DEFAULT NULL,
  `name` varchar(30) DEFAULT NULL,
  `sensortype` int(10) unsigned NOT NULL DEFAULT '1',
  `gaugetype` varchar(20) DEFAULT 'speedometer',
  `min` double DEFAULT NULL,
  `max` double DEFAULT NULL,
  `low` double DEFAULT NULL,
  `high` double DEFAULT NULL,
  `sort` int(10) unsigned DEFAULT '0',
  `dateadded` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `active` tinyint(1) DEFAULT 1,
  `last_reading` float,
  `last_reading_id` int(10) unsigned,
  `previous_reading` float,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Table structure for table `userlevelpermissions` */

DROP TABLE IF EXISTS `userlevelpermissions`;

CREATE TABLE `userlevelpermissions` (
  `pageid` int(11) DEFAULT NULL,
  `userlevelid` int(11) DEFAULT NULL,
  UNIQUE KEY `pageid` (`pageid`,`userlevelid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `userlevels` */

DROP TABLE IF EXISTS `userlevels`;

CREATE TABLE `userlevels` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userlevel` int(10) unsigned NOT NULL,
  `description` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Table structure for table `usermailnotification` */

DROP TABLE IF EXISTS `usermailnotification`;

CREATE TABLE `usermailnotification` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userid` int(11) NOT NULL,
  `passkey` varchar(64) NOT NULL,
  `enabled` tinyint(1) DEFAULT '0',
  `sent` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) NOT NULL,
  `password` char(32) DEFAULT NULL,
  `firstname` varchar(50) DEFAULT NULL,
  `lastname` varchar(50) DEFAULT NULL,
  `userlevel` int(11) DEFAULT '1',
  `email` varchar(50) DEFAULT NULL,
  `editable` tinyint(1) DEFAULT '2' COMMENT '0 = not editable, 1 = userlevel editable, 2 = fully editable',
  `salt` varchar(150) DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

DROP TABLE IF EXISTS `versionchecks`;
create table `versionchecks` (  `name` varchar (30) NOT NULL , `version` varchar (20) NOT NULL , `dismissed` tinyint DEFAULT '0', PRIMARY KEY (`name`));

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
