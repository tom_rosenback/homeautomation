v0.9 -> v1.0beta patches

ALTER TABLE tasks ADD `time_on` time default NULL COMMENT 'format 12:00' AFTER time;
ALTER TABLE tasks ADD `time_off` time default NULL COMMENT 'format 12:00' AFTER time_on;
ALTER TABLE tasks ADD `useextern` tinyint(1) default '0' COMMENT '0 = not using sun / temp control, 1 sun / temp control in use' AFTER active;
ALTER TABLE tasks ADD `offset_sunrise` int(11) default '0' COMMENT 'offset for sunrise in minutes' AFTER useextern;
ALTER TABLE tasks ADD `offset_sunset` int(11) default '0' COMMENT 'offset for sunset in minutes' AFTER offset_sunrise;
ALTER TABLE tasks ADD `templimit` int(11) default NULL COMMENT 'temperature limit' AFTER offset_sunset;

v0.8 -> v0.9 patches

ALTER TABLE devices ALTER telldus_id SET DEFAULT -1;

ALTER TABLE devices ADD `rawdevice` tinyint(4) default '0' AFTER type;
ALTER TABLE devices ADD `rawlearncmd` varchar(150) default NULL AFTER rawdevice;
ALTER TABLE devices ADD `rawoffcmd` varchar(150) default NULL AFTER rawlearncmd;
ALTER TABLE devices ADD `rawoncmd` varchar(150) default NULL AFTER rawoffcmd;

INSERT INTO `devicetypes`(`type`,`text`) VALUES ('absdimmer','dimmer (abs)');

ALTER TABLE events ADD `dimlevel` int(11) default '100' AFTER status;
