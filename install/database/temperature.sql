/*
SQLyog Enterprise - MySQL GUI v6.07
Host - 5.0.51b-community-nt : Database - homeautomation
*********************************************************************
Server version : 5.0.51b-community-nt
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

/*Table structure for table `temps` */

CREATE TABLE `temps` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `sensor_serial` char(24) collate latin1_general_ci default NULL,
  `temp_c` float default NULL,
  `date` datetime default NULL,
  PRIMARY KEY  (`id`),
  KEY `date` (`date`)
) ENGINE=MyISAM AUTO_INCREMENT=367593 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

/*Table structure for table `tempsensors` */

CREATE TABLE `tempsensors` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `serial` char(24) collate latin1_general_ci default NULL,
  `name` varchar(30) collate latin1_general_ci default NULL,
  `sensortype` int(10) unsigned NOT NULL default '0',
  `sort` int(10) unsigned default '0',
  `dateadded` timestamp NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
