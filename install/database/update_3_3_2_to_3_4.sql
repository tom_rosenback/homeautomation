alter table `sensortypes` change `unit` `unit` varchar (20)  NULL  COLLATE utf8_general_ci;
alter table `events` drop key `timestamp`;
alter table `events` add index `timestamp` (`id`, `timestamp`);

alter table systempluginsettings add column overrideable tinyint(1) default 0;

create table systempluginedevicesettings (
	id int(10) unsigned not null auto_increment,
	deviceid int(10) unsigned not null,
	settingsid int(10) unsigned not null,
	value varchar(4096),
	primary key(id)
);

alter table systemplugins add column plugintype varchar(16) default 'action' not null;

alter table tempsensors add column last_reading_date datetime;

alter table sensortypes add column graphtype varchar(20) default 'line';

CREATE TABLE devicenotificationplugins (
	id int(10) unsigned not null auto_increment,
	deviceid int(10) unsigned not null,
	systempluginid int(10) unsigned not null,
	primary key(id)
);

alter table schedules add column macros varchar(100) DEFAULT NULL COMMENT 'macroids separated by semicolon';
alter table versionchecks change `name` `name` varchar (30)  NOT NULL;
