/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

/*Data for the table `devicetypes` */

insert  into `devicetypes`(`type`,`text`) values ('light','light');
insert  into `devicetypes`(`type`,`text`) values ('dimmer','dimmer');
insert  into `devicetypes`(`type`,`text`) values ('sauna','sauna');
insert  into `devicetypes`(`type`,`text`) values ('absdimmer','dimmer (abs)');
insert  into `devicetypes`(`type`,`text`) values ('engineheater','engineheater');
insert  into `devicetypes`(`type`,`text`) values ('dummy','dummy');
insert  into `devicetypes`(`type`,`text`) values ('bell','bell');

/*Data for the table `gcalsettings` */

insert  into `gcalsettings`(`name`,`value`) values ('username',NULL);
insert  into `gcalsettings`(`name`,`value`) values ('password',NULL);
insert  into `gcalsettings`(`name`,`value`) values ('selectedcalendars',NULL);

/*Data for the table `pages` */

insert  into `pages`(`id`,`parentid`,`name`,`translation`,`modulegroup`,`module`,`menuaction`,`sort`,`userlevel`,`isavailableformobile`,`isavailablefordesktop`,`dateadded`) values (1,-1,'houseplan','LBL_HOUSEPLAN',NULL,'houseplan',NULL,1,1,1,1,'2012-11-28 18:35:36');
insert  into `pages`(`id`,`parentid`,`name`,`translation`,`modulegroup`,`module`,`menuaction`,`sort`,`userlevel`,`isavailableformobile`,`isavailablefordesktop`,`dateadded`) values (2,-1,'devices','LBL_DEVICES',NULL,'devices',NULL,2,1,1,1,'2012-11-28 18:35:37');
insert  into `pages`(`id`,`parentid`,`name`,`translation`,`modulegroup`,`module`,`menuaction`,`sort`,`userlevel`,`isavailableformobile`,`isavailablefordesktop`,`dateadded`) values (3,-1,'groups','LBL_GROUPS',NULL,'groups',NULL,3,1,1,1,'2012-11-28 18:35:37');
insert  into `pages`(`id`,`parentid`,`name`,`translation`,`modulegroup`,`module`,`menuaction`,`sort`,`userlevel`,`isavailableformobile`,`isavailablefordesktop`,`dateadded`) values (4,-1,'macros','LBL_MACROS',NULL,'macros',NULL,4,1,1,1,'2012-11-28 18:35:38');
insert  into `pages`(`id`,`parentid`,`name`,`translation`,`modulegroup`,`module`,`menuaction`,`sort`,`userlevel`,`isavailableformobile`,`isavailablefordesktop`,`dateadded`) values (5,-1,'scheduler','LBL_SCHEDULER',NULL,'scheduler',NULL,5,1,0,1,'2011-11-12 14:02:44');
insert  into `pages`(`id`,`parentid`,`name`,`translation`,`modulegroup`,`module`,`menuaction`,`sort`,`userlevel`,`isavailableformobile`,`isavailablefordesktop`,`dateadded`) values (6,-1,'logs','LBL_LOGS','logs','systemlog',NULL,6,1,1,1,'2012-11-28 18:35:41');
insert  into `pages`(`id`,`parentid`,`name`,`translation`,`modulegroup`,`module`,`menuaction`,`sort`,`userlevel`,`isavailableformobile`,`isavailablefordesktop`,`dateadded`) values (7,6,'logs-measurements','LBL_MEASUREMENTS','logs','measurements',NULL,1,1,1,1,'2012-11-28 19:15:21');
insert  into `pages`(`id`,`parentid`,`name`,`translation`,`modulegroup`,`module`,`menuaction`,`sort`,`userlevel`,`isavailableformobile`,`isavailablefordesktop`,`dateadded`) values (8,6,'logs-systemlog','LBL_LOGS','logs','systemlog',NULL,2,1,1,1,'2012-11-28 19:15:22');
insert  into `pages`(`id`,`parentid`,`name`,`translation`,`modulegroup`,`module`,`menuaction`,`sort`,`userlevel`,`isavailableformobile`,`isavailablefordesktop`,`dateadded`) values (9,6,'logs-consumption','LBL_POWERCONSUMPTION','logs','consumption',NULL,3,4,0,1,'2012-06-18 22:24:21');
insert  into `pages`(`id`,`parentid`,`name`,`translation`,`modulegroup`,`module`,`menuaction`,`sort`,`userlevel`,`isavailableformobile`,`isavailablefordesktop`,`dateadded`) values (10,-1,'configurator','LBL_CONFIGURATION','configurator','devices',NULL,7,3,1,1,'2012-11-28 21:04:27');
insert  into `pages`(`id`,`parentid`,`name`,`translation`,`modulegroup`,`module`,`menuaction`,`sort`,`userlevel`,`isavailableformobile`,`isavailablefordesktop`,`dateadded`) values (11,10,'conf-devices','LBL_DEVICES','configurator','devices',NULL,1,3,1,1,'2012-11-28 21:04:28');
insert  into `pages`(`id`,`parentid`,`name`,`translation`,`modulegroup`,`module`,`menuaction`,`sort`,`userlevel`,`isavailableformobile`,`isavailablefordesktop`,`dateadded`) values (12,10,'conf-devicegroups','LBL_DEVICEGROUPS','configurator','devicegroups',NULL,2,3,1,1,'2012-11-28 21:04:28');
insert  into `pages`(`id`,`parentid`,`name`,`translation`,`modulegroup`,`module`,`menuaction`,`sort`,`userlevel`,`isavailableformobile`,`isavailablefordesktop`,`dateadded`) values (13,10,'conf-houseplan','LBL_HOUSEPLAN','configurator','houseplan',NULL,3,3,1,1,'2012-11-28 21:04:28');
insert  into `pages`(`id`,`parentid`,`name`,`translation`,`modulegroup`,`module`,`menuaction`,`sort`,`userlevel`,`isavailableformobile`,`isavailablefordesktop`,`dateadded`) values (14,10,'conf-scenarios','LBL_SCENARIOS','configurator','scenarios',NULL,4,3,1,1,'2012-11-28 21:04:28');
insert  into `pages`(`id`,`parentid`,`name`,`translation`,`modulegroup`,`module`,`menuaction`,`sort`,`userlevel`,`isavailableformobile`,`isavailablefordesktop`,`dateadded`) values (16,10,'conf-usercontrol','LBL_USERMANAGEMENT','configurator','usercontrol',NULL,6,3,1,1,'2012-11-28 21:04:29');
insert  into `pages`(`id`,`parentid`,`name`,`translation`,`modulegroup`,`module`,`menuaction`,`sort`,`userlevel`,`isavailableformobile`,`isavailablefordesktop`,`dateadded`) values (17,10,'conf-sensors','LBL_SENSORS','configurator','sensors',NULL,7,3,1,1,'2012-11-28 21:04:29');
insert  into `pages`(`id`,`parentid`,`name`,`translation`,`modulegroup`,`module`,`menuaction`,`sort`,`userlevel`,`isavailableformobile`,`isavailablefordesktop`,`dateadded`) values (18,10,'conf-sensortypes','LBL_SENSORTYPES','configurator','sensortypes',NULL,8,3,1,1,'2012-11-28 21:04:29');
insert  into `pages`(`id`,`parentid`,`name`,`translation`,`modulegroup`,`module`,`menuaction`,`sort`,`userlevel`,`isavailableformobile`,`isavailablefordesktop`,`dateadded`) values (19,10,'conf-macros','LBL_MACROS','configurator','macros',NULL,9,3,1,1,'2012-11-28 21:04:29');
insert  into `pages`(`id`,`parentid`,`name`,`translation`,`modulegroup`,`module`,`menuaction`,`sort`,`userlevel`,`isavailableformobile`,`isavailablefordesktop`,`dateadded`) values (20,10,'conf-plugins','LBL_PLUGINS','configurator','plugins',NULL,10,4,1,1,'2012-11-28 21:04:30');
insert  into `pages`(`id`,`parentid`,`name`,`translation`,`modulegroup`,`module`,`menuaction`,`sort`,`userlevel`,`isavailableformobile`,`isavailablefordesktop`,`dateadded`) values (21,10,'conf-systemschedules','LBL_SYSTEMSCHEDULES','configurator','systemschedules',NULL,11,3,1,1,'2012-11-28 21:04:30');
/*iinsert  into `pages`(`id`,`parentid`,`name`,`translation`,`modulegroup`,`module`,`menuaction`,`sort`,`userlevel`,`isavailableformobile`,`isavailablefordesktop`,`dateadded`) values (22,10,'conf-pages','LBL_PAGES','configurator','pages',NULL,12,3,1,1,'2012-11-28 21:04:30');*/
insert  into `pages`(`id`,`parentid`,`name`,`translation`,`modulegroup`,`module`,`menuaction`,`sort`,`userlevel`,`isavailableformobile`,`isavailablefordesktop`,`dateadded`) values (23,10,'conf-settings','LBL_SETTINGS','configurator','settings',NULL,13,3,1,1,'2012-11-28 21:04:31');
insert  into `pages`(`id`,`parentid`,`name`,`translation`,`modulegroup`,`module`,`menuaction`,`sort`,`userlevel`,`isavailableformobile`,`isavailablefordesktop`,`dateadded`) values (24,-1,'help','LBL_HELP',NULL,'help',NULL,8,1,1,1,'2012-11-28 18:35:54');
insert  into `pages`(`id`,`parentid`,`name`,`translation`,`modulegroup`,`module`,`menuaction`,`sort`,`userlevel`,`isavailableformobile`,`isavailablefordesktop`,`dateadded`) values (25,24,'help-about','LBL_ABOUT',NULL,'about',NULL,1,1,1,1,'2012-11-28 18:35:55');
insert  into `pages`(`id`,`parentid`,`name`,`translation`,`modulegroup`,`module`,`menuaction`,`sort`,`userlevel`,`isavailableformobile`,`isavailablefordesktop`,`dateadded`) values (26,-1,'logout','LBL_LOGOUT',NULL,'login','logout',27,1,1,1,'2012-11-28 18:35:56');
insert  into `pages`(`id`,`parentid`,`name`,`translation`,`modulegroup`,`module`,`menuaction`,`sort`,`userlevel`,`isavailableformobile`,`isavailablefordesktop`,`dateadded`) values (27,10,'conf-systemplugins','LBL_SYSTEMPLUGINS','configurator','systemplugins',NULL,14,3,0,1,'2012-06-18 22:24:21');
insert into `pages` (`id`, `parentid`, `name`, `translation`, `modulegroup`, `module`, `menuaction`, `sort`, `userlevel`, `isavailableformobile`, `isavailablefordesktop`, `dateadded`) values('28','-1','custompages','LBL_CUSTOMPAGES',NULL,'custompages',NULL,'20','1','1','1','2014-02-20 21:45:55');

/*Data for the table `sensortypes` */

insert  into `sensortypes`(`description`,`unit`,`gaugetype`,`min`,`max`,`low`,`high`,`sort`) values ('Temperature','&deg;C','speedometer',0,100,10,50,0);
insert  into `sensortypes`(`description`,`unit`,`gaugetype`,`min`,`max`,`low`,`high`,`sort`) values ('Humidity','%','speedometer',0,100,10,50,0);
insert  into `sensortypes`(`description`,`unit`,`gaugetype`,`min`,`max`,`low`,`high`,`sort`) values ('Rainfall',' mm','speedometer',0,100,0,10,0);
insert  into `sensortypes`(`description`,`unit`,`gaugetype`,`min`,`max`,`low`,`high`,`sort`) values ('Speed',' m/s','speedometer',0,20,0,5,0);
insert  into `sensortypes`(`description`,`unit`,`gaugetype`,`min`,`max`,`low`,`high`,`sort`) values ('Bearing','&deg;','speedometer',0,360,0,0,0);
insert  into `sensortypes`(`description`,`unit`,`gaugetype`,`min`,`max`,`low`,`high`,`sort`) values ('Speed',' km/h','speedometer',0,100,0,15,0);
insert  into `sensortypes`(`description`,`unit`,`gaugetype`,`min`,`max`,`low`,`high`,`sort`) values ('Rainfall',' mm/h','speedometer',0,100,0,10,0);
insert  into `sensortypes`(`description`,`unit`,`gaugetype`,`min`,`max`,`low`,`high`,`sort`) values ('Pressure','mb','speedometer',0,1000,200,800,0);
insert  into `sensortypes`(`description`,`unit`,`gaugetype`,`min`,`max`,`low`,`high`,`sort`) values ('Digital','','speedometer',0,1,0,1,0);
insert  into `sensortypes`(`description`,`unit`,`gaugetype`,`min`,`max`,`low`,`high`,`sort`) values ('Ambient light','lux','speedometer',0,10000,0,200,0);
insert  into `sensortypes`(`description`,`unit`,`gaugetype`,`min`,`max`,`low`,`high`,`sort`) values ('Power','W','speedometer',0,1000,0,100,0);

/*Data for the table `settings` */

insert  into `settings`(`name`,`grouping`,`label`,`value`,`type`,`sort`) values ('selectedscenario','userinterface',NULL,'-100','text',-1);
insert  into `settings`(`name`,`grouping`,`label`,`value`,`type`,`sort`) values ('latitude','userinterface','latitude','61.689872','text',108);
insert  into `settings`(`name`,`grouping`,`label`,`value`,`type`,`sort`) values ('longitude','userinterface','longitude','19.511719','text',109);
insert  into `settings`(`name`,`grouping`,`label`,`value`,`type`,`sort`) values ('version','system','version','0','text',-1);
insert  into `settings`(`name`,`grouping`,`label`,`value`,`type`,`sort`) values ('dimlevelstep','dimlevels','dimlevelstep','5','dropdown',303);
insert  into `settings`(`name`,`grouping`,`label`,`value`,`type`,`sort`) values ('alwaysuselastknownstatus','userinterface','alwaysshowlaststatus','1','radio',100);
insert  into `settings`(`name`,`grouping`,`label`,`value`,`type`,`sort`) values ('hoursstatusactive','userinterface','hoursstatusactive','1','dropdown',101);
insert  into `settings`(`name`,`grouping`,`label`,`value`,`type`,`sort`) values ('houseplanwidth','userinterface','houseplanwidth','1000','dropdown',102);
insert  into `settings`(`name`,`grouping`,`label`,`value`,`type`,`sort`) values ('houseplaniconheight','userinterface','houseplaniconheight','30','dropdown',103);
insert  into `settings`(`name`,`grouping`,`label`,`value`,`type`,`sort`) values ('useexternaltemperature','userinterface','useexterntemperature','1','radio',105);
insert  into `settings`(`name`,`grouping`,`label`,`value`,`type`,`sort`) values ('externaltempurl','userinterface','externtemperatureurl','http://www.temperatur.nu/termo/abisko/temp.txt','text',106);
insert  into `settings`(`name`,`grouping`,`label`,`value`,`type`,`sort`) values ('externaltemplocation','userinterface','externtemperaturelocation','Abisko','text',107);
insert  into `settings`(`name`,`grouping`,`label`,`value`,`type`,`sort`) values ('debug','userinterface','debugmode','0','radio',118);
insert  into `settings`(`name`,`grouping`,`label`,`value`,`type`,`sort`) values ('timezone','userinterface','timezone','2','dropdown',110);
insert  into `settings`(`name`,`grouping`,`label`,`value`,`type`,`sort`) values ('title','userinterface','title','HomeAutomation 3.3','text',99);
insert  into `settings`(`name`,`grouping`,`label`,`value`,`type`,`sort`) values ('ibshowusername','infobox','showloggedinuser','1','radio',120);
insert  into `settings`(`name`,`grouping`,`label`,`value`,`type`,`sort`) values ('ibshowcurrenttime','infobox','showcurrenttime','1','radio',121);
insert  into `settings`(`name`,`grouping`,`label`,`value`,`type`,`sort`) values ('ibshowsun','infobox','showtimeforsun','1','radio',122);
insert  into `settings`(`name`,`grouping`,`label`,`value`,`type`,`sort`) values ('iblogevents','infobox','numberoflogevents','10','dropdown',123);
insert  into `settings`(`name`,`grouping`,`label`,`value`,`type`,`sort`) values ('ibupcomingevents','infobox','numberofcomingevents','10','dropdown',124);
insert  into `settings`(`name`,`grouping`,`label`,`value`,`type`,`sort`) values ('localip','system','localipaddresses','192.168.1.1/24','text',204);
insert  into `settings`(`name`,`grouping`,`label`,`value`,`type`,`sort`) values ('checkversionsenabled', 'system', 'checkversionsenabled', '1', 'radio', 205);
insert  into `settings`(`name`,`grouping`,`label`,`value`,`type`,`sort`) values ('defaultpage','userinterface','defaultpage','houseplan','dropdown',116);
insert  into `settings`(`name`,`grouping`,`label`,`value`,`type`,`sort`) values ('showinfobox','infobox','showinfobox','1','radio',119);
insert  into `settings`(`name`,`grouping`,`label`,`value`,`type`,`sort`) values ('defaultdimlevel','dimlevels','defaultdimlevel','75','dropdown',304);
insert  into `settings`(`name`,`grouping`,`label`,`value`,`type`,`sort`) values ('theme','userinterface','theme','default','dropdown',115);
insert  into `settings`(`name`,`grouping`,`label`,`value`,`type`,`sort`) values ('dateformat','userinterface','dateformat','j.n.Y','text',111);
insert  into `settings`(`name`,`grouping`,`label`,`value`,`type`,`sort`) values ('timeformat','userinterface','timeformat','H:i','text',112);
insert  into `settings`(`name`,`grouping`,`label`,`value`,`type`,`sort`) values ('iconortext','userinterface','iconortext','text','dropdown',99);
insert  into `settings`(`name`,`grouping`,`label`,`value`,`type`,`sort`) values ('ibsensor','infobox','usethissensor','109AD5230108002C','dropdown',122);

/*Data for the table `userlevels` */

insert  into `userlevels`(`userlevel`,`description`) values (1,'demo');
insert  into `userlevels`(`userlevel`,`description`) values (2,'user');
insert  into `userlevels`(`userlevel`,`description`) values (3,'admin');

/*Data for the table `users` */

insert  into `users`(`username`,`password`,`firstname`,`lastname`,`userlevel`,`email`,`editable`,`salt`) values ('system',NULL,'system',NULL,3,NULL,0,'');
insert  into `users`(`username`,`password`,`firstname`,`lastname`,`userlevel`,`email`,`editable`,`salt`) values ('remote',NULL,'remote',NULL,3,NULL,0,'');
insert  into `users`(`username`,`password`,`firstname`,`lastname`,`userlevel`,`email`,`editable`,`salt`) values ('local',NULL,'local',NULL,2,NULL,1,'HomeAutomation4f211500521bc5.051');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
