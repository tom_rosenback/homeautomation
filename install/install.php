<?php
// HomeAutomation
// Copyright (C) 2021 Tom Rosenback (tom.rosenback@gmail.com), Daniel Malmgren (daniel.malmgren@kolefors.se)
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

session_start();

include("../includes.php");

// always include english first for fallback, after that include client language.
include(HA_ROOT_PATH."/resources/languages/install_en.php");
// missing elements in client language will be in english now.
if($_SESSION[CFG_SESSION_KEY]["language"] != "en")
{
	include(HA_ROOT_PATH."/resources/languages/install_".$_SESSION[CFG_SESSION_KEY]["language"].".php");
}

defineConstants($installTranslation);

$installedVersion = Settings::get("version");

if($installedVersion !== false && getFormVariable("action", "") == "clean") {
	$installedVersion = 0;
	Settings::update("version", $installedVersion);
}

$maxSteps = 6;
$step = getFormVariable("step", "start");

$page = "<!DOCTYPE html>
		<html>
			<head>
				<meta name=\"author\" content=\"Tom Rosenback - Daniel Malmgren\" />
				<meta name=\"description\" content=\"HomeAutomation\" />
				<meta http-equiv=\"Content-type\" content=\"text/html; charset=UTF-8\" />
				<title>".LBL_HOMEAUTOMATIONINSTALLATION." v".THISVERSION."</title>
				<script language=\"javascript\" type=\"text/javascript\" src=\"../plugins/jquery/jquery.min.js\"></script>
				<script language=\"javascript\" type=\"text/javascript\" src=\"../scripts/ha.js\"></script>
				<script language=\"javascript\" type=\"text/javascript\" src=\"../scripts/common.js\"></script>
				<script language=\"javascript\" type=\"text/javascript\" src=\"../scripts/network.js\"></script>
				<script language=\"javascript\" type=\"text/javascript\" src=\"../scripts/install.js\"></script>
				<link href=\"../resources/themes/default.css\" rel=\"stylesheet\">
			</head>
			<body>
				<center>
					<form method=\"post\" action=\"".THISPAGE."\" id=\"installForm\">
						<table width=\"500px\">
							<tr>
								<td class=\"bigBold\" colspan=\"2\">
									".LBL_HOMEAUTOMATIONINSTALLATION." v".THISVERSION."<p>
									<input type=\"hidden\" name=\"step\" id=\"step\" value=\"".($step + 1)."\">
								</td>
							</tr>";
							
							if(file_exists(HA_ROOT_PATH."/install/steps/".$step.".php")) {
								include(HA_ROOT_PATH."/install/steps/".$step.".php");
							}
			
$page .= "			</table>
				</center>
			</body>
		</html>";

echo $page;

// echo "<pre style=\"text-align: left;\">";
// print_r($_POST);
// print_r($_GET);
// print_r($_COOKIE);
// print_r($_SESSION[CFG_SESSION_KEY]);
// print_r($_SERVER);
// print_r($ha);
// print_r(get_defined_constants());
// echo "</pre>";

?>

