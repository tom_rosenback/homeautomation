<?php
	$licenseFile = HA_ROOT_PATH."/license.txt";
	$licenseAgreement = LBL_LICENSEFILEMISSING;
	$notAllowed = "notAllowed";
	
	if (file_exists($licenseFile)) {
		$licenseAgreement = htmlentities(file_get_contents($licenseFile, false));
		$notAllowed = "";
	}
	
	$page .= "<tr>
				<td colspan=\"2\" class=\"bigBold\">
					".LBL_INSTALLDESC."
				</td>
			</tr>
			<tr>
				<td colspan=\"2\">
					".LBL_LICENSE."
				</td>
			</tr>					
			<tr>
				<td colspan=\"2\">
					<textarea style=\"width: 100%; height: 200px;\">".$licenseAgreement."</textarea>
					<br />
					<label><input type=\"checkbox\" id=\"licenseagreementapproved\">".LBL_APPROVELICENSEAGREEMENT."</label>
				</td>
			</tr>
			<tr>
				<td colspan=\"2\" align=\"right\">
					<input type=\"submit\" value=\"".LBL_STARTINSTALLATION."\" id=\"next".$notAllowed."\" disabled=\"disabled\" class=\"button\">
				</td>
			</tr>";
?>