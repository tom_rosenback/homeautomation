﻿<?php
	if(!Users::authenticate(getFormVariable("username", ""), getFormVariable("password", "")) && count(Users::get(getFormVariable("username", ""))) == 0) {
		$params = array(
			new dbParam(dbParamType::$STRING, "username", $_POST["username"]),
			new dbParam(dbParamType::$STRING, "firstname", $_POST["firstname"]),
			new dbParam(dbParamType::$STRING, "lastname", $_POST["lastname"]),
			new dbParam(dbParamType::$INTEGER, "userlevel", 3)
		);

		if(getFormVariable("password", "") != "") {
			$params[] = new dbParam(dbParamType::$STRING, "password", md5(getFormVariable("password", "")));
		}
		
		Users::save($params);		
	}

	registerSession(getFormVariable("username", ""));
				
	$systemPlugins = SystemPlugins::load();
	foreach($systemPlugins as $plugin) {
		$plugin->importDevices();
	}

	$devices = Devices::get();
	$deviceTypes = Devices::getTypes();

	$page .= "<tr>
				<td class=\"bigBold\" colspan=\"2\">
					".LBL_STEP." ".$step." ".mb_convert_case(LBL_OF, MB_CASE_LOWER, "UTF-8")." ".$maxSteps.": ".LBL_CONFIGUREDEVICES."<p>
				</td>
			</tr>
			<tr>
				<td colspan=\"2\">
					".LBL_INSTALLDESC5."
				<td>
			</tr>
			<tr>
				<td colspan=\"2\">
					<br />
					<table>
						<tr>
							<td colspan=\"3\">&nbsp;</td>
							<!--<td>".LBL_POWERCONSUMPTION."</td>-->";
							
	if(count($devices) > 0)							
	{
		foreach($devices as $device) {
			$page .= "<tr>
						<td>
							".$device["description"]."<input type=\"hidden\" name=\"device[]\" value=\"".$device["id"]."\" >
						</td>
						<td>
							".generateSelector($device["typeid"], "devicetype[]", "text2", "id", "text", $deviceTypes, -1, LBL_CHOOSETYPE)."
						</td>
						<!--<td>
							<input type=\"text\" name=\"consumption[]\" class=\"text4\" value=\"".$device["consumption"]."\" />
						</td>-->
					</tr>";
		}
	}
	else
	{
		$page .= "<tr>
						<td colspan=\"3\">".LBL_NODEVICESFOUND."</td>
					</tr>";					
	}
			
	$page .= "		</table>
				</td>
			</tr>
			<tr>
				<td colspan=\"2\">
					&nbsp;
				</td>
			</tr>
			<tr>
				<td colspan=\"2\" align=\"right\">
					<input type=\"submit\" value=\"".LBL_NEXTSTEP."\" name=\"next\" id=\"next\" class=\"button\">
				</td>
			</tr>";
?>
