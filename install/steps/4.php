<?php
	saveSettings($_POST);
	$_SESSION[CFG_SESSION_KEY]["settings"] = Settings::get("", true);
	
	// checking if admin users exist in system => user has created own admin users already, allow login instead of create new user
	if(count(Users::get("", 3)) >= 1) {
		$loginBtn = "<input type=\"button\" value=\"".LBL_LOGIN."\" name=\"checkLogin\" id=\"checkLogin\" class=\"button\">&nbsp;&nbsp;";
		$nextBtnDisabled = " disabled=\"disabled\"";
	}
	
	$page .= "<tr>
				<td class=\"bigBold\" colspan=\"2\">
					".LBL_STEP." ".$step." ".mb_convert_case(LBL_OF, MB_CASE_LOWER, "UTF-8")." ".$maxSteps.": ".LBL_ADMINUSER."<p>
				</td>
			</tr>
			<tr>
				<td colspan=\"2\">";
	
	if($installedVersion == "" || $installedVersion < 1) {
		$page .= LBL_INSTALLDESC4;
	}
	else
	{
		$page .= LBL_UPDATEDESC4;
	}

	$page .= "	<td>
			</tr>
			<tr>
				<td colspan=\"2\">
					&nbsp;
				</td>
			</tr>";
	if(count(Users::get("", 3)) < 1) {
		$page .= "	<tr>
				<td>
					".LBL_FIRSTNAME.":
				</td>
				<td>
					<input type=\"text\" name=\"firstname\" id=\"firstname\" class=\"text2\" />
				</td>
			</tr>
			<tr>
				<td>
					".LBL_LASTNAME.":
				</td>
				<td>
					<input type=\"text\" name=\"lastname\" id=\"lastname\" class=\"text2\" />
				</td>
			</tr>";
	}
	$page .= "	<tr>
				<td>
					".LBL_USERNAME.":
				</td>
				<td>
					<input type=\"text\" name=\"username\" id=\"username\" class=\"text2\" class=\"required\" required />
				</td>
			</tr>
			<tr>
				<td>
					".LBL_PASSWORD.":
				</td>
				<td>
					<input type=\"password\" name=\"password\" id=\"password\" class=\"text2\" value=\"\" class=\"required\" required />
				</td>
			</tr>
			<tr>
				<td colspan=\"2\">
					&nbsp;
				</td>
			</tr>
			<tr>
				<td colspan=\"2\" align=\"right\">
					".$loginBtn."
					<input type=\"submit\" value=\"".LBL_NEXTSTEP."\" name=\"next\" id=\"next\"".$nextBtnDisabled." class=\"button\">
				</td>
			</tr>
			<tr>
				<td colspan=\"2\" align=\"right\">
					<div id=\"success\" style=\"font-weight: bold; display: none;\">
						".LBL_LOGINOK."
					</div>
					<div id=\"fail\" style=\"font-weight: bold; display: none;\">
						".LBL_LOGINFAIL."
					</div>
					<div id=\"wait\" style=\"font-weight: bold; display: none;\">
						".LBL_WAIT."
					</div>
					<div id=\"validationfail\" style=\"font-weight: bold; display: none;\">
						".LBL_CHECKEMPTYFIELDS."
					</div>
				</td>
			</tr>";
?>
