<?php
	$preRequisities = checkPreRequisities();		
	
	$page .= "<tr>
				<td class=\"bigBold\" colspan=\"2\">
					".LBL_STEP." ".$step." ".mb_convert_case(LBL_OF, MB_CASE_LOWER, "UTF-8")." ".$maxSteps.": ".LBL_PREREQUISITIES."<p>
				<td>
			</tr>
			<tr>
				<td colspan=\"2\" style=\"text-align: justify;\">
					".LBL_INSTALLDESC1."
				</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td align=\"center\">".LBL_STATUS."</td>
			</tr>";
						
	foreach($preRequisities as $check) {
		$page .= "	<tr>
						<td>".$check["text"]."</td>
						<td align=\"center\"><img src=\"../".getStatusImage("light", $check["status"])."\" height=\"25px\" /></td>
					</tr>";
	}
						
	$page .= "<tr>
				<td colspan=\"2\">&nbsp;</td>
			</tr>
			<tr>
				<td colspan=\"2\">".LBL_MANUALCHECKS."</td>
			</tr>
			<tr>
				<td colspan=\"2\">&nbsp;</td>
			</tr>
			<tr>
				<td colspan=\"2\" align=\"right\">
					<input type=\"submit\" value=\"".LBL_RELOAD."\" id=\"reload\" class=\"button\" onclick=\"document.getElementById('step').value = (document.getElementById('step').value - 1);\" />&nbsp;<input type=\"submit\" value=\"".LBL_NEXTSTEP."\" id=\"next\" class=\"button\" />
				</td>
			</tr>
			<tr>
				<td colspan=\"2\" align=\"center\">
					<br /><br />
					<b>".LBL_STATUSDESCRIPTION."</b>
					<table>
						<tr>
							<td><img src=\"../".getStatusImage("light", true)."\" height=\"25px\" /></td>
							<td>".LBL_OK."</td>
						</tr>
						<tr>
							<td><img src=\"../".getStatusImage("light", false)."\" height=\"25px\" /></td>
							<td>".LBL_NOTRECOMMENDED."</td>
						</tr>
						<tr>
							<td><img src=\"../".getStatusImage("light", -1)."\" height=\"25px\" /></td>
							<td>".LBL_WASNOTABLETOCHECK."</td>
						</tr>
					</table>
				</td>
			</tr>";
?>