<?php
	// copy defaults configuration to root folder and name it config.php
	if (!file_exists(HA_ROOT_PATH."/config.php")) {
		copy(HA_ROOT_PATH."/system/defaults.php", HA_ROOT_PATH."/config.php");
		// echo "Failed to copy defaults config file";
	}
	
	$mysqlHost = "localhost";
	$mysqlDatabase = "homeautomation";
	$mysqlUsername = "";
	$mysqlPassword = "";
	
	if(file_exists(HA_ROOT_PATH."/system/mysql_settings.php")) {
		include(HA_ROOT_PATH."/system/mysql_settings.php");
	}
	
	$page .= "<tr>
				<td class=\"bigBold\" colspan=\"2\">
					".LBL_STEP." ".$step." ".mb_convert_case(LBL_OF, MB_CASE_LOWER, "UTF-8")." ".$maxSteps.": ".LBL_MYSQLSETTINGS."<p>
				<td>
			</tr>
			<tr>
				<td colspan=\"2\">";
				
	if($installedVersion == "" || $installedVersion < 1) {
		$page .= LBL_INSTALLDESC2;
		$confirmMsg = LBL_CONFIRMCLEANINSTALL;
	}
	else
	{
		$page .= LBL_UPDATEDESC2;
		$confirmMsg = LBL_CONFIRMDATABASEUPDATE;
	}
	
	$page .= "	</td>
			</tr>
			<tr>
				<td colspan=\"2\">
					&nbsp;
				</td>
			</tr>
			<tr>
				<td>
					".LBL_HOST.":
				</td>
				<td>
					<input type=\"text\" name=\"host\" id=\"host\" class=\"text3 required\" value=\"".$mysqlHost."\" required />
				</td>
			</tr>
			<tr>
				<td>
					".LBL_DATABASENAME.":
				</td>
				<td>
					<input type=\"text\" name=\"database\" id=\"database\" class=\"text3 required\" value=\"".$mysqlDatabase."\" required />
				</td>
			</tr>
			<tr>
				<td>
					".LBL_USERNAME.":
				</td>
				<td>
					<input type=\"text\" name=\"username\" id=\"username\" class=\"text3 required\" value=\"".$mysqlUsername."\" required />
				</td>
			</tr>
			<tr>
				<td>
					".LBL_PASSWORD.":
				</td>
				<td>
					<input type=\"password\" name=\"password\" id=\"password\" class=\"text3\" value=\"".$mysqlPassword."\" />
				</td>
			</tr>
			<tr>
				<td colspan=\"2\">
					&nbsp;
				</td>
			</tr>
			<tr>
				<td colspan=\"2\" align=\"right\">
					<input type=\"button\" id=\"checkMysqlConnection\" value=\"".LBL_TESTMYSQLCONNECTION."\" class=\"button\">&nbsp;&nbsp;&nbsp;
					<input type=\"submit\" value=\"".LBL_NEXTSTEP."\" name=\"next\" id=\"next\" disabled=\"disabled\" onclick=\"return confirm('".$confirmMsg."');\" class=\"button\">
				</td>
			</tr>
			<tr>
				<td colspan=\"2\">
					&nbsp;
				</td>
			</tr>
			<tr>
				<td colspan=\"2\" align=\"right\">
					<div id=\"success\" style=\"font-weight: bold; display: none;\">
						".LBL_MYSQLSETTINGSOK."
					</div>
					<div id=\"fail\" style=\"font-weight: bold; display: none;\">
						".LBL_MYSQLSETTINGSFAIL."
					</div>
					<div id=\"wait\" style=\"font-weight: bold; display: none;\">
						".LBL_WAIT."
					</div>
					<div id=\"validationfail\" style=\"font-weight: bold; display: none;\">
						".LBL_CHECKEMPTYFIELDS."
					</div>
				</td>
			</tr>";
?>