<?php
	saveSettingsFile(getFormVariable("host", ""), getFormVariable("database", ""), getFormVariable("username", ""), getFormVariable("password", ""));

	global $db;
	$db->setCredentials(getFormVariable("host", ""), getFormVariable("username", ""), getFormVariable("password", ""), getFormVariable("database", ""));

	$result = installDB($installedVersion);

	$defaultPlugins = array("tdtool", "sendmail", "dummy");

	$availablePlugins = SystemPlugins::load("", true);

	foreach($availablePlugins as $plugin) {
		$pluginName = $plugin->getPluginName();

		if($plugin->installedVersion > -1 || in_array($pluginName, $defaultPlugins)) {
			installSystemPlugin($pluginName);
			Versions::updateChecks($pluginName, $plugin->getVersion());
		}
	}

	$page .= "<tr>
				<td class=\"bigBold\" colspan=\"2\">
					".LBL_STEP." ".$step." ".mb_convert_case(LBL_OF, MB_CASE_LOWER, "UTF-8")." ".$maxSteps.": ".LBL_SETTINGS."<p>
				<td>
			</tr>
			<tr>
				<td colspan=\"2\">";

	if($installedVersion == "" || $installedVersion < 1) {
		$page .= LBL_INSTALLDESC3;
	}
	else
	{
		$page .= LBL_UPDATEDESC3;
	}

	$page .= "	<td>
			</tr>
				".generateSettingsList()."
			<tr>
				<td colspan=\"2\">
					&nbsp;
				</td>
			</tr>
			<tr>
				<td colspan=\"2\" align=\"right\">
					<input type=\"submit\" value=\"".LBL_NEXTSTEP."\" name=\"next\" id=\"next\" class=\"button\">
				</td>
			</tr>";
?>
