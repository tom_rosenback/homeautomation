<?php
	$i = 0;

	if(count(getFormVariable("device", array())) > 0) {
		$devicetypes = getFormVariable("devicetype");
		$consumptions = getFormVariable("consumption");
		
		foreach(getFormVariable("device") as $device) {
			$currDeviceType = $devicetypes[$i];
			$currConsumption = $consumptions[$i];
			
			if($currConsumption == "" || $currConsumption < 0) {
				$currConsumption = 0;
			}
			
			if($currDeviceType != "-1") {					
				$params = array(
					new dbParam(dbParamType::$INTEGER, "type", $currDeviceType),
					new dbParam(dbParamType::$INTEGER, "consumption", $currConsumption)
				);

				$device = Devices::save($params, $device);
			}
			
			$i++;
		}
	}

	Settings::update("version", THISVERSION);
	Versions::updateChecks("ha", THISVERSION);
	Versions::dismiss("ha");

	$page .= "<tr>
				<td class=\"bigBold\" colspan=\"2\">
					".LBL_STEP." ".$step." ".mb_convert_case(LBL_OF, MB_CASE_LOWER, "UTF-8")." ".$maxSteps.": ".LBL_FINALIZING."<p>
				</td>
			</tr>
			<tr>
				<td colspan=\"2\">
					".LBL_INSTALLDESC6."
				</td>
			</tr>
			<tr>
				<td colspan=\"2\">
					&nbsp;
				</td>
			</tr>
			<tr>
				<td colspan=\"2\" align=\"right\">
					<input type=\"button\" value=\"".LBL_FINISHINSTALLATION."\" name=\"finish\" id=\"finish\" class=\"button\">
				</td>
			</tr>
			<tr>
				<td colspan=\"2\">
					&nbsp;
				</td>
			</tr>
			<tr>
				<td colspan=\"2\" align=\"right\">
					<div id=\"fail\" style=\"font-weight: bold; display: none;\">
						".LBL_DELETIONOFINSTALLFOLDERFAILED."
					</div>
					<div id=\"wait\" style=\"font-weight: bold; display: none;\">
						".LBL_WAIT."
					</div>
				</td>
			</tr>
			<tr>
				<td colspan=\"2\" align=\"justify\">
					".LBL_OPTIONALINFOSHARINGDESC."
				</td>
			</tr>";
?>