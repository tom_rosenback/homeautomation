<?php

$translation["about"] ="&Uuml;ber";
$translation["active"] ="Aktiv";
$translation["activefor"] ="Aktiv f&uuml;r";
$translation["activeon"] ="Aktiv auf";
$translation["adminuser"] ="Admin-Benutzer";
$translation["advanceonbeforesunrise"] ="Offset vor Sonnenuntergang";
$translation["all"] ="Alle";
$translation["alldays"] ="Alle Tage";
$translation["alldevices"] ="Alle Ger&auml;te";
$translation["allmissing"] ="Alle fehlenden";
$translation["always"] ="Immer";
$translation["athome"] ="Zu Hause";
$translation["cancel"] ="Abbrechen";
$translation["checkemptyfields"] ="&Uuml;berpr&uuml;fen Sie leere Felder";
$translation["choosetype"] ="W&auml;hlen Sie den Typ";
$translation["clearlog"] ="Log leeren";
$translation["clickhere"] ="Hier klicken";
$translation["clicktodelete"] ="Klicken Sie zum L&ouml;schen";
$translation["confirmcleaninstall"] ="Die Datenbank ist nun geleert und neu. OK";
$translation["confirmclearlog"] ="Sind Sie sicher, dass Sie das Log l&ouml;schen wollen?";
$translation["confirmdatabaseupdate"] ="Die Datenbank wird nun aktualisiert, OK?";
$translation["confirmdelete"] ="Sind Sie sicher, dass Sie l&ouml;schen m&ouml;chten";
$translation["configuration"] ="Konfiguration";
$translation["configurehouseplan"] ="Grundriss konfigurieren";
$translation["configuredevices"] ="Ger&auml;t konfigurieren";
$translation["createnewschedule"] ="Neuen Zeitplan erstellen";
$translation["createnewscenario"] ="Neues Szenario";
$translation["createnewuser"] ="Einen neuen Benutzer anlegen";
$translation["currenttime"] ="Aktuelle Zeit";
$translation["currentuser"] ="Aktueller Benutzer:";
$translation["databasename"] ="Name der Datenbank";
$translation["date"] ="Datum";
$translation["days"] ="Tage";
$translation["delayaftersunrise"] ="Verz&ouml;gerung nach Sonnenaufgang";
$translation["delete"] ="L&ouml;schen";
$translation["deleteall"] ="Alle l&ouml;schen";
$translation["deletedevicefromhouseplan"] ="Ger&auml;te vom Grundriss l&ouml;schen";
$translation["deletealldevicesfromhouseplan"] ="Alle Ger&auml;te vom Grundriss l&ouml;schen";
$translation["deleteinstallfolder"] ="Installationsordner l&ouml;schen";
$translation["deletionofinstallfolderfailed"] ="L&ouml;schen Installationsordner fehlgeschlagen";
$translation["deletionofinstallfolderok"] ="Installationsordner gel&ouml;scht";
$translation["demouser"] ="Demo User";
$translation["demouserip"] ="123.456.768.9";
$translation["description"] ="Beschreibung";
$translation["device"] ="Ger&auml;t";
$translation["devicemanagement"] ="Ger&auml;te verwalten";
$translation["devices"] ="Ger&auml;te";
$translation["devicetype"] ="Ger&auml;tetyp";
$translation["edit"] ="&Auml;ndern";
$translation["eventname"] ="Ereignis";
$translation["events"] ="Ereignisse";
$translation["exttemp"] ="Ext. Temp.";
$translation["finalizing"] ="Abschluss";
$translation["finishinstallation"] ="Installation beenden";
$translation["firstname"] ="Vorname";
$translation["friday"] ="Freitag";
$translation["help"] ="Hilfe";
$translation["homeautomationinstallation"] ="HomeAutomation Installation";
$translation["host"] ="Server-Adresse";
$translation["houseplan"] ="Grundriss";
$translation["houseplanconfigurationdescription"] ="W&auml;hlen Sie das Ger&auml;t und die Position, indem Sie auf das Bild klicken. Klicken auf ein Ger&auml;t auf dem Bild, entfernt dieses";
$translation["ifinstallcorrupted"] ="Entweder war die Installation fehlerhaft oder Sie wollen einfach nur eine <b> Neuinstallation durchf&uuml;hren </ b>";
$translation["image"] ="Bild";
$translation["installfolderexists"] ="<b> Das Installationsverzeichnis existiert noch, entfernen Sie diese bitte zur Inbetriebnahme des Systems. Den Installationsordner nicht zu l&ouml;schen, ist ein grosses Sicherheitsrisiko<b>.";
$translation["ip"] ="IP";
$translation["lastactivities"] ="Die letzten Ereignisse";
$translation["lastknownstatus"] ="Letzte bekannte Status";
$translation["lastname"] ="Name";
$translation["emailaddress"] ="E-Mail Adresse";
$translation["latestlogevents"] ="Aktuell im Log";
$translation["learn"] ="Lernen";
$translation["log"] ="Log";
$translation["login"] ="Anmelden";
$translation["loginfail"] ="&Uuml;berpr&uuml;fen Sie Benutzername / Passwort";
$translation["loginok"] ="Benutzername und Passwort ok";
$translation["logout"] ="Abmelden";
$translation["missing"] ="Fehlend";
$translation["missinginstallfolder"] ="Fehlende Installationsordner, f&uuml;hren Sie bitte eine Neuinstallation der HomeAutomation durch.";
$translation["monday"] ="Montag";
$translation["mysqlsettings"] ="MySQL-Einstellungen";
$translation["mysqlsettingsfail"] ="Es konnte keine Verbindung mit der Datenbank hergestellt werden, bitte &uuml;berpr&uuml;fen Sie die Einstellungen.";
$translation["mysqlsettingsok"] ="MySQL-Einstellungen ok";
$translation["name"] ="Name";
$translation["never"] ="Nie";
$translation["newrawdevice"] ="Neues RAW-Ger&auml;t";
$translation["nextevent"] ="N&auml;chstes Ereignis";
$translation["nextstep"] ="N&auml;chste Schritte";
$translation["no"] ="Nein";
$translation["nodatabaseconnection"] ="Keine Datenbankverbindung";
$translation["noschedulescreated"] ="Keine Termine angelegt.";
$translation["noupcomingevents"] ="Keine anstehenden Termine";
$translation["of"] ="OF";
$translation["off"] ="Aus";
$translation["ok"] ="Ok";
$translation["on"] ="An";
$translation["pagegeneratedin"] ="Seite generiert in";
$translation["password"] ="Kennwort";
$translation["rawcommands"] ="RAW-Befehle";
$translation["rawdevice"] ="RAW-Ger&auml;t";
$translation["resetfilter"] ="Filter zur&uuml;cksetzen";
$translation["runnow"] ="Jetzt ausf&uuml;hren";
$translation["saturday"] ="Samstag";
$translation["save"] ="Speichern";
$translation["scenario"] ="Szenario:";
$translation["scenarios"] ="Szenarios";
$translation["scheduler"] ="Zeitplan";
$translation["schedules"] ="Bestehende Termine";
$translation["scheduletype"] ="Zeitplan-Typ:";
$translation["seconds"] ="Sekunden";
$translation["selectdevice"] ="Ger&auml;t w&auml;hlen";
$translation["selecttempsensor"] ="Temperatursensor w&auml;hlen";
$translation["selectuserlevel"] ="W&auml;hlen Sie Benutzer-Ebene";
$translation["settings"] ="Einstellungen";
$translation["show"] ="Zeigen";
$translation["donotshow"] ="Nicht zeigen";
$translation["showall"] ="Alle anzeigen";
$translation["showing"] ="Anzeigen";
$translation["showingallactivities"] ="Alle Aktivit&auml;ten anzeigen";
$translation["startinstallation"] ="Starten Sie die Installation";
$translation["static"] ="Statisch";
$translation["status"] ="Status";
$translation["step"] ="Schritt";
$translation["suncontrolled"] ="Helligkeitsgesteuert";
$translation["sunday"] ="Sonntag";
$translation["sunrisetoday"] ="Sonnenaufgang heute";
$translation["sunsettoday"] ="Sonnenuntergang heute";
$translation["teach"] ="Lerne";
$translation["telldusdevicesupdated"] ="Telldus Ger&auml;te aktualisiert";
$translation["temperaturecontrolled"] ="Temperaturgef&uuml;hrte";
$translation["temperatures"] ="Die Temperaturen";
$translation["tempsensor"] ="Temperatursensor";
$translation["testmysqlconnection"] ="Test-Mysql-Verbindung";
$translation["testpermissions"] ="Test-Berechtigungen";
$translation["thedatabase"] ="Die Datenbank";
$translation["thursday"] ="Donnerstag";
$translation["time"] ="Zeit";
$translation["timeforactivityoff"] ="Zeit Aktivit&auml;t zu deaktivieren";
$translation["timeforactivityon"] ="Zeit Aktivit&auml;t zu aktivieren";
$translation["timefordepature"] ="Zeit f&uuml;r die Abfahrt";
$translation["toggle"] ="Toggle";
$translation["tuesday"] ="Dienstag";
$translation["turnoff"] ="Deaktivieren";
$translation["turnoffall"] ="Alle Deaktivieren";
$translation["turnon"] ="Aktivieren";
$translation["turnonall"] ="Alle Aktivieren";
$translation["upcomingevents"] ="Kommende Ereignisse";
$translation["update"] ="Aktualisieren";
$translation["updatefromsystemplugins"] ="Aktualisierung durch sie Systemerweiterungen";
$translation["upload"] ="Hochladen";
$translation["uploadhouseplantoconfigure"] ="Laden Sie einen Grundriss hoch, um diesen zu konfigurieren";
$translation["uploadnewhouseplan"] ="Laden Sie einen neuen Grundriss hoch";
$translation["user"] ="Benutzer";
$translation["userlevel"] ="Benutzerebene";
$translation["usermanagement"] ="Benutzerverwaltung";
$translation["username"] ="Benutzername";
$translation["wait"] ="Warte...!";
$translation["waitingfordongletobefree"] ="Warten auf Freigabe des Dongles";
$translation["wednesday"] ="Mittwoch";
$translation["weekdays"] ="Werktage";
$translation["weekends"] ="Wochenenden";
$translation["writepermissionsfail"] ="PHP hat keine Schreibrechte in dem Verzeichnis HomeAutomation, bitte &uuml;berpr&uuml;fen Sie die Berechtigungen.";
$translation["writepermissionsok"] ="Lese / Schreibe Berechtigungen ok";
$translation["yes"] ="Ja";
$translation["icons"] ="Icons";
$translation["id"] ="Id";


$translation["settings_groups"]["userinterface"] ="Userinterface";
$translation["settings_groups"]["system"] ="System";
$translation["settings_groups"]["dimlevels"] ="Dimlevels";
$translation["settings_groups"]["infobox"] ="Infobox";

$translation["settings_labels"]["theme"] ="Themen";
$translation["settings_labels"]["showloggedinuser"] ="Angemeldete Benutzer anzeigen";
$translation["settings_labels"]["showcurrenttime"] ="Aktuelle Uhrzeit anzeigen";
$translation["settings_labels"]["showtimeforsun"] ="Uhzreit Sonnenaufgang/Sonnenuntergang anzeigen";
$translation["settings_labels"]["usethissensor"] ="Benutze diesen Sensor";
$translation["settings_labels"]["numberoflogevents"] ="Anzahl der Events im Log";
$translation["settings_labels"]["numberofcomingevents"] ="Anzahl der kommenden Events";
$translation["settings_labels"]["latitude"] ="Breitengrad";
$translation["settings_labels"]["longitude"] ="L&auml;ngengrad";
$translation["settings_labels"]["version"] ="Version";
$translation["settings_labels"]["dimlevelstep"] ="Dimmlevel Schrittweite (%)";
$translation["settings_labels"]["alwaysshowlaststatus"] ="Immer letzten Status anzeigen";
$translation["settings_labels"]["hoursstatusactive"] ="Stunden Status aktiv";
$translation["settings_labels"]["houseplanwidth"] ="Grundriss Breite";
$translation["settings_labels"]["houseplaniconheight"] ="Grundriss Hohe";
$translation["settings_labels"]["useonewire"] ="Benutzer 1-Wire Temperatur";
$translation["settings_labels"]["useexterntemperature"] ="Benutze externe Temperatur";
$translation["settings_labels"]["externtemperatureurl"] ="URL externe Temperatur";
$translation["settings_labels"]["externtemperaturelocation"] ="Lokation externe Temperatur";
$translation["settings_labels"]["debugmode"] ="Debugmodus";
$translation["settings_labels"]["timezone"] ="Zeitzone";
$translation["settings_labels"]["title"] ="Titel";
$translation["settings_labels"]["username"] ="Benutzername";
$translation["settings_labels"]["password"] ="Passwort";
$translation["settings_labels"]["phppath"] ="PHP- Binary Pfad";
$translation["settings_labels"]["jtpath"] ="JT- Binary Pfad";
$translation["settings_labels"]["tdtoolpath"] ="tdtool- Binary Pfad";
$translation["settings_labels"]["localipaddresses"] ="Lokale IPs";
$translation["settings_labels"]["defaultpage"] ="Default Seite";
$translation["settings_labels"]["defaultdimlevel"] ="Default Dimlevel";
$translation["settings_labels"]["showinfobox"] ="Zeige Infobox";
$translation["settings_labels"]["timeformat"] ="Zeitformat";
$translation["settings_labels"]["dateformat"] ="Datumsformat";

$translation["reseticons"] ="Reset Icons";
$translation["uploadnewicons"] ="Neues Symbol hochladen:";
$translation["serial"] ="Seriennummer:";
$translation["sort"] ="Sortieren";
$translation["updatesensors"] ="Update der Messger&auml;te";
$translation["nosensorsavailable"] ="Keine Messger&auml;te verf&uuml;gbar";

$translation["gcal"] ="Google Kalender";
$translation["gpassword"] ="Passwort f&uuml;r Google";
$translation["gusername"] ="Benutzername f&uuml;r Google";

$translation["warningisnotdirectory"] ="'{d}' ist kein Verzeichnis, &uuml;berpr&uuml;fen Sie bitte die Einstellungen ({s}). <br />";
$translation["warningfilenotfound"] ="Konnte nicht gefunden werden '{f}' im Verzeichnis '{d}', &uuml;berpr&uuml;fen Sie bitte die Einstellungen ({s}). <br />";

$translation["missinghouseplanimage"] ="Der Grundriss fehlt, bitte &uuml;berpr&uuml;fen Sie Ihre Einstellungen.";

$translation["gotodesktopversion"] ="Desktop-Version";
$translation["gotomobileversion"] ="Mobile Version";

$translation["comment"] ="Kommentar";
$translation["activations"] ="Aktivierungen";
$translation["activationssuffix"] ="Aktivierungen";
$translation["addactivation"] ="Aktivierung hinzuf&uuml;gen";
$translation["at"] ="Um";
$translation["offset"] ="Offset";
$translation["activationtoday"] ="Die Aktivierung heute";
$translation["dusk"] ="Abendd&auml;mmerung";
$translation["sunrise"] ="Sonnenaufgang";
$translation["dawn"] ="Morgend&auml;mmerung";
$translation["sunset"] ="Sonnenuntergang";
$translation["engineheater"] ="Engineheater";
$translation["random"] ="Zuf&auml;llig";
$translation["dynamic"] ="Dynamisch";
$translation["earliest"] ="&Auml;lteste";
$translation["latest"] ="Neueste";
$translation["selectoperator"] ="W&auml;hle Bedingung";
$translation["smallerthan"] ="Kleiner als";
$translation["smallerthanorequal"] ="Kleiner oder gleich";
$translation["equal"] ="Gleich";
$translation["largerthanorequal"] ="Gr&oumlr oder gleich";
$translation["largerthan"] ="Gr&ouml;&szlig;er als";

$translation["devicegroups"] ="Ger&auml;tegruppen";
$translation["group"] ="Gruppe";
$translation["createnewgroup"] ="Neue Gruppe erstellen";
$translation["enabled"] ="Aktiviert";
$translation["groups"] ="Gruppen";
$translation["confirmstartcleaninstall"] ="Die Installation von einer sauberen Installation wird nun gestartet werden. Wollen Sie fortfahren?";
$translation["activedevices"] ="Aktive Ger&auml;te";
$translation["activedays"] ="Aktive Tage";
$translation["activegroups"] ="Aktive Gruppen";
$translation["showhide"] ="Anzeigen / ausblenden";
$translation["devicesgroups"] ="Ger&auml;te / Gruppen";
$translation["nogroupsconfigured"] ="Es wurden keine Gruppen konfiguriert";



$translation["help_about"] = "HomeAutomation v{VERSION}, released {DATE}<br><br>
Homepage: <a href=\"https://rosenback.me/ha\" target=\"_blank\">https://rosenback.me/ha</a><br>
Developed by: Tom Rosenback and Daniel Malmgren<br><br>
Licensed under GNU GPL";

$translation["help_text"] = "Help documentation available at <a href=\"https://rosenback.me/ha\" target=\"_blank\">https://rosenback.me/ha</a>";
$translation["settings_labels"]["numsignalrepetitions"] ="Anzahl der Signalwiederholungen";

$translation["macros"] ="Makros";
$translation["macro"] ="Makro";
$translation["createnewmacro"] ="Neues Makro erstellen";
$translation["nomacrosconfigured"] ="Es wurden keine Makros konfiguriert";
$translation["nochange"] ="Keine &Auml;nderung";
$translation["macrodescription"] ="Das Ausf&uuml;hren dieses Makro wirkt entsprechend der Konfiguration unten.";
$translation["icon"] ="Icon";
$translation["reseticon"] ="Icon zur&uuml;cksetzen";
$translation["uploadnewicon"] ="Neues Symbol hochladen:";
$translation["enable"] ="Aktivieren";
$translation["disable"] ="Deaktivieren";
$translation["disabled"] ="Abgeschaltet";
$translation["menu"] ="Menu";
$translation["tomorrow"] ="morgen";
$translation["schedulesmigrated"] ="{numschedules} schedule(s) migrated";
$translation["houseplanmigrated"] ="Grundriss migriert";
$translation["schedulesrebuilt"] ="Zeitplan neu erstellt";
$translation["reload"] ="Neu laden";
$translation["rebuildschedules"] ="Rebuild Termine";
$translation["confirmrebuildschedules"] ="Sind Sie sicher, da&szlig; Sie die Termine neu aufbauen wollen";
$translation["imported"] ="Importiert";
$translation["enterschedulename"] ="Bitte geben Sie einen Namen f&uuml;r den Zeitplan.";
$translation["onedaymustbeselected"] ="Es mu&szlig; mindestens ein Tag gew&auml;hlt werden";
$translation["correcterrorstocontinue"] ="Bitte Fehler korrigieren, bevor Sie fortfahren.";
$translation["activationsmustbeconfigured"] ="Keine Aktivierung konfiguriert, Sie m&uuml;&szlig;en mindestens eine Aktivierung konfiguriern.";
$translation["incorrecttimeformat"] ="Falsche Zeit-Format, hh: mm erlaubt, Offsets erlauben auch negative (-)-Werte.";
$translation["deviceorgroupmustbeselected"] ="Es mu&szlig; mindestens ein Ger&auml;t, oder eine Gruppe ausgew&auml;hlt";

$translation["settings_labels"]["dynamiccheckinterval"] ="Interval zwischen den dynamischen Aktivit&auml;ten";
$translation["hours"] ="Stunden";
$translation["minutes"] ="Minuten";
$translation["settings_groups"]["scheduler"] ="Zeitplan";

$translation["incorrectcredentials"] ="Benutzername oder Pa&szlig;wort falsch";
$translation["usernameandpasswordcannotbeempty"] ="Benutzername und / oder Pa&szlig;wort darf nicht leer sein";

$translation["powerconsumption"] ="Stromverbrauch";

$translation["currentconsumption"] ="Aktueller Stromverbrauch";
$translation["totalconsumption"] ="Gesamtverbrauch";
$translation["since"] ="Seit";
$translation["logs"] ="Protokolle";
$translation["measurements"] ="Messungen";
$translation["sensors"] ="Sensoren";
$translation["sensortypes"] ="Sensortypen";
$translation["unit"] ="Einheit";
$translation["createnewsensortype"] ="Neue Sensor-Typ erstellen";
$translation["sensorname"] ="Sensor Name";
$translation["uniqueid"] ="Eindeutige ID";
$translation["type"] ="Typ";
$translation["calculated"] ="Berechnet";
$translation["latestreading"] ="Letzte Ablesung";
$translation["currentvalues"] ="Aktuelle Werte";
$translation["now"] ="Jetzt";
$translation["30minago"] ="vor 30 min.";
$translation["trend"] ="Trend";
$translation["1yearago"] ="Vor einem Jahr";
$translation["2yearsago"] ="Vor 2 Jahren";
$translation["3yearsago"] ="Vor 3 Jahren";
$translation["average"] ="Durchschnitt";
$translation["hours"] ="Stunden";
$translation["days"] ="Tage";
$translation["years"] ="Jahre";
$translation["overall"] ="Gesamt";
$translation["saunausage"] ="Sauna Benutzung";
$translation["usageatatime"] ="Einmalig";
$translation["usageweekly"] ="W&ouml;chentlich";
$translation["usagemonthly"] ="Monatlich";
$translation["inaweek"] ="Woche";
$translation["inamonth"] ="Monat";
$translation["database"] ="Datenbank";
$translation["firstreading"] ="Erste Ablesung";
$translation["datapoints"] ="Daten-Punkte";
$translation["falling"] ="Steigend";
$translation["rising"] ="Fallend";
$translation["neutral"] ="Gleichbleibend";
$translation["periodlast"] ="Letzte";
$translation["lastupdated"] ="letztes Update vor {time} Minuten";
$translation["lastupdated_single"] ="letztes Update vor {time} Minute";

$translation["defaultimage"] ="Standard- Bild";
$translation["confirmresetsorting"] ="Sind Sie sicher, dass sie die Sortierung nach Namen zur&uuml;cksetzen wollen?";
$translation["resetsorting"] ="Sortierung Zur&uuml;cksetzen";

$translation["settings_labels"]["iconortext"] ="Symbole oder Text im Men&uuml;";
$translation["icon"] ="Symbole";
$translation["text"] ="Text";
$translation["iconandtext"] ="Symbole + Text";
$translation["beginning"] ="am Anfang";
$translation["end"] ="am Ende";
$translation["keyword"] ="eine Kalenderereignis dass das Wort enth&auml;lt";

$translation["nocalendarsavailable"] ="Keine Kalander verf&uuml;gbar";
$translation["availablecalendars"] ="Verf&uuml;gbare Kalender";
$translation["googlecouldnotfetchcalendars"] ="Konnte den Kalender nicht holen";
$translation["googlecouldnotfetchevents"] ="K&ouml;nnten die Kalenderereignisse nicht zu holen";
$translation["googleconnectionproblems"] ="Konnte keine Verbindung zu Google herstellen";
$translation["loading"] ="Lade ...";
$translation["loginfirst"] ="Erstes Login";

$translation["rememberme"] ="Angemeldet bleiben";

$translation["systemschedules"] ="Systemplanung";
$translation["path"] ="Pfad";
$translation["executable"] ="Programm";
$translation["period"] ="Periode";
$translation["nosystemschedulesconfigured"] ="Kein Systemzeitplan konfiguriert";
$translation["createnewsystemschedule"] ="Neun Systemzeitplan erstellen";
$translation["dateandtime"] ="Datum und Zeit";

$translation["plugins"] ="Erweiterungen";
$translation["pages"] ="Seiten";
$translation["page"] ="Seite";

$translation["daily"] ="T&auml;glich";
$translation["weekly"] ="W&ouml;chentlich";
$translation["monthly"] ="Monatlich";

$translation["allmonths"] ="Alle Monate";
$translation["allmonthdays"] ="Alle Tage im Monat";

$translation["interval"] ="Intervall";
$translation["chooseinterval"] ="Intervall w&auml;hlen";
$translation["choosetime"] ="Zeit w&auml;hlen";
$translation["hour"] ="Stunde";
$translation["minute"] ="Minute";

$translation["january"] ="Januar";
$translation["february"] ="Februar";
$translation["march"] ="M&auml;rz";
$translation["april"] ="April";
$translation["may"] ="May";
$translation["june"] ="June";
$translation["july"] ="Juli";
$translation["august"] ="August";
$translation["september"] ="September";
$translation["october"] ="Oktober";
$translation["november"] ="November";
$translation["december"] ="December";

$translation["every"] ="Jeder";
$translation["and"] ="und";

$translation["arguments"] ="Argumente";

$translation["system"] ="System";
$translation["choosesystem"] ="System ausw&auml;hlen";
$translation["systemplugins"] ="System Erweiterungen";
$translation["systemplugin"] ="System Erweiterung";
$translation["systempluginsettings"] ="System Erweiterungs Einstellungen";
$translation["author"] ="Autor";
$translation["installedversion"] ="Installierte Version";
$translation["availableversion"] ="Verf&uuml;gbare Version";
$translation["editsettings"] ="Einstellungen bearbeiten";
$translation["uninstall"] ="Deinstallieren";
$translation["install"] ="Installieren";
$translation["upgrade"] ="Aktualisieren";
$translation["remove"] ="Entfernen";
$translation["confirminstall"] ="Sind Sie sicher, das Sie installieren m&ouml;chten";
$translation["confirmupgrade"] ="Sind Sie sicher, dass Sie aktualisieren m&ouml;chten";
$translation["confirmuninstall"] ="Sind Sie sicher, dass Sie deinstallieren m&ouml;chten";
$translation["confirmremove"] ="Sind Sie sicher, das Sie entfernen m&ouml;chten";
$translation["notinstalled"] ="Nicht installiert";

$translation["exporttoxml"] ="Export nach XML";
$translation["file"] ="Datei";
$translation["installpluginfromxml"] ="Install Plugin von XML";
$translation["confirminstallpluginfromxml"] ="Sind Sie sicher, dass Sie aus XML-Erweiterung installieren m&ouml;chten (Das wird alte Dateien zu &uuml;berschreiben. ) ?";

$translation["light"] ="Licht";
$translation["dimmer"] ="Dimmer";
$translation["sauna"] ="Sauna";
$translation["absdimmer"] ="Dimmer (abs )";
$translation["dummy"] ="Dummy";
$translation["bell"] ="Klingel";

$translation["saveascopy"] ="Speichern als Kopie";
$translation["copy"] ="Kopieren";
$translation["copyof"] ="Kopie von";

$translation["gaugetype"] ="Messger&auml;t Typ";
$translation["speedometer"] ="Geschwindigkeitsmesser";

$translation["min"] ="Min";
$translation["max"] ="Max";
$translation["low"] ="Low";
$translation["high"] ="Hoch";
$translation["lowlimit"] ="Untergrenze";
$translation["highlimit"] ="Obergrenze";

$translation["info"] ="Info";
$translation["close"] ="Schliessen";

$translation["nodevicesconfigured"] ="Keine Ger&auml;te konfiguriert";
$translation["nodevicesfound"] ="Keine Ger&auml;te gefunden , sie m&uuml;ssen m&ouml;glicherweise die Einstellungen f&uuml;r die von Ihnen verwendete Erweiterung &auml;ndern.";

$translation["settings_labels"]["checkversionsenabled"] ="Auf neue Versionen pr&uuml;fen";
$translation["somenewversionsavailable"] ="Neues System Version(en) gefunden, <a href=\"https://rosenback.me/ha/\"target=\"_blank\"> hier </ a> herunterladen.";

$translation["custompages"] ="Benutzerdefinierte Seiten";

$translation["saved_successfully"] = "Saved successfully";
$translation["failed_saving"] = "Failed saving";

?>
