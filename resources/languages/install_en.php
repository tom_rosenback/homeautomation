<?php

$installTranslation["installdesc1"] = "It is recommended that all the prerequisite checks are in 'Ok' status to proceed.";
$installTranslation["installdesc2"] = "Enter your settings for the connection to the Mysql database connection, change the default values as needed.<p>
					The database which you enter must exists, create it manually before proceeding.<p>
					After verifying the settings you can continue.";
$installTranslation["installdesc3"] = "Change the default values that aren't correct for your installation.";
$installTranslation["installdesc4"] = "Create an admin user to make it possible to log in to HomeAutomation. This user can be edited later on.";
$installTranslation["installdesc5"] = "Configure your devices to make them usable in HomeAutomation.";
$installTranslation["installdesc6"] = "You are now ready to start using HomeAutomation, click 'Finish installation' to step in.";

$installTranslation["updatedesc1"] = "Your existing system will now be <b>UPDATED</b>. You are advised to take a backup of existing data in the database!";
$installTranslation["updatedesc2"] = "Check your saved Mysql database settings.<p>After verifying you can proceed and the database will be automatically updated.";
$installTranslation["updatedesc3"] = "Change settings that aren't correct for your installation.";
$installTranslation["updatedesc4"] = "Login using an existing user with admin permissions. If there is currently no admin user in the system you can create one here if you fill in all the fields. This user can be edited later.";

$installTranslation["installdesc"] = "Welcome to the installation of HomeAutomation.";
$installTranslation["license"] = "Read and approve the license agreement to proceed.";
$installTranslation["approvelicenseagreement"] = "Approve license agreement";
$installTranslation["prerequisities"] = "Prerequisites";
$installTranslation["installedversion"] = "Installed version";
$installTranslation["recommendedversion"] = "Recommended version";
$installTranslation["youhave"] = "you have";
$installTranslation["manualchecks"] = "PHP needs to be executable from the command line as the webuser for HomeAutomation to work properly. If you don't know how to check this please refer to the FAQ section of the users manual or on the <a href=\"https://rosenback.me/ha\" target=\"_blank\">HomeAutomation homepage</a>";
$installTranslation["statusdescription"] = "Status description";
$installTranslation["notrecommended"] = "Not recommended";
$installTranslation["wasnotabletocheck"] = "Was not able to check";
$installTranslation["optionalinfosharingdesc"] = "The developers of HomeAutomation would love to hear about your success stories, please use the Guestbook on <a href=\"https://rosenback.me/ha\" target=\"_blank\">HomeAutomation homepage</a> to share your experience with other users.";
$installTranslation["settingnotsetinphpini"] = "Setting '{param}' not set in php.ini, please correct.";
$installTranslation["settingsetinphpini"] = "Setting '{param}' is set to '{config}' in php.ini.";
$installTranslation["phpextensionnotloaded"] = "PHP extension '{ext}' is not loaded";
$installTranslation["phpextensionloaded"] = "PHP extension '{ext}' is loaded";
$installTranslation["readwritepermissions"] = "Read/write permissions";
$installTranslation["folderiswritable"] = "'{folder}' is writable.";
$installTranslation["folderisnotwritable"] = "'{folder}' is not writable, please correct.";
 
?>
