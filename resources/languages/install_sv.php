
<?php

$installTranslation["installdesc1"] = "Det är rekommenderat att alla förhandskrav är i status 'Ok' för att fortsätta.";
$installTranslation["installdesc2"] = "Mata in dina inställningar för Mysql-databas-anslutningen, ändra defaultvärden om de inte passar din installation.<p>
					Databasen du anger måste finnas, skapa den manuellt före du fortsätter.<p>
					När du har verifierat inställningarna kan du gå vidare.";
$installTranslation["installdesc3"] = "Ändra defaultvärden om de inte passar din installation.";
$installTranslation["installdesc4"] = "Skapa en användare som har adminrättigheter så du kan logga in i HomeAutomation. Denna användare kan editeras senare.";
$installTranslation["installdesc5"] = "Konfigurera dina enheter så att du kan använda dem i HomeAutomation.";
$installTranslation["installdesc6"] = "Du är nu redo att börja använda HomeAutomation, klicka 'Avsluta installation' för att stiga in.";

$installTranslation["updatedesc1"] = "Ditt existerande system kommer nu att bli <b>UPPDATERAT</b>. Ta för säkerhets skull vara på det data som redan finns i databasen. Om något går fel vid installation är du tvungen att göra en <b>CLEAN INSTALL</b>";
$installTranslation["updatedesc2"] = "Kolla så att de sparade Mysql-databas-inställningarna är korrekta.<p>När du har verifierat inställningarna kan du gå vidare och då uppdateras databasen automatiskt.";
$installTranslation["updatedesc3"] = "Ändra defaultvärden (kolla även de gamla inställningarna) om de inte passar din installation.";
$installTranslation["updatedesc4"] = "Logga in med en befintligt användare som har ADMIN-rättigheter i HomeAutomation. Om du inte har en ADMIN-användare redan i HomeAutomation så kan du skapa en här, fyll då i alla fälten. Denna användare kan editeras senare.";

$installTranslation["installdesc"] = "Välkommen till installationen av HomeAutomation.";
$installTranslation["license"] = "Läs igenom licensavtalet och godkänn för att fortsätta.";
$installTranslation["approvelicenseagreement"] = "Jag godkänner licensavtalet";
$installTranslation["prerequisities"] = "Förhandskrav";
$installTranslation["installedversion"] = "Installerad version";
$installTranslation["recommendedversion"] = "Rekommenderad version";
$installTranslation["youhave"] = "du har";
$installTranslation["manualchecks"] = "PHP måste vara körbart från kommandoprompten som webuser för att HomeAutomation ska fungera korrekt. Om du inte vet hur du kollar detta läs FAQ sektionen i användar manualen eller på <a href=\"https://rosenback.me/ha\" target=\"_blank\">HomeAutomation hemsidan</a>.";
$installTranslation["statusdescription"] = "Status beskrivning";
$installTranslation["notrecommended"] = "Inte rekommenderat";
$installTranslation["wasnotabletocheck"] = "Kunde inte kolla";
$installTranslation["optionalinfosharingdesc"] = "Utvecklarna av HomeAutomation skulle gärna höra om Er framgång. Använd Gästboken på <a href=\"https://rosenback.me/ha\" target=\"_blank\">HomeAutomation hemsidan</a> för att dela med dig av Dina erfarenheter med andra användare.";
$installTranslation["settingnotsetinphpini"] = "Inställningen '{param}' är inte satt i php.ini, var god korrigera.";
$installTranslation["settingsetinphpini"] = "Inställningen '{param}' är satt till '{config}' i php.ini.";
$installTranslation["phpextensionnotloaded"] = "PHP-tillägget '{ext}' är inte laddat";
$installTranslation["phpextensionloaded"] = "PHP-tillägget '{ext}' är laddat";
$installTranslation["readwritepermissions"] = "Skriv/läs-rättigheter";
$installTranslation["folderiswritable"] = "'{folder}' är skrivbar.";
$installTranslation["folderisnotwritable"] = "'{folder}' är inte skrivbar, var god korrigera.";

?>
