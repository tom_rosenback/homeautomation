<?php

$installTranslation["installdesc1"] = "On suositeltavaa, että kaikki esivaatimukset ovat tilassa 'Ok', ennen kuin asennusta jatketaan";
$installTranslation["installdesc2"] = "Syötä Mysql-tietokannan yhteystiedot. Muuta oletusarvot, jos ne ei sovi asennukseesi.<p>
					Syöttämäsi tietokannan pitää olla olemassa ennen kuin jatkat. Luo sen manuaalisesti ennen kuin jatkat asennusta.<p>
					Kun olet tarkistanut asetukset, voit jatkaa seuraavalle sivulle.";
$installTranslation["installdesc3"] = "Muuta oletusarvot, jos ne ei sovi asennukseesi.";
$installTranslation["installdesc4"] = "Luo käyttäjä, jolla on admin-oikeudet niin, että voit kirjautua sisään HomeAutomationiin. Voit muokata tätä käyttäjää myöhemmin.";
$installTranslation["installdesc5"] = "Määrittele yksiköt käyttääksesi niitä HomeAutomationissa.";
$installTranslation["installdesc6"] = "Olet nyt valmis käyttämään HomeAutomationia. Klikkaa 'Päätä asennus' astuaksesi sisään.";

$installTranslation["updatedesc1"] = "Nykyinen järjestelmäsi <b>PÄIVITETÄÄN</b>. Ole hyvä ja ota varmuuskopio tietokannasta!";
$installTranslation["updatedesc2"] = "Tarkista tallennetut Mysql-tietokanta-asetukset.<p>Kun olet tarkistanut asetukset, voit jatkaa, ja tietokanta päivitetään automaattisesti.";
$installTranslation["updatedesc3"] = "Muuta oletusarvot (tarkista myös vanhat asetukset) niin, että ne sopivat sinun asennukseesi.";
$installTranslation["updatedesc4"] = "Kirjaudu sisään olemassa olevalla käyttäjällä, jolla on ADMIN-oikeudet järjestelmään. Jos sinulla ei ole ADMIN-käyttäjää järjestelmässä, voit luoda sen täällä täyttämällä kaikki kentät. Voit muokata tätä käyttäjää myöhemmin.";

$installTranslation["installdesc"] = "Tervetuloa HomeAutomationin asennukseen.";
$installTranslation["license"] = "Lue ja hyväksy lisenssisopimus jatkaaksesi.";
$installTranslation["approvelicenseagreement"] = "Hyväksy lisenssisopimus";
$installTranslation["prerequisities"] = "Esivaatimukset";
$installTranslation["installedversion"] = "Asennettu versio";
$installTranslation["recommendedversion"] = "Suositeltu versio";
$installTranslation["youhave"] = "sinulla on";
$installTranslation["manualchecks"] = "PHP:n ja tdtool:n täytyy olla suoritettavia komentotulkista web-käyttäjänä, jotta HomeAutomation toimisi oikein. Jos et tiedä, miten tarkistat tämän, ole hyvä ja tarkista käyttäjämanuaalin FAQ-osiosta tai <a href=\"https://rosenback.me/ha\" target=\"_blank\">HomeAutomationin kotisivuilta</a>";
$installTranslation["statusdescription"] = "Tilan selitys";
$installTranslation["notrecommended"] = "Ei suositeltavaa";
$installTranslation["wasnotabletocheck"] = "Ei pystytty tarkistamaan";
$installTranslation["optionalinfosharingdesc"] = "HomeAutomationin kehittäjät haluaisivat kuulla teidän menestystarinoitanne, ole hyvä ja käytä vieraskirjaa <a href=\"https://rosenback.me/ha\" target=\"_blank\">HomeAutomation kotisivuilla</a> jakaaksesi kokemuksesi muiden käyttäjien kesken.";
$installTranslation["settingnotsetinphpini"] = "Asetusta '{param}' ei ole asetettu php.ini:ssä, ole hyvä ja korjaa.";
$installTranslation["settingsetinphpini"] = "Asetus '{param}' on asetettu '{config}':ksi php.ini:ssä.";
$installTranslation["phpextensionnotloaded"] = "PHP-moduulia '{ext}' ei ole ladattu";
$installTranslation["phpextensionloaded"] = "PHP-moduuli '{ext}' on ladattu";
$installTranslation["readwritepermissions"] = "Luku- ja kirjoitusoikeudet";
$installTranslation["folderiswritable"] = "Kansioon '{folder}' voi kirjoittaa.";
$installTranslation["folderisnotwritable"] = "Kansioon '{folder}' ei voi kirjoittaa, ole hyvä ja korjaa.";

?>