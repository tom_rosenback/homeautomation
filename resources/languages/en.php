<?php

$translation["about"] = "About";
$translation["active"] = "Active";
$translation["activefor"] = "Active for";
$translation["activeon"] = "Active on";
$translation["adminuser"] = "Admin user";
$translation["advanceonbeforesunrise"] = "Offset before sunset";
$translation["all"] = "All";
$translation["alldays"] = "All days";
$translation["alldevices"] = "All devices";
$translation["allmissing"] = "All missing";
$translation["always"] = "Always";
$translation["athome"] = "At home";
$translation["cancel"] = "Cancel";
$translation["checkemptyfields"] = "Check empty fields";
$translation["choosetype"] = "Choose type";
$translation["clearlog"] = "Clear log";
$translation["clickhere"] = "Click here";
$translation["clicktodelete"] = "Click to delete";
$translation["confirmcleaninstall"] = "The database is now emptied and recreated. OK?";
$translation["confirmclearlog"] = "Are you sure you want to clear the log?";
$translation["confirmdatabaseupdate"] = "The database is now updated. Please make sure you have got a fresh database backup. OK?";
$translation["confirmdelete"] = "Are you sure that you want to delete";
$translation["configuration"] = "Configuration";
$translation["configurehouseplan"] = "Configure house plan";
$translation["configuredevices"] = "Configure devices";
$translation["createnewschedule"] = "Create new schedule";
$translation["createnewscenario"] = "Create new scenario";
$translation["createnewuser"] = "Create new user";
$translation["currenttime"] = "Time";
$translation["currentuser"] = "User";
$translation["databasename"] = "Database name";
$translation["date"] = "Date";
$translation["days"] = "Days";
$translation["delayaftersunrise"] = "Delay after sunrise";
$translation["delete"] = "Delete";
$translation["deleteall"] = "Delete all";
$translation["deletedevicefromhouseplan"] = "Delete device from house plan?";
$translation["deletealldevicesfromhouseplan"] = "Delete all devices from house plan?";
$translation["deleteinstallfolder"] = "Delete install folder";
$translation["deletionofinstallfolderfailed"] = "Deletion of install folder failed. Please delete the 'install' folder manually and try again.";
$translation["deletionofinstallfolderok"] = "Install folder deleted.";
$translation["demouser"] = "Demo user";
$translation["demouserip"] = "123.456.768.9";
$translation["description"] = "Description";
$translation["device"] = "Device";
$translation["devicemanagement"] = "Manage devices";
$translation["devices"] = "Devices";
$translation["devicetype"] = "Device type";
$translation["edit"] = "Edit";
$translation["eventname"] = "Event";
$translation["events"] = "Events";
$translation["exttemp"] = "Ext. temp";
$translation["finalizing"] = "Completion";
$translation["finishinstallation"] = "Finish installation";
$translation["firstname"] = "First name";
$translation["friday"] = "Friday";
$translation["help"] = "Help";
$translation["homeautomationinstallation"] = "HomeAutomation installation";
$translation["host"] = "Server address";
$translation["houseplan"] = "House plan";
$translation["houseplanconfigurationdescription"] = "Choose device and choose it's position by clicking at the picture. Clicking a device at the picture removes it.";
$translation["ifinstallcorrupted"] = "If something went wrong during installation or you just want to do a <b>CLEAN INSTALL</b>";
$translation["image"] = "Image";
$translation["installfolderexists"] = "<b>The installation directory still exists, please remove it to start using the system (ie the folder 'install'). Keeping the installation folder is a major security risk!</b>";
$translation["ip"] = "IP";
$translation["lastactivities"] = "Most recent events";
$translation["lastknownstatus"] = "Last known status";
$translation["lastname"] = "Last name";
$translation["emailaddress"] = "E-mail address";
$translation["latestlogevents"] = "Latest in log";
$translation["learn"] = "Learn";
$translation["log"] = "Log";
$translation["login"] = "Log in";
$translation["loginfail"] = "Check username / password";
$translation["loginok"] = "Username and password ok";
$translation["logout"] = "Log out";
$translation["missing"] = "Missing";
$translation["missinginstallfolder"] = "Missing installation folder, please do a fresh install of the HomeAutomation files.";
$translation["monday"] = "Monday";
$translation["mysqlsettings"] = "Mysql settings";
$translation["mysqlsettingsfail"] = "Couldn't establish contact with database, please check the settings.";
$translation["mysqlsettingsok"] = "Mysql settings ok";
$translation["name"] = "Name";
$translation["never"] = "Never";
$translation["newrawdevice"] = "New RAW device";
$translation["nextevent"] = "Next event";
$translation["nextstep"] = "Next step";
$translation["no"] = "No";
$translation["nodatabaseconnection"] = "No database connection";
$translation["noschedulescreated"] = "No schedules created yet.";
$translation["noupcomingevents"] = "No upcoming events";
$translation["of"] = "Of";
$translation["off"] = "Off";
$translation["ok"] = "Ok";
$translation["on"] = "On";
$translation["pagegeneratedin"] = "Page generated in";
$translation["password"] = "Password";
$translation["rawcommands"] = "RAW commands";
$translation["rawdevice"] = "RAW device";
$translation["resetfilter"] = "Reset filter";
$translation["runnow"] = "Run now";
$translation["saturday"] = "Saturday";
$translation["save"] = "Save";
$translation["scenario"] = "Scenario";
$translation["scenarios"] = "Scenarios";
$translation["scheduler"] = "Scheduler";
$translation["schedules"] = "Existing schedules";
$translation["scheduletype"] = "Schedule type";
$translation["seconds"] = "Seconds";
$translation["selectdevice"] = "Select device";
$translation["selecttempsensor"] = "Select temperature sensor";
$translation["selectuserlevel"] = "Select user level";
$translation["settings"] = "Settings";
$translation["show"] = "Show";
$translation["donotshow"] = "Do not show";
$translation["showall"] = "Show all";
$translation["showing"] = "Showing";
$translation["showingallactivities"] = "Showing all activities";
$translation["startinstallation"] = "Start installation";
$translation["static"] = "Static";
$translation["status"] = "Status";
$translation["step"] = "Step";
$translation["suncontrolled"] = "Sun controlled";
$translation["sunday"] = "Sunday";
$translation["sunrisetoday"] = "Sunrise";
$translation["sunsettoday"] = "Sunset";
$translation["teach"] = "Teach";
$translation["telldusdevicesupdated"] = "Telldus devices updated";
$translation["temperaturecontrolled"] = "Temperature controlled";
$translation["temperatures"] = "Temperatures";
$translation["tempsensor"] = "Termperature sensor";
$translation["testmysqlconnection"] = "Test Mysql connection";
$translation["testpermissions"] = "Test permissions";
$translation["thedatabase"] = "The database";
$translation["thursday"] = "Thursday";
$translation["time"] = "Time";
$translation["timeforactivityoff"] = "Time for activity off";
$translation["timeforactivityon"] = "Time for activity on";
$translation["timefordepature"] = "Time for departure";
$translation["toggle"] = "Toggle";
$translation["tuesday"] = "Tuesday";
$translation["turnoff"] = "Turn off";
$translation["turnoffall"] = "Turn off all";
$translation["turnon"] = "Turn on";
$translation["turnonall"] = "Turn on all";
$translation["upcomingevents"] = "Coming events";
$translation["update"] = "Update";
$translation["updatefromsystemplugins"] = "Update from system plugins";
$translation["upload"] = "Upload";
$translation["uploadhouseplantoconfigure"] = "Upload a house plan to configure";
$translation["uploadnewhouseplan"] = "Upload a new house plan";
$translation["user"] = "User";
$translation["userlevel"] = "User level";
$translation["usermanagement"] = "User management";
$translation["username"] = "Username";
$translation["wait"] = "Wait...";
$translation["waitingfordongletobefree"] = "Waiting for dongle to be free";
$translation["wednesday"] = "Wednesday";
$translation["weekdays"] = "Weekdays";
$translation["weekends"] = "Weekends";
$translation["writepermissionsfail"] = "PHP hasn't got write permissions in the HomeAutomation directory, please check the permissions.";
$translation["writepermissionsok"] = "Read/write permissions ok";
$translation["yes"] = "Yes";
$translation["icons"] = "Icons";
$translation["id"] = "Id";


$translation["settings_groups"]["userinterface"] = "Userinterface";
$translation["settings_groups"]["system"] = "System";
$translation["settings_groups"]["dimlevels"] = "Dimlevels";
$translation["settings_groups"]["infobox"] = "Infobox";

$translation["settings_labels"]["theme"] = "Theme";
$translation["settings_labels"]["showloggedinuser"] = "Show logged in user";
$translation["settings_labels"]["showcurrenttime"] = "Show current time";
$translation["settings_labels"]["showtimeforsun"] = "Show time for sunrise / sunset";
$translation["settings_labels"]["usethissensor"] = "Use this sensor";
$translation["settings_labels"]["numberoflogevents"] = "Number of log events";
$translation["settings_labels"]["numberofcomingevents"] = "Number of coming events";
$translation["settings_labels"]["latitude"] = "Latitude";
$translation["settings_labels"]["longitude"] = "Longitude";
$translation["settings_labels"]["version"] = "Version";
$translation["settings_labels"]["dimlevelstep"] = "Dimlevel stepsize (%)";
$translation["settings_labels"]["alwaysshowlaststatus"] = "Always show last status";
$translation["settings_labels"]["hoursstatusactive"] = "Hours status active";
$translation["settings_labels"]["houseplanwidth"] = "Houseplan width";
$translation["settings_labels"]["houseplaniconheight"] = "Houseplan icon height";
$translation["settings_labels"]["useonewire"] = "Use 1-wire temperature";
$translation["settings_labels"]["useexterntemperature"] = "Use external temperature";
$translation["settings_labels"]["externtemperatureurl"] = "External temperature URL";
$translation["settings_labels"]["externtemperaturelocation"] = "Location for extern temperature";
$translation["settings_labels"]["debugmode"] = "Debug mode";
$translation["settings_labels"]["timezone"] = "Timezone";
$translation["settings_labels"]["title"] = "Titel";
$translation["settings_labels"]["username"] = "Username";
$translation["settings_labels"]["password"] = "Password";
$translation["settings_labels"]["phppath"] = "PHP bin path";
$translation["settings_labels"]["jtpath"] = "JT bin path";
$translation["settings_labels"]["tdtoolpath"] = "Tdtool path";
$translation["settings_labels"]["localipaddresses"] = "Local IP's";
$translation["settings_labels"]["defaultpage"] = "Default page";
$translation["settings_labels"]["defaultdimlevel"] = "Default dimlevel";
$translation["settings_labels"]["showinfobox"] = "Show infobox";
$translation["settings_labels"]["timeformat"] = "Time format";
$translation["settings_labels"]["dateformat"] = "Date format";

$translation["reseticons"] = "Reset icons";
$translation["uploadnewicons"] = "Upload new icons";
$translation["serial"] = "Serial number";
$translation["sort"] = "Sort";
$translation["updatesensors"] = "Update sensors";
$translation["nosensorsavailable"] = "No sensors available. Did you configure the weather parser? See parser/readme.txt for more information.";

$translation["gcal"] = "Google-calendar";
$translation["gpassword"] = "Password for Google";
$translation["gusername"] = "Username for Google";

$translation["warningisnotdirectory"] = "'{d}' is not a directory, please check settings ({s}).<br />";
$translation["warningfilenotfound"] = "Could not find '{f}' in directory '{d}', please check settings ({s}).<br />";

$translation["missinghouseplanimage"] = "The houseplan image is missing, please check your settings.";

$translation["gotodesktopversion"] = "Desktop version";
$translation["gotomobileversion"] = "Mobil version";

$translation["comment"] = "Comment";
$translation["activations"] = "Activations";
$translation["activationssuffix"] = "activations";
$translation["addactivation"] = "Add activation";
$translation["at"] = "At";
$translation["offset"] = "Offset";
$translation["activationtoday"] = "Activation today";
$translation["dusk"] = "Dusk";
$translation["sunrise"] = "Sunrise";
$translation["dawn"] = "Dawn";
$translation["sunset"] = "Sunset";
$translation["engineheater"] = "Engineheater";
$translation["random"] = "Random";
$translation["dynamic"] = "Dynamic";
$translation["earliest"] = "Earliest";
$translation["latest"] = "Latest";
$translation["selectoperator"] = "Select operator";
$translation["smallerthan"] = "Smaller than";
$translation["smallerthanorequal"] = "Smaller than or equal";
$translation["equal"] = "Equal";
$translation["largerthanorequal"] = "Larger than or equal";
$translation["largerthan"] = "Larger than";

$translation["devicegroups"] = "Device groups";
$translation["group"] = "Group";
$translation["createnewgroup"] = "Create new group";
$translation["enabled"] = "Enabled";
$translation["groups"] = "Groups";
$translation["confirmstartcleaninstall"] = "The installation process of a clean install will now be started. Proceed?";
$translation["activedevices"] = "Active devices";
$translation["activedays"] = "Active days";
$translation["activegroups"] = "Active groups";
$translation["showhide"] = "Show/hide";
$translation["devicesgroups"] = "Devices/groups";
$translation["nogroupsconfigured"] = "No groups have been configured";

$translation["help_about"] = "HomeAutomation v{VERSION}, released {DATE}<br><br>
Homepage: <a href=\"https://rosenback.me/ha\" target=\"_blank\">https://rosenback.me/ha</a><br>
Developed by: Tom Rosenback and Daniel Malmgren<br><br>
Licensed under GNU GPL";

$translation["help_text"] = "Help documentation available at <a href=\"https://rosenback.me/ha\" target=\"_blank\">https://rosenback.me/ha</a>";
$translation["settings_labels"]["numsignalrepetitions"] = "Number of signal repetitions";

$translation["macros"] = "Macros";
$translation["macro"] = "Macro";
$translation["createnewmacro"] = "Create new macro";
$translation["nomacrosconfigured"] = "No macros have been configured";
$translation["nochange"] = "No change";
$translation["macrodescription"] = "Running this macro affects according to the configuration below.";
$translation["icon"] = "Icon";
$translation["reseticon"] = "Reset icon";
$translation["uploadnewicon"] = "Upload new icon";
$translation["enable"] = "Enable";
$translation["disable"] = "Disable";
$translation["disabled"] = "Disabled";
$translation["menu"] = "Menu";
$translation["tomorrow"] = "tomorrow";
$translation["schedulesmigrated"] = "{numschedules} schedule(s) migrated";
$translation["houseplanmigrated"] = "Houseplan migrated";
$translation["schedulesrebuilt"] = "Schedules rebuilt";
$translation["reload"] = "Reload";
$translation["rebuildschedules"] = "Rebuild schedules";
$translation["confirmrebuildschedules"] = "Are you sure you want to rebuild schedules?";
$translation["imported"] = "Imported";
$translation["enterschedulename"] = "Please enter a name for the schedule.";
$translation["onedaymustbeselected"] = "At least one day must be selected.";
$translation["correcterrorstocontinue"] = "Please correct errors before proceeding.";
$translation["activationsmustbeconfigured"] = "No activations configured, you must have at least one activation configured.";
$translation["incorrecttimeformat"] = "Incorrect time format, hh:mm allowed, offsets also allow negative (-) values.";
$translation["deviceorgroupmustbeselected"] = "At least one device or one group must be selected";

$translation["settings_labels"]["dynamiccheckinterval"] = "Interval between dynamic activations";
$translation["hours"] = "hours";
$translation["minutes"] = "minutes";
$translation["settings_groups"]["scheduler"] = "Scheduler";

$translation["incorrectcredentials"] = "Username or password wrong";
$translation["usernameandpasswordcannotbeempty"] = "Username and / or password cannot be empty";

$translation["powerconsumption"] = "Power consumption";

$translation["currentconsumption"] = "Current consumption";
$translation["totalconsumption"] = "Total consumption";
$translation["since"] = "since";
$translation["logs"] = "Logs";
$translation["measurements"] = "Measurements";
$translation["sensors"] = "Sensors";
$translation["sensortypes"] = "Sensor types";
$translation["unit"] = "Unit";
$translation["createnewsensortype"] = "Create new sensor type";
$translation["sensorname"] = "Sensor name";
$translation["uniqueid"] = "Unique ID";
$translation["type"] = "Type";
$translation["calculated"] = "Calculated";
$translation["latestreading"] = "Latest reading";
$translation["currentvalues"] = "Current values";
$translation["now"] = "Now";
$translation["30minago"] = "30 min. ago";
$translation["trend"] = "Trend";
$translation["1yearago"] = "1 year ago";
$translation["2yearsago"] = "2 years ago";
$translation["3yearsago"] = "3 years ago";
$translation["average"] = "Average";
$translation["hours"] = "H";
$translation["days"] = "days";
$translation["years"] = "years";
$translation["overall"] = "Overall";
$translation["saunausage"] = "Sauna usage";
$translation["usageatatime"] = "For a time";
$translation["usageweekly"] = "Weekly";
$translation["usagemonthly"] = "Monthly";
$translation["inaweek"] = "Week";
$translation["inamonth"] = "Month";
$translation["database"] = "Database";
$translation["firstreading"] = "First reading";
$translation["datapoints"] = "Data points";
$translation["falling"] = "Falling";
$translation["rising"] = "Rising";
$translation["neutral"] = "Neutral";
$translation["periodlast"] = "Last";
$translation["lastupdated"] = "updated {time} minutes ago";
$translation["lastupdated_single"] = "updated {time} minute ago";
$translation["lastupdated_hours"] = "updated {time} timmar ago";
$translation["lastupdated_days"] = "updated {time} dagar ago";

$translation["defaultimage"] = "Default image";
$translation["confirmresetsorting"] = "Are you sure you want to reset sorting by name?";
$translation["resetsorting"] = "Reset sorting";

$translation["settings_labels"]["iconortext"] = "Show icons or text in menu";
$translation["icon"] = "Icons";
$translation["text"] = "Text";
$translation["iconandtext"] = "Icons + text";
$translation["beginning"] = "in the beginning";
$translation["end"] = "in the end";
$translation["keyword"] = "of a calender event which contains the word";

$translation["nocalendarsavailable"] = "No calenders available";
$translation["availablecalendars"] = "Available calendars";
$translation["googlecouldnotfetchcalendars"] = "Could not fetch calendars";
$translation["googlecouldnotfetchevents"] = "Could not fetch calendar events";
$translation["googleconnectionproblems"] = "Could not contact Google";
$translation["loading"] = "Loading...";
$translation["loginfirst"] = "Login first";

$translation["rememberme"] = "Remember me";

$translation["systemschedules"] = "System schedules";
$translation["path"] = "Path";
$translation["executable"] = "Executable";
$translation["period"] = "Period";
$translation["nosystemschedulesconfigured"] = "No system schedules configured";
$translation["createnewsystemschedule"] = "Create new system schedule";
$translation["dateandtime"] = "Date and time";
$translation["executabledoesntexist"] = "Executable doesn´t exist";

$translation["plugins"] = "Plugins";
$translation["pages"] = "Pages";
$translation["page"] = "Page";

$translation["daily"] = "Daily";
$translation["weekly"] = "Weekly";
$translation["monthly"] = "Monthly";

$translation["allmonths"] = "All months";
$translation["allmonthdays"] = "All month days";

$translation["interval"] = "Interval";
$translation["chooseinterval"] = "Choose interval";
$translation["choosetime"] = "Choose time";
$translation["hour"] = "Hour";
$translation["minute"] = "Minute";

$translation["january"] = "January";
$translation["february"] = "February";
$translation["march"] = "March";
$translation["april"] = "April";
$translation["may"] = "May";
$translation["june"] = "June";
$translation["july"] = "July";
$translation["august"] = "August";
$translation["september"] = "September";
$translation["october"] = "October";
$translation["november"] = "November";
$translation["december"] = "December";

$translation["every"] = "Every";
$translation["and"] = "and";

$translation["arguments"] = "Arguments";

$translation["system"] = "System";
$translation["choosesystem"] = "Choose system";
$translation["systemplugins"] = "System plugins";
$translation["systemplugin"] = "System plugin";
$translation["systempluginsettings"] = "System plugin settings";
$translation["author"] = "Author";
$translation["installedversion"] = "Installed version";
$translation["availableversion"] = "Available version";
$translation["editsettings"] = "Edit settings";
$translation["uninstall"] = "Uninstall";
$translation["install"] = "Install";
$translation["upgrade"] = "Upgrade";
$translation["remove"] = "Remove";
$translation["confirminstall"] = "Are you sure you want to install";
$translation["confirmupgrade"] = "Are you sure you want to upgrade";
$translation["confirmuninstall"] = "Are you sure you want to uninstall";
$translation["confirmremove"] = "Are you sure you want to remove";
$translation["notinstalled"] = "Not installed";
$translation["systemdeviceid"] = "System device ID";

$translation["exporttoxml"] = "Export to XML";
$translation["file"] = "File";
$translation["installpluginfromxml"] = "Install plugin from XML";
$translation["confirminstallpluginfromxml"] = "Are you sure you want to install plugin from XML? (This will overwrite old files.)";

$translation["light"] = "Light";
$translation["dimmer"] = "Dimmer";
$translation["sauna"] = "Sauna";
$translation["absdimmer"] = "Dimmer (abs.)";
$translation["dummy"] = "Dummy";
$translation["bell"] = "Bell";

$translation["saveascopy"] = "Save as a copy";
$translation["copy"] = "Copy";
$translation["copyof"] = "Copy of";

$translation["gaugetype"] = "Gauge type";
$translation["speedometer"] = "Speedometer";

$translation["graphtype"] = "Graph type";
$translation["line"] = "Line";
$translation["column"] = "Column";

$translation["min"] = "Min";
$translation["max"] = "Max";
$translation["low"] = "Low";
$translation["high"] = "High";
$translation["lowlimit"] = "Low limit";
$translation["highlimit"] = "High limit";

$translation["info"] = "Info";
$translation["close"] = "Close";

$translation["nodevicesconfigured"] = "No devices configured";
$translation["nodevicesfound"] = "No devices found, you might need to change the settings for the plugin you are using.";

$translation["settings_labels"]["checkversionsenabled"] = "Check for new versions";
$translation["somenewversionsavailable"] = "New system version(s) found, <a href=\"https://rosenback.me/ha/\" target=\"_blank\">download here</a>.";

$translation["custompages"] = "Custom pages";
$translation["notsufficientprivileges"] = "Not sufficient privileges.";
$translation["filenotfound"] = "File {file} not found";
$translation["moduleloadedin"] = "Module ({module}) loaded in {time} seconds";

$translation["saved_successfully"] = "Saved successfully";
$translation["deleted_successfully"] = "Deleted successfully";
$translation["failed_saving"] = "Failed saving";

$translation["updates"] = "Updates";
$translation["activemacros"] = "Active macros";
$translation["plugin_successfully_updated"] = "Plugin successfully updated";

?>
