Short description of the HomeAutomation API

To call the api, call api.php.

You always have to provide an argument named "do" that tells HomeAutomation the name of the API function you want to call.

Optionally you can always provide an argument named "requireslogin". Without this argument HomeAutomation simply tries to use any existing connection or local login. With "requireslogin" you always have to provide arguments for "login_username" and "login_password".
Optionally you can provicde an argument named "ignoreresponse" with a value of "1", this will make the server return a 200 response as fast a possible continuing with the request in the background. 

For example:

To get an xml list of all devices:
http://hostname/homeautomation/api.php?do=devices/get&requireslogin=1&login_username=some_user&login_password=abc123&output=xml

To tell HomeAutomation that tellstick device 12 has been turned on:
http://hostname/homeautomation/api.php?do=devices/updateStatus&status=1&systempluginname=tdtool&systemdeviceid=12

The commands are grouped into categories (hence the word "devices" before the slash in the examples above). We'll list the available commands and their arguments grouped under respective category below:

Activations
activations/add: Not yet finished
activations/run: Run a specific activation. Arguments: id (id of activation to run)

Devices
devices/get: Get a list of devices. Arguments: output (output format, json or xml), deviceid (to get only a specific device, omit to get all devices) 
devices/getStatus: Get status for a specific device. Arguments: deviceid, 
devices/toggle: Change status of a specific device. Arguments: deviceid, status
devices/updateStatus: Change status of a specific device. Arguments: status(1 or 0), systempluginname, systemdeviceid

Groups
groups/get: Get a list of all groups. Arguments: output (output format, json or xml)
groups/toggle: Change status of all devices in a group. Arguments: groupid, status

Macros
macros/run: Run a macro. Arguments: macroid

Scenarios
scenarios/get: Get a list of all scenarios. Arguments: output (output format, json or xml)
scenarios/set: Change scenario. Arguments: id

Sensors
sensors/get: Get a list of all sensors and their readings. Arguments: output (output format, json or xml)
sensors/setValue: Tell HA the value of a sensor. Arguments: serial, value

Users
users/login: Login a specific user

NOTE: Old methods and ajaxinterface.php has been removed as of v3.0.2, use api.php and methods in corresponding folders instead.