<?php
// HomeAutomation
// Copyright (C) 2021 Tom Rosenback (tom.rosenback@gmail.com), Daniel Malmgren (daniel.malmgren@kolefors.se)
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

$pageGenerationStart = microtime(true);

session_start();

include("includes.php");

define("HomeAutomationIncluded", TRUE);

$ha = new HomeAutomation();
include(HA_ROOT_PATH."/header.php");

validateSession();
doSystemCheck();

$pageName = getFormVariable("page", "");
if($pageName == "") {
	$pageName = $_SESSION[CFG_SESSION_KEY]["settings"]["defaultpage"];
}

$ha->setCurrentPage($pageName);

$msg = getFormVariable("msg", "");
if($msg != "") {
	$message = new SystemMessage();
	$message->decode($_GET["msg"]);
	// $message->setId(getFormVariable("msgid", ""));
	// $message->setText($msg);
	// $message->setType(getFormVariable("msgtype", "info"));
	// $message->setAutoClose(getFormVariable("msgautoclose", false));
	// $message->setShowOnlyInDebug(getFormVariable("msgshowonlyindebug", false));
	$ha->addMessage($message);
}

// echo "SEO: ".getFormVariable("seo", "noseo");

if($_SESSION[CFG_SESSION_KEY]["userlevel"] >= 1) {
	$ha->showInfoBox = $_SESSION[CFG_SESSION_KEY]["settings"]["showinfobox"];
}

if($ha->getCurrentPageModule() != "custompages") {
	$ha->loadModule(getFormVariable("action", "default"));
}

?>

<!DOCTYPE html>
<html>
	<head>
		<?php echo $ha->generateHeader(); ?>
	</head>
	<body<?php echo $ha->getOnLoadScript();?>>
		<div id="main">
			<div id="systemmessagecontainer"><?php echo generateSystemMessages(); ?></div>
			<div style="margin-bottom: 3px;">
				<table width="100%">
					<tr>
						<td class="bold" style="padding-right: 25px;"><?php echo $_SESSION[CFG_SESSION_KEY]["settings"]["title"];?></td>
						<td class="bold">
							<?php if($_SESSION[CFG_SESSION_KEY]["userlevel"] >= 1) { echo generateMenu(); } ?>
						</td>
					</tr>
				</table>
			</div>
			<table height="92%" width="100%" cellpadding="0" cellspacing="0">
				<tr>
					<td width="100%" height="100%" align="center" valign="top" style="padding-right: 5px;">
						<?php
							if($ha->getCurrentPageModule() != "custompages") {
								echo $ha->getBody();
							} else {
								include($ha->getCurrentPagePath());
							}
						?>
					</td>
					<?php

						if($ha->showInfoBox != "") {
							if($ha->getOnLoadScript() == "") {
								$ha->setOnLoadScript(" onload=\"setInterval('updateInfobox()', 30000);\"");
							}
					?>
						<td id="infobox_content" valign="top"><?php echo generateInfobox();?></td>
					<?php
						}
					?>
				</tr>
			</table>

			<div class="bold">
				<a href="<?php echo CFG_MOBILE_VERSION_INDEX;?>"><?php echo LBL_GOTOMOBILEVERSION;?></a>
				<?php
					if(defined("CFG_SHOWPAGEGENERATETIME")) {
						$pageGenerationTime = microtime(true) - $pageGenerationStart;

						echo "<br /><br />".LBL_PAGEGENERATEDIN." ".number_format($pageGenerationTime, 4)." ".mb_convert_case(LBL_SECONDS, MB_CASE_LOWER, "UTF-8");
					}
				?>
			</div>
		</div>

		<div id="status_holder" style="position: absolute; display: none; z-index: 1000; background-color:white; border: 1px solid black; padding: 5px;">
			<img src="resources/left_arrow.png" width="5px" style="position: absolute; top: 5; left: -6px;" />
			<table>
				<tr>
					<td colspan="2"><?php echo LBL_KEEPCURRENTSTATUSDELAY; ?></td>
				</tr>
				<tr>
					<td><?php echo LBL_DELAYTYPE; ?>:</td>
					<td>
						<select name="delaytype">
							<option value=""><?php echo LBL_NODELAY; ?></option>
							<option value="static"><?php echo LBL_STATIC; ?></option>
							<option value="suncontrolled"><?php echo LBL_SUNCONTROLLED; ?></option>
						</select>
					</td>
				</tr>
				<tr>
					<td><?php echo LBL_KEEPUNTIL; ?>:</td>
					<td><input type="text" class="text" name="status_hold_time" id="status_hold_time" /></td>
				</tr>
				<tr>
					<td colspan="2">&nbsp;</td>
				</tr>
				<tr>
					<td colspan="2" align="right">
						<input type="button" class="button" name="cancel" id="status_holder_cancel" value="<?php echo LBL_CANCEL; ?>" />
						<input type="button" class="button" name="save" id="status_holder_save" value="<?php echo LBL_SAVE; ?>" />
					</td>
				</tr>
			</table>
		</div>
	</body>
</html>

<?php


if($_SESSION[CFG_SESSION_KEY]["settings"]["debug"] && $_SESSION[CFG_SESSION_KEY]["userlevel"] > 2) {
	//removeDuplicateData();
	echo "<pre style=\"text-align: left;\">";
	print_r($_POST);
	print_r($_GET);
	print_r($_COOKIE);
	print_r($_SESSION[CFG_SESSION_KEY]);
	print_r($_FILES);
	print_r($_SERVER);
	// print_r($ha);
	print_r(get_defined_constants());
	echo "</pre>";
}

?>
