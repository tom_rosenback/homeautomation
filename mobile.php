<?php
// HomeAutomation
// Copyright (C) 2021 Tom Rosenback (tom.rosenback@gmail.com), Daniel Malmgren (daniel.malmgren@kolefors.se)
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

$pageGenerationStart = microtime(true);
 
session_start();

include("includes.php");
include(HA_ROOT_PATH."/system/classes/HomeAutomation.php");

define("HomeAutomationIncluded", TRUE);
$ha = new HomeAutomation("mobile");

validateSession();
doSystemCheck();

$pageName = getFormVariable("page", "");
if($pageName == "")
{
	$pageName = $_SESSION[CFG_SESSION_KEY]["settings"]["defaultpage"];
}

$ha->setCurrentPage($pageName);

$customPagePath = "";

include("header.php");
include("header_mobile.php");

if($_SESSION[CFG_SESSION_KEY]["userlevel"] >= 1)
{
	$ha->showInfoBox = $_SESSION[CFG_SESSION_KEY]["settings"]["showinfobox"];
}

if($ha->getCurrentPageModule() != "custompages")
{
	$ha->loadModule(getFormVariable("action", "default"));
}

?>

<!DOCTYPE html>
<html>
	<head>
		<?php echo $ha->generateHeader(); ?>	
	</head>
	<body<?php echo $ha->getOnLoadScript();?>>		
		<div data-role="page" data-content-theme="b" data-theme="b">
			<div data-role="header">
				<h1>	
					<?php 
						if($ha->currentPage["translation"] == "")
						{
							echo $_SESSION[CFG_SESSION_KEY]["settings"]["title"];
						}
						else
						{
							echo $ha->currentPage["translation"];
						}
					?>
				</h1>						
				<?php if($_SESSION[CFG_SESSION_KEY]["userlevel"] >= 1) { ?>
					<a href="<?php echo THISPAGE;?>?page=<?php echo $ha->currentPage["name"];?>#menu" class="ui-btn-left menu-trigger header-btn" data-corners="false" data-icon="bars" data-iconpos="notext"><?php echo LBL_MENU;?></a>
					<a href="<?php echo THISPAGE;?>?page=<?php echo $ha->currentPage["name"];?>#infopanel" class="ui-btn-right infopanel-trigger header-btn" data-corners="false" data-icon="info" data-iconpos="notext" data-position="right"><?php echo LBL_INFO;?></a>
				<?php } ?>
			</div><!-- /header -->

			<div data-role="content">	
				<?php 
					if($ha->getCurrentPageModule() != "custompages")
					{
						echo $ha->getBody();
					}
					else
					{
						include($ha->getCurrentPagePath());
					}
				?>						
			</div><!-- /content -->
			
			<div id="menu" data-role="panel" style="height: 100%;" data-theme="b">
				<h3><?php echo $_SESSION[CFG_SESSION_KEY]["settings"]["title"]; ?></h3>
				<?php if($_SESSION[CFG_SESSION_KEY]["userlevel"] >= 1) { ?>
					<?php echo generateMenu(); ?>
				<?php } ?>
				
				<div class="outline">
					<a data-role="button" data-mini="true" href="<?php echo CFG_FULL_VERSION_INDEX;?>" rel="external" data-corners="false"><?php echo LBL_GOTODESKTOPVERSION;?></a>
				</div>
			</div>
			
			<div id="infopanel" data-role="panel" data-position="right" style="height: 100%;" data-theme="b">
				<?php if($_SESSION[CFG_SESSION_KEY]["userlevel"] >= 1) { ?>
					<h3><?php echo LBL_INFO ?></h3>
					<div id="infobox_content">
						<?php echo generateInfoBox(); ?>
					</div>
				
					<div class="outline">
						<a data-role="button" data-mini="true" href="<?php echo CFG_FULL_VERSION_INDEX;?>" rel="external" data-corners="false"><?php echo LBL_GOTODESKTOPVERSION;?></a>
					</div>
				<?php } ?>
			</div>
		
		</div>		
	</body>
</html>

<?php

if($_SESSION[CFG_SESSION_KEY]["settings"]["debug"] && $_SESSION[CFG_SESSION_KEY]["userlevel"] > 2)
{
	echo "<pre style=\"text-align: left;\">";
	print_r($_POST);
	print_r($_GET);
	print_r($_COOKIE);
	print_r($_SESSION[CFG_SESSION_KEY]);
	print_r($_FILES);
	print_r($_SERVER);
	print_r($ha);
	print_r(get_defined_constants());
	echo "</pre>";
}

?>
