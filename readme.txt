﻿HomeAutomation

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

=========================================================================================
Description
These PHP scripts are created by Tom Rosenback for use with many different automation systems
(eg Tellstick dongle from Telldus.se). The homeautomation system uses a Mysql database as data
storage.

Project started august 2008 and december 2009 Daniel Malmgren joined in to help me. Thank you!

=========================================================================================
For documentation please read the Users guide.
=========================================================================================

Change log - More detailed changelog can be found in Mantis

v3.3.2 2016-08-30
- [Sensors] Making code backward compatible in sense of PHP version

v3.3.1 2016-08-29
- [Installer] Fix for query which fail on older PHP versions

v3.3 2016-08-28
- [Sensors] Abaility to disable sensors. Values from disabled sensors will be silently ignored.
- [Sensors] Actually respect sensor types min/max values.
- [Scheduler] Completely removed the dynamic activation scheduled job, it's not needed anymore
- [System plugins] Added possiblity for custom function calls within system plugins
- [System plugins] Added new system plugin for notifications via pushover.net
- [System plugins] Added new system plugin for executing a user defined command
- [Database] Added fields for last sensor values, makes graph generation much faster

v3.2 2016-04-01
- [General] Fully PHP7 compatible
- [General] Completely rewritten database handling, now uses mysqli. Much faster.
- [General] Now compatible and tested with PHP 7
- [General] Refactored code to use object oriented php

v3.1.1 2016-01-14
- [General] Bug fixes and tweaks
- [Scheduler] Dynamic activations not running as expected

v3.1 2015-12-14
- [Temperatures] Graphs and sensor readings with irregular intervals
- [General] IP behind proxy is displayed wrong
- [API] Deprecate old ajaxinterface methods
- [Scheduler] Selecting all devices will not turn on engineheater
- [General] Move repetition of action on devices from run.php to plugins.
- [Scheduler] Creating a new schedule does not calculate the runtimes like the nightly job does
- [Temperatures] Configure paths in jpgraph
- [General] Saving systemschedules with invalid path generates blank page
- [Tellstick] Remove LockDongle
- [General] Hide custompages menu option if no pages configured
- [General] Installing a new version should dismiss message from db
- [Installer] Remove safe_mode prerequisitie check
- [Configuration] Changing password for user results in error
- [Houseplan] External temperature icon missing
- [General] Add translations for in all places where addMessage is used

v3.0.1 Released 2014-02-25
- [General] Custom pages not working
- [Mobile] Splash screen not showing on iPhone5
- [Configuration] Sliders on macro configuration are not initiziliazed
- [General] Dimming device that is on from eg 40 to 20% doesn´t log into db
- [Configuration] Adding a new device is not showing any icons in the edit form
- [New functionality] Add check for newer versions
- [Devices] Possibility to check devices "real" state more often
- [API] Get status of devices doesn´t mirror real device status
- [Installer] Add PHP5-GD as required
- [Installer] Field validation doesn´t display infomessage about a field being invalid


v3.0 Released 2014-02-07
- [Parser] Merge parser into HA
- [Temperatures] Temperature sensors is in fact much more than only temperature
- [General] Add trailing / to all paths that can be configured by user
- [Installer] Add prerequisities checks
- [Mobile] Add own page for infobox
- [Mobile] Reset of scroll after click
- [General] Rename "Full version" to "Desktop version"
- [Devices] Change toggleSelected
- [General] Remove the need of configuring HA root path
- [General] Convert project to UTF-8
- [General] Add images for menu items
- [Scheduler] Add possibilities to copy schedule
- [Devices] Uninstall of systemplugin doesn't remove devices
- [Configuration] Update from Tellstick needs to be changed according to new System plugins
- [Houseplan] Macro houseplan icons doesn't respect uploaded macro icons
- [Devices] Devicetypes doesn´t use translations
- [Configuration] Add plugin functionality for Telldus, Crestron, Jung... devices
- [Mobile] Mobile GUI
- [Houseplan] Add links to 24h, 7d and 30d graphs in the popup graph
- [System plugins] Make system plugins out of tdtool actions
- [General] Modularize pages for better structure in filesystem
- [Temperatures] Add custom system task scheduler
- [Log] Change log to logs and create submenu for system log, temperatures and consumption
- [General] Night theme has incorrect color on bigBold class
- [Scheduler] Scheduling doesn´t work if username and/or computername contains 'space'
- [Scheduler] Add possibility to configure if Windows scheduler supports force parameter
- [Scheduler] PHP path from settings causes problem if trailing / is not set
- [Configuration] Settings: If not http:// is present in external temp url makes web server use 100% load
- [Configuration] Logged in as local user allows one to delete devices
- [Temperatures] Add last updated time/date on graphs
- [New functionality] Add remember me check box for login and remove local IPs
- [Installer] Add allow_url_fopen to prerequisity check
- [Configuration] Deleting a scenario which is in use in a schedule causes the schedule to not be executed
- [New functionality] Add sorting to more places
- [Configuration] Add configuration for power consumption for devices
- [Users guide] Update User guide

v2.0.2 Released 2011-03-09
- [Houseplan] Houseplan shows icons in wrong place for portrait images
- [New functionality] Add possibility to login with parameters in URL
- [General] Add info to login form when incorrect credentials are given
- [Languages] After name should be Last name in english translation
- [Scheduler] Selecting All devices gives error message
- [Houseplan] Houseplan image is not uploaded

v2.0.1 Released 2011-02-28
- [Scheduler] Saving schedule with only a group selected
- [Installer] Installer complains about database user not having enough privileges.
- [Scheduler] Invalid date formats for AT and SCHTASKS

v2.0 Released 2011-02-16
- [Scheduler] Add possibilty to configure dynamic activation interval
- [Mobile] Change slider back to dropdown
- [Mobile] Menu text needs to be bigger
- [General] Upcoming events displays tomorrow even if it is the day after tomorrow
- [Users guide] Description of different prerequisities needed
- [Users guide] Add Windows UAC notice
- [Scheduler] Add error messages under the activations list also if there is an error in the schedule
- [General] Refer to Users guide.php when config.php is missing

v2.0 RC Released 2011-02-02
- [Scheduler] Rebuilding of scheduler
- [New functionality] Wakeup light
- [Scheduler] Add possibility to configure dimlevel
- [Scheduler] Add possibility to configure random on/off intervals
- [Configuration] Add settings for dusk/dawn which can be used in scheduler
- [New functionality] Add better ways for users to search for errors
- [Scheduler] Add enable/disable toggler for schedules
- [General] Add configurable macros
- [General] Changing scenario should affect devices aswell
- [Houseplan] Make the houseplan image the have dynamic width/height
- [New functionality] Grouping of devices
- [General] All on/off for groups of devices
- [Devices] Use slider instead of Dropdown box
- [Tellstick] Devices doesn't always react when they should
- [Configuration] Make houseplan configuration easier to lineup
- [Devices] Toggling abs dimmer doesn´t change the value in the dropdown
- [Configuration] Show the devices in groups in dropdown in dynamic scheduler activation
- [Log] Make logs folder non readable from web
- [Installer] Add optional info sharing in the end of installation procedure
- [Installer] Add pre-requisities check before starting installation
- [Installer] Add license agreement
- [General] Add grouping of devices, that can be used in tasks and in devices list
- [Configuration] Change sort order of latitude and longitude
- [Installer] Change the way read/write permissions are checked
- [General] Make read/write permissions configurable
- [Configuration] Custom icon for ON status not saved correctly
- [Scheduler] Add comment field to schedules
- [General] Code needs comments

v1.2.2 Released 2010-11-08
- [Mobile] Mobile GUI
- [General] Saving the first scenario gives the impression that it is selected
- [General] Remove root_url from config since this isn´t needed.
- [Tdtool] Add more debug messages when toggling devices
- [General] Move all.css to subfolder
- [Scheduler] Change sort order of schedules for better readability
- [General] Add config_default.php including default values for fallback
- [Mobile] Theme doesn´t seem to have affect on page
- [Languages] Wrong variable name in install_en.php
- [Help] Remove link Mobile to Full

v1.2.1 Released 2010-10-27
- [General] Move debug information to bottom of page
- [Tdtool] Engineheater schedules are activated immidiately even if they shouldn´t
- [Scheduler] Schema with both on and off and off on next day doesn´t work
- [New functionality] Add link from full to mobile version and vice versa

v1.2 Released 2010-10-19
- [Help] Show help in installer
- [General] Getting some warnings and notices from tdtool
- [General] Getting error when user session timed out or overtaken by another application
- [Readme] Add info about that user has to create the database first
- [Readme] Add permission info for tdtool
- [General] Add error messages in the top of the page if vital paths are not working
- [Tellstick] Log actions on stick to file.
- [Configuration] Add configuration page for temperature sensors
- [Devices] Allow users to have custom icons for all devices
- [General] Auto reload of page
- [Scheduler] Updatetasks.php doesn´t execute on certain operating systems
- [Houseplan] Show graph for temperatures when clicking on them in houseplan view
- [General] Infobox calculates sun dependant schedules wrong
- [Tdtool] Dongle lock doesn´t work after making tdtool to run in background
- [General] \\ becomes \ and \ is completely removed
- [Log] Log IP instead of text "local" / "Hemma"

v1.1.1 Released 2010-04-15
- [Installer] Configured devicetype is not selected.
- [Scheduler] JT.exe causes unhandled exception.
- [Scheduler] Combination of different devicetypes in schedules doesn´t work as expected.
- [Houseplan] Removed devices aren't removed from houseplan.
- [Configuration] Unable to edit or delete created scenarios.
- [Configuration] Need to take summer/winter time into account when calculating sunrise/sunset.

v1.1 - Released 2010-04-02
- [Database] Eventlogging with dimlevel true/false is not handled as numeric 1/0 by some database engine versions.
- [Installer] Fix the install script to handle updates as well.
- [General] Verify Mac compatibility.
- [Tdtool] Commandline reports "You are not allowed here" for some OS.
- [Languages] Translate to finnish.
- [General] Add licensing to pages.
- [Tellstick] Verify absolute dimmer function.
- [General] Mobile version.
- [Configuration] Need possibility to clear houseplan configuration.
- [Installer] Install doesn´t seem to understand åäö on some computers.
- [General] Add setting to hide/show the infobox.
- [Scheduler] Add ability to force next event to run now.
- [Log] Add sort, limit, clean.
- [Database] Make queries injection proof.
- [Languages] Add translation support.
- [Database] Make queries support single backslash.
- [Tellstick] Make PHP execute exec in background.
- [Tellstick] Make schedules use last dimlevel.
- [General] Add index.php to links and forms.
- [General] Add configuration for default page.
- [Scheduler] Implement new scenario model.
- [Configuration] Manage scenarios.
- [Languages] Fix languages for settings in database.
- [Languages] Add fallback to english.
- [General] Theming.
- [Languages] Translate to english.
- [General] Add help pages.
- [General] Add info box showing sun rise, sunset, next event.
- [Configuration] Possibility to configure what IP addresses are considered "local".
- [General] Show Scenario setting on houseplan page, eg. Home, Away...

v1.0
- Completely reworked scheduling, now with sun/temperature dependant control
- Linux support
- Houseplans
- Settings and user management from within homeautomation
- Log filtering
- Support for absolute dimmers

v0.9
- Corrected <? to <?php
- Added device configuration page
- Removed Init DB function
- Added support for RAW command devices
- Removed heating devices from scheduler
- Added "Run task" command icon in scheduler
- Added that tasks in scheduler are ran when saved

v0.8
- Initial release
=========================================================================================

If you have any questions or run into any problems please check out the FAQ section of this
projects homepage, https://rosenback.me/ha or on the forum threads on Telldus
http://old.telldus.com/forum/viewforum.php?f=27


Tom Rosenback, Daniel Malmgren 2016
