<?php
global $ha;

// remove headers not used for mobile
$ha->removeHeader("js-ajax");
$ha->removeHeader("js-jquery-ui");
$ha->removeHeader("css-jquery-ui");
$ha->removeHeader("js-sliders");
$ha->removeHeader("js-jquery-datatables");
$ha->removeHeader("js-script");
$ha->removeHeader("css-theme");

// add mobile headers

// SCRIPTS
$ha->addHeader("js-jquery-mobile", "<script type=\"text/javascript\" src=\"plugins/jquery/jquery.mobile/jquery.mobile-1.4.5.min.js\" defer=\"defer\"></script>");
$ha->addHeader("js-mobile", "<script type=\"text/javascript\" src=\"scripts/mobile.js\" defer=\"defer\"></script>");

// CSS

$ha->addHeader("css-jquery-mobile", "<link href=\"plugins/jquery/jquery.mobile/jquery.mobile-1.4.5.min.css\" rel=\"stylesheet\" type=\"text/css\" />");
$ha->addHeader("css-mobile-theme", "<link href=\"resources/themes/mobile/".$_SESSION[CFG_SESSION_KEY]["settings"]["theme"].".css\" rel=\"stylesheet\" />");
$ha->addHeader("css-mobile-menu", "<link href=\"resources/mobile-menu.css\" rel=\"stylesheet\" />");

// META
$ha->addHeader("meta-viewport", "<meta name=\"viewport\" content=\"initial-scale=1\" />");

?>
