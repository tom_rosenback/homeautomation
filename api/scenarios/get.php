<?php
// HomeAutomation
// Copyright (C) 2021 Tom Rosenback (tom.rosenback@gmail.com), Daniel Malmgren (daniel.malmgren@kolefors.se)
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

// disabling notice messages
error_reporting(E_ALL ^ E_NOTICE);

// $pageGenerationStart = microtime(true);

if($_SESSION[CFG_SESSION_KEY]["userlevel"] >= 1) {
	$output = getFormVariable("output", "json");
	$selectedscenario = Settings::get("selectedscenario");

	if ($output == "xml") {
		$xml = new SimpleXMLElement("<?xml version='1.0' encoding='utf-8'?><scenarios/>");
		foreach (Scenarios::get(getFormVariable("scenarioid", "-1"), 1) as $scenario) {
			$track = $xml->addChild('scenario');
			$track->addChild('id', $scenario["id"]);
			$track->addChild('name', $scenario["name"]);
			if ($scenario["id"] == $selectedscenario) {
				$track->addChild('status',1);
			} else {
				$track->addChild('status',0);
			}
		}
		Header('Content-type: text/xml');
		print($xml->asXML());
	} else {
		//TODO
	}
}
else
{
	echo "";
}
