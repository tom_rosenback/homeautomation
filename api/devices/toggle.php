<?php
// HomeAutomation
// Copyright (C) 2021 Tom Rosenback (tom.rosenback@gmail.com), Daniel Malmgren (daniel.malmgren@kolefors.se)
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

// disabling notice messages
error_reporting(E_ALL ^ E_NOTICE);

if($_GET["deviceid"] != "" && $_GET["status"] != "") {
	$pageGenerationStart = microtime(true);
	$error = false;
	$status = getFormVariable("status", "-1");
	$deviceId = getFormVariable("deviceid", "-1");
	$devicesToToggle = Devices::get($deviceId);

	if($status == "-1") {
		$devices = readSelectedStatuses($devicesToToggle);
		$status = convertToNumeric(!convertToBoolean($devices[0]["status"]->state));
	}

	if($deviceId == "-1" && $status == "1") {
		$devicesToToggle = removeDevicesOfType($devicesToToggle, "sauna");
	}

	// echo "<pre>";
	// print_r($devicesToToggle);
	// echo "</pre>";
	// echo $deviceId."#".$status."<br />";
	if($_SESSION[CFG_SESSION_KEY]["userlevel"] > 1 && !$error)
	{
		toggleSelected($status, $devicesToToggle, getIpAddress(), $_SESSION[CFG_SESSION_KEY]["userid"]);
	}

	$json["result"] = array();

	$newStatus = readSelectedStatuses($devicesToToggle);
	//debugVariable($newStatus);

	foreach($newStatus as $device)
	{
		$json["result"][] = array(
									"id" => $device["id"],
									"image" => getStatusImage($device["type"], convertToBoolean($device["status"]->state), $device["id"]),
									"status" => $device["status"]->state,
									"dimlevel" => $device["status"]->dimlevel
								);
	}

	$json["exectime"] = number_format(microtime(true) - $pageGenerationStart, 4);

	$result = json_encode($json);

}

//SaveLog("Trying to toggle status by HTTP GET from IP: ".$_SERVER["REMOTE_ADDR"]." with userlevel: ".$_SESSION[CFG_SESSION_KEY]["userlevel"]." Deviceid: ".$deviceId.", wanted status: ".$status.", response from action: ".$statusImages);

?>
