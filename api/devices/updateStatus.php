<?php
// HomeAutomation
// Copyright (C) 2021 Tom Rosenback (tom.rosenback@gmail.com), Daniel Malmgren (daniel.malmgren@kolefors.se)
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

// disabling notice messages
error_reporting(E_ALL ^ E_NOTICE);

$json["time"] = array();
$json["time"][] = microtime(true);

//debugVariable($_GET);
if($_GET["systemdeviceid"] != "" && $_GET["status"] != "" && $_GET["systempluginname"] != "") {
	$error = false;
	$status = getFormVariable("status", "-1");
	$systemPluginName = getFormVariable("systempluginname", "tdtool");
	$systemDeviceId = getFormVariable("systemdeviceid", "-1");

	$device = Devices::getBySystemDeviceId($systemDeviceId, $systemPluginName);

	$json["result"] = array();

	if(isset($device["id"])) {
		$deviceArray = Devices::get($device["id"]);

		if($_SESSION[CFG_SESSION_KEY]["userlevel"] > 1 && !$error) {
			toggleSelected($status, $deviceArray, getIpAddress(), $_SESSION[CFG_SESSION_KEY]["userid"], true, "externalUpdate");
		}

		$json["result"][] = array(
			"id" => $device["id"]
		);
	} else {
		$json["result"][] = array(
			"error" => "Could not find device"
		);
	}

	$json["time"][] =  microtime(true) - $json["time"][0];
	$result = json_encode($json);
}

//SaveLog("Trying to toggle status by HTTP GET from IP: ".$_SERVER["REMOTE_ADDR"]." with userlevel: ".$_SESSION[CFG_SESSION_KEY]["userlevel"]." Deviceid: ".$deviceId.", wanted status: ".$status.", response from action: ".$statusImages);

?>
