<?php
// HomeAutomation
// Copyright (C) 2021 Tom Rosenback (tom.rosenback@gmail.com), Daniel Malmgren (daniel.malmgren@kolefors.se)
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

// disabling notice messages
error_reporting(E_ALL ^ E_NOTICE);

if($_SESSION[CFG_SESSION_KEY]["userlevel"] > 1)
{
	if($_GET["username"] != "" && $_GET["password"] != "")
	{
		include(HA_ROOT_PATH."/plugins/gcal/hagcal.php");

		$gcalSettings = GoogleCalendar::getSettings();

		$selectedCalendars = explode(";", $gcalSettings["selectedcalendars"]);
		$availableCalendars = getCalendarList($_GET["username"], $_GET["password"]);
		
		$data = array();
		$data["errmsg"] = htmlentities($availableCalendars["errmsg"]);
		
		$list = "<table cellspacing='0' cellpadding='3' class='tablelist' id='availablecalendars' width='100%'>";
		
		if(count($availableCalendars["data"]) > 0) {
			$list .= "<tbody>";
			$i = 0;
			
			foreach($availableCalendars["data"] as $calendar)
			{
				$checked = "";
				if(in_array($calendar["base64url"], $selectedCalendars))
				{
					$checked = " checked='checked'";
				}
				
				$list .= "<tr>
							<td><input type='checkbox' id='selectedcalendars".$i."' name='selectedcalendars[]' value='".$calendar["base64url"]."'".$checked." /></td>
							<td><label for='selectedcalendars".$i."'>".$calendar["name"]."</label></td>
						</tr>";
				
				$i++;
			}
			
			$list .= "</tbody>";
		}
		else
		{
			$list .= "<tfoot>
						".htmlentities(LBL_NOCALENDARSAVAILABLE)."
					</tfoot>";
		}

		$list .= "</table>";
		
		$data["list"] = $list;
		
		// echo "<pre>";
		// print_r($data);
		// echo "</pre>";
		
		echo json_encode($data);	
	}
	else
	{
		echo json_encode(" ");
	}
}
//SaveLog("Trying to toggle status by HTTP GET from IP: ".$_SERVER["REMOTE_ADDR"]." with userlevel: ".$_SESSION[CFG_SESSION_KEY]["userlevel"]." Deviceid: ".$deviceId.", wanted status: ".$status.", response from action: ".$statusImages);

?>