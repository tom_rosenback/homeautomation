<?php
// HomeAutomation
// Copyright (C) 2021 Tom Rosenback (tom.rosenback@gmail.com), Daniel Malmgren (daniel.malmgren@kolefors.se)
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

// disabling notice messages
error_reporting(E_ALL ^ E_NOTICE);

// $pageGenerationStart = microtime(true);

if($_SESSION[CFG_SESSION_KEY]["userlevel"] >= 1)
{
        $output = getFormVariable("output", "json");

        if ($output == "xml") {
		$xml = new SimpleXMLElement("<?xml version='1.0' encoding='utf-8'?><groups/>");
		foreach (DeviceGroups::get(getFormVariable("groupid", "-1")) as $group) {
			$track = $xml->addChild("group");
			$track->addChild('id', $group["id"]);
			$track->addChild('name', $group["name"]);
		}
		Header('Content-type: text/xml');
		print(unhtmlspecialchars($xml->asXML()));
        } else {
                //TODO
        }
}
else
{
	echo "";
}

function unhtmlspecialchars( $string )
{
  $string = str_replace ( '&uuml;', 'ü', $string );
  $string = str_replace ( '&Uuml;', 'Ü', $string );
  $string = str_replace ( '&auml;', 'ä', $string );
  $string = str_replace ( '&Auml;', 'Ä', $string );
  $string = str_replace ( '&ouml;', 'ö', $string );
  $string = str_replace ( '&Ouml;', 'Ö', $string );
  $string = str_replace ( '&aring;', 'å', $string );
  $string = str_replace ( '&Aring;', 'Å', $string );
  return $string;
}
