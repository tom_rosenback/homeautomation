<?php
// HomeAutomation
// Copyright (C) 2021 Tom Rosenback (tom.rosenback@gmail.com), Daniel Malmgren (daniel.malmgren@kolefors.se)
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

// disabling notice messages
error_reporting(E_ALL ^ E_NOTICE);

// $pageGenerationStart = microtime(true);

if($_SESSION[CFG_SESSION_KEY]["userlevel"] > 1)
{
        $output = getFormVariable("output", "json");

        if ($output == "xml") {
                $xml = new SimpleXMLElement("<?xml version='1.0' encoding='utf-8'?><sensors/>");
                foreach (Sensors::getCurrentReadings("", getFormVariable("id", -1)) as $sensor) {
                        $track = $xml->addChild('sensor');
                        $track->addChild('id', $sensor["id"]);
                        $track->addChild('name', $sensor["name"]);
                        $track->addChild('value', $sensor["last_reading"]);
                        $track->addChild('unit', $sensor["unit"]);
                }
                Header('Content-type: text/xml');
                print(unhtmlspecialchars($xml->asXML()));
        } else {
                //TODO
        }
}

function unhtmlspecialchars( $string )
{
  $string = str_replace('&deg;', '°', $string);
  $string = str_replace('&uuml;', 'ü', $string);
  $string = str_replace('&Uuml;', 'Ü', $string);
  $string = str_replace('&auml;', 'ä', $string);
  $string = str_replace('&Auml;', 'Ä', $string);
  $string = str_replace('&ouml;', 'ö', $string);
  $string = str_replace('&Ouml;', 'Ö', $string);
  $string = str_replace('&aring;', 'å', $string);
  $string = str_replace('&Aring;', 'Å', $string);
  return $string;
}

?>
