<?php
// HomeAutomation
// Copyright (C) 2021 Tom Rosenback (tom.rosenback@gmail.com), Daniel Malmgren (daniel.malmgren@kolefors.se)
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

// disabling notice messages
error_reporting(E_ALL ^ E_NOTICE);

// $pageGenerationStart = microtime(true);

if($_SESSION[CFG_SESSION_KEY]["userlevel"] > 1)
{
	$temperatures = "";
	$houseplan = Houseplan::get();

	foreach($houseplan as $hpDevice)
	{
		if($hpDevice["tempsensor"] != -1) {
			if($hpDevice["tempsensor"] != "external")
			{
				$sensorData = Sensors::getCurrentReadings($hpDevice["tempsensor"]);
				$currentValue = $sensorData[0]["last_reading"];
				$currentUnit = $sensorData[0]["unit"];
			}

			if($_SESSION[CFG_SESSION_KEY]["settings"]["useexternaltemperature"] && $hpDevice["tempsensor"] == "external")
			{
				$currentValue = trim(getTemperatureFromExternalUrl());
				$currentUnit = "&deg;C";
			}

			if($currentValue >= 0)
			{
				$image = "temp_high.png";
			}
			else
			{
				$image = "temp_low.png";
			}

			$image = "resources/".$image;

			$temperatures .= $separator.$hpDevice["id"]."$".$currentValue.$currentUnit."$".$image;
			$separator = "�";
		}
	}

	echo $temperatures;
}
else
{
	echo "";
}

?>
