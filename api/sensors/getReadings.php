<?php
// HomeAutomation
// Copyright (C) 2021 Tom Rosenback (tom.rosenback@gmail.com), Daniel Malmgren (daniel.malmgren@kolefors.se)
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

// disabling notice messages
error_reporting(E_ALL ^ E_NOTICE);

$json["result"] = array();
$json["lastupdate"] = -1;

if($_SESSION[CFG_SESSION_KEY]["userlevel"] > 1)
{
	$sensorId = getFormVariable("id", -1);
	$sensorSerial = getFormVariable("serial", "");
	$lastUpdate = getFormVariable("lastupdate", -1);
	$time = getFormVariable("time", -1);
	$lastEventTimestamp = -1; //getLastSensorTimeStamp($lastUpdate, $sensorId);
	$json["lastupdate"] = $lastEventTimestamp;

	if($time == -1) {
		$json["result"] = Sensors::getCurrentReadings($sensorSerial, $sensorId);
	} else {
		$json["result"] = Sensors::getReadingAtTime($time, $sensorSerial);
	}

	if($_SESSION[CFG_SESSION_KEY]["settings"]["useexternaltemperature"] && ($id == -1 || $id == "external"))
	{
		$id = "external";
		$value = trim(getTemperatureFromExternalUrl());
		$unit = "&deg;C";
		$image = getSensorImage("external", $value);

		$json["result"][] = array(	"id"			=> $id,
									"sensor_serial" => $id,
									"name" 			=> $_SESSION[CFG_SESSION_KEY]["settings"]["externaltemplocation"],
									"temp" 			=> $value,
									"value"			=> $value,
									"image"			=> $image,
									"unit" 			=> $unit,
									"gaugetype"		=> "speedometer",
									"min"			=> 0,
									"max"			=> 100,
									"low"			=> 10,
									"high"			=> 50
								);
	}
}

echo json_encode($json);

?>
