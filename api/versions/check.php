<?php
// HomeAutomation
// Copyright (C) 2021 Tom Rosenback (tom.rosenback@gmail.com), Daniel Malmgren (daniel.malmgren@kolefors.se)
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

// disabling notice messages
error_reporting(E_ALL ^ E_NOTICE);

$pageGenerationStart = microtime(true);

$json["result"] = array();
$json["result"]["updatesavailable"] = false;
$json["result"]["message"] = "";
$json["result"]["success"] = false;

$now = time();
$interval = 12 * 60 * 60; //12 hours in seconds
if($_SESSION[CFG_SESSION_KEY]["userlevel"] >= 3)
{
	$message = new SystemMessage();
	$message->setId("updatesavailable");
	$message->setText(LBL_SOMENEWVERSIONSAVAILABLE);

	if(!array_key_exists("lastversionscheck", $_SESSION[CFG_SESSION_KEY]) || 
		 (array_key_exists("lastversionscheck", $_SESSION[CFG_SESSION_KEY]) && ($now - $_SESSION[CFG_SESSION_KEY]["lastversionscheck"] > $interval)))
	{
		$updates = checkVersions();
		
		if(count($updates > 0)) {
			$message->setAdditionalText(LBL_UPDATES.": ".implode(", ", $updates));
		}			

		$_SESSION[CFG_SESSION_KEY]["lastversionscheck"] = $now;
		$json["result"]["updatesavailable"] = count($updates) > 0;
		
		$json["result"]["message"] = ($json["result"]["updatesavailable"] ? $message->generate() : "");
		$json["result"]["success"] = true;		
	}
	else
	{
		$updates = Versions::getUndismissed();
		
		foreach ($updates as $key => $update) {
			if($update["name"] == "ha") {
				$update["name"] = "HomeAutomation";
			}

			$updates[$key] = $update["name"]." v".$update["version"];
		}

		if(is_array($updates) && count($updates > 0)) {
			$message->setAdditionalText(LBL_UPDATES.": ".implode(", ", $updates));
		}

		$json["result"]["updatesavailable"] = count($updates) > 0;
		$json["result"]["message"] = ($json["result"]["updatesavailable"] ? $message->generate() : "");
		$json["result"]["success"] = true;
	}
}

$result = json_encode($json);


//SaveLog("Trying to toggle status by HTTP GET from IP: ".$_SERVER["REMOTE_ADDR"]." with userlevel: ".$_SESSION[CFG_SESSION_KEY]["userlevel"]." Deviceid: ".$deviceId.", wanted status: ".$status.", response from action: ".$statusImages);

?>