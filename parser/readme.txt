﻿Installation

- If it's the first time you're setting up the parser, rename parser_config_sample.php to parser_config.php

- Edit parser_config.php, simply uncomment (remove the //) the systems you want to use

- Configure each enabled system in their own config file, eg system_config\owfs.php. There are sample files for all available systems.

- Add a cron job / Windows scheduledtask that executes the following line with an interval of your choice, typically 15min
	path-to-php\php path-to-parser\parser.php
