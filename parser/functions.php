<?php
// HomeAutomation - sensor input parser
// Copyright (C) 2021 Tom Rosenback (tom.rosenback@gmail.com)
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

function writeReading($reading) {
	Sensors::writeReading(-1 ,$reading["sensor_serial"], $reading["temp_c"], "", $reading["calculateValues"]);
}

function parseSystemFile($system)
{
	echo "Parsing '".$system."'".PARSER_LINEBREAK;

	$systemConfigPath = PARSER_DIR."/system_config/".$system.".php";
	$systemPath = PARSER_DIR."/systems/".$system.".php";

	if(file_exists($systemConfigPath) && file_exists($systemPath)) {
		include($systemConfigPath);
		include($systemPath);
	}
	else
	{
		if(!file_exists($systemConfigPath)) {
			echo "Missing system config file (system_config/".$system.".php). Have you copied the sample config file (system_config_samples/".$system.".php => system_config/".$system.".php) and configured it?".PARSER_LINEBREAK.PARSER_LINEBREAK;
		}
		else
		{
			echo "System not found: ".$system.PARSER_LINEBREAK.PARSER_LINEBREAK;
		}
	}

	echo "Parsing '".$system."' completed".PARSER_LINEBREAK.PARSER_LINEBREAK;
}

function readSensorValue($valuePath)
{
	$value = "";

	$handle = fopen($valuePath, "r");

	while(!feof($handle)) {
		$value .= fread($handle, 8192);
	}

	fclose($handle);

	$value = trim($value);

	if($value == "") {
		$value = 85;
	}

	return $value * 1;
}

function readOWFSSensor($valuePath)
{
	$value = "";

	exec("owread ".$valuePath, $value);
	//exec("more ".getcwd()."\example_logfiles\owfs\owfs.txt", $value);

	if(is_array($value)) {
		$value = trim($value[0]);
	}
	else
	{
		$value = 85;
	}

	return $value * 1;
}

function readLastLineFromFile($file, $ignoreEmptyRows = true)
{
	$f = fopen($file, "r");
	$cursor = -1;

	fseek($f, $cursor, SEEK_END);
	$char = fgetc($f);

	if($ignoreEmptyRows) {
		// Trim trailing newline chars of the file
		while ($char === "\n" || $char === "\r") {
			fseek($f, $cursor--, SEEK_END);
			$char = fgetc($f);
		}
	}

	// Read until the start of file or first newline char
	while ($char !== false && $char !== "\n" && $char !== "\r") {
		$lastLine = $char.$lastLine;
		fseek($f, $cursor--, SEEK_END);
		$char = fgetc($f);
	}

    return trim($lastLine);
}

function readRPiGPIOSensorValue($file)
{
	$value = -85;
	$data = file($file);

	if(count($data) == 2) {
		echo "Dataline: ".$dataLine.PARSER_LINEBREAK;
		$dataLine = $data[1];
		$data = explode("=", $dataLine);
		$value = $data[1] / 1000;
	}
	else
	{
		echo "Invalid data, data must contain exactly 2 rows".PARSER_LINEBREAK;
	}

	return $value;
}


?>
