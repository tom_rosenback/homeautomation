<?php
	// Weather system file parser v0.7.5

	// uncomment the system(s) in use by removing the // in front of the line(s)
	// $systems[] = "wview";
	// $systems[] = "owfs";
	// $systems[] = "owread";
	// $systems[] = "cumulus";
	// $systems[] = "logtemp";
	// $systems[] = "calculations";
	// $systems[] = "telldusduo";
	// $systems[] = "rpigpio";
	// $systems[] = "yoctolight";
	// $systems[] = "wunderground";
	// $systems[] = "openzwave";

	// uncomment the following line (by removing // in front) if you only want to simulate the data parsing without inserting it to the database. Can be handy when testing out new sensors and systems.	
	// define("PARSER_SIMULATE", true);

?>
