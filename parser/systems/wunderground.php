<?php
// HomeAutomation - sensor input parser 
// wonderground V 1.2
// Copyright (C) 2014 Heiko Steinwender
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, 
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.

// WUNDERGROUND
	define ('LOGFILE',		substr( __DIR__ , 0 , -14)."logs\\" . substr(basename(__FILE__), 0, -4) . ".log");
	define ('TAB',			Chr(9));
	define ('SENSORERROR',	'*SENSOR*READING*ERROR*');

	if (isset($parserConfig["query"])) {
		$query=urlencode($parserConfig["query"]);
	} else {
		$query="autoip";
	}
	if (isset($parserConfig["apikey"])) {
			$url = "http://api.wunderground.com/api/".$parserConfig["apikey"]."/conditions/q/" .$query.".xml";
		} else {
			$url = "http://api.wunderground.com/auto/wui/geo/WXCurrentObXML/index.xml?query=".$query;
	}
	
/*
	function my_parse_xml($xml, $path) {
		// get string between tags
		$get_string_between = function ($string, $start, $end){
			$string = " ".$string;
			$ini = strpos($string, $start);
			if ($ini == 0) return "*** not found ***";
			$ini += strlen($start); 
			$len = strpos($string, $end, $ini);
			return substr($string, $ini, $len- $ini);
		};
		// print $path.PARSER_LINEBREAK;
		if(substr($path, 0, 1) == "/") $path = substr($path, 1);
		if(substr($path, -1) == "/") $path = substr($path, 0, -1);
		$ini = strpos($path, '/');
		if ($ini == 0) return $get_string_between($xml, "<".$path.">", "</".$path.">");
		$tag = substr($path, $ini + 1);
		$path = substr($path, 0, $ini);
		return my_parse_xml($get_string_between($xml, "<".$path.">", "</".$path.">"), $tag);		
	}
*/	

	// read sensor value
		
	function parse_xml(&$xml, $path) {
		$result = $xml->xpath("//".$path);
		if (is_array($result)) {
			$result = trim ($result[0]);
			if ($result=='' || strtolower($result) =='n/a' ) return SENSORERROR;
			return $result; 
			}
		return SENSORERROR;
	}

	// get url into string
	function get_url($url){
		$ch = curl_init(); // create curl resource
		curl_setopt($ch, CURLOPT_URL, $url); // set url
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); //return the transfer as a string
		curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1)'); //change the UA to spoof IE7.
		$output = str_replace("    ", TAB, curl_exec($ch)); // $output contains the output string
		curl_close($ch); // close curl resource to free up system resources
		
		write_log_by_ref($output); // write to LOG
		return (trim($output)!=''?$output:'<error>Empty XML</error>');
	}

	function write_log_by_ref(&$line , $mode = "w") {
		if (!defined ("LOGFILE")) return;
		if (!$handle = fopen(LOGFILE, $mode)) return trigger_error("Cannot open file: '".LOGFILE."'.",E_USER_WARNING);
		if (!fwrite($handle, date("o.m.d-H:i:s\t").$line.PHP_EOL)) return trigger_error("Cannot write to file: '".LOGFILE."'.",E_USER_WARNING);
		fclose($handle);
	}
	function write_log($line) { write_log_by_ref($line , "a");}

	echo PARSER_LINEBREAK." WunderGround <a href='http://www.wunderground.com/?apiref=26b54f2c100c03e2' target='_blank'>http://www.wunderground.com</a>";

	
	/////////////////////////////////////////////// MAIN START /////////////////////////////////////////////// 
	if(count($sensorsToParse) > 0) {
		echo PARSER_LINEBREAK." WunderGround <a href='".$url."' target='_blank'>Conditions</a>".PARSER_LINEBREAK;
		$XML = new SimpleXMLElement(get_url ($url));
		
		write_log(str_repeat("#", 80)); // write to LOG

		echo PARSER_LINEBREAK." Location: ".trim(parse_xml($XML, "display_location/full")).PARSER_LINEBREAK;
		echo PARSER_LINEBREAK." Observation location: ".trim(parse_xml($XML, "observation_location/full")).PARSER_LINEBREAK;
		
		foreach($sensorsToParse as $serial => $sensor) {
		$rawvalue = trim(parse_xml($XML, $sensor["path"]));
			$type = (isset($sensor["type"])?$sensor["type"]:"temp_c");
			$value =(($type == "temp_c") ? $rawvalue * 1 : $rawvalue);
			$value =(isset ($sensor["calc"]) ?
					(is_callable ($sensor["calc"])?$sensor["calc"]($value):$value * $sensor["calc"]) :
					$value);
			$data = array(
							"sensor_serial" => "WG_".$serial, 
							$type => $value , 
							"date" => "now"
						);

			echo "<hr/>Found: ".$serial." (".$sensor["path"].")".": ".$rawvalue.PARSER_LINEBREAK;
			write_log ($serial.TAB.$sensor["path"].TAB.$rawvalue.TAB.$value); // write to LOG
			
			if(defined("PARSER_SIMULATE") && PARSER_SIMULATE) // We are running in simulation mode
			{
				echo "<pre>Simulating data insertion: ".($rawvalue==SENSORERROR?"IGNORED":"").PARSER_LINEBREAK;
				print_r($data);
				echo "</pre>".PARSER_LINEBREAK;
			} else {
				if ($rawvalue != SENSORERROR) writeReading($data);
			}
		}
	}
	else
	{
		echo "No sensors configured in 'system_config/wunderground.php'".PARSER_LINEBREAK;
	}
	echo PARSER_LINEBREAK;
/////////////////////////////////////////////// MAIN END /////////////////////////////////////////////// 
?>

