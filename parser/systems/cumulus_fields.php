<?php

/* compatibility tested with v1.9.2 */

$cumulusFields = array();

// 			Field	Fieldname						Example
$cumulusFields[0] = "date";							// dd.mm.yy	date
$cumulusFields[1] = "time";							// 16:03:45	time
$cumulusFields[2] = "outsideTemperature";			// 8.4		outside temperature
$cumulusFields[3] = "relativeHumidity";				// 84		relative humidity relative humidity
$cumulusFields[4] = "dewpoint";						// 5.8		dewpoint
$cumulusFields[5] = "windSpeedAverage";				// 24.2		wind speed (average)
$cumulusFields[6] = "latestWindSpeed";				// 33.0		latest wind speed reading
$cumulusFields[7] = "windBearing";					// 261		wind bearing (degrees)
$cumulusFields[8] = "currentRainRate";				// 0.0		current rain rate
$cumulusFields[9] = "rainToday";					// 1.0		rain today
$cumulusFields[10] = "barometer";					// 999.7	barometer
$cumulusFields[11] = "windDirection";				// W		wind direction
$cumulusFields[12] = "windSpeedBeaufort";			// 6		wind speed (beaufort)
$cumulusFields[13] = "windUnits";					// mph		wind units
$cumulusFields[14] = "temperatureUnits";			// C		temperature units
$cumulusFields[15] = "pressureUnits";				// mb		pressure units
$cumulusFields[16] = "rainUnits";					// mm		rain units
$cumulusFields[17] = "windRunToday";				// 146.6	wind run(today)
$cumulusFields[18] = "pressureTrendValue";			// +0.1		pressure trend value
$cumulusFields[19] = "monthlyRainfall";				// 85.2		monthly rainfall
$cumulusFields[20] = "yearlyRainfall";				// 588.4	yearly rainfall
$cumulusFields[21] = "yesterdayRainfall";			// 11.6		yesterday's rainfall
$cumulusFields[22] = "insideTemperature";			// 20.3		inside temperature
$cumulusFields[23] = "insideHumidity";				// 57		inside humidity
$cumulusFields[24] = "windChill";					// 3.6		wind chill
$cumulusFields[25] = "temperatureTrendValue";		// -0.7		temperature trend value
$cumulusFields[26] = "todayHighTemp";				// 10.9		today's high temp
$cumulusFields[27] = "todayHighTempTime";			// 12:00	time of today's high temp (hh:mm)
$cumulusFields[28] = "todayLowTemp";				// 7.8		today's low temp
$cumulusFields[29] = "todayLowTempTime";			// 14:41	time of today's low temp (hh:mm)
$cumulusFields[30] = "todayHighWindSpeed";			// 37.4		today's high wind speed (average)
$cumulusFields[31] = "todayHighWindSpeedTime";		// 14:38	time of today's high wind speed (average) (hh:mm)
$cumulusFields[32] = "todayWindGust";				// 44.0		today's high wind gust
$cumulusFields[33] = "todayWindGustTime";			// 14:28	time of today's high wind gust (hh:mm)
$cumulusFields[34] = "todayHighPressure";			// 999.8	today's high pressure
$cumulusFields[35] = "todayHighPressureTime";		// 16:01	time of today's high pressure (hh:mm)
$cumulusFields[36] = "todayLowPressure";			// 998.4	today's low pressure
$cumulusFields[37] = "todayLowPressureTime";		// 12:06	time of today's low pressure (hh:mm)
$cumulusFields[38] = "CumulusVersion";				// 1.8.2	Cumulus version
$cumulusFields[39] = "CumulusBuildNumber";			// 448		Cumulus build number
$cumulusFields[40] = "10MinuteHighGust";			// 36.0		10-minute high gust
$cumulusFields[41] = "heatIndex";					// 10.3		heat index
$cumulusFields[42] = "humidex";						// 10.5		humidex
$cumulusFields[43] = "UVIndex";						// 13		UV Index
$cumulusFields[44] = "EvapotranspirationToday";		// 0.2		Evapotranspiration today
$cumulusFields[45] = "SolarRadiation";				// 14		Solar Radiation W/m2
$cumulusFields[46] = "10MinuteAverageWindBearing";	// 260		10-minute average wind bearing (degrees)
$cumulusFields[47] = "rainfallLastHour";			// 2.3		rainfall last hour
$cumulusFields[48] = "currentForecastNumber";		// 13		current forecast number (see samplestrings.ini). Negative means 'exceptional'
$cumulusFields[49] = "IsDayLight"; 					// 1		if currently within daylight hours, 0 if not 1
$cumulusFields[50] = "SensorContactLost";			// 0		if lost contact, if contact 1
$cumulusFields[51] = "windDirectionAverage";		// NNW		wind direction (average)
$cumulusFields[52] = "Cloudbase";					// 2040		Cloudbase
$cumulusFields[53] = "CloudbaseUnits";				// ft		Cloudbase units
$cumulusFields[54] = "ApparentTemperture";			// 12.3		Apparent Temperature
$cumulusFields[55] = "SunshineHoursToday";			// 11.1		Sunshine hours so far today
$cumulusFields[56] = "CurrTheoreticalMaxRadiation";	// 420.1	Current theoretical max solar radiation
$cumulusFields[57] = "IsSunny";						// 1		Is it sunny? 1 if the sun is shining, otherwise 0

?>