<?php
// HomeAutomation - sensor input parser
// Copyright (C) 2021 Tom Rosenback (tom.rosenback@gmail.com)
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

// Yocto-Light sensors
// See http://www.yoctopuce.com/EN/products/usb-sensors/yocto-light

	//$availableSensors = readTelldusDuo();

	if(count($sensors) > 0) {
		if(is_array($sensors)) {
			foreach($sensors as $sensor)
			{
				$command = $yoctolightPath."/YLightSensor ".$sensor." get_currentvalue";
				exec($command, $output);

				//echo $output[0];

				$value = 1*substr($output[0], strpos($output[0], "=")+2);

				//echo $value;

				$sensorReading = array("sensor_serial" => $sensor,
							"temp_c"	=> $value,
							"date"		=> "now");

				echo "Found: ".$sensor.", value: ".$value.PARSER_LINEBREAK;

				writeReading($sensorReading);
			}
		}
		else
		{
			echo "No sensors configured, please check your configuration".PARSER_LINEBREAK;
		}
	}
	else
	{
		echo "No sensors found or Yocto-Light not available".PARSER_LINEBREAK;
	}

?>
