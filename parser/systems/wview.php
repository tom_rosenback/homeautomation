<?php
// HomeAutomation - sensor input parser
// Copyright (C) 2021 Tom Rosenback (tom.rosenback@gmail.com)
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

// Wview

	if(file_exists($fileToParse)) {
		echo "Parsing '".$fileToParse."'".PARSER_LINEBREAK;
		$dataLines = file($fileToParse);

		$date = "";
		$time = "";
			
		foreach($dataLines as $line) {
			$tmp = explode("=", $line);
			
			if(count($tmp) == 2)
			{
				$param = trim($tmp[0]);
				$value = $tmp[1];
				
				if($param == "stationDate")
				{
					// stationDate=mm/dd/yy
					$date = explode($dateFormat["separator"], $value);
					$date = trim($date[$dateFormat["y"]])."-".trim($date[$dateFormat["m"]])."-".trim($date[$dateFormat["d"]]);
					echo "Date: ".$value.PARSER_LINEBREAK;
				}
				
				if($param == "stationTime")
				{
					// stationTime=hh:mm:ss
					$time = $value;
					echo "Time: ".$value.PARSER_LINEBREAK;
				}
				
				$dateTime = "now";
				
				if($date != "" && $time != "")
				{
					$dateTime = $date." ".$time;
				}
				
				if(in_array($param, $valuesToParse))
				{
					// outsideTemp=10.0
					
					$data = array(
									"sensor_serial" => $param,
									"temp_c" => (trim($value) * 1),
									"date" => $dateTime
								);
					
					echo "Found: ".$param.", value: ".$value.PARSER_LINEBREAK;
					
					if(defined("PARSER_SIMULATE") && PARSER_SIMULATE) // We are running in simulation mode
					{
						echo "<pre>Simulating data insertion:";
						print_r($data);
						echo "</pre>".PARSER_LINEBREAK;
					}
					else
					{
						writeReading($data);
					}
				}
			}
		}
		
		echo "Parsing of '".$fileToParse."' completed".PARSER_LINEBREAK;
	}
	else
	{
		echo "Can�t find '".$fileToParse."'".PARSER_LINEBREAK;
	}
?>
