<?php
// HomeAutomation - sensor input parser
// Copyright (C) 2021 Tom Rosenback (tom.rosenback@gmail.com)
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

// Telldus Duo sensors

	$availableSensors = readTelldusLive();

	if(count($availableSensors) > 0) {
		if(is_array($whitelistedSensors)) {
			foreach($availableSensors as $sensor)
			{
				if(in_array($sensor["sensor_serial"], $whitelistedSensors))
				{
					echo "Found: ".$sensor["sensor_serial"].", value: ".$sensor["temp_c"].PARSER_LINEBREAK;

					if(defined("PARSER_SIMULATE") && PARSER_SIMULATE) // We are running in simulation mode
					{
						echo "<pre>Simulating data insertion:";
						print_r($sensor);
						echo "</pre>".PARSER_LINEBREAK;
					}
					else
					{
						writeReading($sensor);
					}
				}
				else
				{
					echo "Sensor with serial '".$sensor["sensor_serial"]."' not whitelisted in configuration, skipping".PARSER_LINEBREAK;
				}
			}
		}
		else
		{
			echo "No sensors whitelisted, please check your configuration".PARSER_LINEBREAK;
		}
	}
	else
	{
		echo "No sensors found or Telldus DUO not available".PARSER_LINEBREAK;
	}

	function readTelldusLive() {
		$debug = false;
		// $debug = true;

		$output = array();

		if($debug) {
			// Debug reading from textfile instead of from tellduslive
			$output = file(PARSER_DIR."/example_logfiles/tellduslive/output.txt");
		}
		else
		{
			$tellduslive = SystemPlugins::load("telldus_live");

			if(!is_array($tellduslive) && is_a($tellduslive, "telldus_live"))
			{
				$availableSensors = $tellduslive->readSensors();
			}
			else
			{
				echo "Systemplugin for Tdtool not found".PARSER_LINEBREAK;
			}
		}

		$availableSensors = array();

		foreach($output as $row) {
			if($row != "")
			{
				$rowData = explode("\t", $row);

				$availableValues = array();
				$protocol = "";
				$model = "";
				$id = 0;
				$time = date('Y-m-d H:i:s');

				foreach($rowData as $value)
				{
					$splittedvalue = explode("=", $value);
					switch($splittedvalue[0])
					{
						case "protocol":
							$protocol = $splittedvalue[1];
							break;
						case "id":
							$id = $splittedvalue[1];
							break;
						case "model":
							$model = $splittedvalue[1];
							break;
						case "time":
							$time = $splittedvalue[1];
							break;
						case "temperature":
						case "humidity":
						case "winddirection":
						case "windaverage":
						case "windgust":
						case "rainrate":
							$availableValues[] = array("name"=>$splittedvalue[0],"value"=>$splittedvalue[1]);
							break;
						case "raintotal":
							$availableValues[] = array("name"=>$splittedvalue[0],"value"=>$splittedvalue[1],"calculateValues"=>true);
							break;
					}
				}

				$haBaseId = $protocol."-".$model."-".$id;

				//print_r($rowData);

				foreach($availableValues as $value)
				{
					$haId = $haBaseId;
					if($value["name"] != "temperature")
					{
						$haId .= "-".$value["name"];
					}
					//echo "Writing id: ".$haId.", value: ".$value["value"].", time: ".$time.".\n";

					$availableSensors[] = array("sensor_serial" => $haId,
												"temp_c"		=> $value["value"],
												"calculateValues"	=> $value["calculateValues"],
												"date" 			=> $time);
				}
			}
		}

		return $availableSensors;
	}


?>
