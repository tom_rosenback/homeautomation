<?php
// HomeAutomation - sensor input parser
// Copyright (C) 2021 Tom Rosenback (tom.rosenback@gmail.com)
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

// Cumulus

	if(file_exists($fileToParse)) {
		echo "Parsing '".$fileToParse."'".PARSER_LINEBREAK;
		
		if(file_exists(PARSER_DIR."/systems/cumulus_fields.php")) {
			include(PARSER_DIR."/systems/cumulus_fields.php");
			$dataLines = file($fileToParse);

			$date = "";
			$time = "";
				
			foreach($dataLines as $line)
			{
				$line = explode(" ", trim($line));
				
				// echo "<pre>";
				// print_r($line);
				// echo "</pre>".PARSER_LINEBREAK;
				
				if(count($line) == count($cumulusFields))
				{
					$date = explode($dateFormat["separator"], $line[0]);
					$date = trim($date[$dateFormat["y"]])."-".trim($date[$dateFormat["m"]])."-".trim($date[$dateFormat["d"]]);
					echo "Date: ".$date.PARSER_LINEBREAK;
					
					$time = trim($line[1]);
					echo "Time: ".$time.PARSER_LINEBREAK;
					
					$dateTime = "now";
				
					if($date != "" && $time != "")
					{
						$dateTime = $date." ".$time;
					}
					
					foreach($fieldsToParse as $field)
					{
						if(array_key_exists($field, $line))
						{
							$value = $line[$field];
							$param = $cumulusFields[$field];
							
							$data = array(
											"sensor_serial" => $param,
											"temp_c" => (trim($value) * 1),
											"date" => $dateTime
										);
							
							echo "Found: ".$param.", value: ".$value.PARSER_LINEBREAK;
							
							if(defined("PARSER_SIMULATE") && PARSER_SIMULATE) // We are running in simulation mode
							{
								echo "<pre>Simulating data insertion:";
								print_r($data);
								echo "</pre>".PARSER_LINEBREAK;
							}
							else
							{
								writeReading($data);
							}
						}
						else
						{
							echo "Invalid field: ".$key.PARSER_LINEBREAK;
						}
					}
				}
				else
				{
					echo "Number of fields in '".$fileToParse."' (".count($line).") doesn�t match 'systems/cumulus_fields.php' (".count($cumulusFields)."), change both files to match system.".PARSER_LINEBREAK."HINT: Check cumulus file before doing anything else.".PARSER_LINEBREAK;
				}
			}
			
			echo "Parsing of '".$fileToParse."' completed".PARSER_LINEBREAK;
		}
		else
		{
			echo "Can�t find 'system_config/cumulus_fields.php'".PARSER_LINEBREAK;
		}
	}
	else
	{
		echo "Can�t find '".$fileToParse."'".PARSER_LINEBREAK.PARSER_LINEBREAK;
	}
?>
