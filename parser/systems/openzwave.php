<?php
// HomeAutomation - sensor input parser
// Copyright (C) 2021 Tom Rosenback (tom.rosenback@gmail.com)
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

// Open-ZWave sensors
// See http://www.openzwave.com

class ZwaveServer {

    private $socket;

    function __construct($host, $port) {

        $this->socket = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
        if ($this->socket === false)
            echo "error";
        else {
            $result = socket_connect($this->socket, $host, $port);

            if ($result === false) {
                echo "error " . socket_strerror(socket_last_error($this->socket));
            }
        }
    }

    function read() {
        return socket_read($this->socket, 2048);
    }

    function send($data) {
        socket_write($this->socket, $data, strlen($data.chr(0)));
    }

    function close() {
        socket_close($this->socket);
    }

}


$zwaveServer = new ZwaveServer($server, $port);
$zwaveServer->send("SENSORLIST");
$list = $zwaveServer->read();
$list = substr($list, 0, strlen($list) - 1);
//$sensors = array();
$devicesList = explode("#", $list);
foreach ($devicesList as $device) {
	$device = explode("~", $device);
	if(!is_array($blacklistedSensors) || !in_array($device["1"], $blacklistedSensors)) {
		$sensors = array("temp_c"=>$device["2"], "sensor_serial"=>$device["1"], "date"=>"now");

		echo "Found: ".$device["0"].", value: ".$device["2"].PARSER_LINEBREAK;

		writeReading($sensors);
	}
}

$zwaveServer->close();

?>
