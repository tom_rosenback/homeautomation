<?php
// HomeAutomation - sensor input parser
// Copyright (C) 2021 Tom Rosenback (tom.rosenback@gmail.com)
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

	$data = fetchYrData($url."/forecast.xml");

	if($data != "") {
		$xml = new SimpleXMLElement($data);
		$node = $xml->xpath("/weatherdata/forecast/tabular/time[1]");

		if(is_array($node) && is_array($sensors)) {
			$node = $node[0];

			foreach($sensors as $sensor) {
				$sNode = $node->xpath($sensor);

				if(is_array($sNode)) {
					$attribute = $sNode[0]->attributes()->value;

					if(is_object($attribute)) {
						$value = (double)$attribute[0];

						echo "Found: '".$sensor."', value: ".$value.PARSER_LINEBREAK;

						$sensorReading = array("sensor_serial" => "yr-forecast-".$sensor,
									"temp_c"	=> $value,
									"date"		=> "now");

						writeReading($sensorReading);
					} else {
						echo "Could not find value for sensor '".$sensor."'".PARSER_LINEBREAK;
					}
				} else {
					echo "Could not find sensor '".$sensor."'".PARSER_LINEBREAK;
				}
			}
		} else {
			echo "No sensors configured, please check your configuration".PARSER_LINEBREAK;
		}
	} else if($data !== false) {
		echo "Could not find any data at ".$url."/forecast.xml".PARSER_LINEBREAK;
	}

	function fetchYrData($xmlPath) {
		$data = false;
		$cachePath = PARSER_DIR."/cache/yr_data/";
		$cacheTime = 10 * 60; // 10 minutes
		$cacheFile = base64_encode($xmlPath).".xml";

		if(!defined("YR_IGNORE_CACHE") && file_exists($cachePath.$cacheFile) && filemtime($cachePath.$cacheFile) + $cacheTime > date("U")) {
				echo "Only ".(date("U") - filemtime($cachePath.$cacheFile))." seconds since last read, minimum interval ".$cacheTime." seconds. Skipping.".PARSER_LINEBREAK;
				//$data = file_get_contents($cachePath.$cacheFile);
		} else {
			$data = readFileOrUrl($xmlPath);

			if (!file_exists($cachePath)) {
			  mkdir($cachePath, 0777, true);
			}

			if (file_exists($cachePath)) {
				file_put_contents($cachePath.$cacheFile, $data);
			}
		}

		return $data;
	}

?>
