<?php
// HomeAutomation - sensor input parser
// Copyright (C) 2021 Tom Rosenback (tom.rosenback@gmail.com)
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

// Calculations

	if(count($calculations) > 0) {
		foreach($calculations as $calcId => $dataArr) {
			$sensorValues = array();
			$error = false;
			
			// read every sensor given first
			foreach($dataArr["sensors"] as $sensor => $sensorPath)
			{
				if(file_exists($sensorPath))
				{	
					$value = readSensorValue($sensorPath);
					
					if($filteringEnabled && $value == 85)
					{
						$error = true;
						echo "Sensor: ".$sensor.", value was filtered out. Value: ".$value.PARSER_LINEBREAK; 
					}
					else
					{
						echo "Sensor: ".$sensor.", value: ".$value.PARSER_LINEBREAK;
				
						$dataArr["formula"] = str_replace($sensor, $value, $dataArr["formula"]);
					}
				}
				else
				{
					echo "Could not read sensor: ".$sensor.PARSER_LINEBREAK;
					$error = true;
				}
			}
			
			$compute = create_function("", "return (".$dataArr["formula"].");" );
			$result = $compute() * 1;
			
			if(!$error && is_numeric($result))
			{				
				$data = array(
								"sensor_serial" => $calcId,
								"temp_c" => (trim($result) * 1),
								"date" => "now"
							);
				
				
				echo "Calculated (".$calcId."): ".$dataArr["formula"]." = ".$result.PARSER_LINEBREAK;
					
				if(defined("PARSER_SIMULATE") && PARSER_SIMULATE) // We are running in simulation mode
				{
					echo "<pre>Simulating data insertion:";
					print_r($data);
					echo "</pre>".PARSER_LINEBREAK;
				}
				else
				{
					writeReading($data);
				}
			}
			else
			{
				echo "Invalid calculation data: '".$calcId."'".PARSER_LINEBREAK;
			}
		}
	}
	else
	{
		echo "No sensors configured in 'system_config/owfs.php'".PARSER_LINEBREAK;
	}
?>
