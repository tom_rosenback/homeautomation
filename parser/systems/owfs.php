<?php
// HomeAutomation - sensor input parser
// Copyright (C) 2021 Tom Rosenback (tom.rosenback@gmail.com)
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

// OWFS

	if(count($sensorsToParse) > 0) {
		foreach($sensorsToParse as $serial => $sensorPath) {
			if(file_exists($sensorPath))
			{				
				$value = readSensorValue($sensorPath);
				
				if($filteringEnabled && $value == 85)
				{
					echo "Found: ".$serial.", value was filtered out. Value: ".$value.PARSER_LINEBREAK; 
				}
				else
				{
					$data = array(
									"sensor_serial" => $serial,
									"temp_c" => (trim($value) * 1),
									"date" => "now"
								);
					
					
					echo "Found: ".$serial.", value: ".$value.PARSER_LINEBREAK;
						
					if(defined("PARSER_SIMULATE") && PARSER_SIMULATE) // We are running in simulation mode
					{
						echo "<pre>Simulating data insertion:";
						print_r($data);
						echo "</pre>".PARSER_LINEBREAK;
					}
					else
					{
						writeReading($data);
					}
				}
			}
			else
			{
				echo "Invalid sensor path for sensor with serial: '".$serial."'".PARSER_LINEBREAK;
			}
		}
	}
	else
	{
		echo "No sensors configured in 'system_config/owfs.php'".PARSER_LINEBREAK;
	}
?>
