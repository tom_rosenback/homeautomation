<?php
// HomeAutomation - sensor input parser v0.7.5
// Copyright (C) 2021 Tom Rosenback (tom.rosenback@gmail.com)
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

	// AS of v0.7 the Parser MUST be located in the HA root folder

	define("PARSER_DIR", dirname(__FILE__));

	// HA common includes
	include(PARSER_DIR."/../includes.php");

	$lineBreak = "<br />";

	if(php_sapi_name() == "cli" && empty($_SERVER["REMOTE_ADDR"])) {
		$lineBreak = "\n";
	}

	define("PARSER_LINEBREAK", $lineBreak);

	$systems = array();

	include(PARSER_DIR."/parser_config.php");
	include(PARSER_DIR."/functions.php");

	if(count($systems) > 0) {
		foreach($systems as $system) {
			parseSystemFile($system);
		}
	} else {
		echo "No system(s) configured".PARSER_LINEBREAK.PARSER_LINEBREAK;
	}

	echo "Completed";
?>
