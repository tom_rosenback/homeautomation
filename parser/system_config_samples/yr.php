<?php

	// To find the URL that matches your location go to yr.no, search your location, if many options are presented, select one, copy that URL here.
	$url = "http://www.yr.no/place/Sweden/Stockholm/Stockholm";
	// If the parser can´t find any data, go to http://om.yr.no/verdata/xml/ to find out what the problem could be. The parser automatically adds /forecast.xml to the URL so don´t add that.

	// sensors as they are named in the data
	$sensors[] = "pressure";

?>
