<?php
/* wview specific config */

	// absolute path to log file
	$fileToParse = "/path/logfile.txt"; 

	// an array of values to parse, stationDate and stationTime MUST NOT be in this array, only sensor values
	$valuesToParse = array(
							"outsideTemp",
							"insideTemp"
						);
						
	//date format, indexed from 0-2, example date mm/dd/yy
	$dateFormat["d"] = 1;
	$dateFormat["m"] = 0;
	$dateFormat["y"] = 2;
	$dateFormat["separator"] = "/";
						
/*
	if NOT both of stationDate and stationTime are found current date and time 
	when the script is executed will be used as log date/time
	
	example contents of logfile
	/////////////////// FILE STARTS HERE //////////////////////////
		stationDate=mm/dd/yy
		stationTime=15:04:10
		outsideTemp=10.0
		insideTemp=24.3
	/////////////////// FILE END HERE ////////////////////////////
*/

?>