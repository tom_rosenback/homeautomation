<?php
/* logtemp specific config */

	// the path to the logtemp sensor files
	$logtempPath = "X:/logtemp/";
	
	//date format, indexed from 0-2, example date dd.mm.yy
	$dateFormat["d"] = 0;
	$dateFormat["m"] = 1;
	$dateFormat["y"] = 2;
	$dateFormat["separator"] = ".";
	
	// an array of sensors to parse, where xyz is the sensor serial eg. 1026CA23010800A7
	// if you have several sensors, just duplicate the row below and change xyz to match the sensor serials for each sensor
	$sensorsToParse[] = "0B0000000C8E831D";
	$sensorsToParse[] = "E3000800ACC6A110";
	
	// When using 1-wire systems the readout of the sensor can sometimes be faulty, enable this if you experience any problems. A faulty value is equal to 85, without decimals.
	$filteringEnabled = true;
	
/*
	example contents of logfile
	/////////////////// FILE STARTS HERE //////////////////////////
	Date; Time; Temp �C
	09.08.2011; 21:29:23;25.38
	/////////////////// FILE END HERE ////////////////////////////
*/

?>