<?php
/* RPi GPIO specific config */

	// an array of sensors to parse, exact paths down to file level MUST be configured, eg. /mnt/1wire/xyz/temperature, where xyz is the sensor serial eg. 1026CA23010800A7	
	// if you have several sensors, just duplicate the row below and change xyz to match the sensor serials for each sensor
	// $sensorsToParse["xyz"] = "/sys/bus/w1/devices/xyz/w1_slave";
	// $sensorsToParse["err"] = "/sys/bus/w1/devices/err/w1_slave";
	
	// When using 1-wire systems the readout of the sensor can sometimes be faulty, enable this if you experience any problems. A faulty value is equal to 85, without decimals.
	$filteringEnabled = true;
	
/*
	example contents of logfile
	/////////////////// FILE STARTS HERE //////////////////////////
	6f 01 4b 46 7f ff 01 10 67 : crc=67 YES
	6f 01 4b 46 7f ff 01 10 67 t=22937
	/////////////////// FILE END HERE ////////////////////////////
*/

?>