<?php
/* owfs specific config */

	// an array of sensors to parse, exact paths down to file level MUST be configured, eg. /mnt/1wire/xyz/temperature, where xyz is the sensor serial eg. 1026CA23010800A7	
	// if you have several sensors, just duplicate the row below and change xyz to match the sensor serials for each sensor
	$sensorsToParse["xyz"] = "/mnt/1wire/xyz/temperature";
	$sensorsToParse["err"] = "/mnt/1wire/err/temperature";
	
	// When using 1-wire systems the readout of the sensor can sometimes be faulty, enable this if you experience any problems. A faulty value is equal to 85, without decimals.
	$filteringEnabled = true;
	
/*
	example contents of logfile
	/////////////////// FILE STARTS HERE //////////////////////////
	24.35
	/////////////////// FILE END HERE ////////////////////////////
*/

?>