<?php
/* calculations specific config */

	// an array of sensors to make the calculations from are given, exact paths down to file level MUST be configured, eg. /mnt/1wire/abc/temperature
	// a formula how to calculate the result, the sensor will be inserted as "venteffiency" in the example, but this need to be changed for every calculation
	// the name of the sensor needs to match the formula
	$calculations["venteffiency"] = array(
											"sensors" => array(
												"outlet"	=> "/mnt/1wire/abc/temperature",
												"inlet"		=> "/mnt/1wire/def/temperature",
												"outside"	=> "/mnt/1wire/ghi/temperature"
											),
											"formula" => "(outlet - outside) / (inlet - outside)"
										);
										
	// When using 1-wire systems the readout of the sensor can sometimes be faulty, enable this if you experience any problems. A faulty value is equal to 85, without decimals.
	$filteringEnabled = true;
	
/*
	example contents of logfile for each sensor
	/////////////////// FILE STARTS HERE //////////////////////////
	24.35
	/////////////////// FILE END HERE ////////////////////////////
*/

?>