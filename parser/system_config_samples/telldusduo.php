<?php
/* telldus duo specific config */

	// In some cases you might want to blacklist a sensor (eg. if your Duo catches a signal from an unknown sensor)
	// To white list a sensor you have to compose a sensor serial. The sensor serial is composed by putting 
	// protocol, model, id (found from tdtool --list output) together with - in between, eg. oregon-ea4c-71 (protocol-model-id).
	// The default value being parsed is the temperature, if one wants to parse humidity the sensor serial has to end with '-humidity', eg. oregon-ea4c-71-humidity (protocol-model-id).
	// So if both temperature and humidity is to be parsed for a given sensor two rows of sensor serials has to be configured.	
	
	// uncomment a line below and edit the sensor serial, add more lines if needed. 
	// $whitelistedSensors[] = "oregon-ea4c-71";
	// $whitelistedSensors[] = "oregon-ea4c-71-humidity";
	// $whitelistedSensors[] = "fineoffset-temperature-255";
	// $whitelistedSensors[] = "mandolyn-temperaturehumidity-11";
	// $whitelistedSensors[] = "mandolyn-temperaturehumidity-11-humidity";
	
/*
	example contents of output from 'tdtool --list' command
	/////////////////// FILE STARTS HERE //////////////////////////
	Number of devices: 1
	1       1DatarumA       OFF
	

	SENSORS:

	PROTOCOL                MODEL                   ID      TEMP    HUMIDITY       L                                                                                                                                                             AST UPDATED
	oregon                  EA4C                    71      11.5�                 2                                                                                                                                                             012-08-28 18:15:57
	fineoffset              temperature             255     -204.7�                                                                                                                                                                             2012-08-28 18:14:54

	/////////////////// FILE END HERE ////////////////////////////
*/

$whitelistedSensors[] = "fineoffset-temperature-35";
$whitelistedSensors[] = "fineoffset-temperature-1";

?>
