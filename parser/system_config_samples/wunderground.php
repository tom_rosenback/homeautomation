<?php
	/*
	wunderground specific configuration

	This parser uses (if no APY key is defined) the old feed.
	However this feed will be deprecated.
	Please switch to http://www.wunderground.com/weather/api/

	You only need to request an APY key at:
	http://deutsch.wunderground.com/weather/api/d/docs
	It is free for requests under 500 per day and that should be more than needed.
	*/
	////////////////////////////////////////////////////////////////////////////////////
	// $parserConfig["apikey"]	= "-- api key here; needs to be requested --"; // MY
	// 'http://www.wunderground.com/?apiref=26b54f2c100c03e2'
	////////////////////////////////////////////////////////////////////////////////////

	/*
	use the query string to localize your request
	if the query is not set the parser will automatically evaluate your location via your IP address
	*/

	// $parserConfig["query"]	= "Cologne,Germany";	// query: town and country
	// $parserConfig["query"]	= "pws:INRWKLN2";		// query: personal weather station id
	
	$latitude  = $_SESSION[CFG_SESSION_KEY]["settings"]["latitude"];
	$longitude = $_SESSION[CFG_SESSION_KEY]["settings"]["longitude"];
	if ($latitude.$longitude != "") $parserConfig["query"]	= $latitude.",".$longitude;

	// $parserConfig["query"]	= "50.849773,7.099825";	// query by latitude,longitude

/*
	An array of sensors to parse, where "path" is the XML tag you want.
	"path" => "temp_c" will return the valuably of the first occurrence of <temp_c> within the 
	if you have several sensors, just duplicate the row below and change xyz to match the sensor serials for each sensor
	
	$sensorsToParse["xyz"]	// XYZ Name of the sensor
	array ( 	"path" => "wind_mph",		// XML Path within the log-file
				"type" => "temp_c" ,		// sensor type
				"calc" => "1.609344 ");	// corrective factor e.g. 1 mile =  1.609344 km 
										// use 1 to enforce numeric output for "temp_c",
	
	$sensorsToParse["windchill"]	= array ( "path" => "wind_mph"					, "type" => "temp_c" , "calc" => "1.609344 ");
	$sensorsToParse["xyz"]			= array ( "path" => "temp_c" 						, "type" => "temp_c");
	$sensorsToParse["xyz"]			= array ( "path" => "current_observation/temp_c"	, "type" => "temp_c");
	$sensorsToParse["xyz"]			= array ( "path" => "display_location/full"		, "type" => "speedometer");
	
*/

	// set [calc] to an corrective factor ()  or set it to 1 to ensure numeric output
	// calc] can be a function too
	$feet_to_meter = 0.3048;
	$miles_to_km = 1.609344;
	$FtoC = function ($F) {return 5/9*$F-32;}; // Fahrenheit to Celsius
	$CtoF = function ($C) {return 9/5*$C+32;}; // Celsius to Fahrenheit
	$CtoK = function ($C) {return $C+273;}; // Kelvin  to Celsius
	$KtoC = function ($K) {return $K-273;}; // Celsius to Kelvin 
	
	$SensorTypes = array (
		array('Temperature','&deg;C','speedometer'),
		array('Humidity','%','speedometer'),
		array('Rainfall',' mm','speedometer'),
		array('Speed',' m/s','speedometer'),
		array('Bearing','&deg;','speedometer'),
		array('Speed',' km/h','speedometer'),
		array('Rainfall',' mm/h','speedometer'),
		array('Pressure','mb','speedometer'),
		array('Digital','','speedometer'));

	
	// default [type] is "temp_c" any other type is not yet supported
	// $sensorsToParse["humidity"]	= array ( "path" => "relative_humidity", "type" => "humidity", "calc" => 1	); 
	// $sensorsToParse["windspeed"]	= array ( "path" => "wind_mph", "type" => "speed_km", "calc" => $miles_to_km);
	
	/*
	$sensorsToParse["localtemp"]	= array (	"path" => "temp_c" );
	$sensorsToParse["localtempK"]	= array (	"path" => "temp_c",
												"calc" => $CtoK);
	$sensorsToParse["location"]		= array (	"path" => "display_location/full",
												"type" => "text");
	$sensorsToParse["location"]		= array (	"path" => "observation_location/full",
												"type" => "text");
	$sensorsToParse["weather"]		= array (	"path" => "weather");
	$sensorsToParse["humidity"]		= array (	"path" => "relative_humidity");
	$sensorsToParse["windchill"]	= array (	"path" => "windchill_c");
	$sensorsToParse["windspeed"]	= array (	"path" => "wind_mph",
												"calc" => $miles_to_km);
	$sensorsToParse["elevation"]	= array (	"path" => "observation_location/elevation",
												"calc" => $feet_to_meter);
	*/
	$sensorsToParse["temperature"]	= array (	"path" => "current_observation/temp_c");
	$sensorsToParse["humidity"]		= array (	"path" => "relative_humidity");
	$sensorsToParse["windspeed"]	= array (	"path" => "wind_kph");
	$sensorsToParse["winddir"]		= array (	"path" => "wind_degrees");
	$sensorsToParse["pressure"]		= array (	"path" => "pressure_mb");
	$sensorsToParse["precipitation"] = array (	"path" => "precip_1hr_metric");

/*
	example contents of logfile (non API)
	/////////////////// FILE STARTS HERE //////////////////////////
	<current_observation>
			<credit>Weather Underground NOAA Weather Station</credit>
			<credit_URL>http://wunderground.com/</credit_URL>
	<termsofservice link="http://www.wunderground.com/weather/api/d/terms.html">This feed will be deprecated. Please switch to http://www.wunderground.com/weather/api/</termsofservice>
			<image>
			<url>http://icons-ak.wxug.com/graphics/wu2/logo_130x80.png</url>
			<title>Weather Underground</title>
			<link>http://wunderground.com/</link>
			</image>
	  <display_location>
		  <full>Cologne, Germany</full>
		  <city>Cologne</city>
		  <state></state>
		  <state_name>Germany</state_name>
		  <country>DL</country>
		  <country_iso3166>DE</country_iso3166>
		  <zip>00000</zip>
		  <latitude>50.849773</latitude>
		  <longitude>7.099825</longitude>
		  <elevation>54.00000000 ft</elevation>
	 </display_location>
	<observation_location>
			<full>Koeln, </full>
			<city>Koeln</city>
			<state></state>
			<country>DL</country>
			<country_iso3166>DE</country_iso3166>
			<latitude>50.86455917</latitude>
			<longitude>7.15747023</longitude>
			<elevation>302 ft</elevation>
			</observation_location>
			<station_id>EDDK</station_id>
			<observation_time>Last Updated on March 6, 10:50 AM CET</observation_time>
			<observation_time_rfc822>Thu, 06 Mar 2014 09:50:00 GMT</observation_time_rfc822>
			<observation_epoch>1394099400</observation_epoch>
			<local_time>March 6, 11:18 AM CET</local_time>
			<local_time_rfc822>Thu, 06 Mar 2014 10:18:30 GMT</local_time_rfc822>
			<local_epoch>1394101110</local_epoch>
			<weather>Clear</weather>
			<temperature_string>45 F (7 C)</temperature_string>
			<temp_f>45</temp_f>
			<temp_c>7</temp_c>
			<relative_humidity>76%</relative_humidity>
			<wind_string>From the SE at 7 MPH </wind_string>
			<wind_dir>SE</wind_dir>
			<wind_degrees>140</wind_degrees>
			<wind_mph>7</wind_mph>
			<wind_gust_mph></wind_gust_mph>
			<pressure_string>30.42 in (1030 mb)</pressure_string>
			<pressure_mb>1030</pressure_mb>
			<pressure_in>30.42</pressure_in>
			<dewpoint_string>37 F (3 C)</dewpoint_string>
			<dewpoint_f>37</dewpoint_f>
			<dewpoint_c>3</dewpoint_c>
			
			<heat_index_string>NA</heat_index_string>
			<heat_index_f>NA</heat_index_f>
			<heat_index_c>NA</heat_index_c>
			
			
			<windchill_string>41 F (5 C)</windchill_string>
			<windchill_f>41</windchill_f>
			<windchill_c>5</windchill_c>
			
			<visibility_mi>5.0</visibility_mi>
			<visibility_km>8.0</visibility_km>
			<icons>
				<icon_set name="Default">
					<icon_url>http://icons-ak.wxug.com/i/c/a/clear.gif</icon_url>
				</icon_set>
				<icon_set name="Smiley">
					<icon_url>http://icons-ak.wxug.com/i/c/b/clear.gif</icon_url>
				</icon_set>
				<icon_set name="Generic">
					<icon_url>http://icons-ak.wxug.com/i/c/c/clear.gif</icon_url>
				</icon_set>
				<icon_set name="Old School">
					<icon_url>http://icons-ak.wxug.com/i/c/d/clear.gif</icon_url>
				</icon_set>
				<icon_set name="Cartoon">
					<icon_url>http://icons-ak.wxug.com/i/c/e/clear.gif</icon_url>
				</icon_set>
				<icon_set name="Mobile">
					<icon_url>http://icons-ak.wxug.com/i/c/f/clear.gif</icon_url>
				</icon_set>
				<icon_set name="Simple">
					<icon_url>http://icons-ak.wxug.com/i/c/g/clear.gif</icon_url>
				</icon_set>
				<icon_set name="Contemporary">
					<icon_url>http://icons-ak.wxug.com/i/c/h/clear.gif</icon_url>
				</icon_set>
				<icon_set name="Helen">
					<icon_url>http://icons-ak.wxug.com/i/c/i/clear.gif</icon_url>
				</icon_set>
				<icon_set name="Incredible">
					<icon_url>http://icons-ak.wxug.com/i/c/k/clear.gif</icon_url>
				</icon_set>
			</icons>
			<icon_url_base>http://icons-ak.wxug.com/graphics/conds/</icon_url_base>
			<icon_url_name>.GIF</icon_url_name>
			<icon>clear</icon>
			<forecast_url>http://www.wunderground.com/global/stations/10513.html</forecast_url>
			<history_url>http://www.wunderground.com/history/airport/EDDK/2014/3/6/DailyHistory.html</history_url>
			<ob_url>http://www.wunderground.com/cgi-bin/findweather/getForecast?query=50.86455917,7.15747023</ob_url>
		</current_observation>
	/////////////////// FILE END HERE ////////////////////////////
	


	example contents of logfile (API)
	/////////////////// FILE STARTS HERE //////////////////////////

<response>
	<version>0.1</version>
	<termsofService>http://www.wunderground.com/weather/api/d/terms.html</termsofService>
	<features>
		<feature>conditions</feature>
	</features>
  <current_observation>
		<image>
		<url>http://icons-ak.wxug.com/graphics/wu2/logo_130x80.png</url>
		<title>Weather Underground</title>
		<link>http://www.wunderground.com</link>
		</image>
		<display_location>
		<full>Cologne, Germany</full>
		<city>Cologne</city>
		<state></state>
		<state_name>Germany</state_name>
		<country>DL</country>
		<country_iso3166>DE</country_iso3166>
		<zip>00000</zip>
		<magic>16</magic>
		<wmo>10513</wmo>
		<latitude>50.849773</latitude>
		<longitude>7.099825</longitude>
		<elevation>54.00000000</elevation>
		</display_location>
		<observation_location>
		<full>Lind, Koeln, NRW</full>
		<city>Lind, Koeln</city>
		<state>NRW</state>
		<country>GERMANY</country>
		<country_iso3166>DE</country_iso3166>
		<latitude>50.849773</latitude>
		<longitude>7.099825</longitude>
		<elevation>177 ft</elevation>
		</observation_location>
		<estimated>
		</estimated>
		<station_id>INRWKLN2</station_id>
		<observation_time>Last Updated on March 6, 11:15 AM CET</observation_time>
		<observation_time_rfc822>Thu, 06 Mar 2014 11:15:07 +0100</observation_time_rfc822>
		<observation_epoch>1394100907</observation_epoch>
		<local_time_rfc822>Thu, 06 Mar 2014 11:17:44 +0100</local_time_rfc822>
		<local_epoch>1394101064</local_epoch>
		<local_tz_short>CET</local_tz_short>
		<local_tz_long>Europe/Berlin</local_tz_long>
		<local_tz_offset>+0100</local_tz_offset>
		<weather>Clear</weather>
		<temperature_string>49.6 F (9.8 C)</temperature_string>
		<temp_f>49.6</temp_f>
		<temp_c>9.8</temp_c>
		<relative_humidity>58%</relative_humidity>
		<wind_string>From the SE at 2.0 MPH Gusting to 5.0 MPH</wind_string>
		<wind_dir>SE</wind_dir>
		<wind_degrees>135</wind_degrees>
		<wind_mph>2.0</wind_mph>
		<wind_gust_mph>5.0</wind_gust_mph>
		<wind_kph>3.2</wind_kph>
		<wind_gust_kph>8.0</wind_gust_kph>
		<pressure_mb>1030</pressure_mb>
		<pressure_in>30.42</pressure_in>
		<pressure_trend>0</pressure_trend>
		
		<dewpoint_string>35 F (2 C)</dewpoint_string>
		<dewpoint_f>35</dewpoint_f>
		<dewpoint_c>2</dewpoint_c>
		
		
		<heat_index_string>NA</heat_index_string>
		<heat_index_f>NA</heat_index_f>
		<heat_index_c>NA</heat_index_c>
		
		
		<windchill_string>50 F (10 C)</windchill_string>
		<windchill_f>50</windchill_f>
		<windchill_c>10</windchill_c>
		
        <feelslike_string>50 F (10 C)</feelslike_string>
        <feelslike_f>50</feelslike_f>
        <feelslike_c>10</feelslike_c>
		<visibility_mi>5.0</visibility_mi>
		<visibility_km>8.0</visibility_km>
		<solarradiation></solarradiation>
		<UV>1.0</UV>
		<precip_1hr_string>0.00 in ( 0 mm)</precip_1hr_string>
		<precip_1hr_in>0.00</precip_1hr_in>
		<precip_1hr_metric> 0</precip_1hr_metric>
		<precip_today_string>0.00 in (0 mm)</precip_today_string>
		<precip_today_in>0.00</precip_today_in>
		<precip_today_metric>0</precip_today_metric>
		
		
		
		<icon>clear</icon>
		<icon_url>http://icons-ak.wxug.com/i/c/k/clear.gif</icon_url>
		<forecast_url>http://www.wunderground.com/global/stations/10513.html</forecast_url>
        
        <history_url>http://www.wunderground.com/weatherstation/WXDailyHistory.asp?ID=INRWKLN2</history_url>
        
		<ob_url>http://www.wunderground.com/cgi-bin/findweather/getForecast?query=50.849773,7.099825</ob_url>
	</current_observation>
</response>
	
	/////////////////// FILE END HERE ////////////////////////////
*/

?>