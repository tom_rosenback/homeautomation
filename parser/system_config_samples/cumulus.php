<?php
/* cumulus specific config */
/* compatibility tested with v1.9.2 */

	// absolute path to log file
	$fileToParse = "C:\\Program Files\\Cumulus\\realtime.txt"; 
						
	//date format, indexed from 0-2, example date dd.mm.yy
	$dateFormat["d"] = 0;
	$dateFormat["m"] = 1;
	$dateFormat["y"] = 2;
	$dateFormat["separator"] = ".";
	
	// check systems/cumulus_fields.php for fields and enter the values of the field that you want to import
	$fieldsToParse = array(	2, 3, 6, 7, 8, 22, 23, 9, 10 );
	
/*
	example contents of realtime
	/////////////////// FILE STARTS HERE //////////////////////////
		16.01.11 11:27:29 23.0 14 -9.6 0.0 0.0 90 0.0 0.0 1003.7 E 0 m/s C mb mm 0.0 0.0 0.0 0.0 0.0 22.1 15 23.0 0 23.0 11:05 0.0 00:00 0.0 00:00 0.0 00:00 1003.9 11:17 0.0 00:00 1.9.0 958 0.0 23.0 23.0 0 0 0 0 0.0 0 1 0 --- 13343 ft
	/////////////////// FILE END HERE ////////////////////////////

*/

?>