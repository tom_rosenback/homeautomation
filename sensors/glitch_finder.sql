SELECT t1.id, (SELECT t2.temp_c FROM temps t2 WHERE t1.sensor_serial = t2.sensor_serial AND t2.date < t1.date ORDER BY t2.date DESC LIMIT 1) AS pre, t1.temp_c, (SELECT t3.temp_c FROM temps t3 WHERE t1.sensor_serial = t3.sensor_serial AND t3.date > t1.date LIMIT 1) AS post
FROM temps t1
WHERE t1.temp_c = 85