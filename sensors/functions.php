<?php

function getTrend($newTemp, $oldTemp)
{
	// Prosedure witch tells us if the temperature is rising or falling.
	if ($newTemp < $oldTemp) // If the last reading is warmer than second last.
	{
		$trend = LBL_FALLING;
	}
	elseif ($newTemp > $oldTemp) // If the last reading is colder than second last.
	{
		$trend = LBL_RISING;
	}
	else // If the last and second last readings are the same.
	{
		$trend = LBL_NEUTRAL;
	}

	return $trend;
}

function formatDateTime($datetime, $returnOnlyTime = false, $returnOnlyTimeIfToday = false)
{
	$tmp = explode(" ", $datetime);
	$date = explode("-", $tmp[0]);
	$time = explode(":", $tmp[1]);
	$time = $time[0].":".$time[1];
	
	$date = ($date[2] * 1).".".($date[1] * 1).".".$date[0];
	
	if($returnOnlyTime)
	{
		return $time;
	}
	else if($returnOnlyTimeIfToday)
	{
		if(date("j.n.Y") == $date)
		{
			return "Idag @ ".$time;
		}
		else
		{
			return $date." ".$time;
		}
	}
	else
	{
		
		return $date." ".$time;
	}
}

function generateMinMaxTable($dataArray, $label)
{
	if(!is_array($dataArray))
	{
		$dataArray = array();
	}
	
	$table = "<table width=\"100%\" class=\"temp_table\">".
				"<tr>".
					"<td colspan=\"6\" class=\"label\">".
						$label.
					"</td>".
				"</tr>".
				"<tr>".
					"<td width=\"150px\" class=\"label\">Sensor</td>".
					"<td class=\"label\">Min</td>".
					"<td class=\"label\">Tidpunkt</td>".
					"<td class=\"label\">Max</td>".
					"<td class=\"label\">Tidpunkt</td>".
					"<td class=\"label\">&nbsp;</td>".
				"</tr>";
		
	foreach($dataArray as $sensor => $data)
	{
		$table .= 	"<tr>".
						"<td class=\"label\">".$sensor."</td>".
						"<td class=\"temp_text\"><font color=\"".getDynamicColor($data["min"])."\">".$data["min"]." &deg;C</font></td>".
						"<td class=\"temp_text\">".formatDateTime($data["min_date"])."</td>".
						"<td class=\"temp_text\"><font color=\"".getDynamicColor($data["max"])."\">".$data["max"]." &deg;C</font></td>".
						"<td class=\"temp_text\">".formatDateTime($data["max_date"])."</td>".
						"<td class=\"temp_text\">&nbsp;</td>".
					"</tr>";
	}
	
	$table .= "</table>";
	
	return $table;
}

function getSqlDate($date, $offsetYears, $offsetMinutes)
{
	$date = new DateTime($date);
	$date->modify($offsetYears." year");
	$date->modify($offsetMinutes." minute");
	
	return $date->format("Y-m-d H:i:s");
}

function getDynamicColor($temp)
{
	if ($temp == "-" || $temp == "" || !is_numeric($temp))
	{
	    $color = "#000000";
	}
	elseif ($temp < -0.00)
	{
	    $color = "#3333ff";
	}
	else 
	{
	    $color = "#ff0033";
	}

	return $color;	
}

function checkGraphData($data)
{
	$prev = "";
	
	for($i = 0; $i < count($data); $i++)
	{
		$current = $data[$i];
				
		if($current == "")
		{
			$current = $prev;
		}
		
		$data[$i] = $current;
		
		$prev = $current;
	}
	
	$prev = "";
	
	for($i = count($data) - 1; $i >= 0; $i--)
	{
		$current = $data[$i];
				
		if($current == "")
		{
			$current = $prev;
		}
		
		$data[$i] = $current;
		
		$prev = $current;
	}
	
	return $data;
}

function sec2time($sec, $returnSeconds = true, $returnMinutes = true, $returnHours = true, $padHours = false) 
{

	// holds formatted string
	$time = "";

	// there are 3600 seconds in an hour, so if we
	// divide total seconds by 3600 and throw away
	// the remainder, we've got the number of hours
	if($returnHours)
	{
		$hours = intval(intval($sec) / 3600); 

		// add to $time, with a leading 0 if asked for
		if($padHours)
		{
			$time .= str_pad($hours, 2, "0", STR_PAD_LEFT);
		}
		else
		{
			$time .= $hours;
		}
	} 

	// dividing the total seconds by 60 will give us
	// the number of minutes, but we're interested in 
	// minutes past the hour: to get that, we need to 
	// divide by 60 again and keep the remainder
	if($returnMinutes)
	{
		$minutes = intval(($sec / 60) % 60); 

		// then add to $time (with a leading 0 if needed)
		
		if($returnHours)
		{
			$time .= ":";
		}
		
		$time .= str_pad($minutes, 2, "0", STR_PAD_LEFT);
	}

	// seconds are simple - just divide the total
	// seconds by 60 and keep the remainder
	if($returnSeconds)
	{
		$seconds = intval($sec % 60); 

		if($returnMinutes)
		{
			$time .= ":";
		}
		
		// add to $time, again with a leading 0 if needed
		$time .= str_pad($seconds, 2, "0", STR_PAD_LEFT);
	}

	return $time;
}


?>