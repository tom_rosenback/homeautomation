<?php

function is_reading_valid($date, $interval = 24 * 60 * 60, $maxDiff = 14 * 24 * 60 * 60) {
	return (abs(date("U") - $interval - $date) < $maxDiff);
}

function generateTemperatureList()
{
	// temperature related functions
	//include ("sensors/functions.php");
	//include ("sensors/dbfunctions.php");

	// Getting date and server uptime for later use.
	$date = date("d.m.Y");

	// get the server statistics with "net statistics server" by shell_exec
	// $winstats = shell_exec("net statistics server");

	// grab the date & time the server started up
	// preg_match("(\d{1,2}/\d{1,2}/\d{4}\s+\d{1,2}\:\d{2}\s+\w{2})", $winstats, $matches);

	// convert the readable date & time to a timestamp and deduct it from the current timestamp
	// thus giving us the total uptime in seconds
	// $uptimeSecs = time() - strtotime($matches[0]);

	$list = "";

	// SQL-QUERIES

	$tempSensors = Sensors::get();
	$tempLoggingInfo = Sensors::getStats();

	// SQL-queries for maximum and minimum points.

	// min max temps
	// ["sensor1"]
	//		["min"]
	//		["max"]
	//		["min_date"]
	//		["max_date"]
	// ["sensor1"]
	//		["min"]
	//		["max"]
	//		["min_date"]
	//		["max_date"]

	// averages
	// ["sensor1"]
	//		["type1"]
	//			temp
	// ["sensor1"]
	//		["type2"]
	//			temp


	// actuals
	// ["sensor1"]
	//		["type1"]
	//			temp
	// ["sensor1"]
	//		["type2"]
	//			temp

	//actual temps
	$query = "SELECT s.name, st.unit, ROUND(last_reading, 1) AS actual FROM tempsensors s INNER JOIN sensortypes st ON st.id = s.sensortype";
	$actual = Sensors::getReadings($query, "act");

	//$query = "SELECT s.name, (SELECT ROUND(temp_c, 1) FROM temps t WHERE t.sensor_serial = s.serial ORDER BY date DESC LIMIT 1,1) AS temp FROM tempsensors s";
	//$query = "SELECT s.name, ROUND(temp_c, 1) FROM temps t INNER JOIN tempsensors s ON t.sensor_serial = s.serial WHERE UNIX_TIMESTAMP(NOW()) - UNIX_TIMESTAMP(date) < 1800 GROUP BY t.sensor_serial ORDER BY date DESC";
	$query = "SELECT s.name, st.unit, ROUND(temp_c, 1) as actual FROM temps t INNER JOIN tempsensors s ON t.sensor_serial = s.serial INNER JOIN sensortypes st ON st.id = s.sensortype WHERE date > DATE_SUB(NOW(), INTERVAL 1800 SECOND) GROUP BY t.sensor_serial ORDER BY date DESC";
	$actual_30min = Sensors::getReadings($query, "act_30min");

	// actual 1 year ago.
	// $query = "SELECT s.name, (SELECT ROUND(temp_c, 1) FROM temps t WHERE t.sensor_serial = s.serial AND date BETWEEN '".getSqlDate(dbParam::now(), -1, -5)."' AND '".getSqlDate(dbParam::now(), -1, 5)."' ORDER BY date DESC LIMIT 1) AS temp FROM tempsensors s";
	// $actual_1y = Sensors::getReadings($query, "act_1y");

	// actual 2 years ago.
	// $query = "SELECT s.name, (SELECT ROUND(temp_c, 1) FROM temps t WHERE t.sensor_serial = s.serial AND date BETWEEN '".getSqlDate(dbParam::now(), -2, -5)."' AND '".getSqlDate(dbParam::now(), -2, 5)."' ORDER BY date DESC LIMIT 1) AS temp FROM tempsensors s";
	// $actual_2y = Sensors::getReadings($query, "act_2y");
	// echo $query;
	// actual 3 years ago.
	// $query = "SELECT s.name, (SELECT ROUND(temp_c, 1) FROM temps t WHERE t.sensor_serial = s.serial AND date BETWEEN '".getSqlDate(dbParam::now(), -3, -5)."' AND '".getSqlDate(dbParam::now(), -3, 5)."' ORDER BY date DESC LIMIT 1) AS temp FROM tempsensors s";
	// $actual_3y = Sensors::getReadings($query, "act_3y");

	// 24 hour min/max.
	$query = "SELECT s.name, st.unit, MIN(ROUND(t.temp_c, 1)) AS min, MAX(ROUND(t.temp_c, 1)) AS max, (SELECT date FROM temps t2 WHERE t2.sensor_serial = t.sensor_serial AND t2.temp_c = MIN(t.temp_c) ORDER BY date DESC LIMIT 1) AS min_date, (SELECT date FROM temps t2 WHERE t2.sensor_serial = t.sensor_serial AND t2.temp_c = MAX(t.temp_c) ORDER BY date DESC LIMIT 1) AS max_date FROM temps t INNER JOIN tempsensors s ON t.sensor_serial = s.serial INNER JOIN sensortypes st ON st.id = s.sensortype WHERE date > DATE_SUB(NOW(), INTERVAL 86400 SECOND) GROUP BY s.name ORDER BY s.sort, date DESC";
	$minmax_24h = Sensors::getMinMaxReadings($query);

	// 7 day min/max.
	// $query = "SELECT s.name, MIN(ROUND(t.temp_c, 1)) AS min, MAX(ROUND(t.temp_c, 1)) AS max, (SELECT date FROM temps t2 WHERE t2.sensor_serial = t.sensor_serial AND t2.temp_c = MIN(t.temp_c) ORDER BY date DESC LIMIT 1) AS min_date, (SELECT date FROM temps t2 WHERE t2.sensor_serial = t.sensor_serial AND t2.temp_c = MAX(t.temp_c) ORDER BY date DESC LIMIT 1) AS max_date FROM temps t INNER JOIN tempsensors s ON t.sensor_serial = s.serial WHERE date > DATE_SUB(NOW(), INTERVAL 604800 SECOND) GROUP BY s.name ORDER BY s.sort, date DESC";
	// $minmax_7d = getMinMaxTemps($query);

	// 30 day min/max.
	// $query = "SELECT s.name, MIN(ROUND(t.temp_c, 1)) AS min, MAX(ROUND(t.temp_c, 1)) AS max, (SELECT date FROM temps t2 WHERE t2.sensor_serial = t.sensor_serial AND t2.temp_c = MIN(t.temp_c) ORDER BY date DESC LIMIT 1) AS min_date, (SELECT date FROM temps t2 WHERE t2.sensor_serial = t.sensor_serial AND t2.temp_c = MAX(t.temp_c) ORDER BY date DESC LIMIT 1) AS max_date FROM temps t INNER JOIN tempsensors s ON t.sensor_serial = s.serial WHERE date > DATE_SUB(NOW(), INTERVAL 2592000 SECOND) GROUP BY s.name ORDER BY s.sort, date DESC";
	// $minmax_30d = getMinMaxTemps($query);

	// overall min/max
	// $query = "SELECT s.name, MIN(ROUND(t.temp_c, 1)) AS min, MAX(ROUND(t.temp_c, 1)) AS max, (SELECT date FROM temps t2 WHERE t2.sensor_serial = t.sensor_serial AND t2.temp_c = MIN(t.temp_c) ORDER BY date DESC LIMIT 1) AS min_date, (SELECT date FROM temps t2 WHERE t2.sensor_serial = t.sensor_serial AND t2.temp_c = MAX(t.temp_c) ORDER BY date DESC LIMIT 1) AS max_date FROM temps t INNER JOIN tempsensors s ON t.sensor_serial = s.serial GROUP BY s.name ORDER BY s.sort, date DESC";
	// $minmax_all = getMinMaxTemps($query);

	// 24 hour averages.
	// $query = "SELECT s.name, ROUND(AVG(temp_c), 1) AS avg FROM temps t INNER JOIN tempsensors s ON t.sensor_serial = s.serial WHERE date > DATE_SUB(NOW(), INTERVAL 86400 SECOND) GROUP BY t.sensor_serial";
	// $avg_24h = getTemps($query, "avg24h");

	// 7 day averages.
	// $query = "SELECT s.name, ROUND(AVG(temp_c), 1) AS avg FROM temps t INNER JOIN tempsensors s ON t.sensor_serial = s.serial WHERE date > DATE_SUB(NOW(), INTERVAL 604800 SECOND) GROUP BY t.sensor_serial";
	// $avg_7d = getTemps($query, "avg7d");

	// 30 day averages.
	// $query = "SELECT s.name, ROUND(AVG(temp_c), 1) AS avg FROM temps t INNER JOIN tempsensors s ON t.sensor_serial = s.serial WHERE date > DATE_SUB(NOW(), INTERVAL 2592000 SECOND) GROUP BY t.sensor_serial";
	// $avg_30d = getTemps($query, "avg30d");

	// overall averages
	// $query = "SELECT s.name, ROUND(AVG(temp_c), 1) AS avg FROM temps t INNER JOIN tempsensors s ON t.sensor_serial = s.serial GROUP BY t.sensor_serial";
	// $avg_all = getTemps($query, "avg_all");

	$averages = array();
	$actuals = array();

	foreach($tempSensors as $sensor)
	{
		$sensor_name = $sensor["name"];

		// if we have a measurement datapoint for 30mins ago, get trend
		if(count($actual[$sensor_name]) == count($actual_30min[$sensor_name])) {
			$actual_trend[$sensor_name]["trend"] = getTrend($actual[$sensor_name]["act"], $actual_30min[$sensor_name]["act_30min"]);
		}

		$averages[$sensor_name] = (array)$averages[$sensor_name] + (array)$avg_24h[$sensor_name];
		$averages[$sensor_name] = (array)$averages[$sensor_name] + (array)$avg_7d[$sensor_name];
		$averages[$sensor_name] = (array)$averages[$sensor_name] + (array)$avg_30d[$sensor_name];
		$averages[$sensor_name] = (array)$averages[$sensor_name] + (array)$avg_all[$sensor_name];

		$actuals[$sensor_name] = (array)$actual[$sensor_name];
		$actuals[$sensor_name] = (array)$actuals[$sensor_name] + (array)$actual_30min[$sensor_name];
		$actuals[$sensor_name] = (array)$actuals[$sensor_name] + (array)$actual_trend[$sensor_name];
		$actuals[$sensor_name] = (array)$actuals[$sensor_name] + (array)$actual_1y[$sensor_name];
		$actuals[$sensor_name] = (array)$actuals[$sensor_name] + (array)$actual_2y[$sensor_name];
		$actuals[$sensor_name] = (array)$actuals[$sensor_name] + (array)$actual_3y[$sensor_name];
	}

	// echo "<pre>";
	// print_r($actuals);
	// echo "</pre>";

	if($tempLoggingInfo["last"] != "")
	{
		$list .= "<table class=\"temp_table\">
					<tr>
						<td colspan=\"2\">
							<table width=\"100%\" class=\"temp_table\">
								<tr>
									<td width=\"150px\" class=\"label\">".LBL_LATESTREADING.":</td>
									<td align=\"left\" class=\"label\">".formatDateTime($tempLoggingInfo["last"])."</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td colspan=\"2\">
							<table width=\"100%\" class=\"temp_table\">
								<tr>
									<td colspan=\"7\" class=\"label\">".LBL_CURRENTVALUES."</td>
								</tr>
								<tr>
									<td width=\"150px\" class=\"label\">".LBL_SENSORNAME."</td>
									<td class=\"label\">".LBL_NOW."</td>
									<td class=\"label\">".LBL_30MINAGO."</td>
									<td class=\"label\">".LBL_TREND."</td>
									<td class=\"label\">".LBL_1YEARAGO."</td>
									<td class=\"label\">".LBL_2YEARSAGO."</td>
									<td class=\"label\">".LBL_3YEARSAGO."</td>
								</tr>";

		foreach($actuals as $sensor => $data) {
			$list .= "<tr>
						<td class=\"label\">".$sensor."</td>";

			foreach($data as $type => $reading)	{
				$value = "";
				$unit = "";

				if($type != "trend") {
					$value = $reading["value"];
					$unit = $reading["unit"];

					if($value == "")
					{
						$value = "-";
					}
				} else {
					$value = $reading;
				}

				$list .= "<td class=\"temp_text\"><font color=\"".getDynamicColor($value)."\">".$value;

				if($type != "trend")
				{
					$list .= " ".$unit;
				}

				$list .= "</font></td>";
			}

			$list .= "</tr>";
		}

		$list .= "			</table>
						</td>
					</tr>";

		foreach(Sensors::getTypes(-1, true) as $type) {
			$list .= '<tr>
						<td colspan="2">
							<div id="graph-'.$type['id'].'" class="graph" graph-type="'.$type['graphtype'].'" data-sensor-type="'.$type['id'].'"></div>
						</td>
					</tr>';
		}

		$list .= "<tr>
						<td colspan=\"2\">
							"./*generateMinMaxTable($minmax_all, "Statistik").*/"
						</td>
					</tr>
					<tr>
						<td colspan=\"2\">
							<table width=\"100%\" class=\"temp_table\">
								<tr>
									<td colspan=\"6\" class=\"label\">".LBL_AVERAGE."</td>
								</tr>
								<tr>
									<td width=\"150px\" class=\"label\">".LBL_SENSORNAME."</td>
									<td class=\"label\">24 ".LBL_HOURS."</td>
									<td class=\"label\">7 ".LBL_DAYS."</td>
									<td class=\"label\">30 ".LBL_DAYS."</td>
									<td class=\"label\">".LBL_OVERALL."</td>
									<td class=\"label\">&nbsp;</td>
								</tr>";

		foreach($averages as $sensor => $data) {
			if($data["avg24h"] == "")
			{
				$data["avg24h"] = "-";
			}

			if($data["avg7d"] == "")
			{
				$data["avg7d"] = "-";
			}

			if($data["avg30d"] == "")
			{
				$data["avg30d"] = "-";
			}

			if($data["avg_all"] == "")
			{
				$data["avg_all"] = "-";
			}

			$list .= "<tr>";
				$list .= "<td class=\"label\">".$sensor."</td>";
				$list .= "<td class=\"temp_text\"><font color=\"".getDynamicColor($data["avg24h"])."\">".$data["avg24h"]." &deg;C</font></td>";
				$list .= "<td class=\"temp_text\"><font color=\"".getDynamicColor($data["avg7d"])."\">".$data["avg7d"]." &deg;C</font></td>";
				$list .= "<td class=\"temp_text\"><font color=\"".getDynamicColor($data["avg30d"])."\">".$data["avg30d"]." &deg;C</font></td>";
				$list .= "<td class=\"temp_text\"><font color=\"".getDynamicColor($data["avg_all"])."\">".$data["avg_all"]." &deg;C</font></td>";
				$list .= "<td class=\"temp_text\">&nbsp;</td>";
			$list .= "</tr>";
		}

		$list .= "			</table>
						</td>
					</tr>
					<tr>
						<td colspan=\"2\">
							"./*generateMinMaxTable($minmax_24h, "Senaste 24h, min och max temperaturer").*/"
						</td>
					</tr>";

		$list .= "	<tr>
						<td colspan=\"2\">
							"./*generateMinMaxTable($minmax_7d, "Senaste 7 dygn, min och max temperaturer").*/"
						</td>
					</tr>";

		$list .= "	<tr>
						<td colspan=\"2\">
							"./*generateMinMaxTable($minmax_30d, "Senaste 30 dygn, min och max temperaturer").*/"
						</td>
					</tr>";

		// add bar graphs for sauna usage?
		//			echo "<tr><td colspan=\"2\"><a href=\"temperature/graph.php?limit=30&interval=d&sensor=".$sensor["sensor_serial"]."&name=".$sensor["name"]."&width=1000&height=500&shownow=1\" target=\"_blank\"><img src=\"".getGraph($sensor["sensor_serial"], $sensor["name"], 30, "d", true, true)."?ts=".date("U")."\" border=\"0\"></a></td></tr>";

					$query = "SELECT s.name, st.unit, ROUND(t.temp_c, 1) AS temp, UNIX_TIMESTAMP(date) AS timestamp FROM temps t INNER JOIN tempsensors s ON t.sensor_serial = s.serial INNER JOIN sensortypes st ON st.id = s.sensortype WHERE s.sensortype = 1 AND date > DATE_SUB(NOW(), INTERVAL 604800 SECOND) ORDER BY t.id;";
					$saunaUsage["week"] = Sensors::getTrends($query);

					$query = "SELECT s.name, st.unit, ROUND(t.temp_c, 1) AS temp, UNIX_TIMESTAMP(date) AS timestamp FROM temps t INNER JOIN tempsensors s ON t.sensor_serial = s.serial INNER JOIN sensortypes st ON st.id = s.sensortype WHERE s.sensortype = 1 AND date > DATE_SUB(NOW(), INTERVAL 2592000 SECOND) ORDER BY t.id;";
					$saunaUsage["month"] = Sensors::getTrends($query);

					$query = "SELECT s.name, st.unit, ROUND(t.temp_c, 1) AS temp, UNIX_TIMESTAMP(date) AS timestamp FROM temps t INNER JOIN tempsensors s ON t.sensor_serial = s.serial INNER JOIN sensortypes st ON st.id = s.sensortype WHERE s.sensortype = 1 AND date > DATE_SUB(NOW(), INTERVAL 31104000 SECOND) ORDER BY t.id;";
					$saunaUsage["year"] = Sensors::getTrends($query);


					// echo "<tr><td colspan=2><pre>";
					// print_r($saunaUsage);
					// echo "</pre></td></tr>";

		if(count($saunaUsage["week"]) > 0 && count($saunaUsage["month"]) > 0) {
			$list .= "	<tr>
							<td colspan=\"2\">
								<table width=\"100%\" class=\"temp_table\">
									<tr>
										<td width=\"150px\" class=\"label\">".LBL_SAUNAUSAGE."</td>
										<td class=\"label\">".LBL_USAGEATATIME."</td>
										<td class=\"label\">".LBL_USAGEWEEKLY."</td>
										<td class=\"label\">".LBL_USAGEMONTHLY."</td>
										<td class=\"label\">&nbsp;</td>
									</tr>
									<tr>
										<td class=\"label\">".LBL_INAWEEK."</td>
										<td class=\"temp_text\">".sec2time(array_sum($saunaUsage["week"]) / count($saunaUsage["week"]), false)."</td>
										<td class=\"temp_text\">".sec2time(array_sum($saunaUsage["week"]), false)."</td>
										<td class=\"temp_text\">-</td>
										<td class=\"temp_text\">&nbsp;</td>
									</tr>
									<tr>
										<td class=\"label\">".LBL_INAMONTH."</td>
										<td class=\"temp_text\">".sec2time(array_sum($saunaUsage["month"]) / count($saunaUsage["month"]), false)."</td>
										<td class=\"temp_text\">".sec2time(array_sum($saunaUsage["month"]) / 4, false)."</td>
										<td class=\"temp_text\">".sec2time(array_sum($saunaUsage["month"]), false)."</td>
										<td class=\"temp_text\">&nbsp;</td>
									</tr>";
									// <tr>
										// <td class=\"label\">Ett �r</td>
										// <td class=\"temp_text\">".sec2time(array_sum($saunaUsage["year"]) / count($saunaUsage["year"]), false)."</td>
										// <td class=\"temp_text\">".sec2time(array_sum($saunaUsage["year"]) / 52, false)."</td>
										// <td class=\"temp_text\">".sec2time(array_sum($saunaUsage["year"]) / 12, false)."</td>
										// <td class=\"temp_text\">&nbsp;</td>
									// </tr>
			$list .= "		</table>
						</td>
					</tr>";
		}

		$list .= "	<tr>
						<td colspan=\"2\">
							<table width=\"100%\" class=\"temp_table\">
								<tr>
									<td width=\"150px\" class=\"label\">".LBL_DATABASE."</td>
									<td class=\"label\">".LBL_FIRSTREADING."</td>
									<td class=\"label\">".LBL_LATESTREADING."</td>
									<td class=\"label\">".LBL_DATAPOINTS."</td>
									<td class=\"label\">&nbsp;</td>
								</tr>
								<tr>
									<td class=\"label\"></td>
									<td class=\"temp_text\">".formatDateTime($tempLoggingInfo["first"])."</td>
									<td class=\"temp_text\">".formatDateTime($tempLoggingInfo["last"])."</td>
									<td class=\"temp_text\">".$tempLoggingInfo["datapoints"]."</td>
									<td class=\"temp_text\">&nbsp;</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>";
	}
	else
	{
		$list = "Inga datapunkter";
	}

	return $list;
}

// <tr>
					// <td colspan=\"2\">
						// <script language=\"javascript\" type=\"text/javascript\">

						// var upSeconds = ".$uptimeSecs.";

						// var uptimeString = \"Server upp tid: \";
						// var secs = parseInt(upSeconds % 60);
						// var mins = parseInt(upSeconds / 60 % 60);
						// var hours = parseInt(upSeconds / 3600 % 24);
						// var days = parseInt(upSeconds / 86400);

						// if (days > 0)
						// {
							// uptimeString += days;
							// uptimeString += ((days == 1) ? \" dag\" : \" dagar\");
						// }

						// if (hours > 0)
						// {
							// uptimeString += ((days > 0) ? \" \" : \"\") + hours;
							// uptimeString += ((hours == 1) ? \" timme\" : \" timmar\");
						// }

						// if (mins > 0)
						// {
							// uptimeString += ((days > 0 || hours > 0) ? \" \" : \"\") + mins;
							// uptimeString += ((mins == 1) ? \" minut\" : \" minuter\");
						// }

						// if (secs > 0)
						// {
							// uptimeString += ((days > 0 || hours > 0 || mins > 0) ? \" och \" : \"\") + secs;
							// uptimeString += ((secs == 1) ? \" sekund\" : \" sekunder\");
						// }

						// document.getElementById(\"uptime\").innerHTML = uptimeString;

						// upSeconds++;

						// </script>
					// </td>
				// </tr>
?>
