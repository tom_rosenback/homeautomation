<?php
// HomeAutomation
// Copyright (C) 2021 Tom Rosenback (tom.rosenback@gmail.com), Daniel Malmgren (daniel.malmgren@kolefors.se)
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

function generateInfobox() {
	global $ha;

	$now = date("H:i");
	$dst = "0".date("I").":00";

	$list = "<div class=\"outline\">";

	$selectedScenario = Settings::get("selectedscenario");
	$allScenarios = Scenarios::get(-1, true);

	if(count($allScenarios) > 0) {
		$list .= "<div class=\"ellipsis extra-margin\">
					<span class=\"bold description\">
						".LBL_SCENARIO.":
					</span>
					<span class=\"right\">
						<!--<img src=\"resources/loading.gif\" id=\"scenario-progressicon\" style=\"display: none; vertical-align: middle;\" height=\"15px\" />-->".generateSelector($selectedScenario, "scenario", "text scenario-selector", "id", "name", $allScenarios, -1, "", false, " data-mini=\"true\" data-corners=\"false\"")."
					</span>
				</div>";
	}

	if($_SESSION[CFG_SESSION_KEY]["settings"]["ibshowusername"] == 1) {
		$list .= "<div class=\"ellipsis\"><span class=\"bold description\">".LBL_CURRENTUSER."</span><span class=\"bold\">:</span><span class=\"username right\">".trim(trim($_SESSION[CFG_SESSION_KEY]["firstname"]." ".$_SESSION[CFG_SESSION_KEY]["lastname"])." (".$_SESSION[CFG_SESSION_KEY]["user"]).")</span></div>";
	}

	if($_SESSION[CFG_SESSION_KEY]["settings"]["ibshowcurrenttime"] == 1) {
		$list .= "<div class=\"ellipsis\"><span class=\"bold description\">".LBL_CURRENTTIME."</span><span class=\"bold\">:</span><span class=\"right\">".$now."</span></div>";
	}

	if($_SESSION[CFG_SESSION_KEY]["settings"]["ibshowsun"] == 1) {
		$dawn = getTimeOfSunAtPosition("dawn", time(), $_SESSION[CFG_SESSION_KEY]["settings"]["latitude"], $_SESSION[CFG_SESSION_KEY]["settings"]["longitude"], $_SESSION[CFG_SESSION_KEY]["settings"]["timezone"]);
		$sunrise = getTimeOfSunAtPosition("sunrise", time(), $_SESSION[CFG_SESSION_KEY]["settings"]["latitude"], $_SESSION[CFG_SESSION_KEY]["settings"]["longitude"], $_SESSION[CFG_SESSION_KEY]["settings"]["timezone"]);
		$dusk = getTimeOfSunAtPosition("dusk", time(), $_SESSION[CFG_SESSION_KEY]["settings"]["latitude"], $_SESSION[CFG_SESSION_KEY]["settings"]["longitude"], $_SESSION[CFG_SESSION_KEY]["settings"]["timezone"]);
		$sunset = getTimeOfSunAtPosition("sunset", time(), $_SESSION[CFG_SESSION_KEY]["settings"]["latitude"], $_SESSION[CFG_SESSION_KEY]["settings"]["longitude"], $_SESSION[CFG_SESSION_KEY]["settings"]["timezone"]);

		$list .= "	<div class=\"ellipsis\"><span class=\"bold description\">".LBL_SUNRISETODAY."</span><span class=\"bold\">:</span><span class=\"right\">".$sunrise." (".$dawn.")</span></div>
					<div class=\"ellipsis\"><span class=\"bold description\">".LBL_SUNSETTODAY."</span><span class=\"bold\">:</span><span class=\"right\">".$sunset." (".$dusk.")</span></div>";
	}

	if(strcmp($_SESSION[CFG_SESSION_KEY]["settings"]["ibsensor"], "nosensor") !== 0) {
		$sensorData = Sensors::getCurrentReadings($_SESSION[CFG_SESSION_KEY]["settings"]["ibsensor"]);
		$sensorInfo = Sensors::get($_SESSION[CFG_SESSION_KEY]["settings"]["ibsensor"]);

		if(count($sensorInfo) == 1 && count($sensorData) == 1) {
			if($_SESSION[CFG_SESSION_KEY]["settings"]["useexternaltemperature"] && $_SESSION[CFG_SESSION_KEY]["settings"]["ibsensor"] == "external") {
				$sensorInfo[0]["description"] = $_SESSION[CFG_SESSION_KEY]["settings"]["externaltemplocation"];
			}

			$currentReading = $sensorData[0]["last_reading"];
			$currentUnit = $sensorData[0]["unit"];
			$unitDesc = $sensorInfo[0]["description"];

			$list .= "<div class=\"ellipsis\"><span class=\"bold description\">".$unitDesc."</span><span class=\"bold\">:</span><span class=\"right\">".$currentReading." ".$currentUnit."</span></div>";
		}
	}

	if($_SESSION[CFG_SESSION_KEY]["settings"]["ibupcomingevents"] > 0) {
		if($list != "") {
			$list .= "<p />";
		}

		$activations = Schedules::getUpcomingActivations($_SESSION[CFG_SESSION_KEY]["settings"]["ibupcomingevents"], $dst);

		if(count($activations) == 0) {
			$list .= "<div class=\"bold\">".LBL_NOUPCOMINGEVENTS."</div>";
		} else {
			$list .= "<div class=\"bold\">".LBL_UPCOMINGEVENTS.":</div>";

			$i = 1;

			foreach($activations as $activation) {
				$tomorrowWord = "";

				if($activation["another_day"] > 0) {
					$tomorrowWord = LBL_TOMORROW."&nbsp;";
				}

				$list .= "<div class=\"ellipsis\"><span class=\"description\">".$activation["name"]."</span>&nbsp;".getStatusText($activation["action"])."<span class=\"right\">".$tomorrowWord.$activation["upcomingruntime"]."&nbsp;<a href=\"api.php?do=activations/run&id=".$activation["id"]."&redirect=".THISPAGE."?page=".$ha->currentPage["name"]."\" class=\"run-activation\" data-activationid=\"".$activation["id"]."\"><img src=\"./resources/run.png\" height=\"15px\" id=\"activationexecutor".$i."\" alt=\"".LBL_RUNNOW."\" title=\"".LBL_RUNNOW."\" align=\"middle\" class=\"activation-run-img\" /></a></span></div>";

				$i++;
			}
		}
	}

	if($_SESSION[CFG_SESSION_KEY]["settings"]["iblogevents"] > 0) {
		if($list != "") {
			$list .= "<p />";
		}

		$events = Events::get(-1, $_SESSION[CFG_SESSION_KEY]["settings"]["iblogevents"]);

		$list .= "<div class=\"bold\">".LBL_LATESTLOGEVENTS.":</div>";

		$systemPlugins = SystemPlugins::load();

		foreach($events as $event) {
      $tmp = explode(" ", $event["timestamp"]);
  		$time = substr($tmp[1], 0, -3);

			$statusText = getStatusText($event["status"]);

			if(is_object($systemPlugins[$event["systempluginname"]])) {
				$statusText = $systemPlugins[$event["systempluginname"]]->getStatusText($event["status"], $event["dimlevel"]);
			}

			$list .= "<div class=\"ellipsis\"><span class=\"description\">".$event["description"]."</span>&nbsp;".$statusText;

			if($event["device_type"] == "absdimmer" && $event["dimlevel"] > 0) {
				$list .= " ".$event["dimlevel"]."%";
			}

			$list .= "<span class=\"right\">".$time."</span></div>";
		}
	}

	$list .= "</div>";

	return $list;
}

?>
