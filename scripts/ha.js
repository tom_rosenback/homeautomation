var HA = {
	lastUpdate: 0,
	enableLogging: true,
	autoUpdate: { // Update interval
		devices: 15000,
		sensors: 30000,
		infobox: 30000,
		versions: 600000
	},
	sensors: {
		fancybox: {
			margin: 20,
			padding: 10,
			widthOffset: 100,
			heightOffset: 100
		}
	},
	log: function(msg) {
		if(HA.enableLogging) {
			console.log(msg);
		}
	}
};