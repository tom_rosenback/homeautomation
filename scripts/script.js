// HomeAutomation
// Copyright (C) 2021 Tom Rosenback (tom.rosenback@gmail.com), Daniel Malmgren (daniel.malmgren@kolefors.se)
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

var intervalId;
var longClick;

$(document).ready(function() {
	console.log("init");
	colorizeTable();
	activateAutoUpdate();
	bindCloseMessageClick();
	$("form").preventDoubleSubmission();
	initInfobox();

	Highcharts.setOptions({
		global: {
			useUTC: false,
			timezoneOffset: HA_Localization.utcOffset
		}
	});

	initSensorFancybox($.mobile);
	initGraphs();

	$(".enterInputDisabled").bind("keydown", function(e) {
		if(e.keyCode == 13)	{
			return false;
		}
	});

	// Return a helper with preserved width of cells
	var fixHelper = function(e, ui) {
		ui.children().each(function() {
			$(this).width($(this).width());
		});
		return ui;
	};

	$("#sortableList tbody").sortable(
	{
		helper: fixHelper,
		axis: 'y',
		cursor: 'move',
		update: function(event, ui) {
			$("#sortableList tbody tr:visible:odd").removeClass("oddrow evenrow");
			$("#sortableList tbody tr:visible:odd").addClass("oddrow");

			$("#sortableList tbody tr:visible:even").removeClass("oddrow evenrow");
			$("#sortableList tbody tr:visible:even").addClass("evenrow");

			$.ajax(
			{
				type: "GET",
				url: "api.php",
				data:
				{
					'do': 'sorting/save',
					'sortable': $(this).sortable('serialize'),
					'sid': Math.random()
				},
				// beforeSend: function (jqXHR, settings) {
					// alert(JSON.stringify(jqXHR) + "\n" + JSON.stringify(settings));
				// },
				error: function(jqXHR, textStatus, errorThrown) {
					// alert("err"+textStatus);
				},
				success: function(html) {
					// alert("success"+html)
				}
			});

			$("#sortableList tbody tr:visible:odd").unbind("mouseenter mouseleave").hover(onMouseHoverOdd, onMouseHoverOutOdd);
			$("#sortableList tbody tr:visible:even").unbind("mouseenter mouseleave").hover(onMouseHoverEven, onMouseHoverOutEven);
		}
	}).disableSelection();

	$("#resetSorting").click(function(e)
	{
		e.preventDefault();

		if(confirm($(this).attr("title")))
		{
			tmpArr = $("#sortableList tbody tr:first").attr("id").split("_");
			type = tmpArr[0];
			url = $(this).attr("href");

			$.ajax(
			{
				type: "GET",
				url: "api.php",
				data:
				{
					'do': 'sorting/reset',
					'type': type,
					'sid': Math.random()
				},
				error: function(jqXHR, textStatus, errorThrown) {
					// alert(textStatus);
				},
				success: function(html) {
					window.location = url;
				}
			});
		}

		return false;
	});

	if($(".expandCollapseChildren").length)
	{
		$(".expandCollapseChildren").hover(function() {$(this).css("cursor","cursor")}, function() {$(this).css("cursor","default")});

		$(".expandCollapseChildren").click(function() {
			var pageId = this.id.split("-")[1];
			var expander = $(this);

			$(".parentid-" + pageId).toggle('fast', function() {
				// Animation complete.

				if($(".parentid-" + pageId).filter(":visible").length > 0)
				{
					expander.attr("src", "resources/collapse.png");
				}
				else
				{
					expander.attr("src", "resources/expand.png");
				}

				$(".tablelist tbody tr:visible").removeClass("oddrow evenrow");
				colorizeTable();
			});

			return false; // returning false to stop propagation
		});
	}

	$("a.device-toggle").on("click", deviceToggleEvent);
	$("select.device-toggle").on("slidestop", sliderToggleEvent);
	$("input.device-slider").on("slidestop", sliderToggleEvent);
	$(".group-toggle").on("click", groupToggleEvent);
	$(".macro-run").on("click", macroRunEvent);

	$("img[id^='device_status']").mousedown(function() {
		thisId = this.id;
		longClick = false;

		/* coming in later version
		intervalId = setInterval(function()
		{
			longClick = true;

			var pos = $("#" + thisId).offset();
			var width = $("#" + thisId).width();
			//show the menu directly over the placeholder
			$("#status_holder").css( { "left": (pos.left + width + 7) + "px", "top": (pos.top) + "px" } );
			$("#status_holder").slideDown();

			id = thisId.replace("device_status", "").split("_");
			id = id[0];

			// show the status hold box with correct data
			$("#status_hold_until").val(id);

		}, 500);
		*/

	}).mouseup(function() {
		// clearInterval(intervalId);

		if(!longClick)
		{
			id = this.id.replace("device_status", "").split("_");
			id = id[0];

			// do toggle here as we haven´t done a long click
			toggleStatus("device_status", id, -1);
		}

		longClick = false;
	});

	$("#status_holder_cancel").click(function()
	{
		$("#status_holder").slideUp();
	});

	$("#status_holder_save").click(function()
	{
		// save changes here
		$("#status_holder").slideUp();
	});

	if($(".systemmessage.autoclose").length) {
		setTimeout(function() {
			$(".systemmessage.autoclose").slideUp({
				complete: function() {
					$(this).remove();
				}
			});
		}, 5000);
	}
});
