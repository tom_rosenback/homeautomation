// HomeAutomation
// Copyright (C) 2021 Tom Rosenback (tom.rosenback@gmail.com), Daniel Malmgren (daniel.malmgren@kolefors.se)
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

$(document).ready(function() {
	$('#licenseagreementapproved').on("change", function(e) {
		e.preventDefault();
		if($(this).is(':checked')) {
			$('#next').removeAttr('disabled');
		}
		else {
			$('#next').attr('disabled', 'disabled');
		}
	});
	/*
	$("#deleteInstall").on("click", function(e) {
		e.preventDefault();

		var options = {
			data: {
				'do': 'install/delDir'
			}
		};

		requestToApi(options);
		return false;
	});
	*/

	$("#finish").on("click", function() {
		window.location = getAbsolutePath() + "../";
	});

	$("#next").on("click", function(e) {
		if(!validateForm("#installForm", "#validationfail")) {
			e.preventDefault();
			return false;
		}
	});

	if($("#checkLogin").length) {
		$("#username").on("change", function(e) {
			$('#next').attr('disabled', 'disabled');
		});

		$("#password").on("change", function(e) {
			$('#next').attr('disabled', 'disabled');
		});

		$("#checkLogin").on("click", function(e) {
			e.preventDefault();

			var options = {
				data: {
					'do': 'install/checkLogin',
					'user': $("#username").val(),
					'pwd': $("#password").val()
				}
			};

			requestToApi(options);
			return false;
		});
	}

	if($("#checkMysqlConnection").length) {
		$("#host").on("change", function(e) {
			validateForm("#installForm", "#validationfail");
			$('#next').attr('disabled', 'disabled');
		});

		$("#database").on("change", function(e) {
			validateForm("#installForm", "#validationfail");
			$('#next').attr('disabled', 'disabled');
		});

		$("#username").on("change", function(e) {
			validateForm("#installForm", "#validationfail");
			$('#next').attr('disabled', 'disabled');
		});

		$("#password").on("change", function(e) {
			validateForm("#installForm", "#validationfail");
			$('#next').attr('disabled', 'disabled');
		});

		$("#checkMysqlConnection").on("click", function(e) {
			e.preventDefault();

			var options = {
				data: {
					'do': 'install/checkMysqlConnection',
					'host': $("#host").val(),
					'db': $("#database").val(),
					'user': $("#username").val(),
					'pwd': $("#password").val(),
					'requireslogin': 0
				}
			};

			requestToApi(options);
			return false;
		});
	}
});

function requestToApi(options) {
	var defaults = {
		url: getAbsolutePath() + '../api.php',
		data: {
			'do': null
		},
		error: function(jqXHR, textStatus, errorThrown) {
			$("#wait").hide();
			$("#fail").show();
		},
		success: function(json) {
			$("#wait").hide();
			if(json.success) {
				$("#success").show();
				$("#next").removeAttr('disabled');
			} else {
				$("#fail").show();
			}
		}
	};

	var settings = $.extend(true, {}, defaults, options);

	if(settings.do !== null) {
		$("#wait").show();
		$("#success").hide();
		$("#fail").hide();
		Network.send(settings);
	} else {
		$("#wait").hide();
		$("#success").hide();
		$("#fail").show();
		console.log("Invalid settings to requestToApi");
	}
}
