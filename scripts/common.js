var mobileLoaders = 0; // used to know if mobile loader should hide or not

function validateInput(arrFields, arrFieldNames, msgPrompt)
{
	result = false;
	var msg = "";

	for(i = 0; i < arrFields.length; i++)
	{
		if(document.getElementById(arrFields[i]).value == "")
		{
			msg = msg + " - " + arrFieldNames[i] + "\n";
		}
	}

	if(msg != "")
	{
		alert(msgPrompt + ",\n" + msg);
		result = false;
	}
	else
	{
		result = true;
	}

	return result;
}

function rowColor(element, color)
{
	element.bgColor = color;
}

function toggleRawCmdUploader(rawSelected, divId)
{
	if(rawSelected == 1)
	{
		document.getElementById(divId).style.display = 'block';
	}
	else
	{
		document.getElementById(divId).style.display = 'none';
	}
}

function toggleSchedulerOptions(type)
{
	sun = "";
	temp = "";
	time = "";

	switch(type)
	{
		case "sun":
		{
			sun = "";
			temp = "none";
			time = "";

			break;
		}
		case "temp":
		{
			sun = "none";
			temp = "";
			time = "none";

			break;
		}
		default:
		{
			sun = "none";
			temp = "none";
			time = "";

			break;
		}
	}

	document.getElementById("setting_sunrise").style.display = sun;
	document.getElementById("setting_sunset").style.display = sun;
	document.getElementById("setting_timeon").style.display = time;
	document.getElementById("setting_timeoff").style.display = time;


	try
	{
		document.getElementById("setting_temp").style.display = temp;
		document.getElementById("setting_temptime").style.display = temp;
	}
	catch(err)
	{
	}
}

function iconAction(checkbox)
{
	if(checkbox == "reseticons")
	{
		if(document.getElementById("reseticons").checked)
		{
			document.getElementById("uploadicons").checked = false;
		}
	}
	else
	{
		if(document.getElementById("uploadicons").checked)
		{
			document.getElementById("reseticons").checked = false;
		}
	}

	if(document.getElementById("uploadicons").checked)
	{
		document.getElementById("newIconsTable").style.display = "";
	}
	else
	{
		document.getElementById("newIconsTable").style.display = "none";
	}
}

function trim(str, chars)
{
	return ltrim(rtrim(str, chars), chars);
}

function ltrim(str, chars)
{
	chars = chars || "\\s";
	return str.replace(new RegExp("^[" + chars + "]+", "g"), "");
}

function rtrim(str, chars)
{
	chars = chars || "\\s";
	return str.replace(new RegExp("[" + chars + "]+$", "g"), "");
}

function deviceToggleEvent(e, self) {
	// HA.log(JSON.stringify(e));
	e.preventDefault();

	var deviceId = $(this).attr("data-deviceid");
	var status = $(this).attr("data-status");

	if(deviceId == null) {
		deviceId = $(self).attr("data-deviceid");
		status = $(self).attr("data-status");
	}

	toggleDevice(deviceId, status);

	return false; // returning false to stop propagation
}

function setLoadingStatus(self, oldImg) {
	if($.mobile) {
		self.attr("disabled", "disabled");
		toggleMobileLoader(true);
	} else {
		self.children("img").attr("src", "resources/loading.gif");
	}
}

function toggleMobileLoader(show) {
	if($.mobile) {
		if(show) {
			mobileLoaders++;

			if(mobileLoaders == 1) {
				$.mobile.loading('show', {});
			}
		} else {
			mobileLoaders--;

			if(mobileLoaders < 0) {
				mobileLoaders = 0;
			}

			if(mobileLoaders == 0) {
				$.mobile.loading('hide', {});
			}
		}
	}
}

function resetStatusImages(self, oldImg) {
	if($.mobile) {
		self.removeAttr("disabled");
		toggleMobileLoader(false);
	}

	self.children("img").attr("src", oldImg);
}

function toggleDevice(deviceId, status)
{
	var allDevices;
	var oldImg = $(".device-img-" + deviceId).first().attr("src");
	$(".device-img-" + deviceId).attr("height", $(".device-img-" + deviceId).height());
	$(".device-img-" + deviceId).attr("src", "resources/loading.gif");

	if(!$("input.device-slider").is(":visible")) {
		$(".device-slider-box-" + deviceId).hide();
	}

	var sliders = $(".device-slider-" + deviceId);
	var togglers = $(".device-toggler-" + deviceId);
	var statusImgs = $("img.device-img-" + deviceId);

	sliders.attr("data-device-busy", "true");
	togglers.attr("data-device-busy", "true");
	togglers.attr("data-device-busy", "true");

	if((deviceId == -1 || !oldImg) && $.mobile) {
		toggleMobileLoader(true);
	} else if(deviceId == -1 && !$.mobile) {
		allDevices = $("a.device-toggle[data-deviceid='-1'][data-status='" + status + "']");
		oldImg = allDevices.children("img").attr("src");
		setLoadingStatus(allDevices, oldImg);
	}

	var options = {
		data: {
			'do': 'devices/toggle',
			'deviceid': deviceId,
			'status': status
		},
		error: function(jqXHR, textStatus, errorThrown) {
			$(".device-img-" + deviceId).attr("src", oldImg);
			$(".device-slider-box-" + deviceId).show();

			sliders.removeAttr("data-device-busy");
			togglers.removeAttr("data-device-busy");
			togglers.removeAttr("data-device-busy");

			if(deviceId == -1 && !$.mobile) {
				resetStatusImages(allDevices, oldImg);
			}

			toggleMobileLoader(false);
		},
		success: function(json) {
			if (json.result) {
				setDeviceStatuses(json.result);
			} else {
				$(".device-img-" + deviceId).attr("src", oldImg);
				$(".device-slider-box-" + deviceId).show();
			}

			sliders.removeAttr("data-device-busy");
			togglers.removeAttr("data-device-busy");
			togglers.removeAttr("data-device-busy");

			if(deviceId == -1 && !$.mobile) {
				resetStatusImages(allDevices, oldImg);
			}

			toggleMobileLoader(false);
		}
	};

	Network.send(options);
}

function updateInfobox()
{
	if($("#infobox_content").length) {
		var options = {
			dataType: "text",
			data: {
				'do': 'infobox/get'
			},
			success: function(html) {
				$("#infobox_content").html(html);

				// HA.log(html);
				if($.mobile) {
					$("#infopanel").trigger("updatelayout");
				//	$("#infobox_content").trigger("create");
				}
			}
		};

		Network.send(options);
	}
}

function checkVersions() {
	if(!$("#systemmessage-updatesavailable").length) {
		var options = {
			dataType: "json",
			data: {
				'do': 'versions/check',
				'requireslogin': 0
			},
			success: function(json) {
				if(json.result && json.result.updatesavailable) {
					$("#systemmessagecontainer").append(json.result.message);
				}
			}
		};

		Network.send(options);
	}
}

function scenarioChangeEvent(e)
{
	var selectedId = $(this).val();
	// $("#scenario-progressicon").show();
	HA.log("Scenario changed: " + selectedId);
	var options = {
		data: {
			'do': 'scenarios/set',
			'id': selectedId
		},
		error: function(jqXHR, textStatus, errorThrown) {
			// $("#scenario-progressicon").hide();
			updateInfobox();
		},
		success: function() {
			// $("#scenario-progressicon").hide();
			updateInfobox();
		}
	};

	Network.send(options);
}

function runActivationEvent(e) {
	// HA.log(JSON.stringify(e));
	e.preventDefault();

	var activationId = $(this).attr("data-activationid");
	var runImage = $(this).children("img").first();
	var oldImgSrc = runImage.attr("src");
	HA.log(oldImgSrc);

	var options = {
		data: {
			'do': 'activations/run',
			'id': activationId
		},
		beforeSend: function() {
			runImage.attr("src", "resources/loading.gif");
		},
		error: function(jqXHR, textStatus, errorThrown) {
			runImage.attr("src", oldImgSrc);
		},
		success: function(json) {
			runImage.attr("src", oldImgSrc);
			updateDeviceStatuses(false);
			updateInfobox();
		}
	};

	Network.send(options);
	return false; // returning false to stop propagation
}

function updateDeviceStatuses(autoUpdate) {
	var autoUpdateRunning = false;

	if(autoUpdate) {
		autoUpdateRunning = true;
	}

	var options = {
		data: {
			'do': 'devices/getStatus',
			'lastupdate': HA.lastUpdate
		},
		success: function(json) {
			// HA.log(json);
			if(json.result) {
				// if(lastUpdate == null || json.lastupdate > lastUpdate) {
					setDeviceStatuses(json.result, autoUpdateRunning);
				// }

				HA.lastUpdate = json.lastupdate;
			}
		}
	};

	HA.log(options);
	Network.send(options);
}

function updateSensors()
{
	var options = {
		data: {
			'do': 'sensors/getReadings',
			'lastupdate': HA.lastUpdate
		},
		success: function(json) {
			HA.log(json);
			if(json.result) {
				// if(lastUpdate == null || json.lastupdate > lastUpdate) {
					setSensorValues(json.result);
				// }

				HA.lastUpdate = json.lastupdate;
			}
		}
	};
	HA.log(options);
	Network.send(options);
}

function setSensorValues(jsonResult)
{
	$.each(jsonResult, function() {
		HA.log(JSON.stringify(this));
		var value = this.last_reading;
		var unit = this.unit;
		var image = this.image;

		$(".sensor-value-" + this.id).html(value + "" + unit);
		$(".sensor-img-" + this.id).attr("src", image);
	});
}

function regexReplace(haystack, pattern, replacement, appendReplacementIfNoMatch)
{
	var regex = new RegExp(pattern, "g");
	var result = haystack;

	if (haystack.match(regex))
	{
		result = haystack.replace(regex, replacement);
	}
	else if(appendReplacementIfNoMatch)
	{
		result += replacement;
	}

	return result;
}

function colorizeTable()
{
	$(".tablelist tbody tr:visible:odd").addClass("oddrow");
	$(".tablelist tbody tr:visible:even").addClass("evenrow");
	$(".tablelist tbody tr:visible:even").unbind("mouseenter mouseleave").hover(onMouseHoverEven, onMouseHoverOutEven);
	$(".tablelist tbody tr:visible:odd").unbind("mouseenter mouseleave").hover(onMouseHoverOdd, onMouseHoverOutOdd);
}


var onMouseHoverEven = function()
						{
							if($(this).parents("table").attr("id") == "sortableList")
							{
								$(this).css("cursor","move");
							}

							$(this).removeClass("evenrow");
							$(this).addClass("markedrow");
						};

var onMouseHoverOutEven = function ()
						{
							$(this).css("cursor","auto");
							$(this).removeClass("markedrow");
							$(this).addClass("evenrow");
						};

var onMouseHoverOdd = function()
						{
							if($(this).parents("table").attr("id") == "sortableList")
							{
								$(this).css("cursor","move");
							}

							$(this).removeClass("oddrow");
							$(this).addClass("markedrow");
						};

var onMouseHoverOutOdd = function ()
						{
							$(this).css("cursor","auto");
							$(this).removeClass("markedrow");
							$(this).addClass("oddrow");
						};


function checkSelections(selector)
{
	numSelected = 0;
	count = 0;
	atLeastOneSelected = false;
	allSelected = false;

	$("input[id^='" + selector + "']").each(function(index)
	{
		if($(this).is(":checked"))
		{
			numSelected++;
		}

		count++;
	});

	if(numSelected > 0)
	{
		atLeastOneSelected = true;

		if(numSelected == count)
		{
			allSelected = true;
		}
	}

	return new Array(atLeastOneSelected, allSelected);
}

function initSensorFancybox(isMobile) {
	var width = $(window).width() - HA.sensors.fancybox.widthOffset;
	var height = $(window).height() - HA.sensors.fancybox.heightOffset;

	if(!isMobile) {
		if(width > 800 || height > 800) {
			width = width / 2 + 200;
			height = height / 2 + 200;
		}
	}

	var options = {
		speedIn: 500,
		speedOut:	500,
		overlayColor:	'#000',
		overlayOpacity: 0.7,
		titlePosition	: 'over',
		transitionIn: 'elastic',
		transitionOut: 'elastic',
		scrolling: 'no',
		margin: HA.sensors.fancybox.margin,
		padding: HA.sensors.fancybox.padding,
		autoDimensions: false,
		width: width,
		height: height,
		beforeShow: function () {
			var sensor = $(this.element).attr("data-sensor");
			var graphtype = $(this.element).attr("graph-type");
			initGraph("#graph", sensor, width, height, "", graphtype);
		}
	};

	$("a.sensors").fancybox(options);
}

function initGraphs() {
	if($("div.graph").length > 0) {
		$("div.graph").each(function() {
			initGraph("#" + this.id, "", 0, 0, jQuery(this).attr("data-sensor-type"), jQuery(this).attr("graph-type"));
		});
	}
}

function initGraph(container, sensor, width, height, type, graphtype) {
	$(container).html("");
	$.fancybox.showLoading();

	if(typeof type == "undefined") {
		type = "";
	}

	if(typeof graphtype == "undefined") {
		graphtype = "line";
	}

	var options = {
		data: {
			'do': 'sensors/getGraphData',
			'sensor': sensor,
			'type': type
		},
		error: function(jqXHR, textStatus, errorThrown) {
			// do data available
		},
		success: function(data) {
			var chartOptions = {
				chart: {
					zoomType: 'x',
					type: graphtype
				},
				plotOptions: {
					series: {
						//pointWidth: 10
						pointPadding: 0
					}
				},
				rangeSelector : {
					inputEnabled: false,
					allButtonsEnabled: true,
	                buttons: [
						{
							type: 'day',
							count: 1,
							text: 'Day'
						},
						{
							type: 'day',
							count: 7,
							text: 'Week'
						},
						{
							type: 'month',
							count: 1,
							text: 'Month',
							/*dataGrouping: {
								forced: true,
								units: [['hour', [1]]]
							}*/
						},
						/*{
							type: 'year',
							count: 1,
							text: 'Year'
						}*/
	                ],
	                buttonTheme: {
	                    width: 60
	                },
	                labelStyle: {
		                display: 'none',
		            },
	                selected: 1
				},
				credits: {
					enabled: false
				},
				title: {
					text: data.title,
				},
				xAxis: {
					type: 'datetime',
					lineWidth: 2,
					showLastLabel: true,
					ordinal: false
				},
				scrollbar : {
	                enabled : false
	            },
	            navigator : {
	                enabled : true,
					series: {
						type: graphtype
					}
	            },
				yAxis: {
					opposite: false,
					title: {
						text: '',
						margin: 80
					},
					labels: {
                        formatter: function () {
                            return (Highcharts.numberFormat(this.value, 1) + '' + data.unit);
                        }
                    },
					lineWidth: 2,
					showLastLabel: true,
				},
				tooltip: {
					formatter: function() {

						var s;
						if(Highcharts.dateFormat('%H:%M', this.x) == "00:00" && graphtype == "column") {
							s = '<b>' + Highcharts.dateFormat('%e %b %Y', this.x) + '</b>';
						} else {
							s = '<b>' + Highcharts.dateFormat('%H:%M %e %b %Y', this.x) + '</b>';
						}

						$.each(this.points, function(i, point) {
							s += '<br/>' + point.series.name + ': ' + Highcharts.numberFormat(point.y, 1) + '' + data.unit ;//+
									//', ' + HA_Localization.labels.min + ': ' + Highcharts.numberFormat(point.series.dataMin, 1) + '' + data.unit +
									//', ' + HA_Localization.labels.max + ': ' + Highcharts.numberFormat(point.series.dataMax, 1) + '' + data.unit;
						});

						return s;
					},
					shared: true
				},
				legend: {
					enabled: type != "" //Object.keys(data.sensors).length > 1
				},
				series: []
			};

			if(width > 0) {
				chartOptions.chart.width = width;
			}

			if(height > 0) {
				chartOptions.chart.height = height;
			}
			
			var foundData = false;
			
			$.each(data.sensors, function(index, sensorData) {
				foundData = true;
				
				chartOptions.series.push({
					name: sensorData.name,
					data: sensorData.values
					/*type: 'column',
					dataGrouping: {
					units: [[
						'week', // unit name
						[1] // allowed multiples
					], [
						'month',
						[1, 2, 3, 4, 6]
					]]}*/
				});
			});

			if(foundData) {
				$(container).highcharts("StockChart", chartOptions);
			} else {
				$(container).parent().parent().remove();
			}
			
			$.fancybox.update();
			$.fancybox.hideLoading();
		}
	};

	Network.send(options);
}

function groupToggleEvent(e) {
	e.preventDefault();
	var self = $(this);

	var oldImg = self.children("img").attr("src");
	var groupId = self.attr("data-groupid");
	var status = self.attr("data-status");

	setLoadingStatus(self, oldImg);

	var options = {
		data: {
			'do': 'groups/toggle',
			'groupid': groupId,
			'status': status
		},
		error: function(jqXHR, textStatus, errorThrown) {
			resetStatusImages(self, oldImg);
		},
		success: function(json) {
			if(json.result) {
				resetStatusImages(self, oldImg);
				setDeviceStatuses(json.result);
			} else {
				resetStatusImages(self, oldImg);
			}
		}
	};

	Network.send(options);
	return false; // returning false to stop propagation
}

function macroRunEvent(e) {
	e.preventDefault();

	var self = $(this);

	var macroId = self.attr("data-macroid");
	var oldImg = self.children("img").attr("src");

	setLoadingStatus(self, oldImg);

	var options = {
		data: {
			'do': 'macros/run',
			'macroid': macroId
		},
		error: function(jqXHR, textStatus, errorThrown) {
			resetStatusImages(self, oldImg);
		},
		success: function(json) {
			if(json.result) {
				resetStatusImages(self, oldImg);
				setDeviceStatuses(json.result);
			} else {
				resetStatusImages(self, oldImg);
			}
		}
	};

	Network.send(options);
	return false; // returning false to stop propagation
}

function sliderToggleEvent(e) {
	$(this).attr("data-status", $(this).val());
	HA.log("Slide toggle event");
	var self = this;
	deviceToggleEvent(e, self);
	return false;
}

function setDeviceStatuses(jsonResult, autoUpdate) {
	$.each(jsonResult, function() {
		// HA.log(JSON.stringify(this));
		var status = (this.status) ? 1 : 0;
		var dimlevel = this.dimlevel;

		var sliders = $(".device-slider-" + this.id);
		var togglers = $(".device-toggler-" + this.id);
		var statusImgs = $("img.device-img-" + this.id);
		// HA.log(statusImgs.length);

		if(sliders.length && (!autoUpdate || (autoUpdate && !hasAttr(sliders, "data-device-busy")))) {
			// abs dimmer - range
			if($.mobile) {
				sliders.val(dimlevel);
				sliders.slider("refresh");
			}
			else {
				sliders.slider("option", "value", dimlevel);
				$(".device-slider-val-" + this.id).html(dimlevel + " %");
			}

			$(".device-slider-box-" + this.id).show();
		}

		if(togglers.length && (!autoUpdate || (autoUpdate && !hasAttr(togglers, "data-device-busy")))) {
			togglers.val(status);
			togglers.slider("refresh");
		}

		if(statusImgs.length && (!autoUpdate || (autoUpdate && !hasAttr(statusImgs, "data-device-busy")))) {
			statusImgs.attr("src", this.image);
		}
	});

	updateInfobox();
}

function activateAutoUpdate() {
	if(HA.autoUpdate && (typeof(disableAutoUpdates) === "undefined" || !disableAutoUpdates)) {
		HA.log("auto update activation");
		var sliders = $(".device-slider");
		var togglers = $(".device-toggle");
		var statusImgs = $("img.device-img-img");
		var sensors = $("a.sensors");

		if((sliders.length || togglers.length || statusImgs.length) && HA.autoUpdate.devices) {
			HA.log("auto update slider/device interval: " + HA.autoUpdate.devices);
			setInterval(updateDeviceStatuses, HA.autoUpdate.devices);
		}

		if(sensors.length && HA.autoUpdate.sensors) {
			HA.log("auto update sensors interval: " + HA.autoUpdate.sensors);
			setInterval(updateSensors, HA.autoUpdate.sensors);
		}

		if(HA.autoUpdate.infobox) {
			HA.log("auto update infobox interval: " + HA.autoUpdate.sensors);
			setInterval(updateInfobox, HA.autoUpdate.infobox);
		}

		if(HA.autoUpdate.versions) {
			HA.log("auto update versions interval: " + HA.autoUpdate.versions);
			setInterval(checkVersions, HA.autoUpdate.versions);
			checkVersions();
		}
	}
}

function validateForm(selector, failSelector) {
	var valid = true;
	$.each($(".required", selector), function() {
		if($(this).val() === "") {
			valid = false;
			return false; // exit loop but not method
		}
	});

	if(failSelector) {
		if(valid) {
			$(failSelector).hide();
		} else {
			$(failSelector).show();
		}
	}

	return valid;
}

function initInfobox() {
	$("#infobox_content").on("click", "a.run-activation", runActivationEvent);
	$("#infobox_content").on("change", "select.scenario-selector", scenarioChangeEvent);
}

function hasAttr(object, attribute) {
	var hasAttr = false;

	var attr = object.attr(attribute);

	if (typeof attr !== 'undefined' && attr !== false) {
		hasAttr = true;
	}

	return hasAttr;
}

function getAbsolutePath() {
    var loc = window.location;
    var pathName = loc.pathname.substring(0, loc.pathname.lastIndexOf('/') + 1);
	var abs = loc.href.substring(0, loc.href.length - ((loc.pathname + loc.search + loc.hash).length - pathName.length));
	console.log(abs);
	return abs;
}

function bindCloseMessageClick() {
	$("#systemmessagecontainer").on("click", ".systemmessage .close", function(e) {
		e.preventDefault();

		var messageContainer = $(this).parent();

		if(messageContainer.prop("id") == "systemmessage-updatesavailable") {
			HA.log("Dismiss versions checks");
			var options = {
				data: {
					'do': 'versions/dismissChecks'
				},
				error: function(jqXHR, textStatus, errorThrown) {
					closeSystemMessage(messageContainer);
				},
				success: function() {
					closeSystemMessage(messageContainer);
				}
			};

			Network.send(options);
		} else {
			closeSystemMessage(messageContainer);
		}
	});
}

function closeSystemMessage(messageContainer) {
	messageContainer.remove();
}

// jQuery plugin to prevent double submission of forms
jQuery.fn.preventDoubleSubmission = function() {
	$(this).on('submit',function(e){
		var $form = $(this);

		if ($form.data('submitted') === true) {
			// Previously submitted - don't submit again
			e.preventDefault();
		} else {
			// Mark it so that the next submit can be ignored
			$form.data('submitted', true);
		}
	});

	// Keep chainability
	return this;
};

if(typeof Highcharts != "undefined") {
	Highcharts.setOptions({
		useUTC: false,
		timezoneOffset: HA_Localization.utcOffset
	});
}
