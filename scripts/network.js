var Network = {
	send: function(options) {
		var defaults = {
			url: "api.php",
			dataType: "json",
			data: {
				'sid': Math.random()
			},
			error: function(jqXHR, textStatus, errorThrown) { HA.log("Failed with request, status: " + textStatus + " XHR: " + jqXHR + ", Error: " + errorThrown)},
			success: function() {}
		};
		
		var settings = $.extend(true, {}, defaults, options);
		HA.log(settings);
		$.ajax(settings);
	}
};