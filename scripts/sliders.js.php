<?php $sliders = "

<script type=\"text/javascript\">
	$(document).ready(function() {
		// used in normal userinterface
		$(\".device-slider\").slider({
			range: \"min\",
			min: 0,
			max: 100,
			step: ".$_SESSION[CFG_SESSION_KEY]["settings"]["dimlevelstep"].",
			create: function(event, ui)
			{
				var deviceId = $(this).attr(\"data-deviceid\");
				//HA.log(\"Slider deviceid: \" + deviceId);
				$(this).slider(\"option\", \"value\", $(this).attr(\"data-status\"));
				//$(\".device-slider-val-\" + deviceId).html($(\".slider-val-\" + deviceId).html() + \" %\");
			},
			stop: function(event, ui)
			{
				var sliderVal = ui.value;
				var deviceId = $(this).attr(\"data-deviceid\");
				$(this).attr(\"data-status\", sliderVal);
				$(\".device-slider-val-\" + deviceId).html(sliderVal + \" %\");
				
				toggleDevice(deviceId, sliderVal);
			},
			slide: function(event, ui)
			{
				var sliderVal = ui.value;
				var deviceId = $(this).attr(\"data-deviceid\");
				//HA.log(\"Slider val: \" + sliderVal);
				
				$(this).attr(\"data-status\", sliderVal);
				$(\".device-slider-val-\" + deviceId).html(sliderVal + \" %\");
			}
		});
	});
	
	function initializeSlidersWithoutAction(useDimStepAsMin)
	{
		// used in scheduler
		$(\".device-slider2\").slider({
			range: \"min\",
			min: (useDimStepAsMin ? ".$_SESSION[CFG_SESSION_KEY]["settings"]["dimlevelstep"]." : 0),
			max: 100,
			step: ".$_SESSION[CFG_SESSION_KEY]["settings"]["dimlevelstep"].",
			create: function(event, ui)
			{
				sliderId = $(this).attr(\"data-sliderid\");
				//HA.log(\"Create slider for id: \" + sliderId);
				$(this).slider(\"option\", \"value\", $(\"#slider-val-\" + sliderId).html());
				$(\"#slider-val-\" + sliderId).html($(\"#slider-val-\" + sliderId).html() + \" %\");
			},
			slide: function(event, ui)
			{
				sliderId = $(this).attr(\"data-sliderid\");
				sliderVal = ui.value;
				$(\"#status-val-\" + sliderId).val(sliderVal);
				$(\"#slider-val-\" + sliderId).html(sliderVal + \" %\");
			}
		});
	}
	
</script>";