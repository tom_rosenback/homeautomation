<?php ob_start(); ?>
<script type="text/javascript">
	var HA_Localization = {
		utcOffset: <?php echo date('Z') / 60; ?>,
		labels: {
			min: '<?php echo LBL_MIN; ?>',
			max: '<?php echo LBL_MAX; ?>'
		}
	};
</script>

<?php $HA_Localization = ob_get_clean(); ?>