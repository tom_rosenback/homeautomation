// HomeAutomation
// Copyright (C) 2021 Tom Rosenback (tom.rosenback@gmail.com), Daniel Malmgren (daniel.malmgren@kolefors.se)
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

// overriding defaults for mobile
HA.sensors.fancybox.margin = 15;
HA.sensors.fancybox.padding = 5;
HA.sensors.fancybox.widthOffset = 50;
HA.sensors.fancybox.heightOffset = 50;

$(document).on("mobileinit", function(event) {
	$.mobile.ajaxLinksEnabled = false;
	$.mobile.ajaxFormsEnabled = false;
	$.mobile.ajaxEnabled = false;
});

$(document).on("pageinit", function(event) {
	HA.log("Problems solved");

	activateAutoUpdate();
	bindCloseMessageClick();
	$("form").preventDoubleSubmission();
	initInfobox();

	Highcharts.setOptions({
		global: {
			useUTC: false,
			timezoneOffset: HA_Localization.utcOffset
		}
	});

	initSensorFancybox($.mobile);
	initGraphs();

	if($.mobile) {
		// Hiding the input box
		$("input.device-toggle").hide();
	}

	$("a.device-toggle").on("click", deviceToggleEvent);
	$("select.device-toggle").on("slidestop", sliderToggleEvent);
	$("input.device-slider.hideinput").hide();
	$("input.device-slider").on("slidestop", sliderToggleEvent);
	$(".group-toggle").on("click", groupToggleEvent);
	$(".macro-run").on("click", macroRunEvent);

	if($("#menu li ul").length) {
		$("#menu li a").on("click", function(e) {
			HA.log("Menu click");
			if($(this).parent().find("ul").first().length > 0) {
				e.preventDefault();
				HA.log("Found submenu");
				var submenu = $(this).parent().find("ul").first();

				isOpen = submenu.is(":visible");
				$("#menu li ul").hide();
				$("#menu li h3 a span").removeClass("open").addClass("closed");

				if(!isOpen) {
					$(this).find("span").first().removeClass("closed").addClass("open");
					submenu.show();
				}

				return false;
			}
		});
	}
});
