<?php

// HomeAutomation
// Copyright (C) 2021 Tom Rosenback (tom.rosenback@gmail.com), Daniel Malmgren (daniel.malmgren@kolefors.se)
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

// disabling notice messages
error_reporting(E_ALL & ~E_NOTICE);


header('Content-Type: text/html; charset=utf-8');
session_start();

if(isset($_GET["ignoreresponse"])) {
	// since we are jumping out early we can prolong the timeout, but we don´t want to make it eternal.
	set_time_limit(100);

	ob_end_clean();
	header("Connection: close");
	ignore_user_abort(true);
	ob_start();
	echo json_encode(array("result" => "response ignored"));
	header("Content-Length: ".ob_get_length());
	ob_end_flush();
	flush();

	if (session_id()) {
		// close session immidiately so that new requests from same user
		// can be done without waiting for current call to finish
		session_write_close();
	}

	/*if($_GET["systempluginname"] == "arduino" && $_GET["status"] == "2") {
		$status = rand (0, 1);
		file_get_contents("http://192.168.1.33/mcp-33-2/".$status);
		file_get_contents("http://192.168.1.33/mcp-32-4/".$status);
		file_get_contents("http://192.168.1.33/mcp-33-2/".$status);
		file_get_contents("http://192.168.1.33/mcp-33-4/".$status);
		exit;
	}*/
}

define("HomeAutomationIncluded", TRUE);

include("includes.php");

$requiresLogin = getFormVariable("requireslogin", 1);

if($requiresLogin) {
	validateSession();

	$username = getFormVariable("login_username", "");
	$password = getFormVariable("login_password", "");

	if($username != "" && $password != "") {
		if(Users::authenticate($username, $password)) {
			registerSession($username);
		}
	} else if(($_SESSION[CFG_SESSION_KEY]["userlevel"] == "" || is_null($_SESSION[CFG_SESSION_KEY]["userlevel"])) && isIpLocal()) {
		registerSession("local");
	}
}

$result = "";

if(file_exists(HA_ROOT_PATH."/api/".$_GET["do"].".php")) {
	$redirect = getFormVariable("redirect", "");

	include(HA_ROOT_PATH."/api/".$_GET["do"].".php");
	$output = getFormVariable("output", "text");

	if($redirect != "") {
		redirectTo($redirect);
	} else {
		echo $result;
	}
}
