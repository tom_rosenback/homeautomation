HomeAutomation
Copyright (C) 2021 Tom Rosenback (tom.rosenback@gmail.com), Daniel Malmgren (daniel.malmgren@kolefors.se)

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

=========================================================================================

This file is intended to enlighten developers of how HomeAutomation is put together.

NOTE: This file might not be up-to-date.

=========================================================================================

Description of file structure:
- index.php, this is the main file, everything is generated through this file
- functions.php, all functions which are NOT database related
- dbfunctions.php, ONLY functions which are database related
- configurator.php, Config page
- devices.php, Devices page
- houseplan.php, Houseplan page
- help.php, Help page
- infobox.php, Infobox which is shown on the right
- mobile.php, Page made to fit mobile phone displays
- log.php, Log page
- scheduler.php, Scheduler page
- layout.php, here is the layout, colors etc.
- tdtool.php, this file is used from the console (scheduler)
- updatetasks.php, this file is used to update on/off times on tasks when sun dependant control is used
- script.js, javascript functions
- resources folder
  - images needed for displaying on page
  - styles.css here is the rest of the layout
- rawcommands folder
  - contains rawcommand files saved from Telldus webpage and files saved by Homeautomation PHP scripts
- ajax folder
  - contains files which are used to update the page without reloading the whole page
- temperature folder
  - contains 1-wire temperature related page files
- system folder
  - contains files vital for the backbone of the system
- logs folder
  - at the moment the only thing saved in this folder is the dongle.lock file which is deleted once tdtool execution has completed.
- install folder
  - contains installation file and sql scripts
- readme.txt, this file :P

=========================================================================================
Developer config parameters, these parameters should be used with caution.

// if defined skips the install check and the install folder can be kept without deleting
$config["skip_install_check"] = true;

// if defined shows the page generation time in the bottom of all pages (not mobile)
$config["showpagegeneratetime"] = true;

// if defined shows a slider instead of dropdown on mobile device page (abs.dimmers)
$config["use_mobile_slider"] = true;